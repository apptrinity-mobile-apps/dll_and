package divorcelovelounge.com.dll

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import divorcelovelounge.com.dll.ApiPojo.ProfilePhotosResponse
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Utils.Utils
import java.util.HashMap
import retrofit2.Call
import retrofit2.Callback
import android.widget.PopupMenu
import android.widget.Toast
import java.io.IOException


class PhotoGalleryActivity : AppCompatActivity() {

    lateinit var rv_gallery_id: RecyclerView

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>
    var photo1_stg = "nodata"
    var photo2_stg = "nodata"
    var photo3_stg = "nodata"
    var photo4_stg = "nodata"
    var photo5_stg = "nodata"
    var photo6_stg = "nodata"
    private val GALLERY = 1
    private val CAMERA = 2

    lateinit var bitmap_data:Bitmap
    lateinit var thumbnail_data:Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_gallery)

        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        rv_gallery_id = findViewById(R.id.rv_gallery_id)


        val apiService = ApiInterface.create()
        val call = apiService.getProfilePhotosApi(user_details.get(DllSessionManager.USER_ID).toString())
        call.enqueue(object : Callback<ProfilePhotosResponse> {
            override fun onResponse(
                call: Call<ProfilePhotosResponse>,
                response: retrofit2.Response<ProfilePhotosResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {
/*
if(response.body()!!.photos!!.photo1!==null){
}*/

                    if (response.body()!!.photos!!.photo1 !== null) {
                        photo1_stg = response.body()!!.photos!!.photo1
                    }
                    if (response.body()!!.photos!!.photo2 !== null) {
                        photo2_stg = response!!.body()!!.photos!!.photo2
                    }
                    if (response.body()!!.photos!!.photo3 !== null) {
                        photo3_stg = response!!.body()!!.photos!!.photo3
                    }
                    if (response.body()!!.photos!!.photo4 !== null) {
                        photo4_stg = response!!.body()!!.photos!!.photo4
                    }
                    if (response.body()!!.photos!!.photo5 !== null) {
                        photo5_stg = response!!.body()!!.photos!!.photo5
                    }
                    if (response.body()!!.photos!!.photo6 !== null) {
                        photo6_stg = response!!.body()!!.photos!!.photo6
                    }


                    val mNoOfColumns = Utils.calculateNoOfColumns(this@PhotoGalleryActivity, 180F)

                    rv_gallery_id.setHasFixedSize(true)
                    val gridLayoutManager = GridLayoutManager(this@PhotoGalleryActivity, mNoOfColumns)
                    rv_gallery_id.layoutManager = gridLayoutManager
                    rv_gallery_id.itemAnimator = DefaultItemAnimator()
                    val details_adapter = GalleryAdapter(
                        photo1_stg,
                        photo2_stg,
                        photo3_stg,
                        photo4_stg,
                        photo5_stg,
                        photo6_stg,
                        this@PhotoGalleryActivity,
                        user_details.get(DllSessionManager.USER_ID).toString()
                    )
                    details_adapter.setHasStableIds(true)
                    rv_gallery_id.adapter = details_adapter
                    details_adapter.notifyDataSetChanged()
                }

                //  }
            }

            override fun onFailure(call: Call<ProfilePhotosResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    class GalleryAdapter(
        val main_photo1: String,
        val main_photo2: String,
        val main_photo3: String,
        val main_photo4: String,
        val main_photo5: String,
        val main_photo6: String,
        val context: Context,
        user_id: String
    ) :
        RecyclerView.Adapter<ViewHolder1>() {

       val mContext = context
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.gallery_list_item, parent, false))

        }

        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            // holder.img_pic_id
            val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

           // if (position == 0) {
                /*Picasso.with(context)
                    .load(photbaseurl + main_photo1)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)*/

                Log.e("url_ll", photbaseurl + main_photo1)
          // }
        /*else if (position == 1) {
                Picasso.with(context)
                    .load(photbaseurl + main_photo2)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)
            } else if (position == 2) {
                Picasso.with(context)
                    .load(photbaseurl + main_photo3)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)
            } else if (position == 3) {
                Picasso.with(context)
                    .load(photbaseurl + main_photo4)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)
            } else if (position == 4) {
                Picasso.with(context)
                    .load(photbaseurl + main_photo5)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)
            } else if (position == 5) {
                Picasso.with(context)
                    .load(photbaseurl + main_photo6)
                    //  .error(R.drawable.movie_error_clip)
                    .into(holder.img_pic_id)
            }*/

            holder.img_click_id.setOnClickListener {
                val popup = PopupMenu(context, holder.img_click_id)
                //inflating menu from xml resource
                popup.inflate(R.menu.option_menu)
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {
                                //context!!.getImageFromAlbum()
                             /*   if(mContext instanceof PhotoGalleryActivity){
                                    ((PhotoGalleryActivity)mContext).getImageFromAlbum();
                                }*/

                            }
                            R.id.action_set_id -> {
                            }
                            R.id.action_delete_id -> {
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return 6
        }


    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {

        var img_pic_id = view.findViewById<ImageView>(R.id.img_pic_id)
        var img_click_id = view.findViewById<ImageView>(R.id.img_click_id)
    }


    private fun getImageFromAlbum() {
        val pictureDialog = AlertDialog.Builder(this)

        pictureDialog.setTitle("Select Photo from")
        val pictureDialogItems = arrayOf("Gallery", "Camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*if (resultCode == this) {
            return
        }*/
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    //  val path = saveImage(bitmap)
                   // Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
                    //holder.img_pic_id.setImageBitmap(bitmap)

                    bitmap_data=bitmap

                } catch (e: IOException) {
                    e.printStackTrace()
                  //  Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
           // holder.img_pic_id.setImageBitmap(thumbnail)
          //  Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
            bitmap_data=thumbnail
        }
    }
}
