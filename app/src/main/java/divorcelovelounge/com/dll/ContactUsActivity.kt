package divorcelovelounge.com.dll

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.ContactusResponse
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import retrofit2.Call
import retrofit2.Callback

class ContactUsActivity : AppCompatActivity() {

    /*lateinit var et_contactus_name_id: EditText
    lateinit var et_contactus_email_id: EditText
    lateinit var et_contactus_phone_id: EditText
    lateinit var et_contactus_message_id: EditText
    lateinit var tv_send_id: CustomTextViewBold
    lateinit var iv_toolbar_back_id: ImageView

    lateinit var success_dialog: Dialog
    lateinit var tv_ok_id: CustomTextViewBold

    var full_name_stg = ""
    var email_stg = ""
    var phone_stg = ""
    var msg_stg = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)

        iv_toolbar_back_id = findViewById(R.id.iv_toolbar_back_id)

        et_contactus_name_id = findViewById(R.id.et_contactus_name_id)
        et_contactus_email_id = findViewById(R.id.et_contactus_email_id)
        et_contactus_phone_id = findViewById(R.id.et_contactus_phone_id)
        et_contactus_message_id = findViewById(R.id.et_contactus_message_id)
        tv_send_id = findViewById(R.id.tv_send_id)

        iv_toolbar_back_id.setOnClickListener { super.onBackPressed() }

        success_dialog = Dialog(this)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        success_dialog.setContentView(R.layout.success_dilaog_layout)
        val window_login = success_dialog.window
        window_login!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tv_ok_id = success_dialog.findViewById(R.id.txt_login_id)
        tv_ok_id.setOnClickListener {
            success_dialog.dismiss()

        }

        tv_send_id.setOnClickListener {

            full_name_stg = et_contactus_name_id.text.toString()
            email_stg = et_contactus_email_id.text.toString()
            phone_stg = et_contactus_phone_id.text.toString()
            msg_stg = et_contactus_message_id.text.toString()
            if (LoginValidCredentials()) {

                val apiService = ApiInterface.create()
                val call = apiService.contactUsApi(full_name_stg, phone_stg, email_stg, msg_stg)
                Log.d("REQUEST", call.toString() + "")
                call.enqueue(object : Callback<ContactusResponse> {
                    override fun onResponse(
                        call: Call<ContactusResponse>,
                        response: retrofit2.Response<ContactusResponse>?
                    ) {

                        if (response!!.body()!!.status.equals("1")) {
                            success_dialog.show()

                            et_contactus_name_id.setText("")
                            et_contactus_email_id.setText("")
                            et_contactus_phone_id.setText("")
                            et_contactus_message_id.setText("")

                        }
                    }

                    override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                        Log.w("Result_Order_details", t.toString())
                    }
                })

            }
        }

    }

    private fun LoginValidCredentials(): Boolean {
        if (TextUtils.isEmpty(full_name_stg))
            Toast.makeText(this@ContactUsActivity, "Full Name Required", Toast.LENGTH_SHORT).show()
        else if (TextUtils.isEmpty(email_stg))
            Toast.makeText(this@ContactUsActivity, "Email Id Required", Toast.LENGTH_SHORT).show()
        else if (!Patterns.EMAIL_ADDRESS.matcher(email_stg).matches())
            Toast.makeText(this@ContactUsActivity, "Invalid Email Id", Toast.LENGTH_SHORT).show()
        else if (TextUtils.isEmpty(phone_stg))
            Toast.makeText(this@ContactUsActivity, "Phone Number Required", Toast.LENGTH_SHORT).show()
        else if (et_contactus_phone_id.length() < 10)
            Toast.makeText(this@ContactUsActivity, "Invalid Phone Number", Toast.LENGTH_SHORT).show()
        else if (TextUtils.isEmpty(msg_stg))
            Toast.makeText(this@ContactUsActivity, "Feel Free to Message me", Toast.LENGTH_SHORT).show()
        else
            return true
        return false
    }*/
}
