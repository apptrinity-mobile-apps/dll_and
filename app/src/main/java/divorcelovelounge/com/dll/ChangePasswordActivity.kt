package divorcelovelounge.com.dll

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.ChangePasswordResponse
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : AppCompatActivity() {

    private lateinit var et_current_password: EditText
    private lateinit var et_new_password: EditText
    private lateinit var et_re_enter_password: EditText

    private lateinit var iv_password_back: ImageView

    private lateinit var tv_password_header: CustomTextViewBold
    private lateinit var tv_password_done: CustomTextViewBold

    private lateinit var progressDialog: Dialog

    private lateinit var sessionManager: DllSessionManager
    private lateinit var user_details: HashMap<String, String>

    private val logTAG = "ChangePasswordActivity"
    var user_id = ""
    var current_pswd_stg = ""
    var new_pswd_stg = ""
    var re_type_pswd_stg = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        sessionManager = DllSessionManager(this@ChangePasswordActivity)
        user_details = sessionManager.userDetails
        user_id = user_details[DllSessionManager.USER_ID]!!

        initializeViews()

        et_current_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = et_current_password.text.toString()
                Log.e("data count : ", password_stg + "---" + password_stg.length)
                if (password_stg.length > 5) {
                    et_current_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )
                } else if (password_stg.length == 0) {
                    et_current_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    et_current_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        et_new_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = et_new_password.text.toString()
                Log.e("data count : ", password_stg + "---" + password_stg.length)
                if (password_stg.length > 5) {
                    et_new_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )
                } else if (password_stg.length == 0) {
                    et_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    et_new_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        et_re_enter_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = et_re_enter_password.text.toString()
                Log.e("data count : ", password_stg + "---" + password_stg.length)
                if (password_stg.length > 5) {
                    et_re_enter_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )
                } else if (password_stg.length == 0) {
                    et_re_enter_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    et_re_enter_password.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        iv_password_back.setOnClickListener {
            onBackPressed()
        }

        tv_password_done.setOnClickListener {

            current_pswd_stg = et_current_password.text.toString()
            new_pswd_stg = et_new_password.text.toString()
            re_type_pswd_stg = et_re_enter_password.text.toString()

            Log.d(
                logTAG,
                "current pswd : $current_pswd_stg ----- new pswd : $new_pswd_stg ----- re typed pswd : $re_type_pswd_stg"
            )
            if (current_pswd_stg != "" && new_pswd_stg != "" && re_type_pswd_stg != "") {
                if (current_pswd_stg.length > 5 && new_pswd_stg.length > 5 && re_type_pswd_stg.length > 5) {
                    if (new_pswd_stg != re_type_pswd_stg) {
                        Toast.makeText(
                            this@ChangePasswordActivity,
                            "Password doesn't match!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        changePassword(user_id, new_pswd_stg, current_pswd_stg)
                    }
                }

            } else {
                Toast.makeText(
                    this@ChangePasswordActivity,
                    "Fields are mandatory!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    private fun initializeViews() {
        et_current_password = findViewById(R.id.et_current_password)
        et_new_password = findViewById(R.id.et_new_password)
        et_re_enter_password = findViewById(R.id.et_re_enter_password)
        iv_password_back = findViewById(R.id.iv_password_back)
        tv_password_header = findViewById(R.id.tv_password_header)
        tv_password_done = findViewById(R.id.tv_password_done)

        progressDialog = Dialog(this@ChangePasswordActivity)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

    }

    private fun changePassword(user_id: String, new_password: String, old_password: String) {

        progressDialog.show()

        val passwordApi = ApiInterface.create()
        val call = passwordApi.changePasswordApi(user_id, new_password, old_password)
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.d(logTAG, "changePasswordApi Response : error : ${t.toString()}")
            }

            override fun onResponse(
                call: Call<ChangePasswordResponse>,
                response: Response<ChangePasswordResponse>
            ) {

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                Log.d(logTAG, "changePasswordApi Response: success: ${response.body()!!.result}")

                if (response.body()!!.status == "1") {
                    Toast.makeText(
                        this@ChangePasswordActivity,
                        response.body()!!.Message,
                        Toast.LENGTH_SHORT
                    ).show()
                    sessionManager.logoutUser()

                    /******** Google Sign-In ********/
                    val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .build()
                    val mGoogleSignInClient =
                        GoogleSignIn.getClient(this@ChangePasswordActivity, gso!!)

                    mGoogleSignInClient!!.signOut()
                        .addOnCompleteListener(this@ChangePasswordActivity) {}

                } else if (response.body()!!.status == "2") {
                    Toast.makeText(
                        this@ChangePasswordActivity,
                        "Existing password and New Password is not be the same",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } else if (response.body()!!.status == "3") {
                    Toast.makeText(
                        this@ChangePasswordActivity,
                        "Current password is in-correct!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

        })
    }

}
