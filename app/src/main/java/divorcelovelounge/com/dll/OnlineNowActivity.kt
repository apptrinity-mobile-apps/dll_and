package divorcelovelounge.com.dll

import android.annotation.TargetApi
import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Helper.InternetConnection
import divorcelovelounge.com.dll.PicassoTransformations.BlurTransformation
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem
import divorcelovelounge.com.dll.ViewPagerAdapters.CardPagerAdapterKotlin
import divorcelovelounge.com.dll.ViewPagerAdapters.ShadowTransformer
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OnlineNowActivity : AppCompatActivity() {

    lateinit var matches_array_list: ArrayList<ActiveUserList>

    lateinit var success_dialog:Dialog
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    var plan_expire_status:Long=0

    lateinit var rv_active_id:RecyclerView


    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id:CustomTextView
    lateinit var img_profile_pic_id:ImageView
    lateinit var iv_close:ImageView
    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    lateinit var ll_member_plans: LinearLayout
    lateinit var ll_no_data: LinearLayout
    lateinit var tv_proceed_membership: CustomTextView

    lateinit var swipe_refresh_layout: SwipeRefreshLayout
    private lateinit var back_chat_toolbar: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_now)
        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        rv_active_id=findViewById(R.id.rv_active_id)

       val user_id=user_details.get(DllSessionManager.USER_ID).toString()
        matchesApiCall(user_id)

        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout)
        swipe_refresh_layout.setOnRefreshListener {
            // Initialize a new Runnable
            // Update the text view text with a random number
            matchesApiCall(user_id)

            swipe_refresh_layout.isRefreshing = false


        }

        back_chat_toolbar = findViewById(R.id.back_chat_toolbar)
        back_chat_toolbar.setOnClickListener {
            onBackPressed()
        }

    }

    private fun matchesApiCall(
        userId: String
    ) {

        matches_array_list = ArrayList()
        matches_array_list .clear()
        // Internet Available...
        val apiService = ApiInterface.create()
        val call = apiService.activeUserListAPI(userId)

        call.enqueue(object : Callback<ActiveUsersResponse> {
            override fun onResponse(
                call: Call<ActiveUsersResponse>,
                response: retrofit2.Response<ActiveUsersResponse>?
            ) {

                try {
                    //  if (response != null) {
                    if (response!!.body()!!.status.equals("1")) {
                       // progress_id.visibility = View.GONE

                        val list: ArrayList<ActiveUserList>? = response.body()!!.data
                        // val favourites_list: ArrayList<All_Matches>? = user_data!!.my_favourites

                        for (i in 0 until list!!.size) {
                            matches_array_list.add(list.get(i))

                        }

                        val gridLayoutManager = GridLayoutManager(this@OnlineNowActivity, 2)
                        rv_active_id.layoutManager = gridLayoutManager
                        rv_active_id.itemAnimator = DefaultItemAnimator()
                        val details_adapter = FMDiscoverListAdapter(
                            matches_array_list, this@OnlineNowActivity, user_details.get(
                                DllSessionManager.USER_ID
                            ).toString()
                        )
                        //details_adapter.setHasStableIds(true)
                        rv_active_id.adapter = details_adapter
                        details_adapter.notifyDataSetChanged()

                        Log.w(
                            "SecondFragment_lll",
                            "matches_array_list: " + matches_array_list.size + "---" + matches_array_list.toString()
                        )

                    }
                    //  }
                } catch (e: Exception) {
                    Log.e("SECONDEXCEPTION", e.toString())
                }
            }

            override fun onFailure(call: Call<ActiveUsersResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })

    }

    inner class FMDiscoverListAdapter(val mServiceList: java.util.ArrayList<ActiveUserList>, val context: Context, user_id: String) :
        RecyclerView.Adapter<ViewHolder1>() {

        var partner_match_distance_stg: String? = null
        var partner_match_search_stg: String? = null
        var partner_ethnicity_stg: String? = null
        var partner_religion_stg: String? = null
        var partner_education_stg: String? = null
        var partner_occupation_stg: String? = null
        var partner_smoke_stg: String? = null
        var partner_drink_stg: String? = null
        var photo1_stg: String? = null

        var photo1_approve: String? = null

        var fav_id_stg: String? = null
        var fav_from_stg: String? = null
        var fav_to_stg: String? = null
        var interest_from_stg: String? = null
        var interest_to_stg: String? = null
        var interest_status_stg: String? = null
        private var user_smoke_stg = ""
        private var user_drink_stg = ""
        private var user_denomination_stg = ""
        var height_ft_stg: String? = null
        var height_inc_stg: String? = null
        var height_cm_stg: String? = null
        var city_stg: String? = null
        var dob_stg: String? = null
        var ethnicity_stg: String? = null
        var education_stg: String? = null
        var occupation_stg: String? = null
        var income_stg: String? = null
        var passionate_stg: String? = null
        var leisure_time_stg: String? = null
        var thankful_1_stg: String? = null
        var thankful_2_stg: String? = null
        var thankful_3_stg: String? = null
        var partner_age_min_stg: String? = null
        var partner_age_max_stg: String? = null
        var religion_stg: String? = null
        var partner_denomination_stg: String? = null
        var partner_phone_number: String? = null
        var mtongue_name: String? = null
        var partner_mtongue_name: String? = null

        var main_user_id = user_id
        val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.favorites_item_list, parent, false))

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10

            holder.tv_convo_name.text = mServiceList.get(position).first_name

            if (mServiceList[position].gender.equals("Woman")) {
                holder.iv_gender.setImageResource(R.drawable.new_female_ic)
            } else {
                holder.iv_gender.setImageResource(R.drawable.new_male_ic)
                holder.iv_gender.setColorFilter(ContextCompat.getColor(context, R.color.gender_color), android.graphics.PorterDuff.Mode.MULTIPLY)

            }



            //Log.e("partner_ma_stg", mServiceList.get(position).partner_match_distance)


            var date_only: Date? = null
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            try {
                date_only = sdf.parse(mServiceList.get(position).dob)
            } catch (e: ParseException) {
                e.printStackTrace()
            }


            val c = Calendar.getInstance()
            //Set time in milliseconds
            // c.setTimeInMillis(user_date_id.toLong())
            c.time = date_only
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)

            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()

            dob.set(mYear, mMonth, mDay)

            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }

            val ageInt = age

            holder.tv_convo_age_id.setText(ageInt.toString())
            if (mServiceList.get(position).partner_match_distance !== null) {
                partner_match_distance_stg = mServiceList.get(position).partner_match_distance!!
                Log.e("partnerstg", partner_match_distance_stg)
            } else {
                partner_match_distance_stg = "0"
                Log.e("partner_ma_stg", partner_match_distance_stg)

            }

            if (mServiceList.get(position).partner_match_search !== null) {
                partner_match_search_stg = mServiceList.get(position).partner_match_search!!
            } else {

                partner_match_search_stg = "0"
            }



            if (mServiceList.get(position).photo1 !== null) {
                photo1_stg = mServiceList.get(position).photo1!!
            } else {
                photo1_stg = "0"
            }


            if (mServiceList[position].photo1_approve !== null) {
                photo1_approve = mServiceList[position].photo1_approve!!
            } else {
                photo1_approve = "0"
            }


            if (mServiceList.get(position).fav_id !== null) {
                fav_id_stg = mServiceList.get(position).fav_id!!
            } else {
                fav_id_stg = "0"
            }
            if (mServiceList.get(position).fav_from !== null) {
                fav_from_stg = mServiceList.get(position).fav_from!!
            } else {

                fav_from_stg = "0"
            }
            if (mServiceList.get(position).fav_to !== null) {
                fav_to_stg = mServiceList.get(position).fav_to!!
            } else {

                fav_to_stg = "0"
            }
            if (mServiceList.get(position).interest_from !== null) {
                interest_from_stg = mServiceList.get(position).interest_from!!
            } else {

                interest_from_stg = "0"
            }
            if (mServiceList.get(position).interest_to !== null) {
                interest_to_stg = mServiceList.get(position).interest_to!!
            } else {

                interest_to_stg = "0"
            }
            if (mServiceList.get(position).interest_status !== null) {
                interest_status_stg = mServiceList.get(position).interest_status!!
            } else {

                interest_status_stg = "0"
            }


            if (fav_from_stg == main_user_id) {
                holder.btn_interest_id.isChecked = true
                // btn_interest_id.isClickable=false
                holder.btn_interest_id.isEnabled = false
            } else {
                holder.btn_interest_id.isChecked = false
            }

            if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS) == "1") {
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    }
                }
            }else{
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .transform(BlurTransformation(context))
                            .into(holder.iv_favorite_user_image)
                    }
                }
            }


            holder.iv_favorite_user_image.setOnClickListener {

                if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("2")) {

                    memberShipAlertDialog()


                } else if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1") && plan_expire_status >= 0) {



                    if (mServiceList.get(position).partner_match_distance !== null) {
                        partner_match_distance_stg = mServiceList.get(position).partner_match_distance!!
                        Log.e("partnerstg", partner_match_distance_stg)
                    } else {
                        partner_match_distance_stg = "0"
                        Log.e("partner_ma_stg", partner_match_distance_stg)

                    }

                    if (mServiceList.get(position).partner_match_search !== null) {
                        partner_match_search_stg = mServiceList.get(position).partner_match_search!!
                    } else {

                        partner_match_search_stg = "0"
                    }



                    if (mServiceList.get(position).photo1 !== null) {
                        photo1_stg = mServiceList.get(position).photo1!!
                    } else {
                        photo1_stg = "0"
                    }


                    if (mServiceList[position].photo1_approve !== null) {
                        photo1_approve = mServiceList[position].photo1_approve!!
                    } else {
                        photo1_approve = "0"
                    }


                    if (mServiceList.get(position).fav_id !== null) {
                        fav_id_stg = mServiceList.get(position).fav_id!!
                    } else {
                        fav_id_stg = "0"
                    }
                    if (mServiceList.get(position).fav_from !== null) {
                        fav_from_stg = mServiceList.get(position).fav_from!!
                    } else {

                        fav_from_stg = "0"
                    }
                    if (mServiceList.get(position).fav_to !== null) {
                        fav_to_stg = mServiceList.get(position).fav_to!!
                    } else {

                        fav_to_stg = "0"
                    }
                    if (mServiceList.get(position).interest_from !== null) {
                        interest_from_stg = mServiceList.get(position).interest_from!!
                    } else {

                        interest_from_stg = "0"
                    }
                    if (mServiceList.get(position).interest_to !== null) {
                        interest_to_stg = mServiceList.get(position).interest_to!!
                    } else {

                        interest_to_stg = "0"
                    }
                    if (mServiceList.get(position).interest_status !== null) {
                        interest_status_stg = mServiceList.get(position).interest_status!!
                    } else {

                        interest_status_stg = "0"
                    }

                    if (mServiceList.get(position).height_ft !== null) {
                        height_ft_stg = mServiceList.get(position).height_ft!!
                    } else {

                        height_ft_stg = "0"
                    }
                    if (mServiceList.get(position).height_inc !== null) {
                        height_inc_stg = mServiceList.get(position).height_inc!!
                    } else {

                        height_inc_stg = "0"
                    }
                    if (mServiceList.get(position).height_cms !== null) {
                        height_cm_stg = mServiceList.get(position).height_cms!!
                    } else {

                        height_cm_stg = "0"
                    }

                    if (interest_from_stg == main_user_id) {
                        holder.btn_interest_id.isChecked = true
                        // btn_interest_id.isClickable=false
                    } else {
                        holder.btn_interest_id.isChecked = false
                    }


                    if (mServiceList.get(position).height_ft !== null) {
                        height_ft_stg = mServiceList.get(position).height_ft!!
                    } else {

                        height_ft_stg = "0"
                    }
                    if (mServiceList.get(position).height_inc !== null) {
                        height_inc_stg = mServiceList.get(position).height_inc!!
                    } else {

                        height_inc_stg = "0"
                    }
                    if (mServiceList.get(position).height_cms !== null) {
                        height_cm_stg = mServiceList.get(position).height_cms!!
                    } else {

                        height_cm_stg = "0"
                    }

                    if (mServiceList.get(position).city !== null) {
                        city_stg = mServiceList.get(position).city
                    } else {
                        city_stg = "0"
                    }
                    if (mServiceList.get(position).dob !== null) {
                        dob_stg = mServiceList.get(position).dob
                    } else {
                        dob_stg = "0"
                    }



                    val internt= Intent(context, FullProfile::class.java)
                    val pairs = arrayOfNulls<Pair<View, String>>(4)
                    pairs[0] = Pair<View, String>(holder.iv_favorite_user_image, "mainimageTreansition")
                    pairs[1] = Pair<View, String>(holder.tv_convo_name, "nametransition")
                    pairs[2] = Pair<View, String>(holder.iv_gender, "gendertransition")
                    pairs[3] = Pair<View, String>(holder.tv_convo_age_id, "agetransition")

                    val aOptions = ActivityOptions.makeSceneTransitionAnimation(context as Activity, *pairs)


                    internt.putExtra("from_screen","active")
                    internt.putExtra("id",mServiceList.get(position).id)
                    internt.putExtra("photo1",photo1_stg)
                    internt.putExtra("name_stg",mServiceList.get(position).first_name)
                    internt.putExtra("gender_stg",mServiceList.get(position).gender)
                    internt.putExtra("age_stg",dob_stg)
                    internt.putExtra("location_stg",city_stg)
                    context.startActivity(internt,aOptions.toBundle())
                }
            }
        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }



    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        //val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
        val tv_convo_name = view.findViewById<CustomTextView>(R.id.tv_convo_name)
        val iv_favorite_user_image = view.findViewById<ImageView>(R.id.iv_favorite_user_image)
        val tv_convo_age_id = view.findViewById<CustomTextView>(R.id.tv_convo_age_id)
        val btn_interest_id = view.findViewById<SparkButton>(R.id.btn_interest_id)
        val iv_gender = view.findViewById<ImageView>(R.id.iv_gender)


    }


    fun memberShipAlertDialog() {
        success_dialog = Dialog(this@OnlineNowActivity)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        img_profile_pic_id=success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id=success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text="Alert!"
        tv_full_msg_id=success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text="You need an active membership plan to view the profile. Proceed to membership plans?"
        iv_close=success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility=View.VISIBLE
        tv_ok_btn_id=success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(this@OnlineNowActivity, MembershipPlanActivity::class.java)
            startActivity(intent)
        }

        Picasso.with(this@OnlineNowActivity)
            .load(photobaseurl+user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()
    }
}
