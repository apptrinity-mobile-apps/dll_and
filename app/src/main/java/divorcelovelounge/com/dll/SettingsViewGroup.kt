package divorcelovelounge.com.dll

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.RelativeLayout


internal class SettingsViewGroup : RelativeLayout {
    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        // Do not propagate the click do children
        return true
        // return super.onInterceptTouchEvent(ev);
    }
}