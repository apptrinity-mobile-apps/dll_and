package divorcelovelounge.com.dll

import android.Manifest
import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.Nullable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.*
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.nanotasks.Completion
import com.nanotasks.Tasks
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.Adapters.AddImageHorizontalAdapter
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession
import divorcelovelounge.com.dll.Helper.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.util.*
import kotlin.collections.ArrayList

//RAVITEJA
class RegisterLoginActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, ViewPagerEx.OnPageChangeListener,
    BaseSliderView.OnSliderClickListener {

    lateinit var btn_signup_id: Button
    lateinit var btn_login_id: Button
    lateinit var login_dialog: Dialog
    lateinit var register_dialog: Dialog
    lateinit var forgot_pass_dialog: Dialog
    private val ALL_PERMISSION_CODE = 23

    lateinit var txt_login_id: CustomTextViewBold
    lateinit var tv_signup_id: CustomTextViewBold

    lateinit var img_male_id: ImageView
    lateinit var img_female_id: ImageView
    lateinit var close_register_id: ImageView
    lateinit var close_login_id: ImageView

    lateinit var login_email_id: EditText
    lateinit var login_password_id: EditText
    lateinit var forgot_password: CustomTextView
    lateinit var card_adv_banner_id: CardView

    lateinit var register_email_id: EditText
    lateinit var register_password_id: EditText
    lateinit var register_conf_password_id: EditText

    lateinit var reset_email_id: EditText
    lateinit var txt_reset_id: CustomTextView
    lateinit var close_reset_id: ImageView

    var female = ""
    var male = ""

    var email_stg = ""
    var password_stg = ""

    lateinit var dllSessionManager: DllSessionManager
    lateinit var progress_bar_login: ProgressBar
    lateinit var user_details: HashMap<String, String>
    //google
    private var gso: GoogleSignInOptions? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val REQUEST_CODE_GOOGLE_SIGN_IN = 100
    private lateinit var iv_gplus: ImageView
    private lateinit var iv_fb: ImageView
    //facebook
    lateinit var login_button: LoginButton
    lateinit var sliderLayout: SliderLayout
    private var callbackManager: CallbackManager? = null
    var email = ""
    var fname = ""
    var device_token_stg = ""
    var add_link = ""
    var device_type_stg = ""
    var dataStorage: DataStorage = DataStorage()

    var final_photo_1 = ""
    private lateinit var progressDialog: Dialog
    private lateinit var img_adv_pic_id: ImageView
    private lateinit var rv_addslist: RecyclerView
    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>

    val adsbaseurl = PhotoBaseUrl().BASE_ADS_URL
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_login)

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })


        dllSessionManager = DllSessionManager(this@RegisterLoginActivity)
        user_details = dllSessionManager.userDetails
        if (dllSessionManager.isLoggedIn) {

            if (user_details.get(DllSessionManager.PHOTO1).equals("0")) {
                val home_intent = Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                startActivity(home_intent)
                finish()
            } else if (user_details.get(DllSessionManager.PHOTO1_APPROVE).equals("APPROVED")) {
                Log.e(
                    "xmpp_login",
                    user_details.get(DllSessionManager.USER_NAME_ID).toString() + "-----" +
                            user_details.get(DllSessionManager.USER_PASSWORD).toString()
                )

                loginAndStart(
                    user_details.get(DllSessionManager.USER_NAME_ID).toString(),
                    user_details.get(DllSessionManager.USER_PASSWORD).toString()
                )


            } else {
                ProfilePhotosAPI()
            }

        }




        device_token_stg = dataStorage.getFcmToken(this)
        device_type_stg = resources.getString(R.string.device_type)

        Log.d("RegisterLoginActivity", "fcm token : $device_token_stg")

        btn_signup_id = findViewById(R.id.btn_signup_id)
        btn_login_id = findViewById(R.id.btn_login_id)

        iv_gplus = findViewById(R.id.iv_gplus)
        iv_fb = findViewById(R.id.iv_fb)
        login_button = findViewById(R.id.login_button)


        addsarrayList = ArrayList()
        //sliderLayout = SliderLayout(this)
        login_dialog = Dialog(this@RegisterLoginActivity)
        login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        login_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        login_dialog.setContentView(R.layout.login_dialog)
        val window_login = login_dialog.window
        window_login!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        close_login_id = login_dialog.findViewById(R.id.close_login_id)
        sliderLayout = login_dialog.findViewById(R.id.slider)
        txt_login_id = login_dialog.findViewById(R.id.txt_login_id)
        login_email_id = login_dialog.findViewById(R.id.login_email_id)
        login_password_id = login_dialog.findViewById(R.id.login_password_id)
        forgot_password = login_dialog.findViewById(R.id.forgot_password)
        progress_bar_login = login_dialog.findViewById(R.id.progress_bar_login)
        //  img_adv_pic_id = login_dialog.findViewById(R.id.img_adv_pic_id)
        rv_addslist = login_dialog.findViewById(R.id.rv_addslist)
        card_adv_banner_id = login_dialog.findViewById(R.id.card_adv_banner_id)



        forgot_pass_dialog = Dialog(this@RegisterLoginActivity)
        forgot_pass_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        forgot_pass_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        forgot_pass_dialog.setContentView(R.layout.forgotpass_layout)
        val window_reset = forgot_pass_dialog.window
        window_reset!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        close_reset_id = forgot_pass_dialog.findViewById(R.id.close_reset_id)
        txt_reset_id = forgot_pass_dialog.findViewById(R.id.txt_reset_id)
        reset_email_id = forgot_pass_dialog.findViewById(R.id.reset_email_id)

        txt_reset_id.setOnClickListener {
            if (reset_email_id.text.equals("")) {
                Toast.makeText(this, "Please Enter Email", Toast.LENGTH_LONG).show()
            } else {
                forgotpassword(reset_email_id.text.toString())
            }

        }

        close_reset_id.setOnClickListener {
            forgot_pass_dialog.dismiss()
        }
        /* val locale = this.getResources().getConfiguration().locale.country
         Log.e("locale_codes",locale)*/

        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        val interpolator = MyBounceInterpolator(0.2, 20.0)
        myAnim.interpolator = interpolator



        login_email_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                Log.e("edittext_edit", "before" + "---" + s)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val email_stg = login_email_id.text.toString()
                if ((!TextUtils.isEmpty(email_stg) && Patterns.EMAIL_ADDRESS.matcher(email_stg).matches())) {
                    login_email_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )

                } else if (email_stg.length == 0) {
                    login_email_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    login_email_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
                Log.e("edittext_edit", "ontext" + "---" + s + "---" + email_stg)
            }

            override fun afterTextChanged(s: Editable) {
                Log.e("edittext_edit", "after" + "---" + s)
            }
        })
        login_password_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                Log.e("edittext", "before" + "---" + s)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = login_password_id.text.toString()
                if (s.length > 5) {
                    login_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )

                } else if (s.length == 0) {
                    login_password_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

                } else {
                    login_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
                Log.e("edittext", "ontextchange" + "---" + s)
            }

            override fun afterTextChanged(s: Editable) {
                Log.e("edittext", "after" + "---" + s)
            }
        })

        forgot_password.setOnClickListener {
            login_dialog.dismiss()
            forgot_pass_dialog.show()
        }


        register_dialog = Dialog(this@RegisterLoginActivity)
        register_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        register_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        register_dialog.setContentView(R.layout.signup_dialog)
        val window_register = register_dialog.window
        window_register!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        img_female_id = register_dialog.findViewById(R.id.img_female_id)
        img_male_id = register_dialog.findViewById(R.id.img_male_id)
        close_register_id = register_dialog.findViewById(R.id.close_register_id)
        register_email_id = register_dialog.findViewById(R.id.register_email_id)
        register_password_id = register_dialog.findViewById(R.id.register_password_id)
        register_conf_password_id = register_dialog.findViewById(R.id.register_conf_password_id)
        tv_signup_id = register_dialog.findViewById(R.id.tv_signup_id)
        //login_dialog.setCancelable(false)
        img_female_id.setOnClickListener {
            if (female.equals("")) {
                female = "female"
                male = ""
                img_female_id.setImageResource(R.drawable.ic_female_select)
                img_male_id.setImageResource(R.drawable.male_default_ic)
            }
        }

        img_male_id.setOnClickListener {
            if (male.equals("")) {
                female = ""
                male = "male"
                img_female_id.setImageResource(R.drawable.female_default_ic)
                img_male_id.setImageResource(R.drawable.ic_male_select)
            }
        }

        close_register_id.setOnClickListener { register_dialog.dismiss() }
        close_login_id.setOnClickListener { login_dialog.dismiss() }
        btn_login_id.setOnClickListener {

            if (!isReadStorageAllowed()) {

                requestStoragePermission()

            } else {
                Log.e("login_lll", "login")
                EnableGPSAutoMatically("login")

            }

            // login_dialog.show()
            btn_login_id.startAnimation(myAnim)
        }

        btn_signup_id.setOnClickListener {
            //register_dialog.show()
            btn_signup_id.startAnimation(myAnim)


            if (!isReadStorageAllowed()) {

                requestStoragePermission()

            } else {
                Log.e("signup_lll", "signup")
                EnableGPSAutoMatically("signup")

            }

            /*val intent = Intent(this@RegisterLoginActivity, RegisterAccount::class.java)
            intent.putExtra("from", "normal_register")
            startActivity(intent)*/
        }

        txt_login_id.setOnClickListener {
            email_stg = login_email_id.text.toString()
            password_stg = login_password_id.text.toString()



            LoginAPI(email_stg, password_stg, "login", device_token_stg, device_type_stg)

            Log.e("token_stg", device_token_stg + "---" + device_type_stg)
        }
        tv_signup_id.setOnClickListener {

            if (!isReadStorageAllowed()) {

                requestStoragePermission()

            } else {
                EnableGPSAutoMatically("signup")

            }

            // startActivity(Intent(this@RegisterLoginActivity, CreateProfileActivity::class.java))
        }


        register_email_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val email_stg = register_email_id.text.toString()
                if ((!TextUtils.isEmpty(email_stg) && Patterns.EMAIL_ADDRESS.matcher(email_stg).matches())) {
                    register_email_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )

                } else if (email_stg.length == 0) {
                    register_email_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

                } else {
                    register_email_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )

                }

            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        register_password_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = register_password_id.text.toString()
                if (password_stg.length > 5) {
                    Log.e("data_count_ll", password_stg + "---" + password_stg.length)
                    register_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )

                } else if (password_stg.length == 0) {
                    register_password_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

                } else {
                    register_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )

                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        register_conf_password_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_conf_stg = register_conf_password_id.text.toString()
                if (password_conf_stg.equals(
                        register_password_id.text.toString(),
                        ignoreCase = true
                    )
                ) {
                    Log.e("data_count_lll", password_conf_stg + "---" + password_conf_stg.length)
                    register_conf_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )

                } else if (password_conf_stg.length == 0) {
                    register_conf_password_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

                } else {
                    register_conf_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )

                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        /******** Google Sign-In ********/
        val serverClientId =
            "606382327519-svqhsnjh96e498u5c7i36ogbhfqpiauh.apps.googleusercontent.com"

        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestIdToken(serverClientId)
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso!!)

        /******** Facebook Sign-In ********/
        callbackManager = CallbackManager.Factory.create()
        login_button.setPermissions(
            Arrays.asList(
                "email",
                "public_profile"
            )
        )
        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult?) {

                val accessTokenFacebook = loginResult!!.accessToken
                //Log.d("RegisterLoginActivity", "Facebook access token: " + accessTokenFacebook.token)
                getFacebookUserData(accessTokenFacebook)

            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException) {
                Log.e("RegisterLoginActivity", "error: $error")
            }
        })

        iv_gplus.setOnClickListener {
            signIn()
        }

        iv_fb.setOnClickListener {
            login_button.performClick()
        }

        printHashKey()

        /*if (isReadStorageAllowed()) {
            //If permission is already having then showing the toast
          //  Toast.makeText(this@RegisterLoginActivity, "You already have the permission", Toast.LENGTH_LONG).show()
            //Existing the method with return
            return
        }
        //If the app has not the permission then asking for the permission
        requestStoragePermission()*/

        if (!isReadStorageAllowed()) {

            requestStoragePermission()

        } else {
            EnableGPSAutoMatically("no")

        }


    }


    //PERMISSIONS

    private fun isReadStorageAllowed(): Boolean {
        //Getting the permission status
        val result_read_storage =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        //  val result_write_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result_camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result_Contacts =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
        val result_location =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val result_telephone =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)

        //If permission is granted returning true
        //return if (result == PackageManager.PERMISSION_GRANTED) true else false
        return result_read_storage == PackageManager.PERMISSION_GRANTED && result_camera == PackageManager.PERMISSION_GRANTED && result_Contacts == PackageManager.PERMISSION_GRANTED && result_location == PackageManager.PERMISSION_GRANTED && result_telephone == PackageManager.PERMISSION_GRANTED

        //If permission is not granted returning false
    }

    //Requesting permission
    private fun requestStoragePermission() {


        //And finally ask for the permission
        ActivityCompat.requestPermissions(
            this,
            arrayOf(READ_EXTERNAL_STORAGE, CAMERA, READ_CONTACTS, ACCESS_FINE_LOCATION, CALL_PHONE),
            ALL_PERMISSION_CODE
        )
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        //Checking the request code of our request
        if (requestCode == ALL_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] + grantResults[1] + grantResults[2] + grantResults[3] + grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show()
                //intents you can use hear

                EnableGPSAutoMatically("no")
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }


    private fun LoginValidCredentials(): Boolean {
        if (TextUtils.isEmpty(email_stg))
            Toast.makeText(
                this@RegisterLoginActivity,
                "Email Id Required",
                Toast.LENGTH_SHORT
            ).show()
        else if (!Patterns.EMAIL_ADDRESS.matcher(email_stg).matches())
            Toast.makeText(
                this@RegisterLoginActivity,
                "Invalid Email Id",
                Toast.LENGTH_SHORT
            ).show()
        else if (TextUtils.isEmpty(password_stg))
            Toast.makeText(
                this@RegisterLoginActivity,
                "Password Required",
                Toast.LENGTH_SHORT
            ).show()
        else if (login_password_id.length() < 6)
            Toast.makeText(
                this@RegisterLoginActivity,
                "Invalid Password",
                Toast.LENGTH_SHORT
            ).show()
        else
            return true
        return false
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printHashKey() {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("key_hash", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: Exception) {
            Log.e("no_hash", "printHashKey()", e)
        }
    }

    //google login
    private fun signIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_SIGN_IN)
    }

    //google logout
    public fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(this) {}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //google
        if (requestCode == REQUEST_CODE_GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            // facebook
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            Log.d("RegisterLoginActivity", "google data: $account")
            email = account!!.email!!
            fname = account.displayName!!


            googlesignApi(email, "google", fname, device_token_stg, device_type_stg)

            // Signed in successfully.
        } catch (e: ApiException) {
            /*The ApiException status code indicates the detailed failure reason.
            Please refer to the GoogleSignInStatusCodes class reference for more information.*/
            Log.d("RegisterLoginActivity", "signInResult:failed code= " + e.statusCode)
            Toast.makeText(this@RegisterLoginActivity, "Something went wrong!", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun getFacebookUserData(accessToken: AccessToken) {
        val request =
            GraphRequest.newMeRequest(accessToken, object : GraphRequest.GraphJSONObjectCallback {

                override fun onCompleted(jObj: JSONObject, response: GraphResponse) {
                    try {
                        Log.d("RegisterLoginActivity", "user object: $jObj")
                        if (jObj.length() > 0) {

                            fname = jObj.getString("name")
                            email = jObj.getString("email")

                        } else {

                            fname = ""
                            email = ""

                        }

                        googlesignApi(email, "facebook", fname, device_token_stg, device_type_stg)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

            })
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id,name,email,picture.width(200),birthday,first_name,last_name"
        )
        request.parameters = parameters
        request.executeAsync()
    }

    fun LoginAPI(
        email: String,
        password: String,
        status_login: String,
        device_token: String,
        device_type: String
    ) {
        if (status_login.equals("login")) {

            var user_city = ""
            var user_dob = ""
            var user_phone_number = ""
            var user_ethnicity = ""
            var user_religion = ""
            var user_denomination = ""
            var user_education = ""
            var user_occupation = ""
            var user_income = ""
            var user_smoke = ""
            var user_drink = ""
            var user_height_ft = ""
            var user_height_inc = ""
            var user_height_cms = ""
            var user_passionate = ""
            var user_leisure_time = ""
            var user_thankful_1 = ""
            var user_thankful_2 = ""
            var user_thankful_3 = ""
            var part_age_min_data = ""
            var part_age_max_data = ""
            var match_distance_data = ""
            var match_search_imp_data = ""
            var partner_ethnicity = ""
            var partner_religion = ""
            var partner_denomination = ""
            var partner_education = ""
            var partner_occupation = ""
            var partner_smoke = ""
            var partner_drink = ""
            var photo1 = ""
            var photo2 = ""
            var photo3 = ""
            var photo4 = ""
            var photo5 = ""
            var photo6 = ""
            var photo7 = ""
            var photo8 = ""
            var photo9 = ""
            var photo10 = ""
            var provider = ""
            var country_stg = ""
            var mother_tongue_stg = ""
            var partner_mother_tongue_stg = ""
            var gender = ""
            var looking_for = ""
            var user_first_name = ""
            var user_email = ""
            var user_zipcode = ""
            var user_state = ""
            var user_referal_from = ""
            var username_stg = ""
            var new_password_stg = ""
            var signup_step_stg = ""

            if (LoginValidCredentials()) {
                progress_bar_login.visibility = View.VISIBLE
                val apiService = ApiInterface.create()
                val call = apiService.loginApi(email, password, device_token, device_type)
                Log.d("REQUEST", call.toString() + "")
                call.enqueue(object : Callback<CheckLoginResponse> {
                    override fun onResponse(
                        call: Call<CheckLoginResponse>,
                        response: retrofit2.Response<CheckLoginResponse>?
                    ) {
                        progress_bar_login.visibility = View.GONE
                        login_dialog.dismiss()
                        //  if (response != null) {
                        if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                "success"
                            )
                        ) {

                            val user_data = response.body()!!.user_info


                            if (user_data!!.city.equals(null)) {
                                user_city = "0"
                            } else {
                                user_city = user_data.city!!
                            }
                            if (user_data.dob.equals(null)) {
                                user_dob = "0"
                            } else {
                                user_dob = user_data.dob!!
                            }
                            if (user_data.phone_number.equals(null)) {
                                user_phone_number = "0"
                            } else {
                                user_phone_number = user_data.phone_number!!
                            }
                            if (user_data.ethnicity.equals(null)) {
                                user_ethnicity = "0"
                            } else {
                                user_ethnicity = user_data.ethnicity!!
                            }
                            if (user_data.religion.equals(null)) {
                                user_religion = "0"
                            } else {
                                user_religion = user_data.religion!!
                            }
                            /*  if (user_data.denomination.equals(null)) {
                                  user_denomination = "0"
                              } else {
                                  user_denomination = user_data.denomination!!
                              }*/
                            if (user_data.education.equals(null)) {
                                user_education = "0"
                            } else {
                                user_education = user_data.education!!
                            }
                            if (user_data.occupation.equals(null)) {
                                user_occupation = "0"
                            } else {
                                user_occupation = user_data.occupation!!
                            }
                            if (user_data.income.equals(null)) {
                                user_income = "0"
                            } else {
                                user_income = user_data.income!!
                            }
                            if (user_data.smoke.equals(null)) {
                                user_smoke = "0"
                            } else {
                                user_smoke = user_data.smoke!!
                            }
                            if (user_data.drink.equals(null)) {
                                user_drink = "0"
                            } else {
                                user_drink = user_data.drink!!
                            }
                            if (user_data.height_ft.equals(null)) {
                                user_height_ft = "0"
                            } else {
                                user_height_ft = user_data.height_ft!!
                            }
                            if (user_data.height_inc.equals(null)) {
                                user_height_inc = "0"
                            } else {
                                user_height_inc = user_data.height_inc!!
                            }
                            if (user_data.height_cms.equals(null)) {
                                user_height_cms = "0"
                            } else {
                                user_height_cms = user_data.height_cms!!
                            }
                            if (user_data.passionate.equals(null)) {
                                user_passionate = "0"
                            } else {
                                user_passionate = user_data.passionate!!
                            }
                            if (user_data.leisure_time.equals(null)) {
                                user_leisure_time = "0"
                            } else {
                                user_leisure_time = user_data.leisure_time!!
                            }
                            if (user_data.thankful_1.equals(null)) {
                                user_thankful_1 = "0"
                            } else {
                                user_thankful_1 = user_data.thankful_1!!
                            }
                            if (user_data.thankful_2.equals(null)) {
                                user_thankful_2 = "0"
                            } else {
                                user_thankful_2 = user_data.thankful_2!!
                            }
                            if (user_data.thankful_3.equals(null)) {
                                user_thankful_3 = "0"
                            } else {
                                user_thankful_3 = user_data.thankful_3!!
                            }
                            if (user_data.partner_age_min.equals(null)) {
                                part_age_min_data = "0"
                            } else {
                                part_age_min_data = user_data.partner_age_min!!
                            }
                            if (user_data.partner_age_max.equals(null)) {
                                part_age_max_data = "0"
                            } else {
                                part_age_max_data = user_data.partner_age_max!!
                            }

                            if (user_data.partner_match_distance.equals(null)) {
                                match_distance_data = "0"
                            } else {
                                match_distance_data = user_data.partner_match_distance!!
                            }
                            if (user_data.partner_match_search.equals(null)) {
                                match_search_imp_data = "0"
                            } else {
                                match_search_imp_data = user_data.partner_match_search!!
                            }
                            if (user_data.partner_ethnicity.equals(null)) {
                                partner_ethnicity = "0"
                            } else {
                                partner_ethnicity = user_data.partner_ethnicity!!
                            }
                            if (user_data.partner_religion.equals(null)) {
                                partner_religion = "0"
                            } else {
                                partner_religion = user_data.partner_religion!!
                            }
                            /*if (user_data.partner_denomination.equals(null)) {
                                partner_denomination = "0"
                            } else {
                                partner_denomination = user_data.partner_religion!!
                            }*/
                            if (user_data.partner_education.equals(null)) {
                                partner_education = "0"
                            } else {
                                partner_education = user_data.partner_education!!
                            }
                            if (user_data.partner_occupation.equals(null)) {
                                partner_occupation = "0"
                            } else {
                                partner_occupation = user_data.partner_occupation!!
                            }
                            if (user_data.partner_smoke.equals(null)) {
                                partner_smoke = "0"
                            } else {
                                partner_smoke = user_data.partner_smoke!!
                            }
                            if (user_data.partner_drink.equals(null)) {
                                partner_drink = "0"
                            } else {
                                partner_drink = user_data.partner_drink!!
                            }
                            if (user_data.photo1.equals(null)) {
                                photo1 = "0"
                                final_photo_1 = "0"
                            } else {
                                photo1 = user_data.photo1!!
                                final_photo_1 = user_data.photo1!!
                            }
                            if (user_data.photo2.equals(null)) {
                                photo2 = "0"
                            } else {
                                photo2 = user_data.photo2!!
                            }
                            if (user_data.photo3.equals(null)) {
                                photo3 = "0"
                            } else {
                                photo3 = user_data.photo3!!
                            }
                            if (user_data.photo4.equals(null)) {
                                photo4 = "0"
                            } else {
                                photo4 = user_data.photo4!!
                            }
                            if (user_data.photo5.equals(null)) {
                                photo5 = "0"
                            } else {
                                photo5 = user_data.photo5!!
                            }
                            if (user_data.photo6.equals(null)) {
                                photo6 = "0"
                            } else {
                                photo6 = user_data.photo6!!
                            }
                            if (user_data.photo7.equals(null)) {
                                photo7 = "0"
                            } else {
                                photo7 = user_data.photo7!!
                            }
                            if (user_data.photo8.equals(null)) {
                                photo8 = "0"
                            } else {
                                photo8 = user_data.photo8!!
                            }
                            if (user_data.photo9.equals(null)) {
                                photo9 = "0"
                            } else {
                                photo9 = user_data.photo9!!
                            }
                            if (user_data.photo10.equals(null)) {
                                photo10 = "0"
                            } else {
                                photo10 = user_data.photo10!!
                            }
                            if (user_data!!.provider.equals(null)) {
                                provider = "0"
                            } else {
                                provider = user_data.provider!!
                            }

                            if (user_data.gender.equals(null)) {
                                gender = "0"
                            } else {
                                gender = user_data.gender!!
                            }
                            if (user_data.looking_for.equals(null)) {
                                looking_for = "0"
                            } else {
                                looking_for = user_data.looking_for!!
                            }
                            if (user_data.first_name.equals(null)) {
                                user_first_name = "0"
                            } else {
                                user_first_name = user_data.first_name!!
                            }
                            if (user_data.email.equals(null)) {
                                user_email = "0"
                            } else {
                                user_email = user_data.email!!
                            }
                            if (user_data.zipcode.equals(null)) {
                                user_zipcode = "0"
                            } else {
                                user_zipcode = user_data.zipcode!!
                            }
                            if (user_data.state.equals(null)) {
                                user_state = "0"
                            } else {
                                user_state = user_data.state!!
                            }
                            if (user_data.referal_from.equals(null)) {
                                user_referal_from = "0"
                            } else {
                                user_referal_from = user_data.referal_from!!
                            }
                            if (user_data.country.equals(null)) {
                                country_stg = "0"
                            } else {
                                country_stg = user_data.country!!
                            }
                            if (user_data.mtongue_name.equals(null)) {
                                mother_tongue_stg = "0"
                            } else {
                                mother_tongue_stg = user_data.mtongue_name!!
                            }
                            if (user_data.partner_mtongue_name.equals(null)) {
                                partner_mother_tongue_stg = "0"
                            } else {
                                partner_mother_tongue_stg = user_data.partner_mtongue_name!!
                            }
                            if (user_data.username.equals(null)) {
                                username_stg = "0"
                            } else {
                                username_stg = user_data.username!!
                            }

                            if (user_data.password!!.equals(null)) {
                                new_password_stg = "0"
                            } else {
                                new_password_stg = user_data.password!!
                            }
                            if (user_data.signup_step.equals(null)) {
                                signup_step_stg = "0"
                            } else {
                                signup_step_stg = user_data.signup_step!!
                            }
                            Log.e("part_age_min_data", part_age_min_data)
                            dllSessionManager.createLoginSession(
                                user_data.id!!,
                                gender,
                                looking_for,
                                user_first_name,
                                user_email,
                                user_zipcode,
                                user_state,
                                user_referal_from,
                                user_data.children!!,
                                user_city,
                                user_dob,
                                user_phone_number,
                                user_ethnicity,
                                user_religion,
                                user_denomination,
                                user_education,
                                user_occupation,
                                user_income,
                                user_smoke,
                                user_drink,
                                user_height_ft,
                                user_height_inc,
                                user_height_cms,
                                user_passionate,
                                user_leisure_time,
                                user_thankful_1,
                                user_thankful_2,
                                user_thankful_3,
                                part_age_min_data,
                                part_age_max_data,
                                match_distance_data,
                                match_search_imp_data,
                                partner_ethnicity,
                                partner_religion,
                                partner_denomination,
                                partner_education,
                                partner_occupation,
                                partner_smoke,
                                partner_drink,
                                photo1,
                                user_data.photo1_approve!!,
                                photo2,
                                user_data.photo2_approve!!,
                                photo3,
                                user_data.photo3_approve!!,
                                photo4,
                                user_data.photo4_approve!!,
                                photo5,
                                user_data.photo5_approve!!,
                                photo6,
                                user_data.photo6_approve!!,
                                photo7,
                                user_data.photo7_approve!!,
                                photo8,
                                user_data.photo8_approve!!,
                                photo9,
                                user_data.photo9_approve!!,
                                photo10,
                                user_data.photo10_approve!!,
                                user_data.status!!,
                                signup_step_stg,
                                provider,
                                country_stg,
                                mother_tongue_stg,
                                partner_mother_tongue_stg,
                                username_stg,
                                new_password_stg,
                                true

                            )

                            dllSessionManager.CreateNotificationSession("Yes", "Yes")

                            val usersize = dllSessionManager.userDetails
                            Log.d(
                                "Login_data",
                                usersize.toString() + "" + photo1 + "--" + user_data.photo1_approve!! + "----" + new_password_stg + "---" + username_stg
                            )




                            loginAndStart(
                                username_stg,
                                new_password_stg
                            )

                            /*if (dllSessionManager.isLoggedIn) {
                                //progressDialog.dismiss()
                                if (final_photo_1.equals("0")) {
                                    val home_intent =
                                        Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                                    //home_intent.putExtra("from_list", "register_list")
                                    startActivity(home_intent)
                                    finish()
                                } else {
                                    val home_intent =
                                        Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                                    home_intent.putExtra("from_list", "register_list")
                                    startActivity(home_intent)
                                    finish()
                                }
                            }*/

                            Log.e("xmpp_user_id", user_data.id)
                            /*if(final_photo_1.equals("0")) {
                                val home_intent = Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                                //home_intent.putExtra("from_list", "register_list")
                                startActivity(home_intent)
                                finish()
                            }else {
                                val home_intent = Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                                home_intent.putExtra("from_list", "register_list")
                                startActivity(home_intent)
                                finish()
                            }*/


                        } else if (response!!.body()!!.status.equals("3")) {

                            accountstatus()


                        } else {
                            Toast.makeText(
                                this@RegisterLoginActivity,
                                response.body()!!.result,
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                        if (response.body()!!.status.equals("status")) {
                            Toast.makeText(
                                this@RegisterLoginActivity,
                                response.body()!!.result,
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                        //  }
                    }

                    override fun onFailure(call: Call<CheckLoginResponse>, t: Throwable) {
                        Log.w("Result_Order_details", t.toString())
                        Toast.makeText(
                            this@RegisterLoginActivity,
                            "Something went wrong! Please try again",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            }
        }
    }

    fun accountstatus() {
        val builder = AlertDialog.Builder(this!!)
        builder.setTitle("Alert!")
            .setMessage("The account you are Trying to view has been suspended. \nPlease Contact Customer Support \nsupport@divorcelovelounge.com")
            .setCancelable(false)
            .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                    /* val intent = Intent(this@RegisterLoginActivity, MembershipPlanActivity::class.java)
                     startActivity(intent)*/
                }
            })

        val alertDialog = builder.create()
        alertDialog.show()
    }


    fun googlesignApi(
        email: String,
        provider: String,
        name: String,
        device_token: String,
        device_type: String
    ) {

        var user_city = ""
        var user_dob = ""
        var user_phone_number = ""
        var user_ethnicity = ""
        var user_religion = ""
        var user_denomination = ""
        var user_education = ""
        var user_occupation = ""
        var user_income = ""
        var user_smoke = ""
        var user_drink = ""
        var user_height_ft = ""
        var user_height_inc = ""
        var user_height_cms = ""
        var user_passionate = ""
        var user_leisure_time = ""
        var user_thankful_1 = ""
        var user_thankful_2 = ""
        var user_thankful_3 = ""
        var part_age_min_data = ""
        var part_age_max_data = ""
        var match_distance_data = ""
        var match_search_imp_data = ""
        var partner_ethnicity = ""
        var partner_religion = ""
        var partner_denomination = ""
        var partner_education = ""
        var partner_occupation = ""
        var partner_smoke = ""
        var partner_drink = ""
        var photo1 = ""
        var photo2 = ""
        var photo3 = ""
        var photo4 = ""
        var photo5 = ""
        var photo6 = ""
        var photo7 = ""
        var photo8 = ""
        var photo9 = ""
        var photo10 = ""
        var provider_stg = ""
        var country_stg = ""
        var mother_tongue_stg = ""
        var partner_mother_tongue_stg = ""
        var referal_from = ""
        var gender = ""
        var looking_for = ""
        var user_first_name = ""
        var user_email = ""
        var user_zipcode = ""
        var user_state = ""
        var username_stg = ""
        var new_password_stg = ""

        val apiService = ApiInterface.create()
        val call = apiService.socialloginApi(email, provider, name, device_token, device_type)
        call.enqueue(object : Callback<SocialLoginresponse> {
            override fun onResponse(
                call: Call<SocialLoginresponse>,
                response: retrofit2.Response<SocialLoginresponse>?
            ) {
                if (response!!.body() != null) {

                    /*   if (progressDialog.isShowing) {
                           progressDialog.dismiss()
                       }
   */
                    if (response.body()!!.status == "1") {

                        val user_data = response.body()!!.data

                        if (user_data!!.city.equals(null)) {
                            user_city = "0"
                        } else {
                            user_city = user_data.city!!
                        }
                        if (user_data.provider.equals(null)) {
                            provider_stg = "0"
                        } else {
                            provider_stg = user_data.provider!!
                        }

                        if (user_data.referal_from.equals(null)) {
                            referal_from = "0"
                        } else {
                            referal_from = user_data.referal_from!!
                        }
                        if (user_data.dob.equals(null)) {
                            user_dob = "0"
                        } else {
                            user_dob = user_data.dob!!
                        }
                        if (user_data.phone_number.equals(null)) {
                            user_phone_number = "0"
                        } else {
                            user_phone_number = user_data.phone_number!!
                        }
                        if (user_data.ethnicity.equals(null)) {
                            user_ethnicity = "0"
                        } else {
                            user_ethnicity = user_data.ethnicity!!
                        }
                        if (user_data.religion.equals(null)) {
                            user_religion = "0"
                        } else {
                            user_religion = user_data.religion!!
                        }

                        if (user_data.education.equals(null)) {
                            user_education = "0"
                        } else {
                            user_education = user_data.education!!
                        }
                        if (user_data.occupation.equals(null)) {
                            user_occupation = "0"
                        } else {
                            user_occupation = user_data.occupation!!
                        }
                        if (user_data.income.equals(null)) {
                            user_income = "0"
                        } else {
                            user_income = user_data.income!!
                        }
                        if (user_data.smoke.equals(null)) {
                            user_smoke = "0"
                        } else {
                            user_smoke = user_data.smoke!!
                        }
                        if (user_data.drink.equals(null)) {
                            user_drink = "0"
                        } else {
                            user_drink = user_data.drink!!
                        }
                        if (user_data.height_ft.equals(null)) {
                            user_height_ft = "0"
                        } else {
                            user_height_ft = user_data.height_ft!!
                        }
                        if (user_data.height_inc.equals(null)) {
                            user_height_inc = "0"
                        } else {
                            user_height_inc = user_data.height_inc!!
                        }
                        if (user_data.height_cms.equals(null)) {
                            user_height_cms = "0"
                        } else {
                            user_height_cms = user_data.height_cms!!
                        }
                        if (user_data.passionate.equals(null)) {
                            user_passionate = "0"
                        } else {
                            user_passionate = user_data.passionate!!
                        }
                        if (user_data.leisure_time.equals(null)) {
                            user_leisure_time = "0"
                        } else {
                            user_leisure_time = user_data.leisure_time!!
                        }
                        if (user_data.thankful_1.equals(null)) {
                            user_thankful_1 = "0"
                        } else {
                            user_thankful_1 = user_data.thankful_1!!
                        }
                        if (user_data.thankful_2.equals(null)) {
                            user_thankful_2 = "0"
                        } else {
                            user_thankful_2 = user_data.thankful_2!!
                        }
                        if (user_data.thankful_3.equals(null)) {
                            user_thankful_3 = "0"
                        } else {
                            user_thankful_3 = user_data.thankful_3!!
                        }
                        if (user_data.partner_age_min.equals(null)) {
                            part_age_min_data = "0"
                        } else {
                            part_age_min_data = user_data.partner_age_min!!
                        }
                        if (user_data.partner_age_max.equals(null)) {
                            part_age_max_data = "0"
                        } else {
                            part_age_max_data = user_data.partner_age_max!!
                        }

                        if (user_data.partner_match_distance.equals(null)) {
                            match_distance_data = "0"
                        } else {
                            match_distance_data = user_data.partner_match_distance!!
                        }
                        if (user_data.partner_match_search.equals(null)) {
                            match_search_imp_data = "0"
                        } else {
                            match_search_imp_data = user_data.partner_match_search!!
                        }
                        if (user_data.partner_ethnicity.equals(null)) {
                            partner_ethnicity = "0"
                        } else {
                            partner_ethnicity = user_data.partner_ethnicity!!
                        }
                        if (user_data.partner_religion.equals(null)) {
                            partner_religion = "0"
                        } else {
                            partner_religion = user_data.partner_religion!!
                        }

                        if (user_data.partner_education.equals(null)) {
                            partner_education = "0"
                        } else {
                            partner_education = user_data.partner_education!!
                        }
                        if (user_data.partner_occupation.equals(null)) {
                            partner_occupation = "0"
                        } else {
                            partner_occupation = user_data.partner_occupation!!
                        }
                        if (user_data.partner_smoke.equals(null)) {
                            partner_smoke = "0"
                        } else {
                            partner_smoke = user_data.partner_smoke!!
                        }
                        if (user_data.partner_drink.equals(null)) {
                            partner_drink = "0"
                        } else {
                            partner_drink = user_data.partner_drink!!
                        }
                        if (user_data.photo1.equals(null)) {
                            photo1 = "0"
                            final_photo_1 = "0"
                        } else {
                            photo1 = user_data.photo1!!
                            final_photo_1 = user_data.photo1!!
                        }
                        if (user_data.photo2.equals(null)) {
                            photo2 = "0"
                        } else {
                            photo2 = user_data.photo2!!
                        }
                        if (user_data.photo3.equals(null)) {
                            photo3 = "0"
                        } else {
                            photo3 = user_data.photo3!!
                        }
                        if (user_data.photo4.equals(null)) {
                            photo4 = "0"
                        } else {
                            photo4 = user_data.photo4!!
                        }
                        if (user_data.photo5.equals(null)) {
                            photo5 = "0"
                        } else {
                            photo5 = user_data.photo5!!
                        }
                        if (user_data.photo6.equals(null)) {
                            photo6 = "0"
                        } else {
                            photo6 = user_data.photo6!!
                        }
                        if (user_data.photo7.equals(null)) {
                            photo7 = "0"
                        } else {
                            photo7 = user_data.photo7!!
                        }
                        if (user_data.photo8.equals(null)) {
                            photo8 = "0"
                        } else {
                            photo8 = user_data.photo8!!
                        }
                        if (user_data.photo9.equals(null)) {
                            photo9 = "0"
                        } else {
                            photo9 = user_data.photo9!!
                        }
                        if (user_data.photo10.equals(null)) {
                            photo10 = "0"
                        } else {
                            photo10 = user_data.photo10!!
                        }

                        if (user_data.gender.equals(null)) {
                            gender = "0"
                        } else {
                            gender = user_data.gender!!
                        }
                        if (user_data.looking_for.equals(null)) {
                            looking_for = "0"
                        } else {
                            looking_for = user_data.looking_for!!
                        }
                        if (user_data.first_name.equals(null)) {
                            user_first_name = "0"
                        } else {
                            user_first_name = user_data.first_name!!
                        }
                        if (user_data.email.equals(null)) {
                            user_email = "0"
                        } else {
                            user_email = user_data.email!!
                        }
                        if (user_data.zipcode.equals(null)) {
                            user_zipcode = "0"
                        } else {
                            user_zipcode = user_data.zipcode!!
                        }
                        if (user_data.state.equals(null)) {
                            user_state = "0"
                        } else {
                            user_state = user_data.state!!
                        }
                        if (user_data.country.equals(null)) {
                            country_stg = "0"
                        } else {
                            country_stg = user_data.country!!
                        }
                        if (user_data.mtongue_name.equals(null)) {
                            mother_tongue_stg = "0"
                        } else {
                            mother_tongue_stg = user_data.mtongue_name!!
                        }
                        if (user_data.partner_mtongue_name.equals(null)) {
                            partner_mother_tongue_stg = "0"
                        } else {
                            partner_mother_tongue_stg = user_data.partner_mtongue_name!!
                        }

                        if (user_data.username.equals(null)) {
                            username_stg = "0"
                        } else {
                            username_stg = user_data.username!!
                        }

                        if (user_data.password.equals(null)) {
                            new_password_stg = "0"
                        } else {
                            new_password_stg = user_data.password!!
                        }

                        Log.e("part_age_min_data", part_age_min_data)
                        dllSessionManager.createLoginSession(
                            user_data.id!!,
                            gender,
                            looking_for,
                            user_first_name,
                            user_email,
                            user_zipcode,
                            user_state,
                            referal_from,
                            user_data.children!!,
                            user_city,
                            user_dob,
                            user_phone_number,
                            user_ethnicity,
                            user_religion,
                            user_denomination,
                            user_education,
                            user_occupation,
                            user_income,
                            user_smoke,
                            user_drink,
                            user_height_ft,
                            user_height_inc,
                            user_height_cms,
                            user_passionate,
                            user_leisure_time,
                            user_thankful_1,
                            user_thankful_2,
                            user_thankful_3,
                            part_age_min_data,
                            part_age_max_data,
                            match_distance_data,
                            match_search_imp_data,
                            partner_ethnicity,
                            partner_religion,
                            partner_denomination,
                            partner_education,
                            partner_occupation,
                            partner_smoke,
                            partner_drink,
                            photo1,
                            user_data.photo1_approve!!,
                            photo2,
                            user_data.photo2_approve!!,
                            photo3,
                            user_data.photo3_approve!!,
                            photo4,
                            user_data.photo4_approve!!,
                            photo5,
                            user_data.photo5_approve!!,
                            photo6,
                            user_data.photo6_approve!!,
                            photo7,
                            user_data.photo7_approve!!,
                            photo8,
                            user_data.photo8_approve!!,
                            photo9,
                            user_data.photo9_approve!!,
                            photo10,
                            user_data.photo10_approve!!,
                            user_data.status!!,
                            user_data.signup_step!!,
                            provider_stg,
                            country_stg,
                            mother_tongue_stg,
                            partner_mother_tongue_stg,
                            username_stg,
                            new_password_stg,
                            false
                        )



                        Log.e("user_data_lll", user_data.toString() + "---" + user_data!!.email)
                        val home_intent =
                            Intent(this@RegisterLoginActivity, RegisterAccount::class.java)
                        home_intent.putExtra("from", "social_register")
                        startActivity(home_intent)

                    } else if (response.body()!!.status == "2") {

                        val user_data = response.body()!!.data

                        if (user_data!!.city.equals(null)) {
                            user_city = "0"
                        } else {
                            user_city = user_data.city!!
                        }
                        if (user_data!!.provider.equals(null)) {
                            provider_stg = "0"
                        } else {
                            provider_stg = user_data.provider!!
                        }

                        if (user_data!!.referal_from.equals(null)) {
                            referal_from = "0"
                        } else {
                            referal_from = user_data.referal_from!!
                        }
                        if (user_data.dob.equals(null)) {
                            user_dob = "0"
                        } else {
                            user_dob = user_data.dob!!
                        }
                        if (user_data.phone_number.equals(null)) {
                            user_phone_number = "0"
                        } else {
                            user_phone_number = user_data.phone_number!!
                        }
                        if (user_data.ethnicity.equals(null)) {
                            user_ethnicity = "0"
                        } else {
                            user_ethnicity = user_data.ethnicity!!
                        }
                        if (user_data.religion.equals(null)) {
                            user_religion = "0"
                        } else {
                            user_religion = user_data.religion!!
                        }
                        if (user_data.denomination.equals(null)) {
                            user_denomination = "0"
                        } else {
                            user_denomination = user_data.denomination!!
                        }
                        if (user_data.education.equals(null)) {
                            user_education = "0"
                        } else {
                            user_education = user_data.education!!
                        }
                        if (user_data.occupation.equals(null)) {
                            user_occupation = "0"
                        } else {
                            user_occupation = user_data.occupation!!
                        }
                        if (user_data.income.equals(null)) {
                            user_income = "0"
                        } else {
                            user_income = user_data.income!!
                        }
                        if (user_data.smoke.equals(null)) {
                            user_smoke = "0"
                        } else {
                            user_smoke = user_data.smoke!!
                        }
                        if (user_data.drink.equals(null)) {
                            user_drink = "0"
                        } else {
                            user_drink = user_data.drink!!
                        }
                        if (user_data.height_ft.equals(null)) {
                            user_height_ft = "0"
                        } else {
                            user_height_ft = user_data.height_ft!!
                        }
                        if (user_data.height_inc.equals(null)) {
                            user_height_inc = "0"
                        } else {
                            user_height_inc = user_data.height_inc!!
                        }
                        if (user_data.height_cms.equals(null)) {
                            user_height_cms = "0"
                        } else {
                            user_height_cms = user_data.height_cms!!
                        }
                        if (user_data.passionate.equals(null)) {
                            user_passionate = "0"
                        } else {
                            user_passionate = user_data.passionate!!
                        }
                        if (user_data.leisure_time.equals(null)) {
                            user_leisure_time = "0"
                        } else {
                            user_leisure_time = user_data.leisure_time!!
                        }
                        if (user_data.thankful_1.equals(null)) {
                            user_thankful_1 = "0"
                        } else {
                            user_thankful_1 = user_data.thankful_1!!
                        }
                        if (user_data.thankful_2.equals(null)) {
                            user_thankful_2 = "0"
                        } else {
                            user_thankful_2 = user_data.thankful_2!!
                        }
                        if (user_data.thankful_3.equals(null)) {
                            user_thankful_3 = "0"
                        } else {
                            user_thankful_3 = user_data.thankful_3!!
                        }
                        if (user_data.partner_age_min.equals(null)) {
                            part_age_min_data = "0"
                        } else {
                            part_age_min_data = user_data.partner_age_min!!
                        }
                        if (user_data.partner_age_max.equals(null)) {
                            part_age_max_data = "0"
                        } else {
                            part_age_max_data = user_data.partner_age_max!!
                        }

                        if (user_data.partner_match_distance.equals(null)) {
                            match_distance_data = "0"
                        } else {
                            match_distance_data = user_data.partner_match_distance!!
                        }
                        if (user_data.partner_match_search.equals(null)) {
                            match_search_imp_data = "0"
                        } else {
                            match_search_imp_data = user_data.partner_match_search!!
                        }
                        if (user_data.partner_ethnicity.equals(null)) {
                            partner_ethnicity = "0"
                        } else {
                            partner_ethnicity = user_data.partner_ethnicity!!
                        }
                        if (user_data.partner_religion.equals(null)) {
                            partner_religion = "0"
                        } else {
                            partner_religion = user_data.partner_religion!!
                        }
                        if (user_data.partner_denomination.equals(null)) {
                            partner_denomination = "0"
                        } else {
                            partner_denomination = user_data.partner_religion!!
                        }
                        if (user_data.partner_education.equals(null)) {
                            partner_education = "0"
                        } else {
                            partner_education = user_data.partner_education!!
                        }
                        if (user_data.partner_occupation.equals(null)) {
                            partner_occupation = "0"
                        } else {
                            partner_occupation = user_data.partner_occupation!!
                        }
                        if (user_data.partner_smoke.equals(null)) {
                            partner_smoke = "0"
                        } else {
                            partner_smoke = user_data.partner_smoke!!
                        }
                        if (user_data.partner_drink.equals(null)) {
                            partner_drink = "0"
                        } else {
                            partner_drink = user_data.partner_drink!!
                        }
                        if (user_data.photo1.equals(null)) {
                            photo1 = "0"
                            final_photo_1 = "0"
                        } else {
                            photo1 = user_data.photo1!!
                            final_photo_1 = user_data.photo1!!
                        }
                        if (user_data.photo2.equals(null)) {
                            photo2 = "0"
                        } else {
                            photo2 = user_data.photo2!!
                        }
                        if (user_data.photo3.equals(null)) {
                            photo3 = "0"
                        } else {
                            photo3 = user_data.photo3!!
                        }
                        if (user_data.photo4.equals(null)) {
                            photo4 = "0"
                        } else {
                            photo4 = user_data.photo4!!
                        }
                        if (user_data.photo5.equals(null)) {
                            photo5 = "0"
                        } else {
                            photo5 = user_data.photo5!!
                        }
                        if (user_data.photo6.equals(null)) {
                            photo6 = "0"
                        } else {
                            photo6 = user_data.photo6!!
                        }
                        if (user_data.photo7.equals(null)) {
                            photo7 = "0"
                        } else {
                            photo7 = user_data.photo7!!
                        }
                        if (user_data.photo8.equals(null)) {
                            photo8 = "0"
                        } else {
                            photo8 = user_data.photo8!!
                        }
                        if (user_data.photo9.equals(null)) {
                            photo9 = "0"
                        } else {
                            photo9 = user_data.photo9!!
                        }
                        if (user_data.photo10.equals(null)) {
                            photo10 = "0"
                        } else {
                            photo10 = user_data.photo10!!
                        }

                        if (user_data.gender.equals(null)) {
                            gender = "0"
                        } else {
                            gender = user_data.gender!!
                        }
                        if (user_data.looking_for.equals(null)) {
                            looking_for = "0"
                        } else {
                            looking_for = user_data.looking_for!!
                        }
                        if (user_data.first_name.equals(null)) {
                            user_first_name = "0"
                        } else {
                            user_first_name = user_data.first_name!!
                        }
                        if (user_data.email.equals(null)) {
                            user_email = "0"
                        } else {
                            user_email = user_data.email!!
                        }
                        if (user_data.zipcode.equals(null)) {
                            user_zipcode = "0"
                        } else {
                            user_zipcode = user_data.zipcode!!
                        }
                        if (user_data.state.equals(null)) {
                            user_state = "0"
                        } else {
                            user_state = user_data.state!!
                        }
                        if (user_data.country.equals(null)) {
                            country_stg = "0"
                        } else {
                            country_stg = user_data.country!!
                        }
                        if (user_data.mtongue_name.equals(null)) {
                            mother_tongue_stg = "0"
                        } else {
                            mother_tongue_stg = user_data.mtongue_name!!
                        }
                        if (user_data.partner_mtongue_name.equals(null)) {
                            partner_mother_tongue_stg = "0"
                        } else {
                            partner_mother_tongue_stg = user_data.partner_mtongue_name!!
                        }
                        if (user_data.username.equals(null)) {
                            username_stg = "0"
                        } else {
                            username_stg = user_data.username!!
                        }

                        if (user_data.password.equals(null)) {
                            new_password_stg = "0"
                        } else {
                            new_password_stg = user_data.password!!
                        }


                        Log.e("part_age_min_data", part_age_min_data)
                        dllSessionManager.createLoginSession(
                            user_data.id!!,
                            gender,
                            looking_for,
                            user_first_name,
                            user_email,
                            user_zipcode,
                            user_state,
                            referal_from,
                            user_data.children!!,
                            user_city,
                            user_dob,
                            user_phone_number,
                            user_ethnicity,
                            user_religion,
                            user_denomination,
                            user_education,
                            user_occupation,
                            user_income,
                            user_smoke,
                            user_drink,
                            user_height_ft,
                            user_height_inc,
                            user_height_cms,
                            user_passionate,
                            user_leisure_time,
                            user_thankful_1,
                            user_thankful_2,
                            user_thankful_3,
                            part_age_min_data,
                            part_age_max_data,
                            match_distance_data,
                            match_search_imp_data,
                            partner_ethnicity,
                            partner_religion,
                            partner_denomination,
                            partner_education,
                            partner_occupation,
                            partner_smoke,
                            partner_drink,
                            photo1,
                            user_data.photo1_approve!!,
                            photo2,
                            user_data.photo2_approve!!,
                            photo3,
                            user_data.photo3_approve!!,
                            photo4,
                            user_data.photo4_approve!!,
                            photo5,
                            user_data.photo5_approve!!,
                            photo6,
                            user_data.photo6_approve!!,
                            photo7,
                            user_data.photo7_approve!!,
                            photo8,
                            user_data.photo8_approve!!,
                            photo9,
                            user_data.photo9_approve!!,
                            photo10,
                            user_data.photo10_approve!!,
                            user_data.status!!,
                            user_data.signup_step!!,
                            provider_stg,
                            country_stg,
                            mother_tongue_stg,
                            partner_mother_tongue_stg,
                            username_stg,
                            new_password_stg,
                            true
                        )

                        Log.e("user_data_lll", user_data.toString() + "---" + user_data!!.email)
                        /*val home_intent = Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                        home_intent.putExtra("from_list", "social_register")
                        startActivity(home_intent)
                        finish()*/

                        loginAndStart(
                            user_data.username!!,
                            new_password_stg
                        )


                    } else {
                        Log.e("user_data_lll", response.body()!!.status)
                        Toast.makeText(
                            this@RegisterLoginActivity,
                            response.body()!!.result, Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                } else {
                    Toast.makeText(
                        this@RegisterLoginActivity,
                        "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<SocialLoginresponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())

                /* if (progressDialog.isShowing) {
                     progressDialog.dismiss()
                 }
 */
                Toast.makeText(
                    this@RegisterLoginActivity,
                    "Something went wrong! Please try again", Toast.LENGTH_SHORT
                ).show()
            }
        })
    }


    //forgotpassowrd
    fun forgotpassword(email: String) {
        val apiService = ApiInterface.create()
        val call = apiService.forgotPasswordApi(email)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<BlockListResponse> {
            override fun onResponse(
                call: Call<BlockListResponse>,
                response: retrofit2.Response<BlockListResponse>?
            ) {
                progress_bar_login.visibility = View.GONE
                login_dialog.dismiss()
                if (response != null) {
                    if (response.body()!!.status == "1") {

                        forgot_pass_dialog.dismiss()
                        Toast.makeText(
                            this@RegisterLoginActivity,
                            response.body()!!.Message,
                            Toast.LENGTH_SHORT
                        )
                            .show()

                    } else if (response.body()!!.status == "3") {
                        accountstatus()
                    } else {
                        Toast.makeText(
                            this@RegisterLoginActivity,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                }
            }

            override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
                Toast.makeText(
                    this@RegisterLoginActivity,
                    "Something went wrong! Please try again",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    private fun loginAndStart(userName: String, password: String) {
        progressDialog.show()


        if (dllSessionManager.isLoggedIn) {
            //progressDialog.dismiss()
            if (final_photo_1.equals("0")) {
                val home_intent =
                    Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                startActivity(home_intent)
                finish()
            } else {
                val home_intent =
                    Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                home_intent.putExtra("from_list", "register_list")
                startActivity(home_intent)
                finish()
            }
        }


       /* Tasks.executeInBackground(this@RegisterLoginActivity, {
            XMPPSession.startService(this@RegisterLoginActivity)
            //((SplashActivity) getApplicationContext()).getService().login(userName, password);
            XMPPSession.getInstance().login(userName.trim(), password)
            Log.e("xmpp_user_id_ll", userName)
            Thread.sleep(XMPPSession.REPLY_TIMEOUT.toLong())
            null
        }, object : Completion<Any> {
            override fun onSuccess(context: Context, result: Any?) {

                //((SplashActivity) getApplicationContext()).startApplication();

                if (dllSessionManager.isLoggedIn) {
                    //progressDialog.dismiss()
                    if (final_photo_1.equals("0")) {
                        val home_intent =
                            Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                        //home_intent.putExtra("from_list", "register_list")
                        startActivity(home_intent)
                        finish()
                    } else {
                        val home_intent =
                            Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                        home_intent.putExtra("from_list", "register_list")
                        startActivity(home_intent)
                        finish()
                    }

                    *//*val home_intent = Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                    home_intent.putExtra("from_list", "register_list")
                    startActivity(home_intent)
                    finish()*//*

                }

            }

            override fun onError(context: Context, e: Exception) {
                Log.e("xmpp_user_id_ll", e.toString())
                progressDialog.dismiss()
                XMPPSession.getInstance().xmppConnection.disconnect()
                XMPPSession.clearInstance()
                // Toast.makeText(context, getString(R.string.error_login), Toast.LENGTH_SHORT).show()

                if (dllSessionManager.isLoggedIn) {
                    //progressDialog.dismiss()
                    if (final_photo_1.equals("0")) {
                        val home_intent =
                            Intent(this@RegisterLoginActivity, ProfilePhotos::class.java)
                        startActivity(home_intent)
                        finish()
                    } else {
                        val home_intent =
                            Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                        home_intent.putExtra("from_list", "register_list")
                        startActivity(home_intent)
                        finish()
                    }
                }

            }
        })*/
    }

    fun ProfilePhotosAPI() {
        progressDialog.show()
        val apiService = ApiInterface.create()
        val call =
            apiService.getProfilePhotosApi(user_details.get(DllSessionManager.USER_ID).toString())
        call.enqueue(object : Callback<ProfilePhotosResponse> {
            override fun onResponse(
                call: Call<ProfilePhotosResponse>,
                response: retrofit2.Response<ProfilePhotosResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                        "success"
                    )
                ) {

                    Log.e(
                        "data_2",
                        response!!.body()!!.photos!!.photo1 + "---" + response!!.body()!!.photos!!.photo1_approve
                    )

                    /*if(response!!.body()!!.photos!!.photo1_approve.equals("APPROVED")){

                        //user_details.get(DllSessionManager.PHOTO1).set
                    }else{
                        dllSessionManager.PhotoApprove(response!!.body()!!.photos!!.photo1_approve)
                    }
*/
                    dllSessionManager.PhotoApprove(response!!.body()!!.photos!!.photo1_approve)


                    loginAndStart(
                        user_details.get(DllSessionManager.USER_NAME_ID).toString()!!,
                        user_details.get(DllSessionManager.USER_PASSWORD).toString()
                    )

                    /* val home_intent = Intent(this@RegisterLoginActivity, DashboardActivity::class.java)
                     home_intent.putExtra("from_list", "register_list")
                     startActivity(home_intent)
                     finish()*/

                    //progressDialog.dismiss()


                } else {

                }

            }

            override fun onFailure(call: Call<ProfilePhotosResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    override fun onBackPressed() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg, "0")

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            card_adv_banner_id.visibility = View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e(
                                    "IMAGEADD",
                                    PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(i).add_image
                                )

                                add_link = response.body()!!.data!!.get(i).add_link.toString()
                                val textSliderView = TextSliderView(this@RegisterLoginActivity);
                                textSliderView
                                    //   .description(response.body()!!.data!!.get(i).add_type)
                                    .description("Ads")
                                    .image(
                                        PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(
                                            i
                                        ).add_image
                                    )
                                    //.image("https://divorcelovelounge.com/uploads/adds/839626_fruit-picture-900X350.png")
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@RegisterLoginActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString(
                                        "extra",
                                        response.body()!!.data!!.get(i).add_link.toString()
                                    );
                                sliderLayout.addSlider(textSliderView);
                            }
                            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000)

                            sliderLayout.addOnPageChangeListener(this@RegisterLoginActivity);


                        } else {
                            card_adv_banner_id.visibility = View.GONE
                            Log.e("date_only", "No Data")
                        }
                    } else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun EnableGPSAutoMatically(login_signup: String) {
        var googleApiClient: GoogleApiClient? = null
        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this@RegisterLoginActivity)
                .addOnConnectionFailedListener(this).build()
            googleApiClient!!.connect()
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = (30 * 1000).toLong()
            locationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            // **************************
            builder.setAlwaysShow(true) // this is the key ingredient
            // **************************

            val result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build())
            result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                override fun onResult(result: LocationSettingsResult) {
                    val status = result.status
                    val state = result
                        .locationSettingsStates
                    when (status.statusCode) {
                        LocationSettingsStatusCodes.SUCCESS -> {

                            if (login_signup.equals("signup")) {
                                val intent =
                                    Intent(this@RegisterLoginActivity, RegisterAccount::class.java)
                                intent.putExtra("from", "normal_register")
                                startActivity(intent)
                            } else if (login_signup.equals("login")) {
                                login_dialog.show()
                                Log.e("login_dialog", "show")
                            } else {
                                Log.e("login_dialog", "dismiss")
                            }
                            val gpsTracker = GPSTracker(this@RegisterLoginActivity)

                            val Dlatitude = gpsTracker.getLatitude()
                            val Dlongitude = gpsTracker.getLongitude()
                            LocationFullAddress(Dlatitude, Dlongitude)


                        }
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            toast("GPS is not on")
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(this@RegisterLoginActivity, 1000)

                            } catch (e: IntentSender.SendIntentException) {
                                // Ignore the error.
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> toast("Setting change not allowed")
                    }// All location settings are satisfied. The client can
                    // initialize location
                    // requests here.
                    // Location settings are not satisfied. However, we have
                    // no way to fix the
                    // settings so we won't show the dialog.
                }
            })
        }
    }

    private fun toast(message: String) {
        try {
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            // log("Window has been closed")
        }

    }

    fun LocationFullAddress(lat: Double, long: Double) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(this, Locale.getDefault())


        try {
            addresses = geocoder.getFromLocation(
                lat,
                long,
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            // val address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            val city = addresses[0].getLocality()
            val state = addresses[0].getAdminArea()
            val country = addresses[0].getCountryName()
            val postalCode = addresses[0].getPostalCode()
            val knownName = addresses[0].getFeatureName()

            getAdsLisrAPI(country, state, city, "Login")
            //getAdsLisrAPI("India", "Telangana", "Hyderabad", "Login")

            dllSessionManager.gpsLocationDetails(country, state, city)
            //dllSessionManager.gpsLocationDetails("India", "Telangana", "Hyderabad")

        } catch (e: java.lang.Exception) {
            getAdsLisrAPI("India", "Telangana", "Hyderabad", "Login")

            dllSessionManager.gpsLocationDetails("India", "Telangana", "Hyderabad")
            Log.e("login_ad_api", "dismiss")
        }

    }

    override fun onConnected(@Nullable bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {
        toast("Suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        toast("Failed")
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        //Toast.makeText(this,""+slider!!.bundle.get("extra")+"",Toast.LENGTH_SHORT).show();
        this.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(slider!!.bundle.get("extra").toString())
            )
        );


    }
}