package divorcelovelounge.com.dll

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.HashMap
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ComposeMessageActivity : AppCompatActivity() {
    private lateinit var rl_prefs: RelativeLayout
    private lateinit var iv_prefs_back: ImageView
    private lateinit var et_select_name_id: EditText
    lateinit var et_compose_msg_id: EditText
    lateinit var tv_send_id: CustomTextViewBold
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var user_dialog: Dialog
    lateinit var rv_user_list_id: RecyclerView
    lateinit var et_search_id: EditText
    lateinit var tv_no_list_id: CustomTextView


    lateinit var users_list_array: ArrayList<String>
    lateinit var users_ids_array: ArrayList<String>
    lateinit var full_users_list_array: ArrayList<All_List>

    lateinit var userListAdapter: MyRecyclerAdapter


    var user_name_srg = ""
    var user_id_srg = ""
    var from_stg=""
    var sent_user_id=""
    var first_name_stg="0"
    var plan_expire_status:Long=0

    lateinit var success_dialog:Dialog

    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id:CustomTextView
    lateinit var img_profile_pic_id:ImageView
    lateinit var iv_close:ImageView
    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compose_message)

        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        if(intent.getStringExtra("from_stg").equals("compose")){
            from_stg=intent.getStringExtra("from_stg")
        }else{
            from_stg=intent.getStringExtra("from_stg")
            sent_user_id=intent.getStringExtra("msg_from_id")
            first_name_stg=intent.getStringExtra("first_name_stg")
        }



        rl_prefs = findViewById(R.id.rl_prefs)
        iv_prefs_back = findViewById(R.id.iv_prefs_back)
        et_select_name_id = findViewById(R.id.et_select_name_id)
        iv_prefs_back.setOnClickListener {
            onBackPressed()
        }
        et_compose_msg_id = findViewById(R.id.et_compose_msg_id)
        tv_send_id = findViewById(R.id.tv_send_id)

        user_dialog = Dialog(this)
        user_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        user_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        user_dialog.setContentView(R.layout.mail_users_dialog)
        val window_login = user_dialog.window
        window_login!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val close_login_id = user_dialog.findViewById(R.id.close_login_id) as ImageView
        close_login_id.setOnClickListener {
            user_dialog.dismiss()
        }
        rv_user_list_id = user_dialog.findViewById(R.id.rvr_user_list_id) as RecyclerView
        et_search_id = user_dialog.findViewById(R.id.et_search_id) as EditText
        tv_no_list_id = user_dialog.findViewById(R.id.tv_no_list_id) as CustomTextView



        if(first_name_stg!="0"){
            user_dialog.dismiss()
            user_id_srg=sent_user_id
            et_select_name_id.setText(first_name_stg)
        }else{
            et_select_name_id.setOnClickListener {
                user_dialog.show()
            }
        }

        et_search_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                /*if (char!!.isNotEmpty()) {
                    userListAdapter.filter.filter(char.toString())

                } else {
                    userListAdapter.filter.filter("")
                }

                tv_no_list_id.visibility = if (userListAdapter.getSize()!! > 0) {
                    View.GONE
                } else {
                    View.VISIBLE
                }*/

                userListAdapter.filter(char.toString())
            }

        })




        UsersList()



        if(user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1")){
            val Server_date = user_details.get(DllSessionManager.MEMBERSHIP_SERVER_DATE)
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val date_server = sdf.parse(Server_date)


            val expir_date = user_details.get(DllSessionManager.MEMBERSHIP_PLAN_EXPIRY)
            val date_expir = sdf.parse(expir_date)


            val diff =date_expir.getTime() - date_server.getTime()

            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

            plan_expire_status= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        }


        tv_send_id.setOnClickListener {

            val get_content_stg = et_compose_msg_id.text.toString()


            if(user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1")&& plan_expire_status >= 0){
                if (user_id_srg != "" && get_content_stg != "") {

                    val apiService = ApiInterface.create()
                    val call = apiService.composeMessageApi(
                        user_details[DllSessionManager.USER_ID]!!.toString(),
                        user_id_srg,
                        get_content_stg
                    )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<BlockListResponse> {
                        override fun onResponse(
                            call: Call<BlockListResponse>,
                            response: retrofit2.Response<BlockListResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success")) {

                                Toast.makeText(this@ComposeMessageActivity,"Message Sent Succesfully",Toast.LENGTH_SHORT).show()

                               // onBackPressed()

                                val home_intent = Intent(this@ComposeMessageActivity, DashboardActivity::class.java)
                                home_intent.putExtra("from_list", "messages_list")
                                startActivity(home_intent)

                            }

                            //  }
                        }

                        override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }else{
                    Toast.makeText(this@ComposeMessageActivity,"Please Write A Message and Select User Name",Toast.LENGTH_SHORT).show()

                }
            }else{
                memberShipAlertDialog()
            }



        }

    }


    fun UsersList() {

        full_users_list_array = ArrayList()
        users_list_array = ArrayList()
        users_list_array.clear()
        full_users_list_array.clear()
        users_ids_array = ArrayList()
        users_ids_array.clear()
        val apiService = ApiInterface.create()
        val call = apiService.composeUsersListApi(user_details[DllSessionManager.USER_ID]!!.toString())
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ComposeUsersListResponse> {
            override fun onResponse(
                call: Call<ComposeUsersListResponse>,
                response: retrofit2.Response<ComposeUsersListResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {


                    for (k in 0 until response!!.body()!!.data!!.size) {

                        full_users_list_array.add(response!!.body()!!.data!!.get(k))
                        users_list_array.add(response!!.body()!!.data!!.get(k).first_name)
                        users_ids_array.add(response!!.body()!!.data!!.get(k).id)

                    }

                    rv_user_list_id.layoutManager = LinearLayoutManager(this@ComposeMessageActivity)
                    rv_user_list_id.itemAnimator = DefaultItemAnimator()
                    //rv_user_list_id.hasFixedSize()
                    userListAdapter = MyRecyclerAdapter(this@ComposeMessageActivity, full_users_list_array)
                   // userListAdapter.setHasStableIds(true)
                    rv_user_list_id.adapter = userListAdapter
                    userListAdapter.notifyDataSetChanged()


                }

                //  }
            }

            override fun onFailure(call: Call<ComposeUsersListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    inner class MyRecyclerAdapter(private val mContext: Context, private val listItems: List<All_List>) :
        RecyclerView.Adapter<MyRecyclerAdapter.MyCustomViewHolder>() {
        private val filterList: MutableList<All_List>?

        init {
            this.filterList = java.util.ArrayList()
            // we copy the original list to the filter list and use it for setting row values
            this.filterList!!.addAll(this.listItems)
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyCustomViewHolder {

            val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.users_list_item, null)
            return MyCustomViewHolder(view)

        }

        override fun onBindViewHolder(customViewHolder: MyCustomViewHolder, position: Int) {

            val All_List = filterList!![position]
            customViewHolder.item_name.text = All_List.first_name
            // customViewHolder.tvPlace.setText(All_List.place);
            //   customViewHolder.imgThumb.setBackgroundResource(All_List.logo);

            customViewHolder.item_name.setOnClickListener {
                user_name_srg = All_List.first_name
                user_id_srg = All_List.id
                Log.e("user_id_srg", user_id_srg + "---" + user_name_srg)
                user_dialog.dismiss()

                et_select_name_id.setText(user_name_srg)

            }


        }

        override fun getItemCount(): Int {
            return filterList?.size ?: 0
        }

        // Do Search...
        fun filter(text: String) {

            // Searching could be complex..so we will dispatch it to a different thread...
            Thread(Runnable {
                // Clear the filter list
                filterList!!.clear()

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(listItems)

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (item in listItems) {
                        if (item.first_name.toLowerCase().contains(text.toLowerCase()) /*|| item.id.toLowerCase().contains(text.toLowerCase())*/) {
                            // Adding Matched items
                            filterList.add(item)
                        }
                    }
                }

                // Set on UI Thread
                (mContext as Activity).runOnUiThread {
                    // Notify the List that the DataSet has changed...
                    notifyDataSetChanged()
                }
            }).start()

        }

        inner class MyCustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var item_name: TextView
            init {
                this.item_name = view.findViewById<View>(R.id.item_name) as TextView
                /*  this.tvPlace = (TextView) view.findViewById(R.id.tvPlace);
                this.imgThumb = (ImageView) view.findViewById(R.id.imgThumb);*/
            }

        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()

        if(from_stg.equals("dashboard")){
            super.onBackPressed()
        }else{
            /*val intent = Intent(this@ComposeMessageActivity, MessagesActivity::class.java)
            startActivity(intent)*/
            val home_intent = Intent(this@ComposeMessageActivity, DashboardActivity::class.java)
            home_intent.putExtra("from_list", "messages_list")
            startActivity(home_intent)
        }


    }

    fun memberShipAlertDialog() {

        success_dialog = Dialog(this@ComposeMessageActivity)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        img_profile_pic_id=success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id=success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text="Alert!"
        tv_full_msg_id=success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text="You need an active membership plan to view the profile. Proceed to membership plans?"
        iv_close=success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility=View.VISIBLE
        tv_ok_btn_id=success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(this@ComposeMessageActivity, MembershipPlanActivity::class.java)
            startActivity(intent)
        }

        Picasso.with(this)
            .load(photbaseurl+user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()

        /*val builder = AlertDialog.Builder(this@ComposeMessageActivity)
        builder.setTitle("Alert!")
            .setMessage("You need an active membership plan to view the profile. Proceed to membership plans?")
            .setCancelable(false)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                    val intent = Intent(this@ComposeMessageActivity, MembershipPlanActivity::class.java)
                    startActivity(intent)
                }
            })
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                }
            })

        val alertDialog = builder.create()
        alertDialog.show()*/
    }
}
