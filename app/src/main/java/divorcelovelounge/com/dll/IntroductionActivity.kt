package divorcelovelounge.com.dll

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.Helper.DllSlideManager
import divorcelovelounge.com.dll.Helper.FadeOutTransformation

class IntroductionActivity : AppCompatActivity() {

    private var viewPager: ViewPager? = null
    private var viewPageAdapter: ViewPageAdapter? = null
    private var ll_dot: LinearLayout? = null
    private var dots: Array<TextView?>? = null
    private var layouts: IntArray? = null
    private var btn_skip: Button? = null
    private var btn_next: ImageView? = null
    private var dllSlideManager: DllSlideManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduction)

        dllSlideManager = DllSlideManager(this)
        if (!dllSlideManager!!.isFirstTimeLaunchOnly) {
            launchHomeScreen()
            finish()
        }

        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }

        viewPager = findViewById(R.id.view_pager)
        ll_dot = findViewById(R.id.ll_dot)
        btn_skip = findViewById(R.id.btn_skip)
        btn_next = findViewById(R.id.btn_next)

        layouts = intArrayOf(
            R.layout.slide_one,
            R.layout.slide_two,
            R.layout.slide_three,
            R.layout.slide_four,
            R.layout.slide_five
        )

        // adding bottom dots//
        addBottomDots(0)

        viewPageAdapter = ViewPageAdapter()
        viewPager!!.adapter = viewPageAdapter
        viewPager!!.setPageTransformer(true, FadeOutTransformation())
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)

        btn_skip!!.visibility = View.GONE
        btn_next!!.visibility = View.GONE
        btn_skip!!.setOnClickListener {
            launchHomeScreen()
        }

        btn_next!!.setOnClickListener {
            val current = getItem(+1)
            if (current < layouts!!.size) {
                viewPager!!.currentItem = current
            } else {
                launchHomeScreen()
            }
        }

    }

    private fun addBottomDots(currentPage: Int) {
        dots = arrayOfNulls(layouts!!.size)

        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)

        ll_dot!!.removeAllViews()
        for (i in dots!!.indices) {
            dots!![i] = TextView(this)
            dots!![i]!!.text = Html.fromHtml("•")
            dots!![i]!!.textSize = 30f
            dots!![i]!!.setTextColor(colorsInactive[currentPage])
            ll_dot!!.addView(dots!![i])
        }

        if (dots!!.isNotEmpty()) {
            dots!![currentPage]!!.setTextColor(colorsActive[currentPage])
            dots!![currentPage]!!.textSize = 50f
        }
    }

    private fun getItem(i: Int): Int {
        return viewPager!!.currentItem + i
    }

    private fun launchHomeScreen() {
        dllSlideManager!!.setFirstTimeLaunch(false)
        startActivity(Intent(this@IntroductionActivity, RegisterLoginActivity::class.java))
        finish()
    }


    private var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            addBottomDots(position)

            if (position == layouts!!.size - 1) {
                btn_next!!.visibility = View.VISIBLE
                btn_skip!!.visibility = View.GONE
            } else {
                btn_next!!.visibility = View.GONE
                btn_skip!!.visibility = View.GONE
            }

        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

        override fun onPageScrollStateChanged(arg0: Int) {}

    }

    // View pager adapter//
    inner class ViewPageAdapter : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            val view = layoutInflater!!.inflate(layouts!![position], container, false)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return layouts!!.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }

}
