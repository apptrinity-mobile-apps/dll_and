package divorcelovelounge.com.dll

import android.annotation.TargetApi
import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.util.Pair
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.PicassoTransformations.BlurTransformation
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class BlockListActivity : AppCompatActivity() {

    private lateinit var rv_block_list: RecyclerView
    private lateinit var tv_block_back: CustomTextView
    private lateinit var tv_block_empty: CustomTextView
    private lateinit var toolbar_block: Toolbar
    private lateinit var sessionManager: DllSessionManager
    private lateinit var progressDialog: Dialog

    private val logTAG = "BlockListActivity"
    private lateinit var userDetails: HashMap<String, String>
    private lateinit var blockedUsersList: ArrayList<All_Matches>
    var plan_expire_status:Long=0


    lateinit var success_dialog:Dialog

    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id:CustomTextView
    lateinit var img_profile_pic_id:ImageView
    lateinit var iv_close:ImageView
    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_block_list)
        title = ""

        sessionManager = DllSessionManager(this@BlockListActivity)
        userDetails = sessionManager.userDetails

        /* custom tool bar */
        toolbar_block = findViewById(R.id.toolbar_block)
        tv_block_back = toolbar_block.findViewById(R.id.tv_block_back)
        setSupportActionBar(toolbar_block)

        initialize()

        progressDialog.show()

        tv_block_back.setOnClickListener {
            super.onBackPressed()
        }

        if(userDetails.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1")){
            val Server_date = userDetails.get(DllSessionManager.MEMBERSHIP_SERVER_DATE)
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val date_server = sdf.parse(Server_date)


            val expir_date = userDetails.get(DllSessionManager.MEMBERSHIP_PLAN_EXPIRY)
            val date_expir = sdf.parse(expir_date)


            val diff =date_expir.getTime() - date_server.getTime()

            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

            plan_expire_status= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        }


        BlockService()

    }

    fun BlockService(){
        blockedUsersList.clear()
        val api = ApiInterface.create()
        val blockedListApi = api.getblockListApi(user_id = userDetails[DllSessionManager.USER_ID]!!.toString())
        blockedListApi.enqueue(object : Callback<BlockListResponse> {
            override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                Log.d(logTAG, "block list response error $t")
                if (progressDialog.isShowing)
                    progressDialog.dismiss()
                Toast.makeText(this@BlockListActivity, "Something went wrong!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<BlockListResponse>, response: Response<BlockListResponse>) {
                Log.d(logTAG, "block list response success: ${response.body()}")

                if (response != null) {

                    if (response.body()!!.status == "1") {

                        // Toast.makeText(this@BlockListActivity, response.body()!!.result, Toast.LENGTH_SHORT).show()
                        val data = response.body()!!.data

                        for (i in 0 until data!!.size) {
                            blockedUsersList.add(data[i])
                        }

                        tv_block_empty.visibility = if (blockedUsersList.size > 0) {
                            View.GONE
                        } else {
                            View.VISIBLE
                        }
                        rv_block_list.visibility = if (blockedUsersList.size > 0) {
                            View.VISIBLE
                        } else {
                            View.GONE
                        }

                        if (data.isEmpty()) {
                            if (progressDialog.isShowing)
                                progressDialog.dismiss()
                            //Toast.makeText(this@BlockListActivity, "List is empty", Toast.LENGTH_SHORT).show()

                        } else {
                            if (progressDialog.isShowing)
                                progressDialog.dismiss()
                            val blockListAdapter = DiscoverListAdapter(
                                blockedUsersList,
                                this@BlockListActivity, userDetails[DllSessionManager.USER_ID]!!.toString()
                            )
                            blockListAdapter.setHasStableIds(true)
                            rv_block_list.adapter = blockListAdapter
                            blockListAdapter.notifyDataSetChanged()
                        }



                    } else {

                        if (progressDialog.isShowing)
                            progressDialog.dismiss()

                        Toast.makeText(this@BlockListActivity, response.body()!!.result, Toast.LENGTH_SHORT).show()
                    }

                } else {

                    if (progressDialog.isShowing)
                        progressDialog.dismiss()

                    Toast.makeText(this@BlockListActivity, "Something went wrong!", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun initialize() {
        rv_block_list = findViewById(R.id.rv_block_list)
        tv_block_empty = findViewById(R.id.tv_block_empty)

        val gridLayoutManager = GridLayoutManager(this@BlockListActivity, 2)
        rv_block_list.layoutManager = gridLayoutManager
        rv_block_list.itemAnimator = DefaultItemAnimator()

        blockedUsersList = ArrayList()

        progressDialog = Dialog(this@BlockListActivity)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    inner class DiscoverListAdapter(val mDataList: java.util.ArrayList<All_Matches>, val context: Context, user_id: String) :
        RecyclerView.Adapter<ViewHolder1>() {

        var partner_match_distance_stg: String? = null
        var partner_match_search_stg: String? = null
        var partner_ethnicity_stg: String? = null
        var partner_religion_stg: String? = null
        var partner_education_stg: String? = null
        var partner_occupation_stg: String? = null
        var partner_smoke_stg: String? = null
        var partner_drink_stg: String? = null
        var photo1_stg: String? = null
        var photo2_stg: String? = null
        var photo3_stg: String? = null
        var photo4_stg: String? = null
        var photo5_stg: String? = null
        var photo6_stg: String? = null
        var photo7_stg: String? = null
        var photo8_stg: String? = null
        var photo9_stg: String? = null
        var photo10_stg: String? = null
        var photo1_approve: String? = null
        var photo2_approve: String? = null
        var photo3_approve: String? = null
        var photo4_approve: String? = null
        var photo5_approve: String? = null
        var photo6_approve: String? = null
        var photo7_approve: String? = null
        var photo8_approve: String? = null
        var photo9_approve: String? = null
        var photo10_approve: String? = null
        var fav_id_stg: String? = null
        var fav_from_stg: String? = null
        var fav_to_stg: String? = null
        var interest_from_stg: String? = null
        var interest_to_stg: String? = null
        var interest_status_stg: String? = null
        private var user_smoke_stg = ""
        private var user_drink_stg = ""
        private var user_denomination_stg = ""
        var height_ft_stg: String? = null
        var height_inc_stg: String? = null
        var height_cm_stg: String? = null
        var city_stg: String? = null
        var dob_stg: String? = null
        var ethnicity_stg: String? = null
        var education_stg: String? = null
        var occupation_stg: String? = null
        var income_stg: String? = null
        var passionate_stg: String? = null
        var leisure_time_stg: String? = null
        var thankful_1_stg: String? = null
        var thankful_2_stg: String? = null
        var thankful_3_stg: String? = null
        var partner_age_min_stg: String? = null
        var partner_age_max_stg: String? = null
        var religion_stg: String? = null
        var block_from_stg: String? = null
        var phone_number_stg: String? = null
        var partner_denomination_stg: String? = null
        var partner_mtongue_name: String? = null
        var mtongue_name: String? = null

        var main_user_id = user_id
        val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.block_item_list, parent, false))

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10

            holder.tv_convo_name.text = mDataList.get(position).first_name

            if (mDataList[position].gender.equals("Woman")) {
                holder.iv_gender.setImageResource(R.drawable.new_female_ic)
            } else {
                holder.iv_gender.setImageResource(R.drawable.new_male_ic)
                holder.iv_gender.setColorFilter(ContextCompat.getColor(context, R.color.gender_color), android.graphics.PorterDuff.Mode.MULTIPLY)
            }


            //Log.e("partner_ma_stg", mDataList.get(position).partner_match_distance)

            var date_only: Date? = null
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            try {
                date_only = sdf.parse(mDataList.get(position).dob)
            } catch (e: ParseException) {
                e.printStackTrace()
            }


            val c = Calendar.getInstance()
            //Set time in milliseconds
            // c.setTimeInMillis(user_date_id.toLong())
            c.time = date_only
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()

            dob.set(mYear, mMonth, mDay)

            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }

            val ageInt = age

            holder.tv_convo_age_id.setText(ageInt.toString())

            if (mDataList.get(position).partner_match_distance !== null) {
                partner_match_distance_stg = mDataList.get(position).partner_match_distance!!
            } else {
                partner_match_distance_stg = "0"
            }
            if (mDataList.get(position).partner_match_search !== null) {
                partner_match_search_stg = mDataList.get(position).partner_match_search!!
            } else {
                partner_match_search_stg = "0"
            }
            if (mDataList.get(position).partner_ethnicity !== null) {
                partner_ethnicity_stg = mDataList.get(position).partner_ethnicity!!
            } else {
                partner_ethnicity_stg = "0"
            }
            if (mDataList.get(position).partner_religion !== null) {
                partner_religion_stg = mDataList.get(position).partner_religion!!
            } else {
                partner_religion_stg = "0"
            }
            if (mDataList.get(position).partner_education !== null) {
                partner_education_stg = mDataList.get(position).partner_education!!
            } else {
                partner_education_stg = "0"
            }
            if (mDataList.get(position).partner_occupation !== null) {
                partner_occupation_stg = mDataList.get(position).partner_occupation!!
            } else {
                partner_occupation_stg = "0"
            }
            if (mDataList.get(position).partner_smoke !== null) {
                partner_smoke_stg = mDataList.get(position).partner_smoke!!
            } else {
                partner_smoke_stg = "0"
            }
            if (mDataList.get(position).partner_drink !== null) {
                partner_drink_stg = mDataList.get(position).partner_drink!!
            } else {
                partner_drink_stg = "0"
            }
            if (mDataList.get(position).photo1 !== null) {
                photo1_stg = mDataList.get(position).photo1!!
            } else {
                photo1_stg = "0"
            }
            if (mDataList.get(position).photo2 !== null) {
                photo2_stg = mDataList.get(position).photo2!!
            } else {
                photo2_stg = "0"
            }
            if (mDataList.get(position).photo3 !== null) {
                photo3_stg = mDataList.get(position).photo3!!
            } else {
                photo3_stg = "0"
            }
            if (mDataList.get(position).photo4 !== null) {
                photo4_stg = mDataList.get(position).photo4!!
            } else {
                photo4_stg = "0"
            }
            if (mDataList.get(position).photo5 !== null) {
                photo5_stg = mDataList.get(position).photo5!!
            } else {
                photo5_stg = "0"
            }
            if (mDataList.get(position).photo6 !== null) {
                photo6_stg = mDataList.get(position).photo6!!
            } else {
                photo6_stg = "0"
            }
            if (mDataList[position].photo7 !== null) {
                photo7_stg = mDataList.get(position).photo7!!
            } else {
                photo7_stg = "0"
            }
            if (mDataList[position].photo8 !== null) {
                photo8_stg = mDataList[position].photo8!!
            } else {
                photo8_stg = "0"
            }
            if (mDataList[position].photo9 !== null) {
                photo9_stg = mDataList[position].photo9!!
            } else {
                photo9_stg = "0"
            }
            if (mDataList[position].photo10 !== null) {
                photo10_stg = mDataList[position].photo10!!
            } else {
                photo10_stg = "0"
            }
            if (mDataList[position].photo1_approve !== null) {
                photo1_approve = mDataList[position].photo1_approve!!
            } else {
                photo1_approve = "0"
            }
            if (mDataList[position].photo2_approve !== null) {
                photo2_approve = mDataList[position].photo2_approve!!
            } else {
                photo2_approve = "0"
            }
            if (mDataList[position].photo3_approve !== null) {
                photo3_approve = mDataList[position].photo3_approve!!
            } else {
                photo3_approve = "0"
            }
            if (mDataList[position].photo4_approve !== null) {
                photo4_approve = mDataList[position].photo4_approve!!
            } else {
                photo4_approve = "0"
            }
            if (mDataList[position].photo5_approve !== null) {
                photo5_approve = mDataList[position].photo5_approve!!
            } else {
                photo5_approve = "0"
            }
            if (mDataList[position].photo6_approve !== null) {
                photo6_approve = mDataList[position].photo6_approve!!
            } else {
                photo6_approve = "0"
            }
            if (mDataList[position].photo7_approve !== null) {
                photo7_approve = mDataList[position].photo7_approve!!
            } else {
                photo7_approve = "0"
            }
            if (mDataList[position].photo8_approve !== null) {
                photo8_approve = mDataList[position].photo8_approve!!
            } else {
                photo8_approve = "0"
            }
            if (mDataList[position].photo9_approve !== null) {
                photo9_approve = mDataList[position].photo9_approve!!
            } else {
                photo9_approve = "0"
            }
            if (mDataList[position].photo10_approve !== null) {
                photo10_approve = mDataList[position].photo10_approve!!
            } else {
                photo10_approve = "0"
            }
            if (mDataList.get(position).fav_id !== null) {
                fav_id_stg = mDataList.get(position).fav_id!!
            } else {
                fav_id_stg = "0"
            }
            if (mDataList.get(position).fav_from !== null) {
                fav_from_stg = mDataList.get(position).fav_from!!
            } else {
                fav_from_stg = "0"
            }
            if (mDataList.get(position).fav_to !== null) {
                fav_to_stg = mDataList.get(position).fav_to!!
            } else {
                fav_to_stg = "0"
            }
            if (mDataList.get(position).interest_from !== null) {
                interest_from_stg = mDataList.get(position).interest_from!!
            } else {
                interest_from_stg = "0"
            }
            if (mDataList.get(position).block_from !== null) {
                block_from_stg = mDataList.get(position).block_from!!
            } else {
                block_from_stg = "0"
            }
            if (mDataList.get(position).interest_to !== null) {
                interest_to_stg = mDataList.get(position).interest_to!!
            } else {
                interest_to_stg = "0"
            }
            if (mDataList.get(position).interest_status !== null) {
                interest_status_stg = mDataList.get(position).interest_status!!
            } else {
                interest_status_stg = "0"
            }
            if (mDataList.get(position).phone_number !== null) {
                phone_number_stg = mDataList.get(position).phone_number
            } else {
                phone_number_stg = "0"
            }
            if (mDataList.get(position).partner_denomination !== null) {
                partner_denomination_stg = mDataList.get(position).partner_denomination
            } else {
                partner_denomination_stg = "0"
            }

            if (mDataList.get(position).partner_mtongue_name == null || mDataList.get(position).partner_mtongue_name == "") {
                partner_mtongue_name = "0"
            } else {
                partner_mtongue_name = mDataList.get(position).partner_mtongue_name!!
            }
            if (mDataList.get(position).mtongue_name == null || mDataList.get(position).mtongue_name == "") {
                mtongue_name = "0"
            } else {
                mtongue_name = mDataList.get(position).mtongue_name!!
            }

            /*if (interest_from_stg == main_user_id) {
                holder.btn_interest_id.isChecked = true
                holder.btn_interest_id.isEnabled = false
            } else {
                holder.btn_interest_id.isChecked = false
            }*/

            if (userDetails.get(DllSessionManager.MEMBERSHIP_STATUS) == "1") {
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    }
                }
            }else{
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .transform(BlurTransformation(context))
                            .into(holder.iv_favorite_user_image)
                    }
                }
            }



            holder.iv_favorite_user_image.setOnClickListener {

                 if (userDetails.get(DllSessionManager.MEMBERSHIP_STATUS).equals("2")) {

                     memberShipAlertDialog()


                 }
                 else if (userDetails.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1") && plan_expire_status >= 0) {

                     if (mDataList.get(position).religion == null || mDataList.get(position).religion == "") {
                         religion_stg = "0"
                     } else {
                         religion_stg = mDataList.get(position).religion
                     }
                     if (mDataList.get(position).partner_match_distance !== null) {
                         partner_match_distance_stg = mDataList.get(position).partner_match_distance!!
                     } else {
                         partner_match_distance_stg = "0"

                     }
                     if (mDataList.get(position).partner_match_search !== null) {
                         partner_match_search_stg = mDataList.get(position).partner_match_search!!
                     } else {
                         partner_match_search_stg = "0"
                     }
                     if (mDataList.get(position).partner_ethnicity !== null) {
                         partner_ethnicity_stg = mDataList.get(position).partner_ethnicity!!
                     } else {
                         partner_ethnicity_stg = "0"
                     }
                     if (mDataList.get(position).partner_religion !== null) {
                         partner_religion_stg = mDataList.get(position).partner_religion!!
                     } else {
                         partner_religion_stg = "0"
                     }
                     if (mDataList.get(position).partner_education !== null) {
                         partner_education_stg = mDataList.get(position).partner_education!!
                     } else {
                         partner_education_stg = "0"
                     }
                     if (mDataList.get(position).partner_occupation !== null) {
                         partner_occupation_stg = mDataList.get(position).partner_occupation!!
                     } else {
                         partner_occupation_stg = "0"
                     }
                     if (mDataList.get(position).partner_smoke !== null) {
                         partner_smoke_stg = mDataList.get(position).partner_smoke!!
                     } else {
                         partner_smoke_stg = "0"
                     }
                     if (mDataList.get(position).partner_drink !== null) {
                         partner_drink_stg = mDataList.get(position).partner_drink!!
                     } else {
                         partner_drink_stg = "0"
                     }
                     if (mDataList.get(position).photo1 !== null) {
                         photo1_stg = mDataList.get(position).photo1!!
                     } else {
                         photo1_stg = "0"
                     }
                     if (mDataList.get(position).photo2 !== null) {
                         photo2_stg = mDataList.get(position).photo2!!
                     } else {
                         photo2_stg = "0"
                     }
                     if (mDataList.get(position).photo3 !== null) {
                         photo3_stg = mDataList.get(position).photo3!!
                     } else {
                         photo3_stg = "0"
                     }
                     if (mDataList.get(position).photo4 !== null) {
                         photo4_stg = mDataList.get(position).photo4!!
                     } else {
                         photo4_stg = "0"
                     }
                     if (mDataList.get(position).photo5 !== null) {
                         photo5_stg = mDataList.get(position).photo5!!
                     } else {
                         photo5_stg = "0"
                     }
                     if (mDataList.get(position).photo6 !== null) {
                         photo6_stg = mDataList.get(position).photo6!!
                     } else {
                         photo6_stg = "0"
                     }
                     if (mDataList.get(position).photo7 !== null) {
                         photo7_stg = mDataList.get(position).photo7!!
                     } else {
                         photo7_stg = "0"
                     }
                     if (mDataList.get(position).photo8 !== null) {
                         photo8_stg = mDataList.get(position).photo8!!
                     } else {
                         photo8_stg = "0"
                     }
                     if (mDataList.get(position).photo9 !== null) {
                         photo9_stg = mDataList.get(position).photo9!!
                     } else {
                         photo9_stg = "0"
                     }
                     if (mDataList.get(position).photo10 !== null) {
                         photo10_stg = mDataList.get(position).photo10!!
                     } else {
                         photo10_stg = "0"
                     }
                     if (mDataList[position].photo1_approve !== null) {
                         photo1_approve = mDataList[position].photo1_approve!!
                     } else {
                         photo1_approve = "0"
                     }
                     if (mDataList[position].photo2_approve !== null) {
                         photo2_approve = mDataList[position].photo2_approve!!
                     } else {
                         photo2_approve = "0"
                     }
                     if (mDataList[position].photo3_approve !== null) {
                         photo3_approve = mDataList[position].photo3_approve!!
                     } else {
                         photo3_approve = "0"
                     }
                     if (mDataList[position].photo4_approve !== null) {
                         photo4_approve = mDataList[position].photo4_approve!!
                     } else {
                         photo4_approve = "0"
                     }
                     if (mDataList[position].photo5_approve !== null) {
                         photo5_approve = mDataList[position].photo5_approve!!
                     } else {
                         photo5_approve = "0"
                     }
                     if (mDataList[position].photo6_approve !== null) {
                         photo6_approve = mDataList[position].photo6_approve!!
                     } else {
                         photo6_approve = "0"
                     }
                     if (mDataList[position].photo7_approve !== null) {
                         photo7_approve = mDataList[position].photo7_approve!!
                     } else {
                         photo7_approve = "0"
                     }
                     if (mDataList[position].photo8_approve !== null) {
                         photo8_approve = mDataList[position].photo8_approve!!
                     } else {
                         photo8_approve = "0"
                     }
                     if (mDataList[position].photo9_approve !== null) {
                         photo9_approve = mDataList[position].photo9_approve!!
                     } else {
                         photo9_approve = "0"
                     }
                     if (mDataList[position].photo10_approve !== null) {
                         photo10_approve = mDataList[position].photo10_approve!!
                     } else {
                         photo10_approve = "0"
                     }
                     if (mDataList.get(position).fav_id !== null) {
                         fav_id_stg = mDataList.get(position).fav_id!!
                     } else {
                         fav_id_stg = "0"
                     }
                     if (mDataList.get(position).fav_from !== null) {
                         fav_from_stg = mDataList.get(position).fav_from!!
                     } else {
                         fav_from_stg = "0"
                     }
                     if (mDataList.get(position).fav_to !== null) {
                         fav_to_stg = mDataList.get(position).fav_to!!
                     } else {
                         fav_to_stg = "0"
                     }
                     if (mDataList.get(position).interest_from !== null) {
                         interest_from_stg = mDataList.get(position).interest_from!!
                     } else {
                         interest_from_stg = "0"
                     }
                     if (mDataList.get(position).block_from !== null) {
                         block_from_stg = mDataList.get(position).block_from!!
                     } else {
                         block_from_stg = "0"
                     }
                     if (mDataList.get(position).interest_to !== null) {
                         interest_to_stg = mDataList.get(position).interest_to!!
                     } else {
                         interest_to_stg = "0"
                     }
                     if (mDataList.get(position).interest_status !== null) {
                         interest_status_stg = mDataList.get(position).interest_status!!
                     } else {
                         interest_status_stg = "0"
                     }
                     if (mDataList.get(position).height_ft !== null) {
                         height_ft_stg = mDataList.get(position).height_ft!!
                     } else {
                         height_ft_stg = "0"
                     }
                     if (mDataList.get(position).height_inc !== null) {
                         height_inc_stg = mDataList.get(position).height_inc!!
                     } else {
                         height_inc_stg = "0"
                     }
                     if (mDataList.get(position).height_cms !== null) {
                         height_cm_stg = mDataList.get(position).height_cms!!
                     } else {
                         height_cm_stg = "0"
                     }
                     if (mDataList.get(position).smoke == null || mDataList.get(position).smoke == "") {
                         user_smoke_stg = "0"
                     } else {
                         user_smoke_stg = mDataList.get(position).smoke!!
                     }
                     if (mDataList.get(position).drink == null || mDataList.get(position).drink == "") {
                         user_drink_stg = "0"
                     } else {
                         user_drink_stg = mDataList.get(position).drink!!
                     }
                     if (mDataList.get(position).denomination == null || mDataList.get(position).denomination == "") {
                         user_denomination_stg = "0"
                     } else {
                         user_denomination_stg = mDataList.get(position).denomination!!
                     }
                     if (mDataList.get(position).height_ft !== null) {
                         height_ft_stg = mDataList.get(position).height_ft!!
                     } else {
                         height_ft_stg = "0"
                     }
                     if (mDataList.get(position).height_inc !== null) {
                         height_inc_stg = mDataList.get(position).height_inc!!
                     } else {
                         height_inc_stg = "0"
                     }
                     if (mDataList.get(position).height_cms !== null) {
                         height_cm_stg = mDataList.get(position).height_cms!!
                     } else {
                         height_cm_stg = "0"
                     }
                     if (mDataList.get(position).city !== null) {
                         city_stg = mDataList.get(position).city
                     } else {
                         city_stg = "0"
                     }
                     if (mDataList.get(position).dob !== null) {
                         dob_stg = mDataList.get(position).dob
                     } else {
                         dob_stg = "0"
                     }
                     if (mDataList.get(position).ethnicity !== null) {
                         ethnicity_stg = mDataList.get(position).ethnicity
                     } else {
                         ethnicity_stg = "0"
                     }
                     if (mDataList.get(position).education !== null) {
                         education_stg = mDataList.get(position).education
                     } else {
                         education_stg = "0"
                     }
                     if (mDataList.get(position).occupation !== null) {
                         occupation_stg = mDataList.get(position).occupation
                     } else {
                         occupation_stg = "0"
                     }
                     if (mDataList.get(position).income !== null) {
                         income_stg = mDataList.get(position).income
                     } else {
                         income_stg = "0"
                     }
                     if (mDataList.get(position).passionate !== null) {
                         passionate_stg = mDataList.get(position).passionate
                     } else {
                         passionate_stg = "0"
                     }
                     if (mDataList.get(position).leisure_time !== null) {
                         leisure_time_stg = mDataList.get(position).leisure_time
                     } else {
                         leisure_time_stg = "0"
                     }
                     if (mDataList.get(position).thankful_1 !== null) {
                         thankful_1_stg = mDataList.get(position).thankful_1
                     } else {
                         thankful_1_stg = "0"
                     }
                     if (mDataList.get(position).thankful_2 !== null) {
                         thankful_2_stg = mDataList.get(position).thankful_2
                     } else {
                         thankful_2_stg = "0"
                     }
                     if (mDataList.get(position).thankful_3 !== null) {
                         thankful_3_stg = mDataList.get(position).thankful_3
                     } else {
                         thankful_3_stg = "0"
                     }
                     if (mDataList.get(position).partner_age_min !== null) {
                         partner_age_min_stg = mDataList.get(position).partner_age_min
                     } else {
                         partner_age_min_stg = "0"
                     }
                     if (mDataList.get(position).partner_age_max !== null) {
                         partner_age_max_stg = mDataList.get(position).partner_age_max
                     } else {
                         partner_age_max_stg = "0"
                     }
                     if (mDataList.get(position).phone_number !== null) {
                         phone_number_stg = mDataList.get(position).phone_number
                     } else {
                         phone_number_stg = "0"
                     }
                     if (mDataList.get(position).partner_denomination !== null) {
                         partner_denomination_stg = mDataList.get(position).partner_denomination
                     } else {
                         partner_denomination_stg = "0"
                     }

                     if (mDataList.get(position).partner_mtongue_name == null || mDataList.get(position).partner_mtongue_name == "") {
                         partner_mtongue_name = "0"
                     } else {
                         partner_mtongue_name = mDataList.get(position).partner_mtongue_name!!
                     }
                     if (mDataList.get(position).mtongue_name == null || mDataList.get(position).mtongue_name == "") {
                         mtongue_name = "0"
                     } else {
                         mtongue_name = mDataList.get(position).mtongue_name!!
                     }



                     val internt=Intent(context, FullProfile::class.java)
                     val pairs = arrayOfNulls<Pair<View, String>>(4)
                     pairs[0] = Pair<View, String>(holder.iv_favorite_user_image, "mainimageTreansition")
                     pairs[1] = Pair<View, String>(holder.tv_convo_name, "nametransition")
                     pairs[2] = Pair<View, String>(holder.iv_gender, "gendertransition")
                     pairs[3] = Pair<View, String>(holder.tv_convo_age_id, "agetransition")

                     val aOptions = ActivityOptions.makeSceneTransitionAnimation(context as Activity, *pairs)


                     internt.putExtra("from_screen","blocked")
                     internt.putExtra("id",mDataList.get(position).id)
                     internt.putExtra("photo1",photo1_stg)
                     internt.putExtra("name_stg",mDataList.get(position).first_name)
                     internt.putExtra("gender_stg",mDataList.get(position).gender)
                     internt.putExtra("age_stg",dob_stg)
                     internt.putExtra("location_stg",city_stg)
                     context.startActivity(internt,aOptions.toBundle())
                 }
             }

            holder.btn_unblock_id.setOnClickListener {



                success_dialog = Dialog(this@BlockListActivity)
                success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                success_dialog.setContentView(R.layout.success_dialog_layout)
                val success_window = success_dialog.window
                success_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                img_profile_pic_id=success_dialog.findViewById(R.id.img_profile_pic_id)
                tv_msg_title_id=success_dialog.findViewById(R.id.tv_msg_title_id)
                tv_msg_title_id.text="Alert!"
                tv_full_msg_id=success_dialog.findViewById(R.id.tv_full_msg_id)
                tv_full_msg_id.text="Are you sure you want to Unblock "+mDataList.get(position).first_name+"?"
                iv_close=success_dialog.findViewById(R.id.iv_close)
                iv_close.visibility=View.VISIBLE
                tv_ok_btn_id=success_dialog.findViewById(R.id.tv_ok_btn_id)
                tv_ok_btn_id.setOnClickListener {
                    success_dialog.dismiss()
                    val api = ApiInterface.create()
                    val blockUserApi = api.blockUserApi(userDetails[DllSessionManager.USER_ID]!!, mDataList.get(position).id!!)
                    blockUserApi.enqueue(object : Callback<BlockUserResponse> {
                        override fun onFailure(call: Call<BlockUserResponse>, t: Throwable) {
                            Toast.makeText(
                                this@BlockListActivity,
                                "Something went wrong!",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }

                        override fun onResponse(
                            call: Call<BlockUserResponse>,
                            response: Response<BlockUserResponse>
                        ) {
                            Log.d("FullProfileActivity", response.body()!!.result)
                            if (response != null) {

                                if (response.body() != null) {

                                    if (response.body()!!.status == "1") {
                                        Toast.makeText(this@BlockListActivity, "UnBlocked", Toast.LENGTH_SHORT)
                                            .show()
                                        BlockService()
                                    } else {
                                        Toast.makeText(
                                            this@BlockListActivity,
                                            "Something went wrong",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    }

                                } else {
                                    Toast.makeText(
                                        this@BlockListActivity,
                                        "Please try again!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                            } else {
                                Toast.makeText(
                                    this@BlockListActivity,
                                    "Something went wrong!",
                                    Toast.LENGTH_SHORT
                                ).show()

                            }

                        }

                    })
                }

                if (mDataList[position].photo1_approve !== null) {
                    photo1_approve = mDataList[position].photo1_approve!!
                } else {
                    photo1_approve = "0"
                }

                if (mDataList.get(position).photo1 == null || mDataList.get(position).photo1 == "") {
                    photo1_stg = "0"
                } else {
                    photo1_stg = mDataList.get(position).photo1
                }


                if (userDetails[DllSessionManager.MEMBERSHIP_STATUS]  == "1") {
                    if (photo1_stg.equals("0")) {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_png)
                            .into(img_profile_pic_id)
                    } else {
                        if (photo1_approve == "UNAPPROVED") {
                            Picasso.with(context)
                                .load(R.drawable.ic_man_png)
                                .into(img_profile_pic_id)
                        } else {
                            Picasso.with(context)
                                .load(photobaseurl + photo1_stg)
                                .error(R.drawable.ic_man_png)
                                .into(img_profile_pic_id)
                        }

                    }
                } else {
                    if (photo1_stg.equals("0")) {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_png)
                            // .transform(BlurTransformation(context))
                            .into(img_profile_pic_id)
                    } else {
                        if (photo1_approve == "UNAPPROVED") {
                            Picasso.with(context)
                                .load(R.drawable.ic_man_png)
                                //.transform(BlurTransformation(context, 25))
                                .into(img_profile_pic_id)
                        } else {
                            Picasso.with(context)
                                .load(photobaseurl + photo1_stg)
                                .error(R.drawable.ic_man_png)
                                .transform(BlurTransformation(context))
                                .into(img_profile_pic_id)
                        }

                    }
                }

                iv_close.setOnClickListener {
                    success_dialog.dismiss()
                }

                success_dialog.show()





                /*val builder = AlertDialog.Builder(this@BlockListActivity)

                    builder.setTitle("Alert!")
                        .setMessage("Are you sure you want to Unblock "+mDataList.get(position).first_name+"?")
                        .setCancelable(false)
                        .setNegativeButton("No", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, id: Int) {
                                dialog!!.dismiss()
                            }

                        })
                        .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, id: Int) {



                            }

                        })

                val alertDialog = builder.create()
                alertDialog.show()*/

            }

        }

        override fun getItemCount(): Int {
            return mDataList.size
        }

    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {

        val tv_convo_name = view.findViewById<CustomTextView>(R.id.tv_convo_name)
        val iv_favorite_user_image = view.findViewById<ImageView>(R.id.iv_favorite_user_image)
        val tv_convo_age_id = view.findViewById<CustomTextView>(R.id.tv_convo_age_id)
        val btn_unblock_id = view.findViewById<ImageView>(R.id.btn_unblock_id)
        val iv_gender = view.findViewById<ImageView>(R.id.iv_gender)

    }

    fun memberShipAlertDialog() {

        success_dialog = Dialog(this@BlockListActivity)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        img_profile_pic_id=success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id=success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text="Alert!"
        tv_full_msg_id=success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text="You need an active membership plan to view the profile. Proceed to membership plans?"
        iv_close=success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility=View.VISIBLE
        tv_ok_btn_id=success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(this@BlockListActivity, MembershipPlanActivity::class.java)
            startActivity(intent)
        }

        Picasso.with(this)
            .load(photbaseurl+userDetails.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()

        /*val builder = AlertDialog.Builder(this!!)
        builder.setTitle("Alert!")
            .setMessage("You need an active membership plan to view the profile. Proceed to membership plans?")
            .setCancelable(false)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                    val intent = Intent(this@BlockListActivity, MembershipPlanActivity::class.java)
                    startActivity(intent)
                }
            })
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                }
            })

        val alertDialog = builder.create()
        alertDialog.show()*/
    }
}
