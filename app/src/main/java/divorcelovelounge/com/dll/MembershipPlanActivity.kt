package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.ViewPagerAdapters.MembershipPlanAdapterKotlin
import divorcelovelounge.com.dll.ViewPagerAdapters.ShadowTransformer
import retrofit2.Call
import retrofit2.Callback
import java.util.*
import android.view.MotionEvent
import android.support.v7.widget.RecyclerView
import android.text.method.Touch.onTouchEvent
import android.view.GestureDetector
import android.widget.LinearLayout
import android.widget.ScrollView
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


class MembershipPlanActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    lateinit var collapsing_toolbar: CollapsingToolbarLayout
    lateinit var iv_toolbar_back_id: ImageView

    var user_id = ""
    lateinit var membershiplistdata_array: ArrayList<CardItemPlan>
    lateinit var mCardShadowTransformer: ShadowTransformer


    lateinit var tv_planname: CustomTextView
    lateinit var tv_duration: CustomTextView
    lateinit var tv_last_payment: CustomTextView
    lateinit var ll_my_plan_id: LinearLayout
    lateinit var sv_plan_id: ScrollView
    lateinit var tv_upgrade_plan_id: CustomTextViewBold

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var mCardAdapter: MembershipPlanAdapterKotlin
    lateinit var mViewPager: ViewPager

    private var back_chat_toolbar: ImageView? = null

    var plan_expire_days:Long=0

    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>
    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_membershipplan)
        /*collapsing_toolbar = findViewById(R.id.collapsing_toolbar)
        iv_toolbar_back_id = findViewById(R.id.iv_toolbar_back_id)
*/
        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        /*  tv_user_name_id = findViewById(R.id.tv_user_name_id)

          iv_toolbar_back_id.setOnClickListener { super.onBackPressed() }*/
        /*val scrollView = findViewById(R.id.nest_scrollview) as NestedScrollView
        scrollView.isFillViewport = true*/

        ll_my_plan_id = findViewById(R.id.ll_my_plan_id)
        tv_planname = findViewById(R.id.tv_planname)
        tv_duration = findViewById(R.id.tv_duration)
        tv_last_payment = findViewById(R.id.tv_last_payment)
        sv_plan_id = findViewById(R.id.sv_plan_id)
        tv_upgrade_plan_id = findViewById(R.id.tv_upgrade_plan_id)

        ll_my_plan_id.visibility = View.GONE

        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()

        location_details_list = dllSessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Membership")

        mViewPager = findViewById(R.id.viewpager_membership)
        mCardAdapter = MembershipPlanAdapterKotlin(this@MembershipPlanActivity)
        membershipPlanApi()

        back_chat_toolbar = findViewById<View>(R.id.back_chat_toolbar) as ImageView
        back_chat_toolbar!!.setOnClickListener {
            onBackPressed()
        }


        MyMemberShipPlan()

        tv_upgrade_plan_id.setOnClickListener {
            sv_plan_id.fullScroll(View.FOCUS_DOWN)
        }

//scroll.fullScroll(View.FOCUS_DOWN);

    }

    private fun MyMemberShipPlan() {


        val apiService = ApiInterface.create()
        val call = apiService.membershipmyplan(user_details.get(DllSessionManager.USER_ID).toString())

        call.enqueue(object : Callback<MemberMyPlanResponse> {
            override fun onResponse(
                call: Call<MemberMyPlanResponse>,
                response: retrofit2.Response<MemberMyPlanResponse>?
            ) {
                Log.d("REQUEST", response!!.body()!!.status + "" + response!!.body()!!.plandetails)
                if (response!!.body()!!.status.equals("1")) {
                    Log.d("REQUEST_lllll", response!!.body()!!.status + "" + response!!.body()!!.plandetails!!.user_id)

                    tv_planname.setText(response!!.body()!!.plandetails!!.package_type)

                    tv_duration.setText(response!!.body()!!.plandetails!!.plan_duration + " Days")
                    tv_last_payment.setText(editDateFormat(response!!.body()!!.plandetails!!.plan_start_date))


                        val Server_date = response!!.body()!!.plandetails!!.server_date
                        val sdf = SimpleDateFormat("yyyy-MM-dd")
                        val date_server = sdf.parse(Server_date)


                        val expir_date =  response!!.body()!!.plandetails!!.plan_expiry
                        val date_expir = sdf.parse(expir_date)


                        val diff =date_expir.getTime() - date_server.getTime()

                        System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

                        plan_expire_days= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)

                    if(plan_expire_days>=0){

                    }else{
                        tv_duration.setText("Plan Expired")
                    }


                    ll_my_plan_id.visibility = View.VISIBLE
                } else {
                    ll_my_plan_id.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<MemberMyPlanResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun membershipPlanApi() {

        membershiplistdata_array = ArrayList()
        membershiplistdata_array.clear()
        val apiService = ApiInterface.create()
        val call = apiService.getMembershipListApi()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<MembershipListResponse> {
            override fun onResponse(
                call: Call<MembershipListResponse>,
                response: retrofit2.Response<MembershipListResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                    val plan_data = response.body()!!.data
                    Toast.makeText(
                        this@MembershipPlanActivity,
                        response.body()!!.result.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e("full_lll_data", plan_data!!.size.toString())
                    for (k in 0 until plan_data!!.size) {
                        Log.e("full_data", plan_data.get(k).plan_amount)
                        membershiplistdata_array.add(plan_data[k])

                    }

                    Log.e("full_lll_data", plan_data.toString() + "----" + membershiplistdata_array.size)

                }

                for (k in 0 until membershiplistdata_array.size) {
                    mCardAdapter.addCardItem(
                        CardItemPlan(
                            /* membershiplistdata_array.get(0).id,
                             membershiplistdata_array.get(0).plan_type,
                             membershiplistdata_array.get(0).plan_amount,
                             membershiplistdata_array.get(0).discount_amount,
                             membershiplistdata_array.get(0).plan_amount_type,
                             membershiplistdata_array.get(0).plan_duration,
                             membershiplistdata_array.get(0).plan_contacts,
                             membershiplistdata_array.get(0).profile,
                             membershiplistdata_array.get(0).favoritedme,
                             membershiplistdata_array.get(0).viewedme,
                             membershiplistdata_array.get(0).plan_msg,
                             membershiplistdata_array.get(0).interest,
                             membershiplistdata_array.get(0).chat,
                             membershiplistdata_array.get(0).plan_offers,
                             membershiplistdata_array.get(0).package_type,
                             membershiplistdata_array.get(0).status,
                             membershiplistdata_array.get(0).created_date,
                             membershiplistdata_array.get(0).durations*/

                            membershiplistdata_array.get(k).id,
                            membershiplistdata_array.get(k).plan_type,
                            membershiplistdata_array.get(k).plan_amount,
                            membershiplistdata_array.get(k).discount_amount,
                            membershiplistdata_array.get(k).plan_amount_type,
                            membershiplistdata_array.get(k).plan_duration,
                            membershiplistdata_array.get(k).plan_contacts,
                            membershiplistdata_array.get(k).profile,
                            membershiplistdata_array.get(k).favoritedme,
                            membershiplistdata_array.get(k).viewedme,
                            membershiplistdata_array.get(k).plan_msg,
                            membershiplistdata_array.get(k).interest,
                            membershiplistdata_array.get(k).chat,
                            membershiplistdata_array.get(k).plan_offers,
                            membershiplistdata_array.get(k).package_type,
                            membershiplistdata_array.get(k).status,
                            membershiplistdata_array.get(k).created_date,
                            membershiplistdata_array.get(k).durations

                        )
                    )
                }


                mCardShadowTransformer = ShadowTransformer(mViewPager, mCardAdapter)
                mViewPager.adapter = mCardAdapter
                mViewPager.setPageTransformer(false, mCardShadowTransformer)
                mViewPager.offscreenPageLimit = 3
                // mCardShadowTransformer.enableScaling(true)


                //  }
            }

            override fun onFailure(call: Call<MembershipListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }
    /*inner class RecyclerItemClickListener(context: Context, private val mListener: OnItemClickListener?) :
        RecyclerView.OnItemTouchListener {

        internal var mGestureDetector: GestureDetector



        init {
            mGestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }
            })
        }

        override fun onInterceptTouchEvent(view: RecyclerView, e: MotionEvent): Boolean {
            val childView = view.findChildViewUnder(e.x, e.y)
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView))
                return true
            }
            return false
        }

        override fun onTouchEvent(view: RecyclerView, motionEvent: MotionEvent) {}

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }*/

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,user_id)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD", PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@MembershipPlanActivity);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@MembershipPlanActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString());
                                sliderLayout.addSlider(textSliderView);
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@MembershipPlanActivity);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }
}
