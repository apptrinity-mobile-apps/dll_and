package divorcelovelounge.com.dll.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ListView
import android.widget.Toast
import divorcelovelounge.com.dll.Adapters.ConversationsAdapter
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ConversationsFragment : Fragment() {

    private var logTag = "ConversationsFragment"
    private var rootView: View? = null
    private var listView: ListView? = null
    private var tv_nodata: CustomTextView? = null
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>
    lateinit var matches_array_list: ArrayList<CardItem>
    lateinit var chatlistadapter: ConversationsAdapter

    lateinit var lastmsg_arraylist: ArrayList<TalkJsData>
    lateinit var lastmsg_senser_id: HashMap<String, String>
    lateinit var total_arraylist: ArrayList<String>

    @SuppressLint("NewApi")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        /*if (rootView != null) {
            val parent = rootView!!.parent as ViewGroup
            parent.removeView(rootView)
        }*/
        try {
            rootView = inflater.inflate(R.layout.activity_conversation_profile, container, false)
        } catch (e: InflateException) {
        }
        dllSessionManager = DllSessionManager(this!!.context!!)

        user_details = dllSessionManager.userDetails

        initialize(rootView)




        listView!!.setOnItemClickListener { parent, view, position, id ->
            Log.d(logTag, "position :" + position.toString())
        }

        return rootView

    }

    private fun initialize(rootView: View?) {
        listView = rootView!!.findViewById(R.id.lv_conversations)
        tv_nodata = rootView!!.findViewById(R.id.tv_nodata)
        listView!!.isFastScrollEnabled = true
        matches_array_list = ArrayList()
        matches_array_list.clear()
        total_arraylist = ArrayList()
        total_arraylist.clear()

        lastmsg_senser_id = HashMap()
        lastmsg_senser_id.clear()
        /*API Call*/
        Log.d("REQUEST_user_id", user_details.get(DllSessionManager.USER_ID).toString())

        val apiService = ApiInterface.create()
        val call = apiService.getChatListApi(user_details.get(DllSessionManager.USER_ID).toString())

        call.enqueue(object : Callback<ChatListResponse> {
            override fun onResponse(
                call: Call<ChatListResponse>,
                response: retrofit2.Response<ChatListResponse>?
            ) {

                try {


                    if (response != null) {

                        Log.d("getChatListApiRES", "" + response)
                        if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                "success"
                            )
                        ) {

                            val user_data = response.body()!!.data
                            // val favourites_list: ArrayList<All_Matches>? = user_data!!.my_favourites

                            for (i in 0 until user_data!!.size) {
                                matches_array_list.add(user_data.get(i))

                                total_arraylist.add(user_data.get(i).id)

                                lastmsg_senser_id.put(user_data.get(i).id,
                                    "0"
                                )
                            }
                            Log.w(
                                "getChatList_array_list",
                                matches_array_list.toString() + "---" + matches_array_list.size
                            )


                        } else if (response!!.body()!!.status.equals("2")) {
                            tv_nodata!!.visibility = View.VISIBLE
                            listView!!.visibility = View.GONE

                        }
                        if (response.body()!!.status.equals("status")) {
                            Toast.makeText(context, response.body()!!.result, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }


                    /* chatlistadapter = ConversationsAdapter(activity!!, matches_array_list)
                     listView!!.adapter = chatlistadapter*/
                    getTalkJsCount(user_details.get(DllSessionManager.USER_ID).toString())

                } catch (e: Exception) {
                    Log.w("CHATLISTEXCEPTION", e.toString())
                }
                /*mViewPager.adapter = mCardAdapter
                mViewPager.setPageTransformer(false, mCardShadowTransformer)
                mViewPager.offscreenPageLimit = 3

                mCardShadowTransformer.enableScaling(true)*/
            }

            override fun onFailure(call: Call<ChatListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })


    }

    private fun getTalkJsCount(user_id: String) {

        lastmsg_arraylist = ArrayList()


        lastmsg_arraylist.clear()

        val getCountsApi = ApiTalkjsInterface.create()
        val religionCall = getCountsApi.getTalkJsApi(user_id)
        religionCall.enqueue(object : Callback<TalkJsDataResponse> {
            override fun onFailure(call: Call<TalkJsDataResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<TalkJsDataResponse>, response: Response<TalkJsDataResponse>
            ) {
                try {
                    Log.d(
                        "getTalkJs", response.body()!!.data!!.size.toString()
                    )
                    if (response.body()!!.data!!.size == 0) {
                        Log.d(
                            "getTalkJs_1", response.body().toString()
                        )

                    } else {
                        val data_last_msg = response.body()!!.data!!

                        Log.d(
                            "getTalkJs_ll", response.body().toString()
                        )

                        for (i in 0 until data_last_msg.size) {

                            lastmsg_arraylist.add(data_last_msg[i])


                        }
                        for (k in 0 until lastmsg_arraylist.size) {

                            Log.d(
                                "contains_else", lastmsg_senser_id.size.toString()
                            )
                            if(total_arraylist.contains(lastmsg_arraylist.get(k).lastMessage!!.senderId)){
                                Log.d(
                                    "contains", lastmsg_arraylist.get(k).lastMessage!!.senderId
                                )
                                lastmsg_senser_id.put(lastmsg_arraylist.get(k).lastMessage!!.senderId,
                                    lastmsg_arraylist.get(k).lastMessage!!.senderId
                                )
                            }/*else{
                                Log.d(
                                    "contains_else", lastmsg_arraylist.get(k).lastMessage!!.senderId
                                )
                                lastmsg_senser_id.put(lastmsg_arraylist.get(k).lastMessage!!.senderId,
                                    "0"
                                )
                            }*/

                        }



                        Log.d(
                            "getTalkJs_ll_ll", lastmsg_senser_id.size.toString()
                        )


                    }

                    chatlistadapter =
                        ConversationsAdapter(activity!!, matches_array_list, lastmsg_senser_id)
                    listView!!.adapter = chatlistadapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}