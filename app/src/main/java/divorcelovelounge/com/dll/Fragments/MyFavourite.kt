package divorcelovelounge.com.dll.Fragments

import android.annotation.TargetApi
import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.All_Matches
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import divorcelovelounge.com.dll.ApiPojo.myFavouriteResponse
import divorcelovelounge.com.dll.FullProfile
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.MembershipPlanActivity
import divorcelovelounge.com.dll.PicassoTransformations.BlurTransformation
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class MyFavourite : Fragment() {
    lateinit var rv_favourite_list: RecyclerView


    lateinit var favourite_me_matches: ArrayList<All_Matches>

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    var plan_expire_status: Long = 0


    lateinit var success_dialog: Dialog
    lateinit var ll_no_data: LinearLayout

    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id: CustomTextView
    lateinit var img_profile_pic_id: ImageView
    lateinit var iv_close: ImageView
    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    var user_id = ""
    var details_adapter: MFDiscoverListAdapter? = null
    lateinit var swipe_refresh_layout: SwipeRefreshLayout
    private lateinit var mRunnable: Runnable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            /* mPosition = getArguments().getInt("position");*/
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.favourite_me_layout, container, false)
        dllSessionManager = DllSessionManager(this!!.context!!)
        user_details = dllSessionManager.userDetails

        user_id = user_details.get(DllSessionManager.USER_ID).toString()
        initialize(rootView)


        return rootView
    }

    companion object {

        fun newInstance(position: Int): MyFavourite {
            val fragment = MyFavourite()
            val args = Bundle()
            args.putInt("position", position)
            fragment.arguments = args

            return fragment
        }
    }

    private fun initialize(rootView: View?) {


        if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1")) {
            val Server_date = user_details.get(DllSessionManager.MEMBERSHIP_SERVER_DATE)
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val date_server = sdf.parse(Server_date)


            val expir_date = user_details.get(DllSessionManager.MEMBERSHIP_PLAN_EXPIRY)
            val date_expir = sdf.parse(expir_date)


            val diff = date_expir.getTime() - date_server.getTime()

            System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

            plan_expire_status = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        }

        swipe_refresh_layout = rootView!!.findViewById(R.id.swipe_refresh_layout)
        ll_no_data = rootView!!.findViewById(R.id.ll_no_data)
        rv_favourite_list = rootView!!.findViewById(R.id.rv_favourite_list)
        rv_favourite_list.isNestedScrollingEnabled = false


        swipe_refresh_layout.setOnRefreshListener {
            // Initialize a new Runnable
            // Update the text view text with a random number
            FavoriteAPI()

            swipe_refresh_layout.isRefreshing = false


        }



        FavoriteAPI()
/**/
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()


    }

    fun refresh() {
        FavoriteAPI()

        Log.e("myfavorite_refresh", "-----")
    }

    fun FavoriteAPI() {
        favourite_me_matches = ArrayList()
        favourite_me_matches.clear()
        val apiService = ApiInterface.create()
        val call = apiService.myFavouritesApi(user_id)
        Log.d("MyFavorite", "REQUEST" + call.toString() + "")
        call.enqueue(object : Callback<myFavouriteResponse> {
            override fun onResponse(
                call: Call<myFavouriteResponse>,
                response: retrofit2.Response<myFavouriteResponse>?
            ) {

                //  if (response != null) {

                try {
                    swipe_refresh_layout.isRefreshing = false
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                            "success"
                        )
                    ) {
                        favourite_me_matches.clear()
                        ll_no_data.visibility = View.GONE
                        val user_data = response.body()!!.data
                        // val favourites_list: ArrayList<All_Matches>? = user_data!!.my_favourites

                        for (k in 0 until user_data!!.size) {

                            favourite_me_matches.add(user_data[k])
                        }
                        if (user_data.size == 0) {
                            ll_no_data.visibility = View.VISIBLE
                        }

                        //  Log.e("matches_favourite_list", user_data.toString() + "---" + user_data.size)

                        // val mNoOfColumns = Utils.calculateNoOfColumns(context!!, 150F)

                        //rv_favourite_list.setHasFixedSize(true)
                        val gridLayoutManager = GridLayoutManager(activity, 2)
                        rv_favourite_list.layoutManager = gridLayoutManager
                        rv_favourite_list.itemAnimator = DefaultItemAnimator()
                        details_adapter = MFDiscoverListAdapter(
                            favourite_me_matches, activity!!, user_details.get(
                                DllSessionManager.USER_ID
                            ).toString()
                        )
                        //  details_adapter.setHasStableIds(true)
                        rv_favourite_list.adapter = details_adapter
                        details_adapter!!.notifyDataSetChanged()
                    }

                } catch (e: Exception) {

                }

                //  }
            }

            override fun onFailure(call: Call<myFavouriteResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    inner class MFDiscoverListAdapter(
        val mServiceList: ArrayList<All_Matches>,
        val context: Context,
        user_id: String
    ) :
        RecyclerView.Adapter<ViewHolder1>() {

        var partner_match_distance_stg: String? = null
        var partner_match_search_stg: String? = null
        var partner_ethnicity_stg: String? = null
        var partner_religion_stg: String? = null
        var partner_education_stg: String? = null
        var partner_occupation_stg: String? = null
        var partner_smoke_stg: String? = null
        var partner_drink_stg: String? = null
        var photo1_stg: String? = null
        var photo2_stg: String? = null
        var photo3_stg: String? = null
        var photo4_stg: String? = null
        var photo5_stg: String? = null
        var photo6_stg: String? = null
        var photo7_stg: String? = null
        var photo8_stg: String? = null
        var photo9_stg: String? = null
        var photo10_stg: String? = null
        var photo1_approve: String? = null
        var photo2_approve: String? = null
        var photo3_approve: String? = null
        var photo4_approve: String? = null
        var photo5_approve: String? = null
        var photo6_approve: String? = null
        var photo7_approve: String? = null
        var photo8_approve: String? = null
        var photo9_approve: String? = null
        var photo10_approve: String? = null
        var fav_id_stg: String? = null
        var fav_from_stg: String? = null
        var fav_to_stg: String? = null
        var interest_from_stg: String? = null
        var interest_to_stg: String? = null
        var interest_status_stg: String? = null
        private var user_smoke_stg = ""
        private var user_drink_stg = ""
        private var user_denomination_stg = ""
        var height_ft_stg: String? = null
        var height_inc_stg: String? = null
        var height_cm_stg: String? = null
        var city_stg: String? = null
        var dob_stg: String? = null
        var ethnicity_stg: String? = null
        var education_stg: String? = null
        var occupation_stg: String? = null
        var income_stg: String? = null
        var passionate_stg: String? = null
        var leisure_time_stg: String? = null
        var thankful_1_stg: String? = null
        var thankful_2_stg: String? = null
        var thankful_3_stg: String? = null
        var partner_age_min_stg: String? = null
        var partner_age_max_stg: String? = null
        var religion_stg: String? = null
        var partner_denomination_stg: String? = null
        var partner_phone_number: String? = null
        var partner_mtongue_name: String? = null
        var mtongue_name: String? = null

        var main_user_id = user_id
        val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(
                LayoutInflater.from(context).inflate(
                    R.layout.favorites_item_list,
                    parent,
                    false
                )
            )

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10

            holder.tv_convo_name.text = mServiceList.get(position).first_name

            if (mServiceList[position].gender.equals("Woman")) {
                holder.iv_gender.setImageResource(R.drawable.new_female_ic)
            } else {
                holder.iv_gender.setImageResource(R.drawable.new_male_ic)
                holder.iv_gender.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        R.color.gender_color
                    ), android.graphics.PorterDuff.Mode.MULTIPLY
                )
            }


            //Log.e("partner_ma_stg", mServiceList.get(position).partner_match_distance)


            var date_only: Date? = null
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            try {
                date_only = sdf.parse(mServiceList.get(position).dob)
            } catch (e: ParseException) {
                e.printStackTrace()
            }


            val c = Calendar.getInstance()
            //Set time in milliseconds
            // c.setTimeInMillis(user_date_id.toLong())
            // c.setTimeInMillis(user_date_id.toLong())
            c.time = date_only
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)

            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()

            dob.set(mYear, mMonth, mDay)

            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }

            val ageInt = age

            holder.tv_convo_age_id.setText(ageInt.toString())
            if (mServiceList.get(position).partner_match_distance !== null) {
                partner_match_distance_stg = mServiceList.get(position).partner_match_distance!!
                Log.e("partnerstg", partner_match_distance_stg)
            } else {
                partner_match_distance_stg = "0"
                Log.e("partner_ma_stg", partner_match_distance_stg)

            }

            if (mServiceList.get(position).partner_match_search !== null) {
                partner_match_search_stg = mServiceList.get(position).partner_match_search!!
            } else {

                partner_match_search_stg = "0"
            }

            if (mServiceList.get(position).partner_ethnicity !== null) {
                partner_ethnicity_stg = mServiceList.get(position).partner_ethnicity!!
            } else {
                partner_ethnicity_stg = "0"
            }
            if (mServiceList.get(position).partner_religion !== null) {
                partner_religion_stg = mServiceList.get(position).partner_religion!!
            } else {
                partner_religion_stg = "0"
            }

            if (mServiceList.get(position).partner_education !== null) {
                partner_education_stg = mServiceList.get(position).partner_education!!
            } else {
                partner_education_stg = "0"
            }
            if (mServiceList.get(position).partner_occupation !== null) {
                partner_occupation_stg = mServiceList.get(position).partner_occupation!!
            } else {
                partner_occupation_stg = "0"
            }
            if (mServiceList.get(position).partner_smoke !== null) {
                partner_smoke_stg = mServiceList.get(position).partner_smoke!!

            } else {
                partner_smoke_stg = "0"

            }
            if (mServiceList.get(position).partner_drink !== null) {
                partner_drink_stg = mServiceList.get(position).partner_drink!!

            } else {
                partner_drink_stg = "0"
            }

            if (mServiceList.get(position).photo1 !== null) {
                photo1_stg = mServiceList.get(position).photo1!!
            } else {
                photo1_stg = "0"
            }
            if (mServiceList.get(position).photo2 !== null) {
                photo2_stg = mServiceList.get(position).photo2!!
            } else {

                photo2_stg = "0"
            }
            if (mServiceList.get(position).photo3 !== null) {
                photo3_stg = mServiceList.get(position).photo3!!
            } else {

                photo3_stg = "0"
            }
            if (mServiceList.get(position).photo4 !== null) {
                photo4_stg = mServiceList.get(position).photo4!!
            } else {
                photo4_stg = "0"
            }
            if (mServiceList.get(position).photo5 !== null) {
                photo5_stg = mServiceList.get(position).photo5!!
            } else {
                photo5_stg = "0"
            }
            if (mServiceList.get(position).photo6 !== null) {
                photo6_stg = mServiceList.get(position).photo6!!
            } else {

                photo6_stg = "0"
            }
            if (mServiceList[position].photo7 !== null) {
                photo7_stg = mServiceList.get(position).photo7!!
            } else {

                photo7_stg = "0"
            }
            if (mServiceList[position].photo8 !== null) {
                photo8_stg = mServiceList[position].photo8!!
            } else {

                photo8_stg = "0"
            }
            if (mServiceList[position].photo9 !== null) {
                photo9_stg = mServiceList[position].photo9!!
            } else {

                photo9_stg = "0"
            }
            if (mServiceList[position].photo10 !== null) {
                photo10_stg = mServiceList[position].photo10!!
            } else {

                photo10_stg = "0"
            }

            if (mServiceList[position].photo1_approve !== null) {
                photo1_approve = mServiceList[position].photo1_approve!!
            } else {
                photo1_approve = "0"
            }
            if (mServiceList[position].photo2_approve !== null) {
                photo2_approve = mServiceList[position].photo2_approve!!
            } else {

                photo2_approve = "0"
            }
            if (mServiceList[position].photo3_approve !== null) {
                photo3_approve = mServiceList[position].photo3_approve!!
            } else {

                photo3_approve = "0"
            }
            if (mServiceList[position].photo4_approve !== null) {
                photo4_approve = mServiceList[position].photo4_approve!!
            } else {
                photo4_approve = "0"
            }
            if (mServiceList[position].photo5_approve !== null) {
                photo5_approve = mServiceList[position].photo5_approve!!
            } else {
                photo5_approve = "0"
            }
            if (mServiceList[position].photo6_approve !== null) {
                photo6_approve = mServiceList[position].photo6_approve!!
            } else {

                photo6_approve = "0"
            }
            if (mServiceList[position].photo7_approve !== null) {
                photo7_approve = mServiceList[position].photo7_approve!!
            } else {

                photo7_approve = "0"
            }
            if (mServiceList[position].photo8_approve !== null) {
                photo8_approve = mServiceList[position].photo8_approve!!
            } else {

                photo8_approve = "0"
            }
            if (mServiceList[position].photo9_approve !== null) {
                photo9_approve = mServiceList[position].photo9_approve!!
            } else {

                photo9_approve = "0"
            }
            if (mServiceList[position].photo10_approve !== null) {
                photo10_approve = mServiceList[position].photo10_approve!!
            } else {

                photo10_approve = "0"
            }

            if (mServiceList.get(position).fav_id !== null) {
                fav_id_stg = mServiceList.get(position).fav_id!!
            } else {
                fav_id_stg = "0"
            }
            if (mServiceList.get(position).fav_from !== null) {
                fav_from_stg = mServiceList.get(position).fav_from!!
            } else {

                fav_from_stg = "0"
            }
            if (mServiceList.get(position).fav_to !== null) {
                fav_to_stg = mServiceList.get(position).fav_to!!
            } else {

                fav_to_stg = "0"
            }
            if (mServiceList.get(position).interest_from !== null) {
                interest_from_stg = mServiceList.get(position).interest_from!!
            } else {

                interest_from_stg = "0"
            }
            if (mServiceList.get(position).interest_to !== null) {
                interest_to_stg = mServiceList.get(position).interest_to!!
            } else {

                interest_to_stg = "0"
            }
            if (mServiceList.get(position).interest_status !== null) {
                interest_status_stg = mServiceList.get(position).interest_status!!
            } else {

                interest_status_stg = "0"
            }

            if (mServiceList.get(position).phone_number !== null) {
                partner_phone_number = mServiceList.get(position).phone_number
            } else {
                partner_phone_number = "0"
            }
            if (mServiceList.get(position).partner_denomination !== null) {
                partner_denomination_stg = mServiceList.get(position).partner_denomination
            } else {
                partner_denomination_stg = "0"
            }

            if (mServiceList.get(position).partner_mtongue_name == null || mServiceList.get(position).partner_mtongue_name == "") {
                partner_mtongue_name = "0"
            } else {
                partner_mtongue_name = mServiceList.get(position).partner_mtongue_name!!
            }
            if (mServiceList.get(position).mtongue_name == null || mServiceList.get(position).mtongue_name == "") {
                mtongue_name = "0"
            } else {
                mtongue_name = mServiceList.get(position).mtongue_name!!
            }

            if (fav_from_stg == main_user_id) {
                holder.btn_interest_id.isChecked = true
                // btn_interest_id.isClickable=false
                holder.btn_interest_id.isEnabled = false
            } else {
                holder.btn_interest_id.isChecked = false
            }



            if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS) == "1") {
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    }
                }
            } else {
                if (photo1_stg == "0") {
                    Picasso.with(context)
                        .load(photobaseurl + photo1_stg)
                        .error(R.drawable.ic_man_user)
                        .into(holder.iv_favorite_user_image)
                } else {
                    if (photo1_approve == "UNAPPROVED") {
                        Picasso.with(context)
                            .load(photobaseurl + "0")
                            .error(R.drawable.ic_man_user)
                            .into(holder.iv_favorite_user_image)
                    } else {
                        Picasso.with(context)
                            .load(photobaseurl + photo1_stg)
                            .error(R.drawable.ic_man_user)
                            .transform(BlurTransformation(context))
                            .into(holder.iv_favorite_user_image)
                    }
                }
            }



            holder.iv_favorite_user_image.setOnClickListener {


                if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("2")) {

                    memberShipAlertDialog()


                } else if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1") && plan_expire_status >= 0) {

                    if (mServiceList.get(position).religion == null || mServiceList.get(position).religion == "") {
                        religion_stg = "0"
                    } else {
                        religion_stg = mServiceList.get(position).religion
                    }
                    if (mServiceList.get(position).partner_match_distance !== null) {
                        partner_match_distance_stg =
                            mServiceList.get(position).partner_match_distance!!
                        Log.e("partnerstg", partner_match_distance_stg)
                    } else {
                        partner_match_distance_stg = "0"
                        Log.e("partner_ma_stg", partner_match_distance_stg)

                    }

                    if (mServiceList.get(position).partner_match_search !== null) {
                        partner_match_search_stg = mServiceList.get(position).partner_match_search!!
                    } else {

                        partner_match_search_stg = "0"
                    }

                    if (mServiceList.get(position).partner_ethnicity !== null) {
                        partner_ethnicity_stg = mServiceList.get(position).partner_ethnicity!!
                    } else {
                        partner_ethnicity_stg = "0"
                    }
                    if (mServiceList.get(position).partner_religion !== null) {
                        partner_religion_stg = mServiceList.get(position).partner_religion!!
                    } else {
                        partner_religion_stg = "0"
                    }

                    if (mServiceList.get(position).partner_education !== null) {
                        partner_education_stg = mServiceList.get(position).partner_education!!
                    } else {
                        partner_education_stg = "0"
                    }
                    if (mServiceList.get(position).partner_occupation !== null) {
                        partner_occupation_stg = mServiceList.get(position).partner_occupation!!
                    } else {
                        partner_occupation_stg = "0"
                    }
                    if (mServiceList.get(position).partner_smoke !== null) {
                        partner_smoke_stg = mServiceList.get(position).partner_smoke!!

                    } else {
                        partner_smoke_stg = "0"

                    }
                    if (mServiceList.get(position).partner_drink !== null) {
                        partner_drink_stg = mServiceList.get(position).partner_drink!!

                    } else {
                        partner_drink_stg = "0"
                    }

                    if (mServiceList.get(position).photo1 !== null) {
                        photo1_stg = mServiceList.get(position).photo1!!
                    } else {
                        photo1_stg = "0"
                    }
                    if (mServiceList.get(position).photo2 !== null) {
                        photo2_stg = mServiceList.get(position).photo2!!
                    } else {

                        photo2_stg = "0"
                    }
                    if (mServiceList.get(position).photo3 !== null) {
                        photo3_stg = mServiceList.get(position).photo3!!
                    } else {

                        photo3_stg = "0"
                    }
                    if (mServiceList.get(position).photo4 !== null) {
                        photo4_stg = mServiceList.get(position).photo4!!
                    } else {
                        photo4_stg = "0"
                    }
                    if (mServiceList.get(position).photo5 !== null) {
                        photo5_stg = mServiceList.get(position).photo5!!
                    } else {
                        photo5_stg = "0"
                    }
                    if (mServiceList.get(position).photo6 !== null) {
                        photo6_stg = mServiceList.get(position).photo6!!
                    } else {
                        photo6_stg = "0"
                    }

                    if (mServiceList.get(position).photo7 !== null) {
                        photo7_stg = mServiceList.get(position).photo7!!
                    } else {

                        photo7_stg = "0"
                    }
                    if (mServiceList.get(position).photo8 !== null) {
                        photo8_stg = mServiceList.get(position).photo8!!
                    } else {

                        photo8_stg = "0"
                    }
                    if (mServiceList.get(position).photo9 !== null) {
                        photo9_stg = mServiceList.get(position).photo9!!
                    } else {

                        photo9_stg = "0"
                    }
                    if (mServiceList.get(position).photo10 !== null) {
                        photo10_stg = mServiceList.get(position).photo10!!
                    } else {

                        photo10_stg = "0"
                    }

                    if (mServiceList[position].photo1_approve !== null) {
                        photo1_approve = mServiceList[position].photo1_approve!!
                    } else {
                        photo1_approve = "0"
                    }
                    if (mServiceList[position].photo2_approve !== null) {
                        photo2_approve = mServiceList[position].photo2_approve!!
                    } else {

                        photo2_approve = "0"
                    }
                    if (mServiceList[position].photo3_approve !== null) {
                        photo3_approve = mServiceList[position].photo3_approve!!
                    } else {

                        photo3_approve = "0"
                    }
                    if (mServiceList[position].photo4_approve !== null) {
                        photo4_approve = mServiceList[position].photo4_approve!!
                    } else {
                        photo4_approve = "0"
                    }
                    if (mServiceList[position].photo5_approve !== null) {
                        photo5_approve = mServiceList[position].photo5_approve!!
                    } else {
                        photo5_approve = "0"
                    }
                    if (mServiceList[position].photo6_approve !== null) {
                        photo6_approve = mServiceList[position].photo6_approve!!
                    } else {

                        photo6_approve = "0"
                    }
                    if (mServiceList[position].photo7_approve !== null) {
                        photo7_approve = mServiceList[position].photo7_approve!!
                    } else {

                        photo7_approve = "0"
                    }
                    if (mServiceList[position].photo8_approve !== null) {
                        photo8_approve = mServiceList[position].photo8_approve!!
                    } else {

                        photo8_approve = "0"
                    }
                    if (mServiceList[position].photo9_approve !== null) {
                        photo9_approve = mServiceList[position].photo9_approve!!
                    } else {

                        photo9_approve = "0"
                    }
                    if (mServiceList[position].photo10_approve !== null) {
                        photo10_approve = mServiceList[position].photo10_approve!!
                    } else {

                        photo10_approve = "0"
                    }

                    if (mServiceList.get(position).fav_id !== null) {
                        fav_id_stg = mServiceList.get(position).fav_id!!
                    } else {
                        fav_id_stg = "0"
                    }
                    if (mServiceList.get(position).fav_from !== null) {
                        fav_from_stg = mServiceList.get(position).fav_from!!
                    } else {

                        fav_from_stg = "0"
                    }
                    if (mServiceList.get(position).fav_to !== null) {
                        fav_to_stg = mServiceList.get(position).fav_to!!
                    } else {

                        fav_to_stg = "0"
                    }
                    if (mServiceList.get(position).interest_from !== null) {
                        interest_from_stg = mServiceList.get(position).interest_from!!
                    } else {

                        interest_from_stg = "0"
                    }
                    if (mServiceList.get(position).interest_to !== null) {
                        interest_to_stg = mServiceList.get(position).interest_to!!
                    } else {

                        interest_to_stg = "0"
                    }
                    if (mServiceList.get(position).interest_status !== null) {
                        interest_status_stg = mServiceList.get(position).interest_status!!
                    } else {

                        interest_status_stg = "0"
                    }

                    if (mServiceList.get(position).height_ft !== null) {
                        height_ft_stg = mServiceList.get(position).height_ft!!
                    } else {

                        height_ft_stg = "0"
                    }
                    if (mServiceList.get(position).height_inc !== null) {
                        height_inc_stg = mServiceList.get(position).height_inc!!
                    } else {

                        height_inc_stg = "0"
                    }
                    if (mServiceList.get(position).height_cms !== null) {
                        height_cm_stg = mServiceList.get(position).height_cms!!
                    } else {

                        height_cm_stg = "0"
                    }

                    /*if (interest_from_stg == main_user_id) {
                        holder.btn_interest_id.isChecked = true
                        // btn_interest_id.isClickable=false
                    } else {
                        holder.btn_interest_id.isChecked = false
                    }*/
                    if (mServiceList.get(position).smoke == null || mServiceList.get(position).smoke == "") {
                        user_smoke_stg = "0"
                    } else {
                        user_smoke_stg = mServiceList.get(position).smoke!!
                    }
                    if (mServiceList.get(position).drink == null || mServiceList.get(position).drink == "") {
                        user_drink_stg = "0"
                    } else {
                        user_drink_stg = mServiceList.get(position).drink!!
                    }
                    if (mServiceList.get(position).denomination == null || mServiceList.get(position).denomination == "") {
                        user_denomination_stg = "0"
                    } else {
                        user_denomination_stg = mServiceList.get(position).denomination!!
                    }


                    if (mServiceList.get(position).height_ft !== null) {
                        height_ft_stg = mServiceList.get(position).height_ft!!
                    } else {

                        height_ft_stg = "0"
                    }
                    if (mServiceList.get(position).height_inc !== null) {
                        height_inc_stg = mServiceList.get(position).height_inc!!
                    } else {

                        height_inc_stg = "0"
                    }
                    if (mServiceList.get(position).height_cms !== null) {
                        height_cm_stg = mServiceList.get(position).height_cms!!
                    } else {

                        height_cm_stg = "0"
                    }

                    if (mServiceList.get(position).city !== null) {
                        city_stg = mServiceList.get(position).city
                    } else {
                        city_stg = "0"
                    }
                    if (mServiceList.get(position).dob !== null) {
                        dob_stg = mServiceList.get(position).dob
                    } else {
                        dob_stg = "0"
                    }

                    if (mServiceList.get(position).ethnicity !== null) {
                        ethnicity_stg = mServiceList.get(position).ethnicity
                    } else {
                        ethnicity_stg = "0"
                    }
                    if (mServiceList.get(position).education !== null) {
                        education_stg = mServiceList.get(position).education
                    } else {
                        education_stg = "0"
                    }
                    if (mServiceList.get(position).occupation !== null) {
                        occupation_stg = mServiceList.get(position).occupation
                    } else {
                        occupation_stg = "0"
                    }
                    if (mServiceList.get(position).income !== null) {
                        income_stg = mServiceList.get(position).income
                    } else {
                        income_stg = "0"
                    }

                    if (mServiceList.get(position).passionate !== null) {
                        passionate_stg = mServiceList.get(position).passionate
                    } else {
                        passionate_stg = "0"
                    }
                    if (mServiceList.get(position).leisure_time !== null) {
                        leisure_time_stg = mServiceList.get(position).leisure_time
                    } else {
                        leisure_time_stg = "0"
                    }
                    if (mServiceList.get(position).thankful_1 !== null) {
                        thankful_1_stg = mServiceList.get(position).thankful_1
                    } else {
                        thankful_1_stg = "0"
                    }
                    if (mServiceList.get(position).thankful_2 !== null) {
                        thankful_2_stg = mServiceList.get(position).thankful_2
                    } else {
                        thankful_2_stg = "0"
                    }
                    if (mServiceList.get(position).thankful_3 !== null) {
                        thankful_3_stg = mServiceList.get(position).thankful_3
                    } else {
                        thankful_3_stg = "0"
                    }
                    if (mServiceList.get(position).partner_age_min !== null) {
                        partner_age_min_stg = mServiceList.get(position).partner_age_min
                    } else {
                        partner_age_min_stg = "0"
                    }
                    if (mServiceList.get(position).partner_age_max !== null) {
                        partner_age_max_stg = mServiceList.get(position).partner_age_max
                    } else {
                        partner_age_max_stg = "0"
                    }

                    if (mServiceList.get(position).phone_number !== null) {
                        partner_phone_number = mServiceList.get(position).phone_number
                    } else {
                        partner_phone_number = "0"
                    }
                    if (mServiceList.get(position).partner_denomination !== null) {
                        partner_denomination_stg = mServiceList.get(position).partner_denomination
                    } else {
                        partner_denomination_stg = "0"
                    }

                    if (mServiceList.get(position).partner_mtongue_name == null || mServiceList.get(
                            position
                        ).partner_mtongue_name == ""
                    ) {
                        partner_mtongue_name = "0"
                    } else {
                        partner_mtongue_name = mServiceList.get(position).partner_mtongue_name!!
                    }
                    if (mServiceList.get(position).mtongue_name == null || mServiceList.get(position).mtongue_name == "") {
                        mtongue_name = "0"
                    } else {
                        mtongue_name = mServiceList.get(position).mtongue_name!!
                    }


                    val internt = Intent(context, FullProfile::class.java)
                    val pairs = arrayOfNulls<Pair<View, String>>(4)
                    pairs[0] =
                        Pair<View, String>(holder.iv_favorite_user_image, "mainimageTreansition")
                    pairs[1] = Pair<View, String>(holder.tv_convo_name, "nametransition")
                    pairs[2] = Pair<View, String>(holder.iv_gender, "gendertransition")
                    pairs[3] = Pair<View, String>(holder.tv_convo_age_id, "agetransition")

                    val aOptions =
                        ActivityOptions.makeSceneTransitionAnimation(context as Activity, *pairs)


                    internt.putExtra("from_screen", "my_favorite")
                    internt.putExtra("id", mServiceList.get(position).id)
                    internt.putExtra("photo1", photo1_stg)
                    internt.putExtra("name_stg", mServiceList.get(position).first_name)
                    internt.putExtra("gender_stg", mServiceList.get(position).gender)
                    internt.putExtra("age_stg", dob_stg)
                    internt.putExtra("location_stg", city_stg)
                    context.startActivity(internt, aOptions.toBundle())
                }

            }
        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        //val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
        val tv_convo_name = view.findViewById<CustomTextView>(R.id.tv_convo_name)
        val iv_favorite_user_image = view.findViewById<ImageView>(R.id.iv_favorite_user_image)
        val tv_convo_age_id = view.findViewById<CustomTextView>(R.id.tv_convo_age_id)
        val btn_interest_id = view.findViewById<SparkButton>(R.id.btn_interest_id)
        val iv_gender = view.findViewById<ImageView>(R.id.iv_gender)


    }

    fun memberShipAlertDialog() {

        success_dialog = Dialog(context)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        img_profile_pic_id = success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text = "Alert!"
        tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text =
            "You need an active membership plan to view the profile. Proceed to membership plans?"
        iv_close = success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility = View.VISIBLE
        tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(context, MembershipPlanActivity::class.java)
            context!!.startActivity(intent)
        }

        Picasso.with(context)
            .load(photbaseurl + user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()

    }
}