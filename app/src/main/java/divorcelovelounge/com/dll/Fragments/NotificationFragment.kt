package divorcelovelounge.com.dll.Fragments

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import divorcelovelounge.com.dll.Adapters.NotificationAdapter
import divorcelovelounge.com.dll.Pojos.MyNotificationsPojo
import divorcelovelounge.com.dll.R

class NotificationFragment : Fragment() {
    val logTag = "NotificationFragment"
    private var rootView: View? = null
    private var listView: ListView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        if (rootView != null) {
            val parent = rootView!!.parent as ViewGroup
            parent.removeView(rootView)
        }
        try {
            rootView = inflater.inflate(R.layout.activity_notification_profile, container, false)
        } catch (e: InflateException) {

        }

        initialize(rootView)

      //  dummyData()


        listView!!.setOnItemClickListener { parent, view, position, id ->

            try {
                Log.d(logTag, "position $position ")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    listView!!.getChildAt(position).setBackgroundColor(
                        activity!!.resources.getColor(R.color.notification_list_item_selected, activity!!.theme)
                    )
                } else {
                    listView!!.getChildAt(position)
                        .setBackgroundColor(activity!!.resources.getColor(R.color.notification_list_item_selected))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        return rootView

    }

    private fun initialize(rootView: View?) {
        listView = rootView!!.findViewById(R.id.lv_notifications)
        listView!!.isFastScrollEnabled = true


    }

    private fun dummyData() {
        val pojo = MyNotificationsPojo()
        val notificationsData = ArrayList<MyNotificationsPojo>()

        notificationsData.add(pojo)
        val adapter = NotificationAdapter(activity!! /*,notificationsData*/)
        listView!!.adapter = adapter
    }

}