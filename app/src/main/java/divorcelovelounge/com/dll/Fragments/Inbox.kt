package divorcelovelounge.com.dll.Fragments

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.ComposeMessageActivity
import divorcelovelounge.com.dll.Helper.*
import divorcelovelounge.com.dll.MessageDetailsActivity
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import divorcelovelounge.com.dll.SparkButtonHelper.SparkEventListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class Inbox : Fragment(), BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var rv_inbox_list_id: RecyclerView

    private lateinit var iv_compose_mail_id: ImageView

    lateinit var inbox_list_array: ArrayList<InboxlistArray>

    var inbox_adpter: InboxAdapter? = null

    lateinit var swipeController: SwipeController
    lateinit var progress_1: ProgressBar
    var imp_status_stg = ""
    var imp_sent_reciever_stg = ""
    var msg_photo_status_stg = ""
    lateinit var et_msg_search_id: EditText
    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: java.util.ArrayList<AdsListDataResponse>
    lateinit var ll_no_data: LinearLayout

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg = ""
    var gps_state_stg = ""
    var gps_city_stg = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            /* mPosition = getArguments().getInt("position");*/
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.inbox_frgment, container, false)
        dllSessionManager = DllSessionManager(this.context!!)
        user_details = dllSessionManager.userDetails
        initialize(rootView)

        return rootView
    }

    companion object {

        fun newInstance(position: Int): Inbox {
            val fragment = Inbox()
            val args = Bundle()
            args.putInt("position", position)
            fragment.arguments = args

            return fragment
        }
    }

    private fun initialize(rootView: View?) {
        rv_inbox_list_id = rootView!!.findViewById(R.id.rv_inbox_list_id)
        et_msg_search_id = rootView!!.findViewById(R.id.et_msg_search_id)

        ll_no_data = rootView.findViewById<LinearLayout>(R.id.ll_no_data)
        progress_1 = rootView.findViewById(R.id.progress_1)

        et_msg_search_id.visibility = View.GONE
        et_msg_search_id.visibility = View.VISIBLE

        rv_inbox_list_id.visibility = View.GONE
        rv_inbox_list_id.visibility = View.VISIBLE

        sliderLayout = rootView.findViewById(R.id.slider)
        addsarrayList = ArrayList()

        location_details_list = dllSessionManager.locationDetails
        gps_county_stg = location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg = location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg = location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "MessagesInbox")
        InboxApi()
        progress_1.visibility = View.VISIBLE

        iv_compose_mail_id = rootView!!.findViewById(R.id.iv_compose_mail_id)
        iv_compose_mail_id.setOnClickListener {
            val intent = Intent(context, ComposeMessageActivity::class.java)
            intent.putExtra("from_stg", "compose")
            startActivity(intent)
        }


    }


    fun InboxApi() {
        inbox_list_array = ArrayList()
        inbox_list_array.clear()


        val api = ApiInterface.create()
        val inboxlistApi =
            api.messengersListApi(user_id = user_details[DllSessionManager.USER_ID]!!.toString())
        inboxlistApi.enqueue(object : Callback<InboxmeResponse> {
            override fun onResponse(
                call: Call<InboxmeResponse>,
                response: Response<InboxmeResponse>
            ) {


                try {
                    inbox_list_array.clear()
                    progress_1.visibility = View.GONE
                    if (response != null) {

                        if (response.body()!!.status == "1") {
                            val user_data = response.body()!!.userlist


                            for (i in 0 until user_data!!.size) {
                                inbox_list_array.add(user_data[i])
                            }

                            rv_inbox_list_id.layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                            rv_inbox_list_id.itemAnimator = DefaultItemAnimator()
                            inbox_adpter = InboxAdapter(activity!!, inbox_list_array)
                            //inbox_adpter!!.setHasStableIds(true)
                            rv_inbox_list_id.adapter = inbox_adpter
                            inbox_adpter!!.notifyDataSetChanged()

                            Log.e("data_size_stg", user_data!!.size.toString())
                        } else if (response.body()!!.status == "2") {
                            Log.e("data_size_stg_ll", response.body()!!.userlist!!.size.toString())
                            ll_no_data.visibility = View.VISIBLE
                        }

                        Log.e("data_size_lll", response.body()!!.userlist!!.size.toString())
                    }

                } catch (e: Exception) {
                    Log.e("data_size_lll", e.toString())

                    ll_no_data.visibility = View.VISIBLE
                }

                Log.e("data_size_lll", "kkkkk")
            }

            override fun onFailure(call: Call<InboxmeResponse>, t: Throwable) {

                Log.d("list", "block list response error $t")
                ll_no_data.visibility = View.VISIBLE
            }
        })

        swipeController = SwipeController(context, object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) {


                val builder = AlertDialog.Builder(context!!)
                builder.setTitle("Alert!")
                    .setMessage("Are You Sure you want to proceed with Delete?")
                    .setCancelable(false)
                    .setPositiveButton("DELETE", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, id: Int) {
                            dialog!!.dismiss()

                            if (inbox_adpter!!.inbox_main_array.get(position).msg_from.equals(
                                    user_details[DllSessionManager.USER_ID]!!.toString()
                                )
                            ) {

                                imp_sent_reciever_stg = "sender"

                            } else {
                                imp_sent_reciever_stg = "reciever"
                            }


                            val apiService = ApiInterface.create()
                            val call = apiService.makeTrashApi(
                                inbox_adpter!!.inbox_main_array.get(position).msg_id,
                                "Yes",
                                imp_sent_reciever_stg
                            )
                            Log.d("REQUEST", call.toString() + "")
                            call.enqueue(object : Callback<BlockListResponse> {
                                override fun onResponse(
                                    call: Call<BlockListResponse>,
                                    response: retrofit2.Response<BlockListResponse>?
                                ) {

                                    //  if (response != null) {
                                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                            "success"
                                        )
                                    ) {

                                        inbox_adpter!!.inbox_main_array.removeAt(position)
                                        inbox_adpter!!.notifyItemRemoved(position)
                                        inbox_adpter!!.notifyItemRangeChanged(
                                            position,
                                            inbox_adpter!!.getItemCount()
                                        )
                                        InboxApi()
                                    }

                                    //  }
                                }

                                override fun onFailure(
                                    call: Call<BlockListResponse>,
                                    t: Throwable
                                ) {
                                    Log.w("Result_Order_details", t.toString())
                                }
                            })


                        }
                    })
                    .setNegativeButton("CANCEL", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, id: Int) {
                            dialog!!.dismiss()
                        }
                    })

                val alertDialog = builder.create()
                alertDialog.show()


            }
        })


        val itemTouchhelper = ItemTouchHelper(swipeController)
        itemTouchhelper.attachToRecyclerView(rv_inbox_list_id)

        rv_inbox_list_id.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                swipeController.onDraw(c)

            }
        })
        et_msg_search_id.setText("")
        et_msg_search_id.isFocusable = false
        et_msg_search_id.setOnClickListener {
            et_msg_search_id.isFocusable = true
            et_msg_search_id.isFocusableInTouchMode = true
            et_msg_search_id.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (char!!.trim() != "") {
                        if (inbox_list_array.size > 0) {
                            inbox_adpter!!.filter(char.toString())
                        }

                    }
                    /*tv_no_list_id.visibility = if (userListAdapter.getSize()!! > 0) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }*/


                }

            })

        }


    }

    inner class InboxAdapter(
        val context: Context,
        val inbox_main_array: ArrayList<InboxlistArray>
    ) :
        RecyclerView.Adapter<ViewHolder1>() {
        private var filterList: ArrayList<InboxlistArray>? = null

        init {
            this.filterList = ArrayList()
            // we copy the original list to the filter list and use it for setting row values
            this.filterList!!.addAll(this.inbox_main_array)
        }


        val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(
                LayoutInflater.from(context).inflate(
                    R.layout.messages_list_item,
                    parent,
                    false
                )
            )

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //Log.e("dadadad",filterList!!.get(position).photo1_approve)

            /*if (filterList!!.get(position).photo1_approve.equals("UNAPPROVED")) {
                Picasso.with(context)
                    .load("0")
                    .error(R.drawable.ic_man_user)
                    .into(holder.iv_profile_pic_id)
            } else {*/
            Picasso.with(context)
                .load(photobaseurl + filterList!!.get(position).photo1)
                .error(R.drawable.ic_man_user)
                .into(holder.iv_profile_pic_id)
            // }

            holder.tv_first_name_id.text = filterList!!.get(position).first_name
            holder.tv_messager_id.text = filterList!!.get(position).msg_content
            holder.tv_msg_time_id.text = editDateFormat(filterList!![position].msg_date)


            if (filterList!!.get(position).msg_from.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {
                if (filterList!!.get(position).msg_important_sender.equals("Yes")) {
                    holder.iv_msg_imp_status_id.isChecked = true
                } else {
                    holder.iv_msg_imp_status_id.isChecked = false
                }
            }
            if (filterList!!.get(position).msg_from != (user_details[DllSessionManager.USER_ID]!!.toString())) {
                if (filterList!!.get(position).msg_important_receiver.equals("Yes")) {
                    holder.iv_msg_imp_status_id.isChecked = true
                } else {
                    holder.iv_msg_imp_status_id.isChecked = false
                }
            }





            holder.iv_msg_imp_status_id.setEventListener(object : SparkEventListener {
                override fun onEvent(button: ImageView, buttonState: Boolean) {
                    if (buttonState) {
                        //Toast.makeText(context, buttonState.toString(), Toast.LENGTH_SHORT).show()


                        if (filterList!!.get(position).msg_from.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {

                            imp_sent_reciever_stg = "sender"

                        } else {
                            imp_sent_reciever_stg = "reciever"
                        }
                        /*if(inbox_main_array.get(position).msg_from!=(user_details[DllSessionManager.USER_ID]!!.toString())){


                        }*/


                        val apiService = ApiInterface.create()
                        val call = apiService.makeImportantApi(
                            filterList!!.get(position).msg_id,
                            "Yes",
                            imp_sent_reciever_stg
                        )
                        Log.d("REQUEST", call.toString() + "")
                        call.enqueue(object : Callback<BlockListResponse> {
                            override fun onResponse(
                                call: Call<BlockListResponse>,
                                response: retrofit2.Response<BlockListResponse>?
                            ) {

                                //  if (response != null) {
                                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                        "success"
                                    )
                                ) {

                                    // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                    // fav_from_stg = main_user_id
                                }

                                //  }
                            }

                            override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                                Log.w("Result_Order_details", t.toString())
                            }
                        })

                    } else {
                        if (filterList!!.get(position).msg_from.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {

                            imp_sent_reciever_stg = "sender"

                        } else {
                            imp_sent_reciever_stg = "reciever"
                        }
                        /*if(inbox_main_array.get(position).msg_from!=(user_details[DllSessionManager.USER_ID]!!.toString())){


                        }*/

                        val apiService = ApiInterface.create()
                        val call = apiService.makeImportantApi(
                            filterList!!.get(position).msg_id,
                            "No",
                            imp_sent_reciever_stg
                        )
                        Log.d("REQUEST", call.toString() + "")
                        call.enqueue(object : Callback<BlockListResponse> {
                            override fun onResponse(
                                call: Call<BlockListResponse>,
                                response: retrofit2.Response<BlockListResponse>?
                            ) {

                                //  if (response != null) {
                                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                        "success"
                                    )
                                ) {

                                    // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                    // fav_from_stg = main_user_id
                                }

                                //  }
                            }

                            override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                                Log.w("Result_Order_details", t.toString())
                            }
                        })
                    }
                }

                override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
                }

                override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
                }
            })

            Log.e("date_ids_lll", filterList!!.get(position).date_id)

            holder.ll_inbox_id.setOnClickListener {
                /*if (filterList!!.get(position).photo1_approve.equals("UNAPPROVED")) {
                    msg_photo_status_stg = "0"
                } else {*/
                msg_photo_status_stg = photobaseurl + filterList!!.get(position).photo1

                // }


                val intent = Intent(context, MessageDetailsActivity::class.java)
                intent.putExtra("msg_id_stg", filterList!!.get(position).msg_id)
                intent.putExtra("msg_to_stg", filterList!!.get(position).msg_to)
                intent.putExtra("msg_from_stg", filterList!!.get(position).msg_from)
                intent.putExtra("msg_content_stg", filterList!!.get(position).msg_content)
                intent.putExtra(
                    "msg_important_sender_stg",
                    filterList!!.get(position).msg_important_sender
                )
                intent.putExtra(
                    "msg_important_receiver_stg",
                    filterList!!.get(position).msg_important_receiver
                )
                intent.putExtra("msg_date_stg", filterList!!.get(position).msg_date)
                intent.putExtra("trash_sender_stg", filterList!!.get(position).trash_sender)
                intent.putExtra("trash_receiver_stg", filterList!!.get(position).trash_receiver)
                intent.putExtra("first_name_stg", filterList!!.get(position).first_name)
                intent.putExtra("photo1", msg_photo_status_stg)
                intent.putExtra("date_id_stg", filterList!!.get(position).date_id)
                intent.putExtra("from_stg", "Inbox")
                context.startActivity(intent)
            }

            if (filterList!!.get(position).totalcount.equals("0")) {
                holder.tv_msg_count_id.visibility = View.GONE
            } else {
                holder.tv_msg_count_id.visibility = View.VISIBLE
                holder.tv_msg_count_id.setText(filterList!!.get(position).totalcount)


            }


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return filterList?.size ?: 0
        }

        fun filter(text: String) {

            // Searching could be complex..so we will dispatch it to a different thread...
            Thread(Runnable {
                // Clear the filter list
                filterList!!.clear()

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList!!.addAll(inbox_main_array)

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (item in inbox_main_array) {
                        if (item.first_name.toLowerCase().contains(text.toLowerCase()) /*|| item.id.toLowerCase().contains(text.toLowerCase())*/) {
                            // Adding Matched items
                            filterList!!.add(item)
                        }
                    }
                }

                // Set on UI Thread
                (context as Activity).runOnUiThread {
                    // Notify the List that the DataSet has changed...
                    notifyDataSetChanged()
                }
            }).start()

        }

    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {

        val iv_profile_pic_id = view.findViewById<ImageView>(R.id.iv_profile_pic_id)
        val tv_first_name_id = view.findViewById<CustomTextViewBold>(R.id.tv_first_name_id)
        val tv_messager_id = view.findViewById<CustomTextView>(R.id.tv_messager_id)
        val tv_msg_time_id = view.findViewById<CustomTextView>(R.id.tv_msg_time_id)
        val iv_msg_imp_status_id = view.findViewById<SparkButton>(R.id.iv_msg_imp_status_id)
        val ll_list_view_id = view.findViewById<LinearLayout>(R.id.ll_list_view_id)
        val tv_msg_count_id = view.findViewById<CustomTextView>(R.id.tv_msg_count_id)
        val ll_inbox_id = view.findViewById<LinearLayout>(R.id.ll_inbox_id)

    }


    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }


    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(
            country_stg,
            state_stg,
            city_stg,
            from_stg,
            user_details.get(DllSessionManager.USER_ID).toString()
        )

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility = View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e(
                                    "IMAGEADD",
                                    PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(i).add_image
                                )

                                val textSliderView = TextSliderView(context!!);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(
                                        PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(
                                            i
                                        ).add_image
                                    )
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@Inbox);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString(
                                        "extra",
                                        response.body()!!.data!!.get(i).add_link.toString()
                                    );
                                sliderLayout.addSlider(textSliderView);
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@Inbox);


                        } else {
                            sliderLayout.visibility = View.GONE
                            Log.e("date_only", "No Data")
                        }
                    } else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(slider!!.bundle.get("extra").toString())
            )
        );
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }


}