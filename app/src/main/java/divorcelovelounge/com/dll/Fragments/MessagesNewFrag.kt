package divorcelovelounge.com.dll.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import divorcelovelounge.com.dll.Adapters.MessagesAdapter
import divorcelovelounge.com.dll.Adapters.MyAdapter
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.UserInterestCountResponse
import divorcelovelounge.com.dll.ComposeMessageActivity
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.TabLayoutHelper.NonSwipeableViewPager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MessagesNewFrag : Fragment() {

    private var rootView: View? = null
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: NonSwipeableViewPager
    private lateinit var rl_prefs: RelativeLayout
    private lateinit var iv_prefs_back: ImageView
    private lateinit var iv_compose_mail_id: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.activity_messages, container, false)
        val mViewPager = view.findViewById<View>(R.id.viewPager_id) as ViewPager
        val tabLayout = view.findViewById<View>(R.id.tabLayout) as TabLayout
        val mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        mViewPager.adapter = mSectionsPagerAdapter

        tabLayout.setupWithViewPager(mViewPager)
        //initialize(view)

        /*tabLayout.addTab(tabLayout.newTab().setText("Inbox"))
        tabLayout.addTab(tabLayout.newTab().setText("Sent"))

        tabLayout.addTab(tabLayout.newTab().setText("Important"))
        tabLayout.addTab(tabLayout.newTab().setText("Trash"))*/
        iv_compose_mail_id = view.findViewById(R.id.iv_compose_mail_id)
        iv_compose_mail_id.setOnClickListener {
            val intent = Intent(context, ComposeMessageActivity::class.java)
            intent.putExtra("from_stg","compose")
            startActivity(intent)
        }
        return view

    }
    private inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return Inbox.newInstance(1)
                1 -> return Sent.newInstance(2)
                2 -> return Importent.newInstance(3)
                else -> return Trash.newInstance(4)
            }

        }

        override fun getCount(): Int {
            // Show 4 total pages.
            return 4
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return "Inbox"
                1 -> return "Sent"
                2 -> return "Important"
                else -> return "Trash"
            }


        }
    }

    /*private fun initialize(rootView: View?) {
        rl_prefs = rootView!!.findViewById(R.id.rl_prefs)
        iv_compose_mail_id = rootView.findViewById(R.id.iv_compose_mail_id)
        iv_compose_mail_id.setOnClickListener {
            val intent = Intent(context, ComposeMessageActivity::class.java)
            intent.putExtra("from_stg","compose")
            startActivity(intent)
        }
        tabLayout=rootView.findViewById(R.id.tabLayout)
        viewPager=rootView.findViewById(R.id.viewPager_id)

        tabLayout.addTab(tabLayout.newTab().setText("Inbox"))
        tabLayout.addTab(tabLayout.newTab().setText("Sent"))

        tabLayout.addTab(tabLayout.newTab().setText("Important"))
        tabLayout.addTab(tabLayout.newTab().setText("Trash"))
        // tabLayout.setTabGravity(TabLayout.GRAVITY_FILL)

        val adapter = MessagesAdapter(activity!!.supportFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit=1

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }*/

}