package divorcelovelounge.com.dll.Fragments

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.*
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession
import divorcelovelounge.com.dll.Chat.realm.RealmManager
import divorcelovelounge.com.dll.Chat.utils.Preferences
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Helper.SwitchButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class ProfileFragment : Fragment(), View.OnClickListener {


    lateinit var img_add_pic_id: ImageView
    lateinit var img_profile_pic_id: ImageView

    private val GALLERY = 1
    private val CAMERA = 2
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var tv_email_id: CustomTextView
    lateinit var tv_fstname_id: CustomTextView
    lateinit var tv_dob_id: CustomTextView
    lateinit var tv_status_id: CustomTextView
    lateinit var tv_gender_id: CustomTextView
    lateinit var tv_enthicity_id: CustomTextView
    lateinit var tv_height_id: CustomTextView
    lateinit var tv_occupation_id: CustomTextView
    lateinit var tv_education_id: CustomTextView
    lateinit var tv_mobile_id: CustomTextView
    lateinit var tv_style_id: CustomTextView
    lateinit var tv_smoking_id: CustomTextView
    lateinit var tv_drinking_id: CustomTextView
    lateinit var tv_kids_id: CustomTextView
    lateinit var tv_state_id: CustomTextView
    lateinit var ll_contactus_id: LinearLayout
    lateinit var ll_membershipplan: LinearLayout
    lateinit var ll_membershipmyplan: LinearLayout
    lateinit var ll_info_id: LinearLayout
    lateinit var img_info_id: ImageView
    lateinit var ll_info_content_id: LinearLayout
    lateinit var ll_location_id: LinearLayout
    lateinit var img_location_id: ImageView
    lateinit var ll_location_content_id: LinearLayout
    lateinit var ll_setting_id: LinearLayout
    lateinit var push_notify_switch: SwitchButton
    lateinit var email_notify_switch: SwitchButton
    lateinit var ll_setting_content_id: LinearLayout
    lateinit var tv_sign_out_id: CustomTextViewBold
    lateinit var ll_block_users: LinearLayout
    lateinit var ll_contacts_list_id: LinearLayout
    lateinit var ll_messages_list_id: LinearLayout
    lateinit var ll_privacy_policy_id: LinearLayout
    lateinit var ll_terms_conditions: LinearLayout
    lateinit var ll_change_password_id: LinearLayout
    lateinit var ll_quiz_edit_id: LinearLayout
    lateinit var ll_user_online_id: LinearLayout
    lateinit var tv_country_id: CustomTextView
    lateinit var tv_msg_count_id: CustomTextView

    lateinit var antiRotateClk: Animation
    lateinit var clockRotateClk: Animation

    var info_stg = "open"
    var location_stg = "close"
    var setting_stg = "close"
    var user_id = ""
    var push_checked = ""
    var noti_checked = ""
    var noti_checked_frm_api = ""
    var email_checked_frm_api = ""

    private lateinit var sessionManager: DllSessionManager

    lateinit var iv_profile_edit: ImageView
    private val pattern: Pattern? = null
    lateinit var matcher: Matcher
    private val DATE_PATTERN = "(0?[1-9]|1[012]) [/.-] (0?[1-9]|[12][0-9]|3[01]) [/.-] ((19|20)\\d\\d)"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        //Returning the layout file after inflating0
        //Change R.layout.tab1 in you classes

        val rootview = inflater.inflate(R.layout.activity_account_profile, container, false)
        dllSessionManager = DllSessionManager(this.context!!)
        img_add_pic_id = rootview.findViewById(R.id.img_add_pic_id)
        img_profile_pic_id = rootview.findViewById(R.id.img_profile_pic_id)

        tv_email_id = rootview.findViewById(R.id.tv_email_id)
        tv_fstname_id = rootview.findViewById(R.id.tv_fstname_id)
        tv_dob_id = rootview.findViewById(R.id.tv_dob_id)
        tv_status_id = rootview.findViewById(R.id.tv_status_id)
        tv_gender_id = rootview.findViewById(R.id.tv_gender_id)
        tv_enthicity_id = rootview.findViewById(R.id.tv_enthicity_id)
        tv_height_id = rootview.findViewById(R.id.tv_height_id)
        tv_occupation_id = rootview.findViewById(R.id.tv_occupation_id)
        tv_education_id = rootview.findViewById(R.id.tv_education_id)
        tv_mobile_id = rootview.findViewById(R.id.tv_mobile_id)
        tv_style_id = rootview.findViewById(R.id.tv_style_id)
        tv_smoking_id = rootview.findViewById(R.id.tv_smoking_id)
        tv_drinking_id = rootview.findViewById(R.id.tv_drinking_id)
        tv_kids_id = rootview.findViewById(R.id.tv_kids_id)
        tv_state_id = rootview.findViewById(R.id.tv_state_id)
        ll_contactus_id = rootview.findViewById(R.id.ll_contactus_id)
        ll_membershipplan = rootview.findViewById(R.id.ll_membershipplan)
        ll_membershipmyplan = rootview.findViewById(R.id.ll_membershipmyplan)
        ll_info_id = rootview.findViewById(R.id.ll_info_id)
        img_info_id = rootview.findViewById(R.id.img_info_id)
        ll_info_content_id = rootview.findViewById(R.id.ll_info_content_id)
        ll_location_id = rootview.findViewById(R.id.ll_location_id)
        img_location_id = rootview.findViewById(R.id.img_location_id)
        ll_location_content_id = rootview.findViewById(R.id.ll_location_content_id)
        ll_setting_id = rootview.findViewById(R.id.ll_setting_id)
        push_notify_switch = rootview.findViewById(R.id.push_notify_switch)
        email_notify_switch = rootview.findViewById(R.id.email_notify_switch)
        ll_setting_content_id = rootview.findViewById(R.id.ll_setting_content_id)
        tv_sign_out_id = rootview.findViewById(R.id.tv_sign_out_id)
        iv_profile_edit = rootview.findViewById(R.id.iv_profile_edit)
        ll_block_users = rootview.findViewById(R.id.ll_block_users)
        ll_contacts_list_id = rootview.findViewById(R.id.ll_contacts_list_id)
        ll_messages_list_id = rootview.findViewById(R.id.ll_messages_list_id)
        ll_privacy_policy_id = rootview.findViewById(R.id.ll_privacy_policy_id)
        ll_terms_conditions = rootview.findViewById(R.id.ll_terms_conditions)
        ll_change_password_id = rootview.findViewById(R.id.ll_change_password_id)
        ll_quiz_edit_id = rootview.findViewById(R.id.ll_quiz_edit_id)
        tv_country_id = rootview.findViewById(R.id.tv_country_id)
        tv_msg_count_id = rootview.findViewById(R.id.tv_msg_count_id)
        ll_user_online_id = rootview.findViewById(R.id.ll_user_online_id)

        ll_info_id.setOnClickListener(this)
        ll_location_id.setOnClickListener(this)
        ll_setting_id.setOnClickListener(this)
        tv_sign_out_id.setOnClickListener(this)
        ll_user_online_id.setOnClickListener(this)

        sessionManager = DllSessionManager(activity!!)



        if(sessionManager.userDetails[DllSessionManager.PUSH_NOTIFY_STATUS].equals("Yes")){

            push_notify_switch.isChecked = true
        }else{
            push_notify_switch.isChecked = false
        }

        if(sessionManager.userDetails[DllSessionManager.EMAIL_NOTIFY_STATUS].equals("Yes")){

            email_notify_switch.isChecked = true
        }else{
            email_notify_switch.isChecked = false
        }


        push_notify_switch.setOnCheckedChangeListener(object :SwitchButton.OnCheckedChangeListener{
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {
                Log.e("PUSHNOTIFICATION",""+isChecked)
                if(isChecked){
                    push_checked = "Yes"
                    noti_checked = ""

                    pushNotification(push_checked,noti_checked)

                }else{
                    push_checked = "No"
                    noti_checked = ""

                    pushNotification(push_checked,noti_checked)
                }


            }

        })

        email_notify_switch.setOnCheckedChangeListener(object :SwitchButton.OnCheckedChangeListener{
            override fun onCheckedChanged(view: SwitchButton, isChecked: Boolean) {

                Log.e("EMAILNOTIFICATION",""+isChecked)
                if(isChecked){
                    push_checked = ""
                    noti_checked = "Yes"

                    pushNotification(push_checked,noti_checked)

                }else{
                    push_checked = ""
                    noti_checked = "No"

                    pushNotification(push_checked,noti_checked)
                }

            }

        })


        img_add_pic_id.setOnClickListener {
            //getImageFromAlbum()
            val intent = Intent(context, ProfilePhotos::class.java)
            startActivity(intent)
        }

        iv_profile_edit.setOnClickListener {
            val intent = Intent(context, EditMatchPreferencesActivity::class.java)
            startActivity(intent)
        }

        user_details = dllSessionManager.userDetails

        user_id = user_details[DllSessionManager.USER_ID]!!

        val height = String.format(
            "%s'%s\"",
            user_details.get(DllSessionManager.USER_HEIGHT_FT), user_details.get(DllSessionManager.USER_HEIGHT_INC)
        )

        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        toFormat.isLenient = false
        if (user_details.get(DllSessionManager.USER_DOB).toString().equals("0")) {
            tv_dob_id.text = "0"
        } else {
            val date = fromFormat.parse(user_details.get(DllSessionManager.USER_DOB).toString())
            val dob = toFormat.format(date).toString()
            tv_dob_id.text = dob
        }

        /*if (user_details.get(DllSessionManager.USER_DOB).toString().equals("0")) {
            tv_dob_id.text = ""
        } else {
            val date = fromFormat.parse(user_details.get(DllSessionManager.USER_DOB).toString())
            val dob = toFormat.format(date).toString()
            tv_dob_id.text = dob
        }*/

        if (user_details.get(DllSessionManager.USER_ETHNICITY).toString().equals("0")) {
            tv_enthicity_id.text = ""
        } else {
            tv_enthicity_id.text = user_details.get(DllSessionManager.USER_ETHNICITY)
        }

        if (user_details.get(DllSessionManager.USER_HEIGHT_CMS).toString().equals("0")) {
            tv_height_id.text = ""
        } else {
            tv_height_id.text = height + "/" + user_details.get(DllSessionManager.USER_HEIGHT_CMS)
        }

        if (user_details.get(DllSessionManager.USER_OCCUPATION).toString().equals("0")) {
            tv_occupation_id.text = ""
        } else {
            tv_occupation_id.text = user_details.get(DllSessionManager.USER_OCCUPATION)
        }

        if (user_details.get(DllSessionManager.USER_EDUCATION).toString().equals("0")) {
            tv_education_id.text = ""
        } else {
            tv_education_id.text = user_details.get(DllSessionManager.USER_EDUCATION)
        }

        if (user_details.get(DllSessionManager.USER_PASSIONATE).toString().equals("0")) {
            tv_style_id.text = ""
        } else {
            tv_style_id.text = user_details.get(DllSessionManager.USER_PASSIONATE)
        }

        if (user_details.get(DllSessionManager.USER_SMOKE).toString().equals("0")) {
            tv_smoking_id.text = ""
        } else {
            tv_smoking_id.text = user_details.get(DllSessionManager.USER_SMOKE)
        }

        if (user_details.get(DllSessionManager.USER_DRINK).toString().equals("0")) {
            tv_drinking_id.text = ""
        } else {
            tv_drinking_id.text = user_details.get(DllSessionManager.USER_DRINK)
        }

        /*if (user_details.get(DllSessionManager.USER_CHILD).toString().equals("0")) {
            tv_kids_id.text =""
        } else {*/
        tv_kids_id.text = user_details.get(DllSessionManager.USER_CHILD)
        //}

        if (user_details.get(DllSessionManager.USER_CITY).toString().equals("0")) {
            if (user_details.get(DllSessionManager.USER_STATE).toString().equals("0")) {
                tv_state_id.text = ""
            } else {
                tv_state_id.text = user_details.get(DllSessionManager.USER_STATE)
            }
        } else {
            tv_state_id.text =
                    user_details.get(DllSessionManager.USER_CITY) + ", " +
                    user_details.get(DllSessionManager.USER_STATE)
        }

        if (user_details.get(DllSessionManager.USER_COUNTRY).toString().equals("0")) {
            tv_country_id.text = ""
        } else {
            tv_country_id.text = user_details.get(DllSessionManager.USER_COUNTRY)
        }

        if (user_details.get(DllSessionManager.USER_PHONE_NUMBER).toString() == "0") {
            tv_mobile_id.text = ""
        } else {
            tv_mobile_id.text = user_details.get(DllSessionManager.USER_PHONE_NUMBER)
        }



        tv_email_id.text = user_details.get(DllSessionManager.USER_EMAIL)
        tv_fstname_id.text = user_details.get(DllSessionManager.USER_NAME)

        tv_status_id.text = user_details.get(DllSessionManager.USER_STATUS)
        tv_gender_id.text = user_details.get(DllSessionManager.USER_GENDER)


        val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL
        Picasso.with(context)
            .load(photbaseurl + user_details.get(DllSessionManager.PHOTO1))
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        ll_contactus_id.setOnClickListener {
            val intent_contact_us = Intent(context, SupportActivity::class.java)
            activity!!.startActivity(intent_contact_us)
        }

        ll_membershipplan.setOnClickListener {
            val intent_membershipplan = Intent(context, MembershipPlanActivity::class.java)
            activity!!.startActivity(intent_membershipplan)
        }
        ll_membershipmyplan.setOnClickListener {
            val intent_membershipplan = Intent(context, MembershipMyPlanActivity::class.java)
            intent_membershipplan.putExtra("from_membership", "profile_fragment")
            activity!!.startActivity(intent_membershipplan)
        }
        ll_block_users.setOnClickListener {
            val intent_membershipplan = Intent(context, BlockListActivity::class.java)
            activity!!.startActivity(intent_membershipplan)
        }

        ll_contacts_list_id.setOnClickListener {
            val intent_membershipplan = Intent(context, ContactsListActivity::class.java)
            activity!!.startActivity(intent_membershipplan)
        }

        ll_messages_list_id.setOnClickListener {
            val intent_membershipplan = Intent(context, MessagesActivity::class.java)
            activity!!.startActivity(intent_membershipplan)
        }

        ll_privacy_policy_id.setOnClickListener {
            val intent = Intent(context, PrivacyPolicyActivity::class.java)
            intent.putExtra("from","privacy")
            activity!!.startActivity(intent)
        }


        ll_terms_conditions.setOnClickListener {
            val intent = Intent(context, PrivacyPolicyActivity::class.java)
            intent.putExtra("from","terms")
            activity!!.startActivity(intent)
        }

        ll_change_password_id.setOnClickListener {
            val intent = Intent(context, ChangePasswordActivity::class.java)
            activity!!.startActivity(intent)
        }

        ll_quiz_edit_id.setOnClickListener {
            val intent = Intent(context, QuizEdit::class.java)
            activity!!.startActivity(intent)
        }

        // antiRotateClk = AnimationUtils.loadAnimation(context, R.anim.rotate_anticlockwise)
        // clockRotateClk = AnimationUtils.loadAnimation(context, R.anim.rotate_clockwise)


        getMsgCounts(user_id)

        return rootview
    }


    fun validate(date: String): Boolean {

        /* val dateData=DateData.convertToDate(date)

         Log.e("dateData",dateData.toString())*/
        /*matcher = pattern!!.matcher(date)

        if (matcher.matches()) {
            matcher.reset()

            if (matcher.find()) {
                val day = matcher.group(1)
                val month = matcher.group(2)
                val year = Integer.parseInt(matcher.group(3))

                return if (day == "31" && (month == "4" || month == "6" || month == "9" ||
                            month == "11" || month == "04" || month == "06" ||
                            month == "09")
                ) {
                    false // only 1,3,5,7,8,10,12 has 31 days
                } else if (month == "2" || month == "02") {
                    //leap year
                    if (year % 4 == 0) {
                        if (day == "30" || day == "31") {
                            false
                        } else {
                            true
                        }
                    } else {
                        if (day == "29" || day == "30" || day == "31") {
                            false
                        } else {
                            true
                        }
                    }
                } else {
                    true
                }

                tv_dob_id.text = month + "-" + day + "-" + year
            } else {
                return false
            }
        } else {
            return false
        }
*/
        return false
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ll_info_id -> {

                if (location_stg.equals("open")) {
                    location_stg = "close"
                    // img_info_id.startAnimation(clockRotateClk)
                    img_location_id.setRotation(360f)

                }

                if (info_stg.equals("open")) {
                    info_stg = "close"
                    // img_info_id.startAnimation(clockRotateClk)
                    img_info_id.setRotation(360f)
                    ll_info_content_id.visibility = View.GONE
                    ll_location_content_id.visibility = View.GONE
                    ll_setting_content_id.visibility = View.GONE
                } else {
                    info_stg = "open"
                    img_info_id.setRotation(180f)
                    // img_info_id.startAnimation(clockRotateClk)
                    ll_info_content_id.visibility = View.VISIBLE
                    ll_location_content_id.visibility = View.GONE
                    ll_setting_content_id.visibility = View.GONE
                }
            }
            R.id.ll_location_id -> {
                if (info_stg.equals("open")) {
                    info_stg = "close"
                    img_info_id.setRotation(360f)
                }

                if (location_stg.equals("close")) {
                    location_stg = "open"
                    // img_info_id.startAnimation(clockRotateClk)
                    img_location_id.setRotation(180f)
                    ll_info_content_id.visibility = View.GONE
                    ll_location_content_id.visibility = View.VISIBLE
                    ll_setting_content_id.visibility = View.GONE
                } else {
                    location_stg = "close"
                    img_location_id.setRotation(360f)
                    // img_info_id.startAnimation(clockRotateClk)
                    ll_info_content_id.visibility = View.GONE
                    ll_location_content_id.visibility = View.GONE
                    ll_setting_content_id.visibility = View.GONE
                }


            }
            R.id.ll_setting_id -> {

                info_stg = "open"
                location_stg = "open"

                /*if (setting_stg.equals("open")) {
                    setting_stg = "close"
                    img_info_id.setRotation(360f)
                }

                if (location_stg.equals("close")) {
                    location_stg = "open"
                    // img_info_id.startAnimation(clockRotateClk)
                    img_location_id.setRotation(180f)
                    ll_info_content_id.visibility = View.GONE
                    ll_location_content_id.visibility = View.VISIBLE
                    ll_setting_content_id.visibility = View.GONE
                } else {
                    location_stg = "close"
                    img_location_id.setRotation(360f)
                    // img_info_id.startAnimation(clockRotateClk)
                    ll_info_content_id.visibility = View.GONE
                    ll_location_content_id.visibility = View.GONE
                    ll_setting_content_id.visibility = View.GONE
                }*/

                img_location_id.setRotation(360f)
                img_info_id.setRotation(360f)

                ll_info_content_id.visibility = View.GONE
                ll_location_content_id.visibility = View.GONE
                ll_setting_content_id.visibility = View.VISIBLE
            }

            R.id.ll_user_online_id -> {

                val intent = Intent(context, OnlineNowActivity::class.java)
                startActivity(intent)

                //img_location_id.setRotation(360f)
               // img_info_id.setRotation(360f)

               // ll_info_content_id.visibility = View.GONE
               // ll_location_content_id.visibility = View.GONE
            }
            R.id.tv_sign_out_id -> {

                logout(user_id = user_id)

               // dllSessionManager.logoutUser()

                /******** Google Sign-In ********/
                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .build()
                val mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso!!)

                mGoogleSignInClient!!.signOut()
                    .addOnCompleteListener(activity!!) {}
            }
        }
    }

    private fun getImageFromAlbum() {
        val pictureDialog = AlertDialog.Builder(this.activity!!)

        pictureDialog.setTitle("Select Photo from")
        val pictureDialogItems = arrayOf("Gallery", "Camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*if (resultCode == this) {
            return
        }*/
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, contentURI)
                    //  val path = saveImage(bitmap)
                    Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                    img_profile_pic_id.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            img_profile_pic_id.setImageBitmap(thumbnail)
            Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun logout(user_id: String) {
        val logout = ApiInterface.create()
        val call = logout.logoutApi(user_id)
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
                Toast.makeText(activity, "Something went wrong! Please try again", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ChangePasswordResponse>, response: Response<ChangePasswordResponse>) {
                if (response.body() != null) {

                    if (response.body()!!.status == "1") {


                        Preferences.getInstance().deleteAll()
                        RealmManager.getInstance().deleteAll()

                        XMPPSession.getInstance().logoff()

                        //(mViewpagerMainMenu.getAdapter() as ViewPagerMainMenuAdapter).clearFragmentsList()

                       /* val splashActivityIntent = Intent(this, SplashActivity::class.java)
                        splashActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        this.startActivity(splashActivityIntent)
                        finish()*/

                        dllSessionManager.logoutUser()

                        /******** Google Sign-In ********/
                        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .build()
                        val mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso!!)

                        mGoogleSignInClient!!.signOut()
                            .addOnCompleteListener(activity!!) {}


                        LoginManager.getInstance().logOut()


                    }
                }
            }

        })
    }



    fun pushNotification(push_check_stg:String,email_check_stg:String){
        Log.e("CHECKED_NOTIFICATION",push_check_stg+""+email_check_stg)
        val api = ApiInterface.create()
        val callApi = api.updateMatchPreferences(
            user_id,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            push_check_stg,
            email_check_stg

        )
        callApi.enqueue(object : Callback<UpdateMatchPreferenceResponse> {
            override fun onFailure(call: Call<UpdateMatchPreferenceResponse>, t: Throwable) {
                Log.e("PUSHNOTIFICATION", "UpdateMatchPreferenceResponse : error : ${t.toString()}")

                Toast.makeText(
                    context,
                    "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<UpdateMatchPreferenceResponse>,
                response: Response<UpdateMatchPreferenceResponse>
            ) {
                Log.d("NOTIFICATIONRESPONSE", "UpdateMatchPreferenceResponse: success: ${response.body()!!.result}")


                if (response.body() != null) {
                    if (response.body()!!.status.equals("1")) {

                        val user_info = response.body()!!.user_info
                        noti_checked_frm_api = user_info!!.push_notification_status
                        email_checked_frm_api = user_info!!.email_notification_status

                        sessionManager.CreateNotificationSession(noti_checked_frm_api,email_checked_frm_api)
                        Toast.makeText(
                            context, "Settings Saved Successfully!", Toast.LENGTH_SHORT
                        ).show()

                    } else {
                        Toast.makeText(
                            context, "Failed to Save!", Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        context,
                        "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                    ).show()
                }

            }
        })

    }


    private fun getMsgCounts(user_id: String) {
        val getCountsApi = ApiInterface.create()
        val religionCall = getCountsApi.getUsermessagesCountsApi(user_id = user_id)
        religionCall.enqueue(object : Callback<MessagecountResponse> {
            override fun onFailure(call: Call<MessagecountResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<MessagecountResponse>, response: Response<MessagecountResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "UserInterestCountResponse: success: ${response.body()!!.status}"
                    )

                    if (response.body()!!.status == "1") {

                        if(response.body()!!.totalcount=="0"){
                            tv_msg_count_id.visibility=View.GONE
                        }else{
                            tv_msg_count_id.visibility=View.VISIBLE
                            tv_msg_count_id.setText(response.body()!!.totalcount)
                        }


                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }



}