package divorcelovelounge.com.dll.Fragments

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.AbsoluteSizeSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import divorcelovelounge.com.dll.Adapters.LocationSearchAdapter
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Helper.InternetConnection
import divorcelovelounge.com.dll.MatchPreferencesWelcome
import divorcelovelounge.com.dll.MultiSliderLib.MultiSlider
import divorcelovelounge.com.dll.ProfileSetupWelcome
import divorcelovelounge.com.dll.QuizSetupWelcome
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.SwitchButtonLib.ToggleSwitch
import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem
import divorcelovelounge.com.dll.ViewPagerAdapters.CardPagerAdapterKotlin
import divorcelovelounge.com.dll.ViewPagerAdapters.ShadowTransformer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SecondFragment : Fragment() {
    lateinit var ll_filter_id: LinearLayout
    lateinit var filter_location: RelativeLayout
    lateinit var mCardAdapter: CardPagerAdapterKotlin
    lateinit var mViewPager: ViewPager
    lateinit var mCardShadowTransformer: ShadowTransformer
    private lateinit var filter_dialog: Dialog
    private lateinit var iv_close: ImageView
    private lateinit var tv_age_min: CustomTextView
    private lateinit var tv_age_max: CustomTextView
    private lateinit var tv_distance: CustomTextView
    private lateinit var tv_height_min: CustomTextView
    private lateinit var tv_height_max: CustomTextView
    private lateinit var tv_location: CustomTextView
    private lateinit var tv_done: CustomTextViewBold

    private lateinit var slider_age: MultiSlider
    private lateinit var slider_distance: MultiSlider
    private lateinit var slider_height: MultiSlider

    private lateinit var mts_switch: ToggleSwitch

    private lateinit var builder: AlertDialog.Builder
    private lateinit var searchEditText: EditText
    private lateinit var searchListView: ListView
    private lateinit var searchEmpty: TextView
    private lateinit var iv_alert_close: ImageView
    private lateinit var tv_alert_done: CustomTextViewBold
    private lateinit var locationSearchAdapter: LocationSearchAdapter
    private var location = ""
    private lateinit var ll_filter_gender: LinearLayout
    private lateinit var ll_filter_prefs: LinearLayout
    private lateinit var ll_filter_age: LinearLayout
    private lateinit var ll_filter_distance: LinearLayout
    private lateinit var ll_filter_height: LinearLayout
    private lateinit var mts_prefs_switch: ToggleSwitch
    private var state_code: String? = null
    private var user_id: String? = null
    private var my_preference: String? = null
    private var height_min: String? = null
    private var height_max: String? = null
    private var city: String? = null
    private var age_min: String? = null
    private var age_max: String? = null
    private var cm_max = ""
    private var cm_min = ""
    private var feet_min: Int? = null
    private var inch_min: Int? = null
    private var feet_max: Int? = null
    private var inch_max: Int? = null

    private lateinit var citiesDataList: ArrayList<CitiesDataResponse>
    private lateinit var citiesList: ArrayList<String>
    private val SCALE_FACTOR = 10.0

    lateinit var matches_array_list: ArrayList<CardItem>
    // lateinit var images_array: ArrayList<Onlyimagename>
    lateinit var images_array: ArrayList<String>
    lateinit var usename_array: ArrayList<String>

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var progress_id: ProgressBar

    lateinit var rl_main_id: CoordinatorLayout
    lateinit var tv_progress_number_id: CustomTextViewBold
    lateinit var ll_progress_id: LinearLayout
    lateinit var circularProgressbar: ProgressBar
    lateinit var tv_no_data_id: CustomTextView

    private var plan_status = ""
    private var plan_start_date = ""
    private var plan_expiry = ""
    private var server_date = ""
    private var chat_status = ""
    private var favorited_me_status = ""
    private var viewed_me_status = ""
    private var _hasLoadedOnce = false
    lateinit var location_details_list: java.util.HashMap<String, String>

    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        val rootView = inflater.inflate(R.layout.main_list_fragment, container, false)
        dllSessionManager = DllSessionManager(this.context!!)

        user_details = dllSessionManager.userDetails
        /*dllSessionManager.Imagesonly("")
        dllSessionManager.Nameonly("")*/
        initialize(rootView)
        return rootView
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (this.isVisible()) {
// we check that the fragment is becoming visible
            if (isVisibleToUser && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
        super.setUserVisibleHint(isVisibleToUser)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun initialize(rootView: View?) {
        state_code = user_details[DllSessionManager.USER_STATE]
        user_id = user_details[DllSessionManager.USER_ID]


/*String s= "Hello Everyone";
 SpannableString ss1=  new SpannableString(s);
 ss1.setSpan(new RelativeSizeSpan(2f), 0,5, 0); // set size
 ss1.setSpan(new ForegroundColorSpan(Color.RED), 0, 5, 0);// set color
 TextView tv= (TextView) findViewById(R.id.textview);
 tv.setText(ss1); */

        location_details_list = dllSessionManager!!.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()



        UserLiveStaus()

        val stg_percent = "%"
        val ssl = SpannableString(stg_percent)
        ssl.setSpan(AbsoluteSizeSpan(1, true), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)


        rl_main_id = rootView!!.findViewById(R.id.rl_main_id)
        ll_filter_id = rootView!!.findViewById(R.id.ll_filter_id)
        tv_progress_number_id = rootView!!.findViewById(R.id.tv_progress_number_id)
        ll_progress_id = rootView!!.findViewById(R.id.ll_progress_id)
        circularProgressbar = rootView.findViewById(R.id.circularProgressbar)
        tv_no_data_id = rootView.findViewById(R.id.tv_no_data_id)

        tv_progress_number_id.setText("35")
        progress_id = rootView!!.findViewById(R.id.progress_id)
        progress_id.visibility = View.VISIBLE
        mViewPager = rootView.findViewById(R.id.viewPager)

        filter_dialog = Dialog(this.activity!!)
        filter_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filter_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        filter_dialog.setContentView(R.layout.filter_dialog)
        val filter_window = filter_dialog.window
        filter_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        mts_switch = filter_dialog.findViewById(R.id.mts_switch)
        iv_close = filter_dialog.findViewById(R.id.iv_close)
        slider_height = filter_dialog.findViewById(R.id.slider_height)
        slider_distance = filter_dialog.findViewById(R.id.slider_distance)
        slider_age = filter_dialog.findViewById(R.id.slider_age)
        tv_done = filter_dialog.findViewById(R.id.tv_done)
        tv_location = filter_dialog.findViewById(R.id.tv_location)
        tv_height_max = filter_dialog.findViewById(R.id.tv_height_max)
        tv_height_min = filter_dialog.findViewById(R.id.tv_height_min)
        tv_distance = filter_dialog.findViewById(R.id.tv_distance)
        tv_age_min = filter_dialog.findViewById(R.id.tv_age_min)
        tv_age_max = filter_dialog.findViewById(R.id.tv_age_max)
        filter_location = filter_dialog.findViewById(R.id.filter_location)

        ll_filter_gender = filter_dialog.findViewById(R.id.ll_filter_gender)
        ll_filter_prefs = filter_dialog.findViewById(R.id.ll_filter_prefs)
        ll_filter_age = filter_dialog.findViewById(R.id.ll_filter_age)
        ll_filter_distance = filter_dialog.findViewById(R.id.ll_filter_distance)
        ll_filter_height = filter_dialog.findViewById(R.id.ll_filter_height)
        mts_prefs_switch = filter_dialog.findViewById(R.id.mts_prefs_switch)


        ll_filter_gender.visibility = View.GONE
        ll_filter_distance.visibility = View.GONE

        val prefsArray = resources.getStringArray(R.array.myPrefs)
        val genderArray = resources.getStringArray(R.array.gender)

        tv_location.text = ""


        /* Alert dialog for location search - Start */
        val location_dialog = Dialog(this.activity!!)
        location_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        location_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        location_dialog.setContentView(R.layout.alert_dialog_location_search)
        val location_filter_window = location_dialog.window
        location_filter_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )


        searchEditText = location_dialog.findViewById(R.id.searchEditText)
        searchListView = location_dialog.findViewById(R.id.searchListView)
        searchEmpty = location_dialog.findViewById(R.id.searchEmpty)
        iv_alert_close = location_dialog.findViewById(R.id.iv_alert_close)
        tv_alert_done = location_dialog.findViewById(R.id.tv_alert_done)

        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {
                if (char.isNotEmpty()) {
                    locationSearchAdapter.filter.filter(char.toString())
                } else {
                    locationSearchAdapter.filter.filter("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        searchEditText.hint = activity!!.resources.getString(R.string.search_location)
        searchListView.emptyView = searchEmpty

        searchListView.setOnItemClickListener { adapterView, view, position, l ->

            try {
                location = locationSearchAdapter.getItem(position).toString()
                locationSearchAdapter.setSelectedIndex(position)
                Log.d("SecondFragment", "location : $location")
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }
        iv_alert_close.setOnClickListener {
            location_dialog.dismiss()
        }
        tv_alert_done.setOnClickListener {
            tv_location.text = location
            location_dialog.dismiss()
        }

        /* Alert dialog for location search - End */

        mts_switch.setCheckedPosition(0)
        mts_prefs_switch.setCheckedPosition(0)
        slider_distance.removeThumb(1)

        slider_age.min = 18
        slider_age.max = 100

        slider_height.setMin(40, true, true)
        slider_height.setMax(80, true, true)
        slider_height.step = 5

        // set drawable for both thumbs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            slider_age.getThumb(0).thumb =
                resources.getDrawable(R.drawable.seekbar_dot, activity!!.theme)
            slider_age.getThumb(1).thumb =
                resources.getDrawable(R.drawable.seekbar_dot, activity!!.theme)
            slider_height.getThumb(0).thumb =
                resources.getDrawable(R.drawable.seekbar_dot, activity!!.theme)
            slider_height.getThumb(1).thumb =
                resources.getDrawable(R.drawable.seekbar_dot, activity!!.theme)
            slider_distance.getThumb(0).thumb =
                resources.getDrawable(R.drawable.seekbar_dot, activity!!.theme)
        } else {
            slider_age.getThumb(0).thumb =
                ContextCompat.getDrawable(activity!!, R.drawable.seekbar_dot)
            slider_age.getThumb(1).thumb =
                ContextCompat.getDrawable(activity!!, R.drawable.seekbar_dot)
            slider_height.getThumb(0).thumb =
                ContextCompat.getDrawable(activity!!, R.drawable.seekbar_dot)
            slider_height.getThumb(1).thumb =
                ContextCompat.getDrawable(activity!!, R.drawable.seekbar_dot)
            slider_distance.getThumb(0).thumb =
                ContextCompat.getDrawable(activity!!, R.drawable.seekbar_dot)
        }

        /* Listeners - Start */
        mts_prefs_switch.onChangeListener = object : ToggleSwitch.OnChangeListener {
            override fun onToggleSwitchChanged(position: Int) {

                my_preference = prefsArray[position]
            }
        }

        mts_switch.onChangeListener = object : ToggleSwitch.OnChangeListener {
            override fun onToggleSwitchChanged(position: Int) {

                // Toast.makeText(activity!!, "Checked: " + genderArray[position], Toast.LENGTH_SHORT).show()
            }
        }

        slider_age.setOnThumbValueChangeListener(object : MultiSlider.SimpleChangeListener() {
            override fun onValueChanged(
                multiSlider: MultiSlider, thumb: MultiSlider.Thumb, thumbIndex: Int, value: Int
            ) {
                if (thumbIndex == 0) {
                    tv_age_min.text = value.toString()
                    // Toast.makeText(activity, "Index: " + thumbIndex + "Age: " + value.toString(), Toast.LENGTH_SHORT).show()
                } else {
                    tv_age_max.text = value.toString()
                    // Toast.makeText(activity, "Index: " + thumbIndex + "Age: " + value.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        })

        slider_height.setOnThumbValueChangeListener(object : MultiSlider.SimpleChangeListener() {
            override fun onValueChanged(
                multiSlider: MultiSlider,
                thumb: MultiSlider.Thumb,
                thumbIndex: Int,
                value: Int
            ) {
                if (thumbIndex == 0) {
                    val min = value / (SCALE_FACTOR)
                    tv_height_min.text = min.toString()

                } else {
                    val max = value / (SCALE_FACTOR)
                    tv_height_max.text = max.toString()

                }
            }
        })

        slider_distance.setOnThumbValueChangeListener(object : MultiSlider.SimpleChangeListener() {
            override fun onValueChanged(
                multiSlider: MultiSlider,
                thumb: MultiSlider.Thumb,
                thumbIndex: Int,
                value: Int
            ) {
                tv_distance.text = value.toString()

            }
        })
        /* Listeners - End */


        my_preference = prefsArray[mts_prefs_switch.checkedPosition!!]
        height_min = tv_height_min.text.toString()
        height_max = tv_height_max.text.toString()
        city = tv_location.text.toString()
        age_min = tv_age_min.text.toString()
        age_max = tv_age_max.text.toString()
        feet_min = height_min!!.toFloat().toInt()
        inch_min = (height_min!!.toFloat().toInt() % 1)
        feet_max = height_max!!.toFloat().toInt()
        inch_max = (height_max!!.toFloat().toInt() % 1)
        cm_min =
            feetToCentimeters(feet_min.toString(), inch_min.toString()).toFloat().toInt().toString()
        cm_max =
            feetToCentimeters(feet_max.toString(), inch_max.toString()).toFloat().toInt().toString()

        /* OnClickListeners - Start */
        ll_filter_id.setOnClickListener {
            progress_id.visibility = View.VISIBLE
            citiesDataList = ArrayList()
            citiesList = ArrayList()

            val cityApi = ApiInterface.create()
            val cityCall = cityApi.getCities(state_code!!)
            cityCall.enqueue(object : Callback<CitiesResponse> {
                override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                    Log.e("SecondFragment", "CitiesResponse : error : ${t.toString()}")
                    Toast.makeText(context, "Please Check Your Connection", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<CitiesResponse>,
                    response: Response<CitiesResponse>
                ) {
                    Log.d("SecondFragment", "CitiesResponse: success: ${response.body()!!.result}")
                    progress_id.visibility = View.GONE
                    if (response.body()!!.status.equals("1")) {
                        citiesDataList = response.body()!!.data!!

                        for (i in 0 until citiesDataList.size) {
                            citiesList.add(citiesDataList[i].city_name)
                        }
                        locationSearchAdapter = LocationSearchAdapter(activity!!, citiesList)
                        searchListView.adapter = locationSearchAdapter

                    } else if (response.body()!!.status.equals("2")) {
                        citiesDataList = ArrayList()

                    }

                }
            })

            Log.d(
                "SecondFragment",
                "default values : my_preference $my_preference, height_min $height_min --- $cm_min, height_max $height_max --- $cm_max, city $city, age_min $age_min, age_max $age_max"
            )
            filter_dialog.show()

        }

        filter_location.setOnClickListener {
            location_dialog.show()
        }

        iv_close.setOnClickListener { filter_dialog.dismiss() }
        tv_done.setOnClickListener {
            //            mViewPager = rootView.findViewById(R.id.viewPager)

            my_preference = prefsArray[mts_prefs_switch.checkedPosition!!]
            height_min = tv_height_min.text.toString()
            height_max = tv_height_max.text.toString()
            city = tv_location.text.toString()
            age_min = tv_age_min.text.toString()
            age_max = tv_age_max.text.toString()

            feet_min = height_min!!.toFloat().toInt()
            inch_min = (height_min!!.toFloat().toInt() % 1)
            feet_max = height_max!!.toFloat().toInt()
            inch_max = (height_max!!.toFloat().toInt() % 1)
            cm_min = feetToCentimeters(feet_min.toString(), inch_min.toString()).toFloat().toInt()
                .toString()
            cm_max = feetToCentimeters(feet_max.toString(), inch_max.toString()).toFloat().toInt()
                .toString()

            Log.d(
                "SecondFragment",
                "selected values : my_preference $my_preference, height_min $height_min --- $cm_min, height_max $height_max --- $cm_max, city $city, age_min $age_min, age_max $age_max"
            )
            /* Api call */


            if (InternetConnection.checkConnection(this.context!!)) {
                matchesApiCall(
                    user_id!!,
                    my_preference!!,
                    cm_min,
                    cm_max,
                    city!!,
                    age_min!!,
                    age_max!!
                )
                Log.d("REQUEST_user_id", user_details.get(DllSessionManager.USER_ID).toString())
            } else {
                // Internet Not Available...

                //Toast.makeText(context, "Please Check Your Connection", Toast.LENGTH_SHORT).show()
                val snackbar: Snackbar =
                    Snackbar.make(
                        rl_main_id,
                        "Please Check Your Connection",
                        Snackbar.LENGTH_INDEFINITE
                    )
                snackbar.show()
                snackbar.setAction("Reload", View.OnClickListener {

                    if (InternetConnection.checkConnection(this.context!!)) {
                        snackbar.dismiss()
                        matchesApiCall(user_id!!, "", "", "", "", "", "")
                    } else {
                        /*snackbar.dismiss()
                        snackbar.show()*/

                        val snackbar1: Snackbar =
                            Snackbar.make(
                                rl_main_id,
                                "Please Check Your Connection",
                                Snackbar.LENGTH_INDEFINITE
                            )
                        snackbar1.show()
                        snackbar1.setAction("Reload", View.OnClickListener {

                            if (InternetConnection.checkConnection(this.context!!)) {
                                snackbar.dismiss()
                                matchesApiCall(user_id!!, "", "", "", "", "", "")
                            } else {
                                snackbar.show()
                            }
                        })
                    }
                })

                /*


                // Set the Callback function.
                snackbar.setCallback(new Snackbar . Callback (){
                    @Override
                    public void onShown(Snackbar sb) {
                        Toast.makeText(SnackBarActivity.this, "Snackbar has been shown.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        Toast.makeText(SnackBarActivity.this, "Snackbar is dismessed.", Toast.LENGTH_SHORT).show();
                    }
                });

                // Show it at screen bottom.
                snackbar.show();*/
            }


            filter_dialog.dismiss()
            progress_id.visibility = View.VISIBLE
        }

        /* OnClickListeners - End */

        /* Checking existing membership plan - start */

        val apiService = ApiInterface.create()
        val call = apiService.membershipmyplan(user_id = user_id!!)

        call.enqueue(object : Callback<MemberMyPlanResponse> {
            override fun onResponse(
                call: Call<MemberMyPlanResponse>,
                response: retrofit2.Response<MemberMyPlanResponse>?
            ) {
                try {


                    if (response != null) {
                        Log.d(
                            "SecondFragment",
                            "MemberMyPlanResponse success status ${response.body()!!.status}"
                        )

                        plan_status = response.body()!!.status
                        if (response.body()!!.status == "1") {
                            val plan_details = response.body()!!.plandetails

                            if (plan_details!!.plan_start_date == "") {
                                plan_start_date = "0"
                            } else {
                                plan_start_date = plan_details.plan_start_date
                            }
                            if (plan_details.plan_expiry == "") {
                                plan_expiry = "0"
                            } else {
                                plan_expiry = plan_details.plan_expiry
                            }
                            if (plan_details.chat == "") {
                                chat_status = "0"
                            } else {
                                chat_status = plan_details.chat
                            }
                            if (plan_details.server_date == "") {
                                server_date = "0"
                            } else {
                                server_date = plan_details.server_date
                            }
                            if (plan_details.favoritedme == "") {
                                favorited_me_status = "0"
                            } else {
                                favorited_me_status = plan_details.favoritedme
                            }
                            if (plan_details.viewedme == "") {
                                viewed_me_status = "0"
                            } else {
                                viewed_me_status = plan_details.viewedme
                            }

                            dllSessionManager.updateMemberShipDetails(
                                plan_status,
                                plan_start_date,
                                plan_expiry,
                                server_date,
                                chat_status,
                                favorited_me_status,
                                viewed_me_status
                            )
                        } else if (response.body()!!.status == "2") {
                            dllSessionManager.updateMemberShipDetails(
                                plan_status,
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            )
                            Log.d(
                                "SecondFragment",
                                "MemberMyPlanResponse status: ${response.body()!!.status} result: ${response.body()!!.result}"
                            )
                            Toast.makeText(activity, response.body()!!.result, Toast.LENGTH_SHORT)
                                .show()
                        } else {

                        }
                    } else {
                        Toast.makeText(activity, "Something went wrong!", Toast.LENGTH_SHORT).show()
                    }


                } catch (e: Exception) {

                }

            }


            override fun onFailure(call: Call<MemberMyPlanResponse>, t: Throwable) {
                Log.d("SecondFragment", "MemberMyPlanResponse error $t")
            }
        })

        /* Checking existing membership plan - end */

        /*API Call*/
        if (InternetConnection.checkConnection(this.context!!)) {
            matchesApiCall(user_id!!, "", "", "", "", "", "")
            Log.d("REQUEST_user_id", user_details.get(DllSessionManager.USER_ID).toString())
        } else {
            // Internet Not Available...

            //Toast.makeText(context, "Please Check Your Connection", Toast.LENGTH_SHORT).show()
            val snackbar: Snackbar =
                Snackbar.make(
                    rl_main_id,
                    "Please Check Your Connection",
                    Snackbar.LENGTH_INDEFINITE
                )
            snackbar.show()
            snackbar.setAction("Reload", View.OnClickListener {

                if (InternetConnection.checkConnection(this.context!!)) {
                    snackbar.dismiss()
                    matchesApiCall(user_id!!, "", "", "", "", "", "")
                } else {
                    /*snackbar.dismiss()
                    snackbar.show()*/

                    val snackbar1: Snackbar =
                        Snackbar.make(
                            rl_main_id,
                            "Please Check Your Connection",
                            Snackbar.LENGTH_INDEFINITE
                        )
                    snackbar1.show()
                    snackbar1.setAction("Reload", View.OnClickListener {

                        if (InternetConnection.checkConnection(this.context!!)) {
                            snackbar.dismiss()
                            matchesApiCall(user_id!!, "", "", "", "", "", "")
                        } else {
                            snackbar.show()
                        }
                    })
                }
            })


        }

        if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("1")) {
            ll_progress_id.visibility = View.VISIBLE
            tv_progress_number_id.text = "25"
            circularProgressbar.setProgress(1)
        } else if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("2")) {
            ll_progress_id.visibility = View.VISIBLE
            tv_progress_number_id.text = "50"
            circularProgressbar.setProgress(2)
        } else if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("3")) {
            ll_progress_id.visibility = View.VISIBLE
            tv_progress_number_id.text = "75"
            circularProgressbar.setProgress(3)
        } else {
            tv_progress_number_id.text = "100"
            ll_progress_id.visibility = View.GONE
            circularProgressbar.setProgress(4)
            val layoutParams = ll_filter_id.getLayoutParams() as RelativeLayout.LayoutParams
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
            ll_filter_id.setLayoutParams(layoutParams)
        }
        ll_progress_id.setOnClickListener {
            if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("1")) {
                val home_intent = Intent(context, ProfileSetupWelcome::class.java)
                startActivity(home_intent)

            } else if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("2")) {
                val home_intent = Intent(context, MatchPreferencesWelcome::class.java)
                startActivity(home_intent)

            } else if (user_details.get(DllSessionManager.USER_SIGNUP_STATUS).equals("3")) {
                val home_intent = Intent(context, QuizSetupWelcome::class.java)
                startActivity(home_intent)

            }
        }


    }

    private fun matchesApiCall(
        userId: String,
        preference: String,
        height_min_cm: String,
        height_max_cm: String,
        city: String,
        age_min: String,
        age_max: String
    ) {

        mCardAdapter = CardPagerAdapterKotlin(this.context!!, user_id!!)
        matches_array_list = ArrayList()
        matches_array_list.clear()
        images_array = ArrayList()
        images_array.clear()

        usename_array = ArrayList()
        usename_array.clear()


        Log.d(
            "SecondFragment",
            "data : $userId, $preference, $height_max_cm, $height_min_cm, $city, $age_min, $age_max"
        )




        // Internet Available...
        val apiService = ApiInterface.create()
        val call = apiService.matchesApi(
            userId,
            preference,
            height_max_cm,
            height_min_cm,
            city,
            age_min,
            age_max,
            gps_county_stg,gps_state_stg,gps_city_stg/*,"Home"*/
        )

        call.enqueue(object : Callback<CheckMatchesResponse> {
            override fun onResponse(
                call: Call<CheckMatchesResponse>,
                response: retrofit2.Response<CheckMatchesResponse>?
            ) {

                try {
                    //  if (response != null) {
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                            "success"
                        )
                    ) {
                        progress_id.visibility = View.GONE


                        val user_data = response.body()!!.data


                        val list: ArrayList<CardItem>? = user_data!!.match_data
                        // val favourites_list: ArrayList<All_Matches>? = user_data!!.my_favourites

                        for (i in 0 until list!!.size) {
                            matches_array_list.add(list.get(i))

                            if (matches_array_list.get(i).username == "") {
                                usename_array.add("0")
                                images_array.add(matches_array_list.get(i).photo1)
                            } else {
                                usename_array.add(matches_array_list.get(i).username)
                                images_array.add(matches_array_list.get(i).photo1)
                            }


                        }
                        Log.w(
                            "SecondFragment",
                            "matches_array_list: " + matches_array_list.size + "---" + matches_array_list.toString()
                        )

                        dllSessionManager.Imagesonly(images_array.toString())
                        dllSessionManager.Nameonly(usename_array.toString())

                        Log.e("matches_array_list_ll", images_array.size.toString())

                        if (matches_array_list.size.equals(0)) {
                            tv_no_data_id.visibility = View.VISIBLE
                        } else {
                            tv_no_data_id.visibility = View.GONE
                        }


                    }
                    if (response.body()!!.status.equals("status")) {
                        Toast.makeText(context, response.body()!!.result, Toast.LENGTH_SHORT)
                            .show()
                    }
                    //  }


                    for (k in 0 until matches_array_list.size) {

                        if (matches_array_list.get(k).view_type.equals("user")) {
                            mCardAdapter.addCardItem(
                                CardItem(
                                    matches_array_list.get(k).id,
                                    matches_array_list.get(k).date_id,
                                    matches_array_list.get(k).gender,
                                    matches_array_list.get(k).looking_for,
                                    matches_array_list.get(k).first_name,
                                    matches_array_list.get(k).username,
                                    matches_array_list.get(k).email,
                                    matches_array_list.get(k).zipcode,
                                    matches_array_list.get(k).state,
                                    matches_array_list.get(k).referal_from,
                                    matches_array_list.get(k).children,
                                    matches_array_list.get(k).city,
                                    matches_array_list.get(k).dob,
                                    matches_array_list.get(k).ethnicity,
                                    matches_array_list.get(k).religion,
                                    matches_array_list.get(k).denomination,
                                    matches_array_list.get(k).education,
                                    matches_array_list.get(k).occupation,
                                    matches_array_list.get(k).income,
                                    matches_array_list.get(k).smoke,
                                    matches_array_list.get(k).drink,
                                    matches_array_list.get(k).height_ft,
                                    matches_array_list.get(k).height_inc,
                                    matches_array_list.get(k).height_cms,
                                    matches_array_list.get(k).passionate,
                                    matches_array_list.get(k).leisure_time,
                                    matches_array_list.get(k).thankful_1,
                                    matches_array_list.get(k).thankful_2,
                                    matches_array_list.get(k).thankful_3,
                                    matches_array_list.get(k).partner_age_min,
                                    matches_array_list.get(k).partner_age_max,
                                    matches_array_list.get(k).partner_match_distance,
                                    matches_array_list.get(k).partner_match_search,
                                    matches_array_list.get(k).partner_ethnicity,
                                    matches_array_list.get(k).partner_religion,
                                    matches_array_list.get(k).partner_denomination,
                                    matches_array_list.get(k).partner_education,
                                    matches_array_list.get(k).partner_occupation,
                                    matches_array_list.get(k).partner_smoke,
                                    matches_array_list.get(k).partner_drink,
                                    matches_array_list.get(k).photo1,
                                    matches_array_list.get(k).photo1_approve,
                                    matches_array_list.get(k).photo2,
                                    matches_array_list.get(k).photo2_approve,
                                    matches_array_list.get(k).photo3,
                                    matches_array_list.get(k).photo3_approve,
                                    matches_array_list.get(k).photo4,
                                    matches_array_list.get(k).photo4_approve,
                                    matches_array_list.get(k).photo5,
                                    matches_array_list.get(k).photo5_approve,
                                    matches_array_list.get(k).photo6,
                                    matches_array_list.get(k).photo6_approve,
                                    matches_array_list.get(k).photo7,
                                    matches_array_list.get(k).photo7_approve,
                                    matches_array_list.get(k).photo8,
                                    matches_array_list.get(k).photo8_approve,
                                    matches_array_list.get(k).photo9,
                                    matches_array_list.get(k).photo9_approve,
                                    matches_array_list.get(k).photo10,
                                    matches_array_list.get(k).photo10_approve,
                                    matches_array_list.get(k).status,
                                    matches_array_list.get(k).signup_step,
                                    matches_array_list.get(k).fav_id,
                                    matches_array_list.get(k).fav_from,
                                    matches_array_list.get(k).fav_to,
                                    matches_array_list.get(k).interest_from,
                                    matches_array_list.get(k).interest_to,
                                    matches_array_list.get(k).interest_status,
                                    plan_status,
                                    matches_array_list.get(k).view_type,
                                "",
                                    ""
                                )
                            )
                        }else{
                            mCardAdapter.addCardItem(
                                CardItem(
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "", "", "", "", "", "", "","","","",
                                    "","","",  "","","",
                                    "","","","","","","","","","","",
                                    "","","","","","","","","","","","","","","","","","",
                                    "","","","","","",
                                    matches_array_list.get(k).view_type,
                                    matches_array_list.get(k).add_image,
                                    matches_array_list.get(k).add_link
                                )
                            )
                        }
                    }



                    mCardShadowTransformer = ShadowTransformer(mViewPager, mCardAdapter)
                    mViewPager.adapter = mCardAdapter
                    mCardAdapter!!.notifyDataSetChanged()
                    mViewPager.setPageTransformer(false, mCardShadowTransformer)
                    mViewPager.offscreenPageLimit = matches_array_list!!.size
                    //mCardShadowTransformer.enableScaling(true)


                } catch (e: Exception) {
                    Log.e("SECONDEXCEPTION", e.toString())
                }
            }

            override fun onFailure(call: Call<CheckMatchesResponse>, t: Throwable) {
                Log.w("Result_Order_details_ll", t.toString())
                //Toast.makeText(context, "Please Check Your Connection", Toast.LENGTH_SHORT).show()
                val snackbar: Snackbar =
                    Snackbar.make(
                        rl_main_id,
                        "Please Check Your Connection",
                        Snackbar.LENGTH_INDEFINITE
                    )
                snackbar.show()
                snackbar.setAction("Reload", View.OnClickListener {

                    if (InternetConnection.checkConnection(context!!)) {
                        snackbar.dismiss()
                        matchesApiCall(user_id!!, "", "", "", "", "", "")
                    } else {
                        /*snackbar.dismiss()
                        snackbar.show()*/

                        val snackbar1: Snackbar =
                            Snackbar.make(
                                rl_main_id,
                                "Please Check Your Connection",
                                Snackbar.LENGTH_INDEFINITE
                            )
                        snackbar1.show()
                        snackbar1.setAction("Reload", View.OnClickListener {

                            if (InternetConnection.checkConnection(context!!)) {
                                snackbar.dismiss()
                                matchesApiCall(user_id!!, "", "", "", "", "", "")
                            } else {
                                snackbar.show()
                            }
                        })
                    }
                })
            }
        })


    }

    private fun feetToCentimeters(feet: String, inch: String): Long {
        var heightInFeet = 0.0
        var heightInInches = 0.0
        try {
            heightInFeet = feet.toDouble()
            heightInInches = inch.toDouble()

        } catch (nfe: NumberFormatException) {
            nfe.printStackTrace()
        }
        val cm = (heightInFeet * 30.48) + (heightInInches * 2.54)
        val cms = Math.round(cm)
        return cms
    }


    fun UserLiveStaus() {
        val apiService = ApiInterface.create()
        val call =
            apiService.checkMemberStatusApi(user_details.get(DllSessionManager.USER_ID).toString())

        call.enqueue(object : Callback<PromoCodeResponse> {
            override fun onResponse(
                call: Call<PromoCodeResponse>,
                response: retrofit2.Response<PromoCodeResponse>?
            ) {
                // Log.d("REQUEST", response!!.body()!!.status + "")
                if (!response!!.body()!!.status.equals("1")) {
                    accountstatus()
                } else {
                    //data
                }
            }

            override fun onFailure(call: Call<PromoCodeResponse>, t: Throwable) {
                Log.e("Result_Order_details", t.toString())
            }
        })
    }

    fun accountstatus() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Alert!")
            .setMessage("The account you are Trying to view has been suspended. \nPlease Contact Customer Support \nsupport@divorcelovelounge.com")
            .setCancelable(false)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                    dllSessionManager.logoutUser()

                    /******** Google Sign-In ********/
                    val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .build()
                    val mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso!!)

                    mGoogleSignInClient!!.signOut()
                        .addOnCompleteListener(activity!!) {}
                }
            })

        val alertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    fun refresh() {
        matchesApiCall(user_id!!, "", "", "", "", "", "")
    }

}