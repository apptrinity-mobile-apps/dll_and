package divorcelovelounge.com.dll.Fragments

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import divorcelovelounge.com.dll.Adapters.MyAdapter
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.UserInterestCountResponse
import divorcelovelounge.com.dll.Chat.Activities.ChatsListsFragment
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewFavoriteFragment: Fragment() {


    lateinit var new_tabLayout: TabLayout
    lateinit var new_view_pager: ViewPager

    private var user_id = ""
    private var interestsMeCount = "0"
    private var favouritedMeCount = "0"


    private lateinit var dllSessionManager: DllSessionManager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.favorites_fragment_new, container, false)

        dllSessionManager = DllSessionManager(activity!!)
        if (dllSessionManager.userDetails[DllSessionManager.USER_ID].toString() != "") {
            user_id = dllSessionManager.userDetails[DllSessionManager.USER_ID].toString()
        }

        new_view_pager=view.findViewById(R.id.new_view_pager)

        setupViewPager(new_view_pager)

        new_tabLayout =view. findViewById(R.id.new_tabLayout) as TabLayout
        new_tabLayout.setupWithViewPager(new_view_pager)
        getCounts(user_id,new_view_pager)

        return view
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFrag(MyFavourite(), "My Favourites")
        adapter.addFrag(FavouriteMe(), "Favourited Me")
        adapter.addFrag(MyInterest(), "My Interest")
        adapter.addFrag(InterestMe(), "Interested Me")
        adapter.addFrag(ViewedMeFragment(), "Viewed Me")
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentPagerAdapter(manager) {
        private val mFragmentList = java.util.ArrayList<Fragment>()
        private val mFragmentTitleList = java.util.ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
    private fun getCounts(user_id: String,viewPager: ViewPager) {
        val getCountsApi = ApiInterface.create()
        val religionCall = getCountsApi.getUserCountsApi(user_id = user_id)
        religionCall.enqueue(object : Callback<UserInterestCountResponse> {
            override fun onFailure(call: Call<UserInterestCountResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "UserInterestCountResponse : error : ${t.toString()}")


                if (favouritedMeCount.equals("0")) {
                    favouritedMeCount = ""
                } else {
                    favouritedMeCount = "($favouritedMeCount)"
                }

                if (interestsMeCount.equals("0")) {
                    interestsMeCount = ""
                } else {
                    interestsMeCount = "($interestsMeCount)"
                }
                // set tabs if error in response
                val adapter = ViewPagerAdapter(childFragmentManager)
                adapter.addFrag(MyFavourite(), "My Favourites")
                adapter.addFrag(FavouriteMe(), "Favourited Me"+ favouritedMeCount)
                adapter.addFrag(MyInterest(), "My Interest")
                adapter.addFrag(InterestMe(), "Interested Me"+ interestsMeCount)
                adapter.addFrag(ViewedMeFragment(), "Viewed Me")
                viewPager.adapter = adapter

               /* tabLayout.addTab(tabLayout.newTab().setText("My Favourites"))
                tabLayout.addTab(tabLayout.newTab().setText("Favourited Me" + favouritedMeCount))
                tabLayout.addTab(tabLayout.newTab().setText("My Interest"))
                tabLayout.addTab(tabLayout.newTab().setText("Interested Me" + interestsMeCount))
                tabLayout.addTab(tabLayout.newTab().setText("Viewed Me"))
                val adapter = MyAdapter(activity!!.supportFragmentManager, tabLayout.tabCount)
                viewPager.adapter = adapter*/
            }

            override fun onResponse(
                call: Call<UserInterestCountResponse>, response: Response<UserInterestCountResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "UserInterestCountResponse: success: ${response.body()!!.status}"
                    )
                    if (response.body()!!.status == "1") {
                        val data = response.body()!!.allcounts
                        interestsMeCount = data!!.interestscount
                        //favouritesCount = data.favouritescount
                        favouritedMeCount = data.favouritedmecount

                        Log.d(
                            "FavouritesFragment",
                            interestsMeCount + "----" + favouritedMeCount
                        )

                    } else {
                        Toast.makeText(
                            activity, "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (favouritedMeCount.equals("0")) {
                        favouritedMeCount = ""
                    } else {
                        favouritedMeCount = "($favouritedMeCount)"
                    }

                    if (interestsMeCount.equals("0")) {
                        interestsMeCount = ""
                    } else {
                        interestsMeCount = "($interestsMeCount)"
                    }
                    // setting tabs
                    val adapter = ViewPagerAdapter(childFragmentManager)
                    adapter.addFrag(MyFavourite(), "My Favourites")
                    adapter.addFrag(FavouriteMe(), "Favourited Me"+ favouritedMeCount)
                    adapter.addFrag(MyInterest(), "My Interest")
                    adapter.addFrag(InterestMe(), "Interested Me"+ interestsMeCount)
                    adapter.addFrag(ViewedMeFragment(), "Viewed Me")
                    viewPager.adapter = adapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}