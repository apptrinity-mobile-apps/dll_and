package divorcelovelounge.com.dll.Fragments

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import divorcelovelounge.com.dll.Adapters.MyAdapter
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.UserInterestCountResponse
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavouritesFragment : Fragment() {

    lateinit var rv_favourite_list: RecyclerView
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    private var _hasLoadedOnce = false

    private var interestsMeCount = "0"
    private var favouritesCount = "0"
    private var favouritedMeCount = "0"
    private var user_id = ""

    private lateinit var dllSessionManager: DllSessionManager
    private lateinit var progressDialog: Dialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.favourite_layout, container, false)
        val mViewPager = view.findViewById<View>(R.id.viewPager_id) as ViewPager
        val mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        mViewPager.adapter = mSectionsPagerAdapter

        dllSessionManager = DllSessionManager(activity!!)
        if (dllSessionManager.userDetails[DllSessionManager.USER_ID].toString() != "") {
            user_id = dllSessionManager.userDetails[DllSessionManager.USER_ID].toString()
        }

        initialize(view)

        return view

    }


    private inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return MyFavourite.newInstance(1)
                1 -> return FavouriteMe.newInstance(2)
                2 -> return MyInterest.newInstance(3)
                3 -> return MyInterest.newInstance(4)
                else -> return MyInterest.newInstance(5)
            }

        }

        override fun getCount(): Int {
            // Show 4 total pages.
            return 5
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return "My Favourites"
                1 -> return "Nested 2"
                2 -> return "Nested 3"
                3 -> return "Nested 4"
                else -> return "Nested 3"
            }


        }
    }
    private fun initialize(rootView: View?) {

        tabLayout = rootView!!.findViewById(R.id.tabLayout)
        viewPager = rootView.findViewById(R.id.viewPager_id)

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        //viewPager.offscreenPageLimit=0
        /* val activity = activity as DashboardActivity?
         val results = activity!!.getMyData()
         val value1 = results.getString("message")
         Log.d("value1", "$value1")*/
        viewPager.currentItem = 0
        val bundle = this.arguments
        if (bundle != null) {
            if (bundle.getString("message") == "interest_me") {
                viewPager.currentItem = 3
            }
        }

        viewPager.offscreenPageLimit=4


        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
                Log.e("tab_selection", tab.position.toString())
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        getCounts(user_id)

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {

        if (this.isVisible) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !_hasLoadedOnce) {
                //NetCheck().execute()
                _hasLoadedOnce = true
            }
        }
        super.setUserVisibleHint(isVisibleToUser)
    }

    private fun getCounts(user_id: String) {
        val getCountsApi = ApiInterface.create()
        val religionCall = getCountsApi.getUserCountsApi(user_id = user_id)
        religionCall.enqueue(object : Callback<UserInterestCountResponse> {
            override fun onFailure(call: Call<UserInterestCountResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "UserInterestCountResponse : error : ${t.toString()}")


                if (favouritedMeCount.equals("0")) {
                    favouritedMeCount = ""
                } else {
                    favouritedMeCount = "($favouritedMeCount)"
                }

                if (interestsMeCount.equals("0")) {
                    interestsMeCount = ""
                } else {
                    interestsMeCount = "($interestsMeCount)"
                }
                // set tabs if error in response
                tabLayout.addTab(tabLayout.newTab().setText("My Favourites"))
                tabLayout.addTab(tabLayout.newTab().setText("Favourited Me" + favouritedMeCount))
                tabLayout.addTab(tabLayout.newTab().setText("My Interest"))
                tabLayout.addTab(tabLayout.newTab().setText("Interested Me" + interestsMeCount))
                tabLayout.addTab(tabLayout.newTab().setText("Viewed Me"))
                val adapter = MyAdapter(activity!!.supportFragmentManager, tabLayout.tabCount)
                viewPager.adapter = adapter
            }

            override fun onResponse(
                call: Call<UserInterestCountResponse>, response: Response<UserInterestCountResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "UserInterestCountResponse: success: ${response.body()!!.status}"
                    )
                    if (response.body()!!.status == "1") {
                        val data = response.body()!!.allcounts
                        interestsMeCount = data!!.interestscount
                        //favouritesCount = data.favouritescount
                        favouritedMeCount = data.favouritedmecount

                        Log.d(
                            "FavouritesFragment",
                            interestsMeCount + "----" + favouritedMeCount
                        )

                    } else {
                        Toast.makeText(
                            activity, "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (favouritedMeCount.equals("0")) {
                        favouritedMeCount = ""
                    } else {
                        favouritedMeCount = "($favouritedMeCount)"
                    }

                    if (interestsMeCount.equals("0")) {
                        interestsMeCount = ""
                    } else {
                        interestsMeCount = "($interestsMeCount)"
                    }
                    // setting tabs
                    tabLayout.addTab(tabLayout.newTab().setText("My Favourites"))
                    tabLayout.addTab(tabLayout.newTab().setText("Favourited Me" + favouritedMeCount))
                    tabLayout.addTab(tabLayout.newTab().setText("My Interest"))
                    tabLayout.addTab(tabLayout.newTab().setText("Interested Me" + interestsMeCount))
                    tabLayout.addTab(tabLayout.newTab().setText("Viewed Me"))
                    val adapter = MyAdapter(activity!!.supportFragmentManager, tabLayout.tabCount)
                    viewPager.adapter = adapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}