package divorcelovelounge.com.dll.SparkButtonHelper;

import android.widget.ImageView;


public interface SparkEventListener {
    void onEvent(ImageView button, boolean buttonState);
    void onEventAnimationEnd(ImageView button, boolean buttonState);
    void onEventAnimationStart(ImageView button, boolean buttonState);
}
