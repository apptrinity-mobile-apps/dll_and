package divorcelovelounge.com.dll

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.LinearLayout
import divorcelovelounge.com.dll.Helper.DllSessionManager

//RAVITEJA
class MatchPreferencesWelcome : AppCompatActivity() {

    private lateinit var ll_ready: LinearLayout
    private lateinit var ll_skip: LinearLayout
    private lateinit var iv_match_img: ImageView
    private lateinit var sessionManager: DllSessionManager
    private lateinit var userDetails: HashMap<String,String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_preferences_welcome)

        sessionManager = DllSessionManager(this@MatchPreferencesWelcome)
        ll_ready = findViewById(R.id.ll_ready)
        ll_skip = findViewById(R.id.ll_skip)
        iv_match_img= findViewById(R.id.iv_match_img)
        userDetails = sessionManager.userDetails

        iv_match_img.setImageResource(R.drawable.matches_img)

        ll_ready.setOnClickListener {
            val intent = Intent(this@MatchPreferencesWelcome, MatchPreferencesOne::class.java)
            startActivity(intent)

        }

        ll_skip.setOnClickListener {
            val builder = AlertDialog.Builder(this@MatchPreferencesWelcome)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to SKIP?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {



                        val photo1 = userDetails.get(DllSessionManager.PHOTO1)
                        if(photo1.equals("0")){
                            val home_intent = Intent(this@MatchPreferencesWelcome, ProfilePhotos::class.java)
                            //home_intent.putExtra("from_list","match_setup")
                            startActivity(home_intent)
                            finish()
                        }else {

                            val home_intent = Intent(this@MatchPreferencesWelcome, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "match_setup")
                            startActivity(home_intent)
                            finish()
                        }


                        /*val home_intent = Intent(this@MatchPreferencesWelcome, DashboardActivity::class.java)
                        home_intent.putExtra("from_list","match_setup")
                        startActivity(home_intent)
                        finish()*/
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }

    }

    /*override fun onBackPressed() {
        finish()
    }*/

}
