package divorcelovelounge.com.dll

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextWatcher
import android.util.Log
import android.view.View
import divorcelovelounge.com.dll.Adapters.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import android.text.Editable
import android.text.InputFilter
import android.view.KeyEvent
import android.view.ViewGroup
import android.view.Window
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.*
import divorcelovelounge.com.dll.Utils.InputFilterMinMax
import kotlin.collections.ArrayList

//RAVITEJA
class ProfileSetupOne : AppCompatActivity() {

    /* UI components start */
    // Layouts
    private lateinit var ll_location: LinearLayout
    private lateinit var ll_born: LinearLayout
    private lateinit var ll_height: LinearLayout
    private lateinit var ll_children: LinearLayout
    private lateinit var ll_ethnicity: LinearLayout
    private lateinit var ll_spirituality: LinearLayout
    private lateinit var ll_mTongue: LinearLayout
    private lateinit var ll_education: LinearLayout
    private lateinit var ll_occupation: LinearLayout
    private lateinit var ll_income: LinearLayout
    private lateinit var ll_smoke: LinearLayout
    private lateinit var ll_drink: LinearLayout
    private lateinit var ll_passionate_about: LinearLayout
    private lateinit var ll_leisure_time: LinearLayout
    private lateinit var ll_thankful: ScrollView

    // EditTexts
    private lateinit var et_search_location: EditText
    private lateinit var et_born_date: EditText
    private lateinit var et_height_ft: EditText
    private lateinit var et_height_inch: EditText
    private lateinit var et_height_cms: EditText
    private lateinit var et_search_mTongue: EditText
    private lateinit var et_search_occupation: EditText
    private lateinit var et_passionate_about: EditText
    private lateinit var et_leisure_time: EditText
    private lateinit var et_thankful_1: EditText
    private lateinit var et_thankful_2: EditText
    private lateinit var et_thankful_3: EditText

    // ListViews
    private lateinit var listView_location: ListView
    private lateinit var listView_ethnicity: ListView
    private lateinit var listView_education: ListView
    private lateinit var listView_occupation: ListView
    private lateinit var listView_income: ListView
    private lateinit var listView_smoking: ListView
    private lateinit var listView_drinking: ListView

    // GridViews
    private lateinit var gridView_spirituality: GridView
    private lateinit var gridView_mother_tongue: RecyclerView

    // TextViews
    private lateinit var tv_next: TextView
    private lateinit var tv_back: TextView
    private lateinit var tv_skip: TextView
    private lateinit var tv_ethnicity_qtion_id: CustomTextViewBold
    private lateinit var tv_occupation_qtion_id: CustomTextViewBold
    private lateinit var tv_income_qtion_id: CustomTextViewBold
    private lateinit var tv_education_qtion_id: CustomTextViewBold
    private lateinit var tv_smoke_qtion_id: CustomTextViewBold
    private lateinit var tv_drink_qtion_id: CustomTextViewBold
    private lateinit var location_empty: CustomTextView
    private lateinit var occupation_empty: CustomTextView
    private lateinit var mTongue_empty: CustomTextView

    //Toolbar
    private lateinit var customToolBar: Toolbar

    // Dialog
    private lateinit var progressDialog: Dialog

    // LinearLayoutManager
    private lateinit var gridLayoutManager: GridLayoutManager

    // ProgressBar
    private lateinit var horizontal_progress_bar: SignSeekBar

    // Spinner
    private lateinit var sp_children: Spinner
    /* UI components end */

    /* Adapters */
    private lateinit var locationAdapter: ProfileLocationAdapter
    private lateinit var drinkingAdapter: ProfileDrinkingAdapter
    private lateinit var educationAdapter: ProfileEducationAdapter
    private lateinit var occupationAdapter: ProfileOccupationAdapter
    private lateinit var ethnicityAdapter: ProfileEthnicityAdapter
    private lateinit var incomeAdapter: ProfileIncomeAdapter
    private lateinit var smokingAdapter: ProfileSmokingAdapter
    private lateinit var spiritualityAdapter: ProfileSpiritualityAdapter
    private lateinit var motherTongueAdapter: MotherTongueAdapter
    private lateinit var child_adapter: ArrayAdapter<String>

    /* ArrayLists */
    private lateinit var ethnicityList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var educationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var occupationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var incomeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var smokeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var drinkList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var citiesDataList: ArrayList<CitiesDataResponse>
    private lateinit var citiesList: ArrayList<String>

    private var questionsList: ArrayList<RegisterProfileSetupData>? = null
    private var religionDataList: ArrayList<ReligionData>? = null
    private var casteDataList: ArrayList<CasteData>? = null
    private var motherTongueDataList: ArrayList<MotherTongueData>? = null

    private lateinit var sessionManager: DllSessionManager

    // constants
    private val logTAG = "ProfileSetupOne"
    var page_count = 1
    var user_id: String? = null
    var state_code: String? = null
    var country: String? = null

    var children = ""
    var location = ""
    var occupation = ""
    var dob = ""
    var height_ft = ""
    var height_inc = ""
    var height_cms = ""
    var passionate = ""
    var leisure_time = ""
    var thankful_1 = ""
    var thankful_2 = ""
    var thankful_3 = ""
    var ethnicity = ""
    var religion = ""
    var education = ""
    var mother_tongue = ""
    var income = ""
    var smoke = ""
    var drink = ""

    var user_children = ""
    var user_city = ""
    var user_dob = ""
    var user_ethnicity = ""
    var user_religion = ""
    var user_education = ""
    var user_occupation = ""
    var user_income = ""
    var user_smoke = ""
    var user_drink = ""
    var user_height_ft = ""
    var user_height_inc = ""
    var user_height_cms = ""
    var user_passionate = ""
    var user_leisure_time = ""
    var user_thankful_1 = ""
    var user_thankful_2 = ""
    var user_thankful_3 = ""
    var user_mother_tongue = ""
    var signup_step = ""
    var cms_text = ""
    var religion_name = ""

    var pos = -1
    var pos_occ = -1


    lateinit var dllsesstionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_setup_one)
        title = ""


        dllsesstionManager = DllSessionManager(this)
        user_details = dllsesstionManager.userDetails

        /* setting custom tool bar */
        customToolBar = findViewById(R.id.customToolBar)
        tv_next = customToolBar.findViewById(R.id.tv_next)
        tv_back = customToolBar.findViewById(R.id.tv_back)

        setSupportActionBar(customToolBar)

        sessionManager = DllSessionManager(this@ProfileSetupOne)
        if (!sessionManager.userDetails[DllSessionManager.USER_ID].equals("")) {
            user_id = sessionManager.userDetails[DllSessionManager.USER_ID]
            state_code = sessionManager.userDetails[DllSessionManager.USER_STATE]
            country = sessionManager.userDetails[DllSessionManager.USER_COUNTRY]
        }

        Log.d(logTAG, "user ID : $user_id ===== state_code $state_code")

        initializeViews()

        progressDialog.show()
        tv_next.visibility = View.GONE
        listView_location.emptyView = location_empty
        listView_occupation.emptyView = occupation_empty

        /*if (country.equals("USA")) {
            getCityApi()
            et_search_location.hint = "Search your location"

        } else {*/

        if (progressDialog.isShowing)
            progressDialog.dismiss()
        ll_location.visibility = View.GONE
        tv_next.visibility = View.VISIBLE
        tv_back.visibility = View.VISIBLE
        location_empty.visibility = View.GONE
        et_search_location.visibility = View.GONE
        listView_location.visibility = View.GONE
        et_search_location.hint = "Enter your location"
        ll_height.visibility = View.VISIBLE
//        }

        getRegisterApi()
        getReligionApi()
        getMotherTongueApi()


        // val lengthFilter = InputFilter.LengthFilter(1)

        sp_children.adapter = child_adapter
        et_born_date.hint = resources.getString(R.string.date_format)
        et_height_ft.hint = resources.getString(R.string.ft)
        et_height_inch.hint = resources.getString(R.string.inches)
        et_height_cms.hint = resources.getString(R.string.cms)
        et_height_ft.filters =
            arrayOf<InputFilter>(InputFilterMinMax(0, 9), InputFilter.LengthFilter(1))
        et_height_inch.filters =
            arrayOf<InputFilter>(InputFilterMinMax(0, 11), InputFilter.LengthFilter(2))
        et_height_cms.filters =
            arrayOf<InputFilter>(InputFilterMinMax(0, 274), InputFilter.LengthFilter(3))


        // listeners
        listView_location.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                location = locationAdapter.getItem(position).toString()
                locationAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position location $position $location")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        listView_ethnicity.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                ethnicity = ethnicityList[position].answer
                ethnicityAdapter.setSelectedIndex(position)
                Log.d(
                    logTAG,
                    "position ethnicity $position $ethnicity" + "---" + ethnicityList[position].answer_id
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        gridView_spirituality.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            casteDataList!!.clear()
            try {
                religion = religionDataList!![position].religion_name
                spiritualityAdapter.setSelectedIndex(position)
                // val religion_id = religionDataList!![position].religion_id
                religion_name = religionDataList!![position].religion_name

                // progressDialog.show()

                // tv_next.visibility = View.GONE

                /* val casteApi = ApiInterface.create()
                 val callCasteApi = casteApi.getCasteApi(religion_id)

                 callCasteApi.enqueue(object : Callback<CasteResponse> {
                     override fun onFailure(call: Call<CasteResponse>, t: Throwable) {
                         Log.e(logTAG, "CasteResponse : error : ${t.toString()}")
                         Toast.makeText(
                             this@ProfileSetupOne,
                             "Something went wrong. Please try again!",
                             Toast.LENGTH_SHORT
                         ).show()
                     }

                     override fun onResponse(
                         call: Call<CasteResponse>, response: Response<CasteResponse>
                     ) {
                         Log.d(logTAG, "CasteResponse: success: ${response.body()!!.data}")
                         if (response.body()!!.status.equals("1")) {
                             casteDataList = response.body()!!.data

                             if (progressDialog.isShowing) {
                                 progressDialog.dismiss()
                             }
                             val castesList = ArrayList<String>()
                             for (i in 0 until casteDataList!!.size) {
                                 castesList.add(casteDataList!![i].caste_name)
                             }
                             tv_next.visibility = View.VISIBLE
                             denominationAdapter =
                                 ProfileDenominationAdapter(this@ProfileSetupOne, castesList)
                             gridView_denomination.adapter = denominationAdapter

                         }
                     }

                 })*/
                Log.d(logTAG, "position religion $position $religion")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


        gridView_mother_tongue.addOnItemTouchListener(
            RecyclerItemClickListener(this@ProfileSetupOne, gridView_mother_tongue,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        // pos = position
                        mother_tongue = motherTongueAdapter.getItem(position)
                        for (i in 0 until motherTongueDataList!!.size) {
                            if (mother_tongue == motherTongueDataList!![i].mtongue_name) {
                                pos = i
                            }
                        }
                        motherTongueAdapter.setSelectedIndex(position)
                        Log.d(logTAG, "selected mother tongue $pos $position $mother_tongue")
                    }
                })
        )

        /* gridView_denomination.setOnItemClickListener { parent, view, position, id ->
             view.isSelected = true
             try {
                 denomination = casteDataList!![position].caste_name
                 denominationAdapter.setSelectedIndex(position)
                 Log.d(logTAG, "position denomination $position $denomination")
             } catch (e: Exception) {
                 e.printStackTrace()
             }

         }*/

        listView_education.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                education = educationList[position].answer
                educationAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position education $position $education")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        listView_occupation.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                occupation = occupationAdapter.getItem(position).toString()
                occupationAdapter.setSelectedIndex(position)
                for (i in 0 until occupationList.size) {
                    if (occupation == occupationList[i].answer) {
                        pos_occ = i
                    }
                }
                Log.d(logTAG, "position occupation $position $pos_occ $occupation")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        listView_income.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                income = incomeList[position].answer
                incomeAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position income $position $income")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        listView_smoking.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                smoke = smokeList[position].answer
                smokingAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position smoke $position $smoke")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        listView_drinking.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                drink = drinkList[position].answer
                drinkingAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position drink $position $drink")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        et_born_date.setOnClickListener {
            Log.d(logTAG, "DateTime : MSMNAUIN")

            showDateTimePicker()
        }

        et_search_location.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {

                if (country.equals("USA")) {
                    if (char.isNotEmpty()) {
                        locationAdapter.filter.filter(char.toString())
                    } else {
                        locationAdapter.filter.filter("")
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        et_search_mTongue.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {


                if (char.isNotEmpty()) {
                    motherTongueAdapter.filter.filter(char.toString())
                    motherTongueAdapter.setSelectedIndex(-1)
                } else {
                    motherTongueAdapter.filter.filter("")
                    motherTongueAdapter.setSelectedIndex(pos)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        et_search_occupation.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {
                if (char.isNotEmpty()) {
                    occupationAdapter.filter.filter(char.toString())
                    occupationAdapter.setSelectedIndex(-1)
                } else {
                    occupationAdapter.filter.filter("")
                    occupationAdapter.setSelectedIndex(pos_occ)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        et_height_cms.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                et_height_cms.removeTextChangedListener(this)

                if (et_height_cms.text.toString() != "") {
                    val ht = et_height_cms.text.toString()
                    val ht_int = ht.toFloat().toInt()
                    et_height_cms.setText(ht_int.toString())
                    et_height_cms.setSelection(et_height_cms.text.length)
                }

                et_height_cms.addTextChangedListener(this)

                if (et_height_cms.isFocused) {
                    if (et_height_cms.text.toString() != "") {
                        cms_text = "1"
                        centimetersToFeet(et_height_cms.text.toString())
                    }
                }

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        et_height_ft.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                Log.e("CMSTEXTFT", cms_text)

                et_height_ft.removeTextChangedListener(this)

                if (et_height_ft.text.toString() != "") {
                    val ht = et_height_ft.text.toString()
                    val ht_int = ht.toFloat().toInt()
                    et_height_ft.setText(ht_int.toString())
                    et_height_ft.setSelection(et_height_ft.text.length)
                }

                et_height_ft.addTextChangedListener(this)

                if (cms_text != "1") {
                    if (et_height_ft.text.toString() != "" && et_height_inch.text.toString() != "") {
                        feetToCentimeters(
                            et_height_ft.text.toString(),
                            et_height_inch.text.toString()
                        )
                    }
                    if (s.length > 0) {
                        et_height_inch.requestFocus()
                    }
                }

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        et_height_inch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                Log.e("CMSTEXTINC", cms_text)

                et_height_inch.removeTextChangedListener(this)

                if (et_height_inch.text.toString() != "") {
                    val ht = et_height_inch.text.toString()
                    val ht_int = ht.toFloat().toInt()
                    et_height_inch.setText(ht_int.toString())
                    et_height_inch.setSelection(et_height_inch.text.length)

                }

                et_height_inch.addTextChangedListener(this)
                if (cms_text != "1") {
                    if (et_height_ft.text.toString() != "" && et_height_inch.text.toString() != "") {
                        feetToCentimeters(
                            et_height_ft.text.toString(),
                            et_height_inch.text.toString()
                        )
                    }
                }

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        sp_children.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                children = parentView.getItemAtPosition(position).toString()
                Log.d(logTAG, "children : $children")
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // do nothing
            }

        }

        tv_next.setOnClickListener {

            closeKeyboard()

            when (page_count) {

                /*1 -> {

                    if (country.equals("USA")) {
                        if (locationAdapter.getSelectedIndex() == -1) {
                            location = ""
                            Toast.makeText(
                                this@ProfileSetupOne,
                                "Please select your location!",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        } else {
                            ll_location.visibility = View.GONE
                            ll_born.visibility = View.GONE
                            ll_height.visibility = View.VISIBLE
                            ll_children.visibility = View.GONE
                            ll_spirituality.visibility = View.GONE
                            ll_mTongue.visibility = View.GONE
                            ll_ethnicity.visibility = View.GONE
                            ll_education.visibility = View.GONE
                            ll_occupation.visibility = View.GONE
                            ll_income.visibility = View.GONE
                            ll_smoke.visibility = View.GONE
                            ll_drink.visibility = View.GONE
                            ll_passionate_about.visibility = View.GONE
                            ll_leisure_time.visibility = View.GONE
                            ll_thankful.visibility = View.GONE

                            tv_back.visibility = View.VISIBLE

                            page_count = 2
                            //progress_value += 1f

                            horizontal_progress_bar.setProgress(5f)
                            horizontal_progress_bar.animate()


                        }
                    } else {
                        if (et_search_location.text.equals("")) {
                            location = ""
                            Toast.makeText(
                                this@ProfileSetupOne,
                                "Please enter your location!",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        } else {
                            ll_location.visibility = View.GONE
                            ll_born.visibility = View.GONE
                            ll_height.visibility = View.VISIBLE
                            ll_children.visibility = View.GONE
                            ll_spirituality.visibility = View.GONE
                            ll_mTongue.visibility = View.GONE
                            ll_ethnicity.visibility = View.GONE
                            ll_education.visibility = View.GONE
                            ll_occupation.visibility = View.GONE
                            ll_income.visibility = View.GONE
                            ll_smoke.visibility = View.GONE
                            ll_drink.visibility = View.GONE
                            ll_passionate_about.visibility = View.GONE
                            ll_leisure_time.visibility = View.GONE
                            ll_thankful.visibility = View.GONE

                            tv_back.visibility = View.VISIBLE

                            page_count = 2
                            //progress_value += 1f

                            horizontal_progress_bar.setProgress(5f)
                            horizontal_progress_bar.animate()


                        }
                    }


                }*/
/*2 -> {
if (et_born_date.text.toString() == "" || et_born_date.text.toString().isEmpty()) {
   Toast.makeText(this@ProfileSetupOne, "Please select your Date of Birth!", Toast.LENGTH_SHORT)
       .show()
} else {
   ll_location.visibility = View.GONE
   ll_born.visibility = View.GONE
   ll_height.visibility = View.VISIBLE
   ll_children.visibility = View.GONE
   ll_ethnicity.visibility = View.GONE
   ll_spirituality.visibility = View.GONE
   ll_denomination.visibility = View.GONE
   ll_education.visibility = View.GONE
   ll_occupation.visibility = View.GONE
   ll_income.visibility = View.GONE
   ll_smoke.visibility = View.GONE
   ll_drink.visibility = View.GONE
   ll_passionate_about.visibility = View.GONE
   ll_leisure_time.visibility = View.GONE
   ll_thankful.visibility = View.GONE

   tv_back.visibility = View.VISIBLE

   page_count = 3
   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
       horizontal_progress_bar.setProgress(10f)
   } else {
       horizontal_progress_bar.setProgress(10f)
       horizontal_progress_bar.animate()

   }
}

}*/
                1 -> {
                    val ft = et_height_ft.text.toString().trim()
                    val inch = et_height_inch.text.toString().trim()
                    val cm = et_height_cms.text.toString().trim()
                    if (ft == "" || inch == "" || cm == "") {
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please mention your height!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (ft == "0" && inch == "0" && cm == "0") {
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Height cannot be zero!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (cm == "0" || cm == "00" || cm == "000") {
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Height cannot be zero!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.VISIBLE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 2

                        horizontal_progress_bar.setProgress(5f)
                        horizontal_progress_bar.animate()

                    }

                }
                2 -> {

                    if (children == "Select Children") {
                        children = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select Children!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {

                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.VISIBLE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 3

                        horizontal_progress_bar.setProgress(10f)
                        horizontal_progress_bar.animate()


                    }

                }
                3 -> {

                    if (spiritualityAdapter.getSelectedIndex() == -1) {
                        religion = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your Religion!",
                            Toast.LENGTH_SHORT
                        ).show()

                    } else {


                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.VISIBLE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE


                        page_count = 4

                        horizontal_progress_bar.setProgress(15f)
                        horizontal_progress_bar.animate()


                    }

                }
                4 -> {


                    if (motherTongueAdapter.getSelectedIndex() == -1) {
                        mother_tongue = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your mother tongue!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {

                        /* if (casteDataList!!.isEmpty()) {
                             Toast.makeText(
                                 this@ProfileSetupOne,
                                 "You have selected $religion_name! \nDenomination is empty. Skipping layout",
                                 Toast.LENGTH_SHORT
                             ).show()
                             ll_location.visibility = View.GONE
                             ll_born.visibility = View.GONE
                             ll_height.visibility = View.GONE
                             ll_children.visibility = View.GONE
                             ll_ethnicity.visibility = View.GONE
                             ll_spirituality.visibility = View.GONE
                             ll_denomination.visibility = View.GONE
                             ll_education.visibility = View.VISIBLE
                             ll_occupation.visibility = View.GONE
                             ll_income.visibility = View.GONE
                             ll_smoke.visibility = View.GONE
                             ll_drink.visibility = View.GONE
                             ll_passionate_about.visibility = View.GONE
                             ll_leisure_time.visibility = View.GONE
                             ll_thankful.visibility = View.GONE

                             tv_back.visibility = View.VISIBLE

                             page_count = 7

                             horizontal_progress_bar.setProgress(30f)
                             horizontal_progress_bar.animate()


                         } else {
                             ll_location.visibility = View.GONE
                             ll_born.visibility = View.GONE
                             ll_height.visibility = View.GONE
                             ll_children.visibility = View.GONE
                             ll_ethnicity.visibility = View.GONE
                             ll_spirituality.visibility = View.GONE
                             ll_denomination.visibility = View.VISIBLE
                             ll_education.visibility = View.GONE
                             ll_occupation.visibility = View.GONE
                             ll_income.visibility = View.GONE
                             ll_smoke.visibility = View.GONE
                             ll_drink.visibility = View.GONE
                             ll_passionate_about.visibility = View.GONE
                             ll_leisure_time.visibility = View.GONE
                             ll_thankful.visibility = View.GONE

                             tv_back.visibility = View.VISIBLE

                             page_count = 6

                             horizontal_progress_bar.setProgress(25f)
                             horizontal_progress_bar.animate()


                         }*/

                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_ethnicity.visibility = View.VISIBLE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 5

                        horizontal_progress_bar.setProgress(20f)
                        horizontal_progress_bar.animate()
                    }

                }
                5 -> {
//  if (casteDataList!!.isNotEmpty()) {

                    if (ethnicityAdapter.getSelectedIndex() == -1) {
                        ethnicity = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your Ethnicity!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.VISIBLE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 6

                        horizontal_progress_bar.setProgress(25f)
                        horizontal_progress_bar.animate()


                    }

/* } else {
    ll_location.visibility = View.GONE
    ll_born.visibility = View.GONE
    ll_height.visibility = View.GONE
    ll_children.visibility = View.GONE
    ll_ethnicity.visibility = View.GONE
    ll_spirituality.visibility = View.GONE
    ll_denomination.visibility = View.GONE
    ll_education.visibility = View.VISIBLE
    ll_occupation.visibility = View.GONE
    ll_income.visibility = View.GONE
    ll_smoke.visibility = View.GONE
    ll_drink.visibility = View.GONE
    ll_passionate_about.visibility = View.GONE
    ll_leisure_time.visibility = View.GONE
    ll_thankful.visibility = View.GONE

    tv_back.visibility = View.VISIBLE

    page_count = 7

    horizontal_progress_bar.setProgress(30f)
    horizontal_progress_bar.animate()


}*/


                }
                6 -> {
                    if (educationAdapter.getSelectedIndex() == -1) {
                        education = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your Education!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.VISIBLE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 7

                        horizontal_progress_bar.setProgress(30f)
                        horizontal_progress_bar.animate()


                    }

                }
                7 -> {
                    if (occupationAdapter.getSelectedIndex() == -1) {
                        occupation = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your Occupation!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.VISIBLE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 8

                        horizontal_progress_bar.setProgress(40f)
                        horizontal_progress_bar.animate()


                    }

                }
                8 -> {
                    if (incomeAdapter.getSelectedIndex() == -1) {
                        income = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select your Income!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.VISIBLE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 9

                        horizontal_progress_bar.setProgress(50f)
                        horizontal_progress_bar.animate()


                    }

                }
                9 -> {
                    if (smokingAdapter.getSelectedIndex() == -1) {
                        smoke = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select one!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.VISIBLE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE


                        page_count = 10

                        horizontal_progress_bar.setProgress(60f)
                        horizontal_progress_bar.animate()


                    }

                }
                10 -> {
                    if (drinkingAdapter.getSelectedIndex() == -1) {
                        drink = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please select one!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.VISIBLE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 11

                        horizontal_progress_bar.setProgress(70f)
                        horizontal_progress_bar.animate()


                    }

                }
                11 -> {
                    val passionate_txt = et_passionate_about.text.toString().trim()
                    if (passionate_txt.isEmpty() || passionate_txt == "") {
                        passionate = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please fill the required field!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.VISIBLE
                        ll_thankful.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_count = 12


                        horizontal_progress_bar.setProgress(80f)
                        horizontal_progress_bar.animate()


                    }
                    tv_skip.visibility = View.VISIBLE
                }
                12 -> {
                    val leisure = et_leisure_time.text.toString().trim()
                    if (leisure.isEmpty() || leisure == "") {
                        leisure_time = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please fill the required field!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.VISIBLE

                        tv_back.visibility = View.VISIBLE

                        page_count = 13

                        horizontal_progress_bar.setProgress(90f)
                        horizontal_progress_bar.animate()


                    }
                    tv_skip.visibility = View.GONE
                    tv_next.text = "Continue"
                }
                13 -> {
                    val thank1 = et_thankful_1.text.toString().trim()
                    val thank2 = et_thankful_2.text.toString().trim()
                    val thank3 = et_thankful_3.text.toString().trim()
                    if (thank1 == "" || thank2 == "" || thank3 == "") {
                        thankful_1 = ""
                        thankful_2 = ""
                        thankful_3 = ""
                        Toast.makeText(
                            this@ProfileSetupOne,
                            "Please fill the required field!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {

                        ll_location.visibility = View.GONE
                        ll_born.visibility = View.GONE
                        ll_height.visibility = View.GONE
                        ll_children.visibility = View.GONE
                        ll_spirituality.visibility = View.GONE
                        ll_mTongue.visibility = View.GONE
                        ll_ethnicity.visibility = View.GONE
                        ll_education.visibility = View.GONE
                        ll_occupation.visibility = View.GONE
                        ll_income.visibility = View.GONE
                        ll_smoke.visibility = View.GONE
                        ll_drink.visibility = View.GONE
                        ll_passionate_about.visibility = View.GONE
                        ll_leisure_time.visibility = View.GONE
                        ll_thankful.visibility = View.GONE


                        horizontal_progress_bar.setProgress(100f)
                        horizontal_progress_bar.animate()


                        progressDialog.show()

                        /*  val fromFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                          fromFormat.isLenient = false
                          val toFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                          toFormat.isLenient = false
                          val date = fromFormat.parse(et_born_date.text.toString())
                          System.out.println("Date: " + toFormat.format(date))

                          dob = toFormat.format(date).toString()  // YYYY-MM-DD*/
                        height_ft = et_height_ft.text.toString()
                        height_inc = et_height_inch.text.toString()
                        height_cms = et_height_cms.text.toString()
                        passionate = et_passionate_about.text.toString()
                        leisure_time = et_leisure_time.text.toString()
                        thankful_1 = et_thankful_1.text.toString()
                        thankful_2 = et_thankful_2.text.toString()
                        thankful_3 = et_thankful_3.text.toString()

                        Log.d(
                            logTAG,
                            "$user_id,/* $location,*/ $height_ft, $height_inc, $height_cms, $children, " +
                                    "$ethnicity, $religion, $mother_tongue, $education, $occupation, $income, $smoke, $drink, " +
                                    "$passionate, $leisure_time, $thankful_1, $thankful_2, $thankful_3"
                        )

                        // submit data
                        val apiService = ApiInterface.create()
                        val call = apiService.profileSetupApi(
                            user_id!!,
                            children, /*location, dob,*/
                            ethnicity,
                            religion,
                            mother_tongue,
                            education,
                            occupation,
                            income,
                            smoke,
                            drink,
                            height_ft,
                            height_inc,
                            height_cms,
                            passionate,
                            leisure_time,
                            thankful_1,
                            thankful_2,
                            thankful_3
                        )
                        Log.d(logTAG, "Api Call Request : " + call.toString())
                        call.enqueue(object : Callback<ProfileSetupResponse> {
                            override fun onFailure(call: Call<ProfileSetupResponse>, t: Throwable) {
                                Log.e(logTAG, "Result error : $t")
                                Toast.makeText(
                                    this@ProfileSetupOne,
                                    "Something went wrong. Please try again!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<ProfileSetupResponse>,
                                response: Response<ProfileSetupResponse>
                            ) {

                                Log.d(logTAG, "Response success: ${response.body().toString()}")
                                if (response.body()!!.status == "1") {

                                    if (progressDialog.isShowing) {
                                        progressDialog.dismiss()
                                    }

                                    val profile_data = response.body()!!.profile_data

                                    if (profile_data!!.children.equals(null)) {
                                        user_children = "0"
                                    } else {
                                        user_children = profile_data.children!!
                                    }

                                    if (profile_data.city.equals(null)) {
                                        user_city = "0"
                                    } else {
                                        user_city = profile_data.city!!
                                    }

                                    if (profile_data.ethnicity.equals(null)) {
                                        user_ethnicity = "0"
                                    } else {
                                        user_ethnicity = profile_data.ethnicity!!
                                    }
                                    if (profile_data.religion.equals(null)) {
                                        user_religion = "0"
                                    } else {
                                        user_religion = profile_data.religion!!
                                    }
                                    /*if (profile_data.denomination.equals(null)) {
                                        user_denomination = "0"
                                    } else {
                                        user_denomination = profile_data.denomination!!
                                    }*/
                                    if (profile_data.education.equals(null)) {
                                        user_education = "0"
                                    } else {
                                        user_education = profile_data.education!!
                                    }
                                    if (profile_data.occupation.equals(null)) {
                                        user_occupation = "0"
                                    } else {
                                        user_occupation = profile_data.occupation!!
                                    }
                                    if (profile_data.income.equals(null)) {
                                        user_income = "0"
                                    } else {
                                        user_income = profile_data.income!!
                                    }
                                    if (profile_data.smoke.equals(null)) {
                                        user_smoke = "0"
                                    } else {
                                        user_smoke = profile_data.smoke!!
                                    }
                                    if (profile_data.drink.equals(null)) {
                                        user_drink = "0"
                                    } else {
                                        user_drink = profile_data.drink!!
                                    }
                                    if (profile_data.height_ft.equals(null)) {
                                        user_height_ft = "0"
                                    } else {
                                        user_height_ft = profile_data.height_ft!!
                                    }
                                    if (profile_data.height_inc.equals(null)) {
                                        user_height_inc = "0"
                                    } else {
                                        user_height_inc = profile_data.height_inc!!
                                    }
                                    if (profile_data.height_cms.equals(null)) {
                                        user_height_cms = "0"
                                    } else {
                                        user_height_cms = profile_data.height_cms!!
                                    }
                                    if (profile_data.height_ft.equals(null)) {
                                        user_height_ft = "0"
                                    } else {
                                        user_height_ft = profile_data.height_ft!!
                                    }
                                    if (profile_data.passionate.equals(null)) {
                                        user_passionate = "0"
                                    } else {
                                        user_passionate = profile_data.passionate!!
                                    }
                                    if (profile_data.leisure_time.equals(null)) {
                                        user_leisure_time = "0"
                                    } else {
                                        user_leisure_time = profile_data.leisure_time!!
                                    }
                                    if (profile_data.thankful_1.equals(null)) {
                                        user_thankful_1 = "0"
                                    } else {
                                        user_thankful_1 = profile_data.thankful_1!!
                                    }
                                    if (profile_data.thankful_2.equals(null)) {
                                        user_thankful_2 = "0"
                                    } else {
                                        user_thankful_2 = profile_data.thankful_2!!
                                    }
                                    if (profile_data.thankful_3.equals(null)) {
                                        user_thankful_3 = "0"
                                    } else {
                                        user_thankful_3 = profile_data.thankful_3!!
                                    }
                                    if (profile_data.mtongue_name.equals(null)) {
                                        user_mother_tongue = "0"
                                    } else {
                                        user_mother_tongue = profile_data.mtongue_name!!
                                    }

                                    if (!profile_data.signup_step.equals(null)) {
                                        signup_step = profile_data.signup_step!!
                                    }

                                    sessionManager.userProfileSetup(
                                        user_children,
                                        user_city,
                                        user_ethnicity,
                                        user_religion,
                                        user_mother_tongue,
                                        user_education,
                                        user_occupation,
                                        user_income,
                                        user_smoke,
                                        user_drink,
                                        user_height_ft,
                                        user_height_inc,
                                        user_height_cms,
                                        user_passionate,
                                        user_leisure_time,
                                        user_thankful_1,
                                        user_thankful_2,
                                        user_thankful_3,
                                        signup_step
                                    )

                                    val intent1 = Intent(
                                        this@ProfileSetupOne,
                                        MatchPreferencesWelcome::class.java
                                    )
                                    startActivity(intent1)
                                    this@ProfileSetupOne.finish()
                                } else {
                                    if (progressDialog.isShowing) {
                                        progressDialog.dismiss()
                                    }
                                    Toast.makeText(
                                        this@ProfileSetupOne,
                                        response.body()!!.result,
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                }
                            }

                        })
                    }
                }
            }
            Log.d(logTAG, "page_count : $page_count")
        }

        tv_back.setOnClickListener {

            closeKeyboard()

            when (page_count) {
                1 -> {
                    super.onBackPressed()
                }
                /*2 -> {
                    ll_location.visibility = View.VISIBLE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    horizontal_progress_bar.setProgress(0f)
                    horizontal_progress_bar.animate()

                    page_count = 1
                }*/
/* 3 -> {
ll_location.visibility = View.GONE
ll_born.visibility = View.VISIBLE
ll_height.visibility = View.GONE
ll_children.visibility = View.GONE
ll_ethnicity.visibility = View.GONE
ll_spirituality.visibility = View.GONE
ll_denomination.visibility = View.GONE
ll_education.visibility = View.GONE
ll_occupation.visibility = View.GONE
ll_income.visibility = View.GONE
ll_smoke.visibility = View.GONE
ll_drink.visibility = View.GONE
ll_passionate_about.visibility = View.GONE
ll_leisure_time.visibility = View.GONE
ll_thankful.visibility = View.GONE

tv_back.visibility = View.GONE

*//* if (citiesDataList.size == 0 || citiesDataList.isEmpty()) {
    page_count = 3
} else {
    page_count = 2

}*//*

page_count = 2

horizontal_progress_bar.setProgress(5f)
horizontal_progress_bar.animate()


}*/
                2 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.VISIBLE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 1

                    horizontal_progress_bar.setProgress(5f)
                    horizontal_progress_bar.animate()

                }
                3 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.VISIBLE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 2

                    horizontal_progress_bar.setProgress(10f)
                    horizontal_progress_bar.animate()


                }
                4 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.VISIBLE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 3

                    horizontal_progress_bar.setProgress(15f)
                    horizontal_progress_bar.animate()


                }
                5 -> {

                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.VISIBLE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 4

                    horizontal_progress_bar.setProgress(20f)
                    horizontal_progress_bar.animate()


                }
                6 -> {
/* if (casteDataList!!.isEmpty()) {
    Toast.makeText(
        this@ProfileSetupOne,
        "Denomination is empty. Skipping layout!",
        Toast.LENGTH_SHORT
    ).show()
    ll_location.visibility = View.GONE
    ll_born.visibility = View.GONE
    ll_height.visibility = View.GONE
    ll_children.visibility = View.GONE
    ll_ethnicity.visibility = View.GONE
    ll_spirituality.visibility = View.VISIBLE
    ll_denomination.visibility = View.GONE
    ll_education.visibility = View.GONE
    ll_occupation.visibility = View.GONE
    ll_income.visibility = View.GONE
    ll_smoke.visibility = View.GONE
    ll_drink.visibility = View.GONE
    ll_passionate_about.visibility = View.GONE
    ll_leisure_time.visibility = View.GONE
    ll_thankful.visibility = View.GONE

    tv_back.visibility = View.GONE

    page_count = 5

    horizontal_progress_bar.setProgress(20f)
    horizontal_progress_bar.animate()


} else {*/

                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_ethnicity.visibility = View.VISIBLE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE


                    page_count = 5

                    horizontal_progress_bar.setProgress(25f)
                    horizontal_progress_bar.animate()

//  }

                }
                7 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.VISIBLE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE


                    page_count = 6

                    horizontal_progress_bar.setProgress(30f)
                    horizontal_progress_bar.animate()


                }
                8 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.VISIBLE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 7

                    horizontal_progress_bar.setProgress(40f)
                    horizontal_progress_bar.animate()


                }
                9 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.VISIBLE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 8

                    horizontal_progress_bar.setProgress(50f)
                    horizontal_progress_bar.animate()


                }
                10 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.VISIBLE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 9

                    horizontal_progress_bar.setProgress(60f)
                    horizontal_progress_bar.animate()


                }
                11 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.VISIBLE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 10

                    horizontal_progress_bar.setProgress(70f)
                    horizontal_progress_bar.animate()


                }
                12 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.VISIBLE
                    ll_leisure_time.visibility = View.GONE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_count = 11

                    horizontal_progress_bar.setProgress(80f)
                    horizontal_progress_bar.animate()


                }
                13 -> {
                    ll_location.visibility = View.GONE
                    ll_born.visibility = View.GONE
                    ll_height.visibility = View.GONE
                    ll_children.visibility = View.GONE
                    ll_spirituality.visibility = View.GONE
                    ll_mTongue.visibility = View.GONE
                    ll_ethnicity.visibility = View.GONE
                    ll_education.visibility = View.GONE
                    ll_occupation.visibility = View.GONE
                    ll_income.visibility = View.GONE
                    ll_smoke.visibility = View.GONE
                    ll_drink.visibility = View.GONE
                    ll_passionate_about.visibility = View.GONE
                    ll_leisure_time.visibility = View.VISIBLE
                    ll_thankful.visibility = View.GONE

                    tv_back.visibility = View.GONE


                    horizontal_progress_bar.setProgress(90f)
                    horizontal_progress_bar.animate()


                    page_count = 12

                    tv_skip.visibility = View.VISIBLE
                    tv_next.text = "Next"
                }
            }

        }

        tv_skip.setOnClickListener {
            val builder = AlertDialog.Builder(this@ProfileSetupOne)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to SKIP?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {


                        val photo1 = user_details.get(DllSessionManager.PHOTO1)
                        if(photo1.equals("0")){
                            val home_intent = Intent(this@ProfileSetupOne, ProfilePhotos::class.java)
                            //home_intent.putExtra("from_list","match_setup")
                            startActivity(home_intent)
                            finish()
                        }else {

                            val home_intent = Intent(this@ProfileSetupOne, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "profile_setup")
                            startActivity(home_intent)
                            finish()
                        }


                        /*val home_intent =
                            Intent(this@ProfileSetupOne, DashboardActivity::class.java)
                        home_intent.putExtra("from_list", "profile_setup")
                        startActivity(home_intent)
                        finish()*/
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }

    }

    private fun initializeViews() {
// Layouts
        ll_location = findViewById(R.id.ll_location)
        ll_born = findViewById(R.id.ll_born)
        ll_height = findViewById(R.id.ll_height)
        ll_children = findViewById(R.id.ll_children)
        ll_ethnicity = findViewById(R.id.ll_ethnicity)
        ll_spirituality = findViewById(R.id.ll_spirituality)
        ll_mTongue = findViewById(R.id.ll_mTongue)
        ll_education = findViewById(R.id.ll_education)
        ll_occupation = findViewById(R.id.ll_occupation)
        ll_income = findViewById(R.id.ll_income)
        ll_smoke = findViewById(R.id.ll_smoke)
        ll_drink = findViewById(R.id.ll_drink)
        ll_passionate_about = findViewById(R.id.ll_passionate_about)
        ll_leisure_time = findViewById(R.id.ll_leisure_time)
        ll_thankful = findViewById(R.id.ll_thankful)

// EditTexts
        et_search_location = findViewById(R.id.et_location)
        et_born_date = findViewById(R.id.et_born_date)
        et_height_ft = findViewById(R.id.et_height_ft)
        et_height_inch = findViewById(R.id.et_height_inch)
        et_height_cms = findViewById(R.id.et_height_cms)
        et_search_mTongue = findViewById(R.id.et_mTongue)
        et_search_occupation = findViewById(R.id.et_occupation)
        et_passionate_about = findViewById(R.id.et_passionate_about)
        et_leisure_time = findViewById(R.id.et_leisure_time)
        et_thankful_1 = findViewById(R.id.et_thankful_1)
        et_thankful_2 = findViewById(R.id.et_thankful_2)
        et_thankful_3 = findViewById(R.id.et_thankful_3)

// ListViews
        listView_location = findViewById(R.id.listView_location)
        listView_ethnicity = findViewById(R.id.listView_ethnicity)
        gridView_spirituality = findViewById(R.id.gridView_spirituality)
        gridView_mother_tongue = findViewById(R.id.rv_mother_tongue)
        listView_education = findViewById(R.id.listView_education)
        listView_occupation = findViewById(R.id.listView_occupation)
        listView_income = findViewById(R.id.listView_income)
        listView_smoking = findViewById(R.id.listView_smoking)
        listView_drinking = findViewById(R.id.listView_drinking)

        sp_children = findViewById(R.id.sp_children)

        tv_ethnicity_qtion_id = findViewById(R.id.tv_ethnicity_qtion_id)
        tv_occupation_qtion_id = findViewById(R.id.tv_occupation_qtion_id)
        tv_income_qtion_id = findViewById(R.id.tv_income_qtion_id)
        tv_education_qtion_id = findViewById(R.id.tv_education_qtion_id)
        tv_smoke_qtion_id = findViewById(R.id.tv_smoke_qtion_id)
        tv_drink_qtion_id = findViewById(R.id.tv_drink_qtion_id)
        location_empty = findViewById(R.id.location_empty)
        occupation_empty = findViewById(R.id.occupation_empty)
        mTongue_empty = findViewById(R.id.mTongue_empty)

        gridLayoutManager = GridLayoutManager(this, 2)
        gridView_mother_tongue.layoutManager = gridLayoutManager
        gridView_mother_tongue.setHasFixedSize(true)

        tv_skip = findViewById(R.id.tv_skip)

        ethnicityList = ArrayList()
        educationList = ArrayList()
        occupationList = ArrayList()
        incomeList = ArrayList()
        smokeList = ArrayList()
        drinkList = ArrayList()
        citiesDataList = ArrayList()
        citiesList = ArrayList()

        questionsList = ArrayList()
        religionDataList = ArrayList()
        casteDataList = ArrayList()

        child_adapter = object : ArrayAdapter<String>(
            this@ProfileSetupOne, R.layout.item_spinner, resources.getStringArray(R.array.children)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view.findViewById<TextView>(R.id.textView)
                if (position == 0) {
// Set the hint text color gray
// select children
                    tv.setTextColor(Color.GRAY)
                }
                return view
            }
        }
        child_adapter.setDropDownViewResource(R.layout.item_spinner)

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

        horizontal_progress_bar = findViewById(R.id.horizontal_progress_bar)
        horizontal_progress_bar.setClickable(false)
        horizontal_progress_bar.setFocusable(false)
        horizontal_progress_bar.setEnabled(false)

    }

    override fun onBackPressed() {
        if (page_count == 1) {
            progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish()
                        progressDialog.dismiss()
                    }
                    return true
                }
            })
        } else {
            val builder = AlertDialog.Builder(this@ProfileSetupOne)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to head start again?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {

                        val intent = Intent(this@ProfileSetupOne, ProfileSetupWelcome::class.java)
                        startActivity(intent)
                        this@ProfileSetupOne.finish()
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }

    }

    private fun closeKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun centimetersToFeet(centimeters: String) {
        val cm = centimeters.toFloat()
        val feet = Math.floor(cm / 2.54 / 12).toInt()
        val inch = Math.floor(cm / 2.54 - feet * 12).toInt()
        et_height_ft.setText(feet.toString())
        et_height_inch.setText(inch.toString())

        cms_text = "0"
    }

    private fun feetToCentimeters(feet: String, inch: String) {
        var heightInFeet = 0.0
        var heightInInches = 0.0
        try {
            heightInFeet = feet.toDouble()
            heightInInches = inch.toDouble()

        } catch (nfe: NumberFormatException) {
            nfe.printStackTrace()
        }
        val cm = (heightInFeet * 30.48) + (heightInInches * 2.54)
        val cms = Math.round(cm)
        et_height_cms.setText(cms.toInt().toString())
    }

    private fun showDateTimePicker() {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        currentDate.add(Calendar.YEAR, -18)
        currentDate.add(Calendar.DATE, -1)
        val datePickerDialog =
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
                    date.set(year, monthofyear, dayofmonth)

                    Log.v("dateShowPicker", "The choosen one " + date.time)

                    val myFormat = "MM-dd-yyyy"
                    val sdf = SimpleDateFormat(myFormat, Locale.UK)
                    val date2 = sdf.format(date.time)

                    et_born_date.setText(date2)

                },
                currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DATE)
            )
// datePickerDialog.datePicker.maxDate = currentDate.timeInMillis
        datePickerDialog.show()


    }

    private fun getRegisterApi() {
        val api = ApiInterface.create()
        val apiCall = api.registerProfileApi()
        apiCall.enqueue(object : Callback<RegisterProfileSetupResponse> {
            override fun onFailure(call: Call<RegisterProfileSetupResponse>, t: Throwable) {
                Log.e(logTAG, "RegisterProfileSetupResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@ProfileSetupOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<RegisterProfileSetupResponse>,
                response: Response<RegisterProfileSetupResponse>
            ) {

                Log.d(
                    logTAG,
                    "RegisterProfileSetupResponse: success: ${response.body()!!.data.toString()}"
                )

                if (response.body()!!.status.equals("1")) {
                    questionsList = response.body()!!.data


                    for (i in 0 until questionsList!!.size) {

                        if (questionsList!!.get(i).title.equals("ethnicity")) {
                            ethnicityList = questionsList!![i].answer!!
                            tv_ethnicity_qtion_id.setText(questionsList!!.get(i).question)
                        }
                        if (questionsList!!.get(i).title.equals("education")) {
                            educationList = questionsList!![i].answer!!
                            tv_education_qtion_id.setText(questionsList!!.get(i).question)
                        }
                        if (questionsList!!.get(i).title.equals("occupation")) {
                            occupationList = questionsList!![i].answer!!
                            tv_occupation_qtion_id.setText(questionsList!!.get(i).question)
                        }
                        if (questionsList!!.get(i).title.equals("income")) {
                            incomeList = questionsList!![i].answer!!
                            tv_income_qtion_id.setText(questionsList!!.get(i).question)
                        }
                        if (questionsList!!.get(i).title.equals("smoke")) {
                            smokeList = questionsList!![i].answer!!
                            tv_smoke_qtion_id.setText(questionsList!!.get(i).question)
                        }
                        if (questionsList!!.get(i).title.equals("drink")) {
                            drinkList = questionsList!![i].answer!!
                            tv_drink_qtion_id.setText(questionsList!!.get(i).question)
                        }

                    }

                }

// adapters
                drinkingAdapter = ProfileDrinkingAdapter(this@ProfileSetupOne, drinkList)
                educationAdapter = ProfileEducationAdapter(this@ProfileSetupOne, educationList)
                occupationAdapter = ProfileOccupationAdapter(this@ProfileSetupOne, occupationList)
                ethnicityAdapter = ProfileEthnicityAdapter(this@ProfileSetupOne, ethnicityList)
                smokingAdapter = ProfileSmokingAdapter(this@ProfileSetupOne, smokeList)
                incomeAdapter = ProfileIncomeAdapter(this@ProfileSetupOne, incomeList)


                listView_ethnicity.adapter = ethnicityAdapter
                listView_drinking.adapter = drinkingAdapter
                listView_education.adapter = educationAdapter
                listView_occupation.adapter = occupationAdapter
                listView_income.adapter = incomeAdapter

                listView_smoking.adapter = smokingAdapter

            }

        })
    }

    private fun getReligionApi() {
        val religionApi = ApiInterface.create()
        val religionCall = religionApi.getReligionApi()
        religionCall.enqueue(object : Callback<ReligionResponse> {
            override fun onFailure(call: Call<ReligionResponse>, t: Throwable) {
                Log.e(logTAG, "ReligionResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@ProfileSetupOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<ReligionResponse>, response: Response<ReligionResponse>
            ) {
                Log.d(logTAG, "ReligionResponse: success: ${response.body()!!.data.toString()}")
                if (response.body()!!.status == "1") {

                    religionDataList = response.body()!!.data

                    if (religionDataList!!.size > 0) {
                        for (i in 0 until religionDataList!!.size) {
                            if (religionDataList!![i].religion_name == "All") {
                                religionDataList!!.removeAt(i)
                                break
                            }
                        }
                        spiritualityAdapter =
                            ProfileSpiritualityAdapter(this@ProfileSetupOne, religionDataList!!)
                        gridView_spirituality.adapter = spiritualityAdapter

                    }

                }

            }

        })
    }

    private fun getMotherTongueApi() {
        val motherTongueApi = ApiInterface.create()
        val motherTongueCall = motherTongueApi.getMotherTongueApi()
        motherTongueCall.enqueue(object : Callback<MotherTongueResponse> {
            override fun onFailure(call: Call<MotherTongueResponse>, t: Throwable) {
                Log.e(logTAG, "motherTongueResponse : error : $t")
                Toast.makeText(
                    this@ProfileSetupOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<MotherTongueResponse>, response: Response<MotherTongueResponse>
            ) {

                Log.d(logTAG, "motherTongueResponse: success: ${response.body()!!.data.toString()}")
                if (response.body() != null) {
                    if (response.body()!!.status == "1") {

                        motherTongueDataList = response.body()!!.data

                        Log.d(logTAG, "motherTongueResponse: size: ${motherTongueDataList!!.size}")
                        if (motherTongueDataList!!.size > 0) {
                            motherTongueAdapter =
                                MotherTongueAdapter(this@ProfileSetupOne, motherTongueDataList!!)
                            gridView_mother_tongue.adapter = motherTongueAdapter
                        }

                    } else {
                        Toast.makeText(
                            this@ProfileSetupOne,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } else {
                    Toast.makeText(
                        this@ProfileSetupOne,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }


            }

        })
    }

    private fun getCityApi() {
        val cityApi = ApiInterface.create()
        val cityCall = cityApi.getCities(state_code!!)
        cityCall.enqueue(object : Callback<CitiesResponse> {
            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                Log.e(logTAG, "CitiesResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@ProfileSetupOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                Log.d(logTAG, "CitiesResponse: success: ${response.body()!!.result}")

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                if (response.body()!!.status.equals("1")) {
                    citiesDataList = response.body()!!.data!!

                    for (i in 0 until citiesDataList.size) {
                        citiesList.add(citiesDataList[i].city_name)
                    }
                    locationAdapter = ProfileLocationAdapter(this@ProfileSetupOne, citiesList)

                    listView_location.adapter = locationAdapter

                    ll_location.visibility = View.VISIBLE
                    tv_next.visibility = View.VISIBLE
                    tv_back.visibility = View.VISIBLE


                } else if (response.body()!!.status.equals("2")) {
                    citiesDataList = ArrayList()
                    page_count = 2
                    locationAdapter = ProfileLocationAdapter(this@ProfileSetupOne, citiesList)
                    listView_location.adapter = locationAdapter
                    ll_location.visibility = View.VISIBLE
                    tv_next.visibility = View.VISIBLE
                    tv_back.visibility = View.VISIBLE
                }

            }
        })
    }

}