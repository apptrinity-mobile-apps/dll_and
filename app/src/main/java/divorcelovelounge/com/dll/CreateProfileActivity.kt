package divorcelovelounge.com.dll

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import divorcelovelounge.com.dll.Helper.CustomTextView
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class CreateProfileActivity : AppCompatActivity() {

    lateinit var cprofile_progress_id: ProgressBar
    lateinit var tv_profile_nxt_id: CustomTextView
    lateinit var img_profile_pic_id: ImageView
    lateinit var dob_id: EditText
    private val GALLERY = 1
    private val CAMERA = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile)

        cprofile_progress_id = findViewById(R.id.cprofile_progress_id)
        tv_profile_nxt_id = findViewById(R.id.tv_profile_nxt_id)
        img_profile_pic_id = findViewById(R.id.img_profile_pic_id)
        dob_id = findViewById(R.id.dob_id)

        tv_profile_nxt_id.setOnClickListener {
            tv_profile_nxt_id.visibility = View.GONE
            cprofile_progress_id.visibility = View.VISIBLE

            startActivity(Intent(this@CreateProfileActivity, GPSLocationActivity::class.java))
        }

        dob_id.setOnClickListener {
            showDateTimePicker()
        }

        img_profile_pic_id.setOnClickListener {
            getImageFromAlbum()
        }

    }

    fun showDateTimePicker() {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        currentDate.add(Calendar.YEAR, -18)
        currentDate.add(Calendar.DATE,-1)
        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
            date.set(year, monthofyear, dayofmonth)

            Log.v("dateShowPicker", "The choosen one " + date.time)

            val myFormat = "dd/MM/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.UK)
            val date2 = sdf.format(date.time)

            dob_id.setText(date2)

            //  }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show()


    }

    private fun getImageFromAlbum() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*if (resultCode == this) {
            return
        }*/
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    //  val path = saveImage(bitmap)
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
                    img_profile_pic_id.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }

        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            img_profile_pic_id.setImageBitmap(thumbnail)
            Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

}
