package divorcelovelounge.com.dll

import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    private var images: ArrayList<Int>? = null
    lateinit var images_only: ArrayList<String>
    private var options: BitmapFactory.Options? = null
    private var viewPager: ViewPager? = null

    private var adapter: FragmentStatePagerAdapter? = null
    private var thumbnailsContainer: LinearLayout? = null
    var resourceIDs = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)


    var user_photo1 = ""
    var user_photo2 = ""
    var user_photo3 = ""
    var user_photo4 = ""
    var user_photo5 = ""
    var user_photo6 = ""
    var user_photo7 = ""
    var user_photo8 = ""
    var user_photo9 = ""
    var user_photo10 = ""
    var user_photo1_approve = ""
    var user_photo2_approve = ""
    var user_photo3_approve = ""
    var user_photo4_approve = ""
    var user_photo5_approve = ""
    var user_photo6_approve = ""
    var user_photo7_approve = ""
    var user_photo8_approve = ""
    var user_photo9_approve = ""
    var user_photo10_approve = ""


    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    lateinit var iv_toolbar_back_id: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iv_toolbar_back_id = findViewById(R.id.iv_toolbar_back_id)
        iv_toolbar_back_id.setOnClickListener { super.onBackPressed() }


        //user_photo1 = intent.getStringExtra("user_photo1")
//        user_photo2 = intent.getStringExtra("user_photo2")
//        user_photo3 = intent.getStringExtra("user_photo3")
//        user_photo4 = intent.getStringExtra("user_photo4")
//        user_photo5 = intent.getStringExtra("user_photo5")
//        user_photo6 = intent.getStringExtra("user_photo6")
//        user_photo7 = intent.getStringExtra("user_photo7")
//        user_photo8 = intent.getStringExtra("user_photo8")
//        user_photo9 = intent.getStringExtra("user_photo9")
//        user_photo10 = intent.getStringExtra("user_photo10")
        user_photo1_approve = intent.getStringExtra("user_photo1_approve")
        user_photo2_approve = intent.getStringExtra("user_photo2_approve")
        user_photo3_approve = intent.getStringExtra("user_photo3_approve")
        user_photo4_approve = intent.getStringExtra("user_photo4_approve")
        user_photo5_approve = intent.getStringExtra("user_photo5_approve")
        user_photo6_approve = intent.getStringExtra("user_photo6_approve")
        user_photo7_approve = intent.getStringExtra("user_photo7_approve")
        user_photo8_approve = intent.getStringExtra("user_photo8_approve")
        user_photo9_approve = intent.getStringExtra("user_photo9_approve")
        user_photo10_approve = intent.getStringExtra("user_photo10_approve")

        if (user_photo1_approve != "UNAPPROVED") {
            user_photo1 = intent.getStringExtra("user_photo1")
        }
        if (user_photo2_approve != "UNAPPROVED") {
            user_photo2 = intent.getStringExtra("user_photo2")
        }
        if (user_photo3_approve != "UNAPPROVED") {
            user_photo3 = intent.getStringExtra("user_photo3")
        }
        if (user_photo4_approve != "UNAPPROVED") {
            user_photo4 = intent.getStringExtra("user_photo4")
        }
        if (user_photo5_approve != "UNAPPROVED") {
            user_photo5 = intent.getStringExtra("user_photo5")
        }
        if (user_photo6_approve != "UNAPPROVED") {
            user_photo6 = intent.getStringExtra("user_photo6")
        }
        if (user_photo7_approve != "UNAPPROVED") {
            user_photo7 = intent.getStringExtra("user_photo7")
        }
        if (user_photo8_approve != "UNAPPROVED") {
            user_photo8 = intent.getStringExtra("user_photo8")
        }
        if (user_photo9_approve != "UNAPPROVED") {
            user_photo9 = intent.getStringExtra("user_photo9")
        }
        if (user_photo10_approve != "UNAPPROVED") {
            user_photo10 = intent.getStringExtra("user_photo10")
        }

        images = ArrayList()
        images_only = ArrayList()
        images_only!!.add(user_photo1)
        images_only!!.add(user_photo2)
        images_only!!.add(user_photo3)
        images_only!!.add(user_photo4)
        images_only!!.add(user_photo5)
        images_only!!.add(user_photo6)
        images_only!!.add(user_photo7)
        images_only!!.add(user_photo8)
        images_only!!.add(user_photo9)
        images_only!!.add(user_photo10)
        //find view by id
        viewPager = findViewById<View>(R.id.view_pager) as ViewPager
        thumbnailsContainer = findViewById<View>(R.id.container) as LinearLayout

        // btnPrev.setOnClickListener(onClickListener(0))
        //  btnNext.setOnClickListener(onClickListener(1))

        for (i in resourceIDs.indices) {
            images!!.add(resourceIDs.get(i))
        }
        // init viewpager adapter and attach
        adapter = ViewPagerAdapter(supportFragmentManager, images!!, images_only)
        viewPager!!.setAdapter(adapter)

        inflateThumbnails()
    }


    private fun inflateThumbnails() {
        for (i in images!!.indices) {
            val imageLayout = layoutInflater.inflate(R.layout.item_image, null)
            val imageView = imageLayout.findViewById<View>(R.id.img_thumb) as ImageView
            imageView.setOnClickListener(onChagePageClickListener(i))
            options = BitmapFactory.Options()
            options!!.inSampleSize = 3
            options!!.inDither = false

            if (i == 0) {

                if (user_photo1.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo1_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load( photobaseurl + "0")
                            //  .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo1)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }

                }


            } else if (i == 1) {
                if (user_photo2.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo2_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            //  .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo2)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 2) {
                if (user_photo3.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo3_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo3)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 3) {
                if (user_photo4.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo4_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo4)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 4) {
                if (user_photo5.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo5_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo5)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 5) {
                if (user_photo6.equals("0")) {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo6_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo6)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 6) {
                if (user_photo7 == "0") {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo7_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo7)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 7) {
                if (user_photo8 == "0") {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo8_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo8)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 8) {
                if (user_photo9 == "0") {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo9_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo9)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            } else if (i == 9) {
                if (user_photo10 == "0") {
                    imageView.visibility = View.GONE
                } else {
                    if (user_photo10_approve == "UNAPPROVED") {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + "0")
                            // .error(R.drawable.ic_man_user)
                            .into(imageView)
                    } else {
                        imageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                            .load(photobaseurl + user_photo10)
                            .error(R.drawable.ic_man_user)
                            .into(imageView)
                    }
                }

            }


            /* val bitmap = BitmapFactory.decodeResource(resources, images!!.get(i), options)
             imageView.setImageBitmap(bitmap)
             //set to image view
             imageView.setImageBitmap(bitmap)*/
            //add imageview
            thumbnailsContainer!!.addView(imageLayout)
        }
    }

    private fun onChagePageClickListener(i: Int): View.OnClickListener {

        return View.OnClickListener { viewPager!!.setCurrentItem(i) }
    }


}
