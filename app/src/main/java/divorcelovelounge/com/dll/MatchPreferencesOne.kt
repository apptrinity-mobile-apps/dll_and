package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import divorcelovelounge.com.dll.Adapters.*
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Helper.RecyclerItemClickListener
import divorcelovelounge.com.dll.Helper.SignSeekBar
import divorcelovelounge.com.dll.MultiSelectSpinnerLib.KeyPairBoolData
import divorcelovelounge.com.dll.MultiSelectSpinnerLib.MultiSpinnerSearch
import divorcelovelounge.com.dll.MultiSelectSpinnerLib.SpinnerListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

//RAVITEJA
class MatchPreferencesOne : AppCompatActivity() {

    /* UI components start */
    // Layouts
    private lateinit var ll_match_age_range: LinearLayout
    private lateinit var ll_match_search_distance: LinearLayout
    private lateinit var ll_match_search_imp: LinearLayout
    private lateinit var ll_match_ethnicity: LinearLayout
    private lateinit var ll_match_religion: LinearLayout
    private lateinit var ll_match_mTongue: LinearLayout
    private lateinit var ll_match_education: LinearLayout
    private lateinit var ll_match_occupation: LinearLayout
    private lateinit var ll_match_smoke: LinearLayout
    private lateinit var ll_match_drink: LinearLayout

    //Toolbar
    private lateinit var customToolBar: Toolbar

    // TextViews
    private lateinit var tv_next: TextView
    private lateinit var tv_back: TextView
    private lateinit var tv_skip: TextView

    // Spinners
    private lateinit var sp_match_min: Spinner
    private lateinit var sp_match_max: Spinner

    // List views
    private lateinit var listView_match_search_distance: ListView
    private lateinit var listView_match_search_imp: ListView

    private lateinit var rv_match_ethncity: RecyclerView
    private lateinit var rv_match_religion: RecyclerView
    private lateinit var rv_match_mTongue: RecyclerView
    private lateinit var rv_match_education: RecyclerView
    private lateinit var rv_match_smoke: RecyclerView
    private lateinit var rv_match_drink: RecyclerView

    // Dialog
    private lateinit var progressDialog: Dialog

    // TextViews
    private lateinit var match_mTongue_empty: CustomTextView

    // EditTexts
    private lateinit var et_match_mTongue: EditText

    // ProgressBar
    private lateinit var horizontal_progress_bar: SignSeekBar

    private var progress_value = 0
    /* UI components end */

    // Adapters
    private lateinit var min_age_adapter: ArrayAdapter<String>
    private lateinit var max_age_adapter: ArrayAdapter<String>
    private lateinit var searchDistanceAdapter: MatchPrefsSearchDistanceAdapter
    private lateinit var searchImportantAdapter: MatchPrefsImportantAdapter
    private lateinit var ethnicityAdapter: MatchPrefsEthnicityAdapter
    private lateinit var religionAdapter: MatchPrefsReligionAdapter
    private lateinit var mTongueAdapter: MotherTongueAdapter
    private lateinit var educationAdapter: MatchPrefsEducationAdapter
    private lateinit var smokeAdapter: MatchPrefsSmokeAdapter
    private lateinit var drinkAdapter: MatchPrefsDrinkAdapter
    private lateinit var occupationAdapter: MatchPrefsOccupationAdapter

    private lateinit var lm_ethnicity: LinearLayoutManager
    private lateinit var lm_religion: LinearLayoutManager
    private lateinit var lm_mTongue: LinearLayoutManager
    private lateinit var lm_education: LinearLayoutManager
    private lateinit var lm_smoke: LinearLayoutManager
    private lateinit var lm_drink: LinearLayoutManager

    // Constants
    private val logTAG = "MatchPreferencesOne"
    private var user_id: String? = null
    private var search_distance = ""
    private var search_imp = ""
    private var education = ""
    private var ethnicity = ""
    private var religion = ""
    private var mTongue = ""
    private var smoke = ""
    private var drink = ""
    private var occupation = ""
    private var min_age = ""
    private var max_age = ""
    private var partner_age_min = ""
    private var partner_age_max = ""
    private var partner_match_distance = ""
    private var partner_match_search = ""
    private var partner_ethnicity = ""
    private var partner_religion = ""
    private var partner_mTongue = ""
    private var partner_education = ""
    private var partner_occupation = ""
    private var partner_smoke = ""
    private var partner_drink = ""
    private var signup_step = ""

    private var page_counter = 1
    private val MIN_AGE = 18
    private val MIN_AGE_mx = 24
    private val MAX_AGE = 99
    private val AGE_DIFF = 6
    private var pos = -1

    private lateinit var searchMultiSpinner_occ: MultiSpinnerSearch
    private lateinit var sessionManager: DllSessionManager

    private var selectedOccupation: ArrayList<String> = ArrayList()
    private lateinit var religionDataList: ArrayList<ReligionData>
    private lateinit var motherTongueDataList: ArrayList<MotherTongueData>
    private lateinit var questionsList: ArrayList<RegisterProfileSetupData>

    private lateinit var ethnicityList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var educationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var occupationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var incomeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var smokeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var drinkList: ArrayList<RegisterProfileSetupAnswer>

    private lateinit var min_age_array: ArrayList<String>
    private lateinit var max_age_array: ArrayList<String>
    private lateinit var selectedEducation: ArrayList<String>
    private lateinit var selectedReligion: ArrayList<String>
    private lateinit var selectedmTongue: ArrayList<String>
    private lateinit var selectedEthnicity: ArrayList<String>
    private lateinit var selectedSmoking: ArrayList<String>
    private lateinit var selectedDrinking: ArrayList<String>


    lateinit var dllsesstionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_preferences_one)
        title = ""

        sessionManager = DllSessionManager(this@MatchPreferencesOne)
        if (!sessionManager.userDetails["userid"].equals("")) {
            user_id = sessionManager.userDetails["userid"]
        }


        dllsesstionManager = DllSessionManager(this)
        user_details = dllsesstionManager.userDetails

        Log.d(logTAG, "USER ID : $user_id")

        /* setting custom tool bar */
        customToolBar = findViewById(R.id.match_CustomToolBar)
        tv_next = customToolBar.findViewById(R.id.tv_match_next)
        tv_back = customToolBar.findViewById(R.id.tv_match_back)

        setSupportActionBar(customToolBar)

        initialize()

        progressDialog.show()

        getReligionApi()
        getMotherTongueApi()
        getRegisterApi()

        ll_match_age_range.visibility = View.VISIBLE
        tv_next.visibility = View.VISIBLE
        tv_back.visibility = View.VISIBLE

        // set adapters
        sp_match_min.adapter = min_age_adapter
        sp_match_max.adapter = max_age_adapter

        listView_match_search_distance.adapter = searchDistanceAdapter
        listView_match_search_imp.adapter = searchImportantAdapter

        // empty text view

        et_match_mTongue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (char!!.isNotEmpty()) {
                    mTongueAdapter.filter.filter(char.toString())
                    mTongueAdapter.setSelectedIndex(-1)
                } else {
                    mTongueAdapter.filter.filter("")
                    mTongueAdapter.selectedList(selectedmTongue)
                }

                match_mTongue_empty.visibility = if (mTongueAdapter.getSize()!! > 0) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        })

        // Listeners
        sp_match_min.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                min_age = parentView.getItemAtPosition(position).toString()
                Log.e(logTAG, "min_age : $min_age")

                /*if(min_age!="99"){

                    for (i in  min_age.toInt()+1..MAX_AGE) {
                        val age = i.toString()
                        max_age_array.add(age)
                    }
                }*/


            }

            override fun onNothingSelected(parentView: AdapterView<*>) {}

        }

        sp_match_max.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                max_age = parentView.getItemAtPosition(position).toString()
                Log.e(logTAG, "max_age :$max_age")
            }
            override fun onNothingSelected(parentView: AdapterView<*>) {}
        }


        listView_match_search_distance.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                search_distance =
                    listView_match_search_distance.getItemAtPosition(position).toString()
                searchDistanceAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position search_distance $position $search_distance")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        listView_match_search_imp.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                search_imp = listView_match_search_imp.getItemAtPosition(position).toString()
                searchImportantAdapter.setSelectedIndex(position)
                Log.d(logTAG, "position search_imp $position $search_imp")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        rv_match_ethncity.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_ethncity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = ethnicityAdapter.getItem(position)
                        ethnicityAdapter.selectedList(selectedEthnicity)
                        if (selectedEthnicity.contains(data)) {
                            selectedEthnicity.remove(data)
                        } else {
                            selectedEthnicity.add(data)
                        }

                        Log.d(
                            logTAG,
                            "selected ethnicity $data array size ${selectedEthnicity.size}"
                        )
                    }
                })
        )

        rv_match_religion.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_religion,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = religionAdapter.getItem(position)
                        if (data == "All") {
                            if (selectedReligion.isEmpty()) {
                                for (i in 0 until religionDataList.size) {
                                    selectedReligion.add(religionDataList[i].religion_name)
                                }
                                religionAdapter.selectedList(selectedReligion)
                            } else if (!selectedReligion.contains("All")) {
                                selectedReligion.clear()
                                for (i in 0 until religionDataList.size) {
                                    selectedReligion.add(religionDataList[i].religion_name)
                                }
                                religionAdapter.selectedList(selectedReligion)
                            } else if (selectedReligion.contains("All")) {
                                selectedReligion.clear()
                                religionAdapter.selectedList(selectedReligion)
                            }

                        } else {
                            if (selectedReligion.contains("All")) {
                                selectedReligion.remove("All")
                            }
                            if (selectedReligion.contains(data)) {
                                selectedReligion.remove(data)
                            } else {
                                selectedReligion.add(data)
                            }
                            religionAdapter.selectedList(selectedReligion)
                        }
                        Log.d(logTAG, "selected religion $data array size ${selectedReligion.size}")
                    }
                })
        )

        rv_match_mTongue.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_mTongue,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = mTongueAdapter.getItem(position)
                        mTongueAdapter.selectedList(selectedmTongue)
                        for (i in 0 until motherTongueDataList.size) {
                            if (data == motherTongueDataList[i].mtongue_name) {
                                pos = i
                            }
                        }
                        if (selectedmTongue.contains(data)) {
                            selectedmTongue.remove(data)
                        } else {
                            selectedmTongue.add(data)
                        }
                        Log.d(logTAG, "selected religion $data array size ${selectedmTongue.size}")
                    }
                })
        )

        rv_match_education.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_education,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = educationAdapter.getItem(position)
                        educationAdapter.selectedList(selectedEducation)
                        if (selectedEducation.contains(data)) {
                            selectedEducation.remove(data)
                        } else {
                            selectedEducation.add(data)
                        }

                        Log.d(
                            logTAG,
                            "selected education $data array size ${selectedEducation.size}"
                        )
                    }
                })
        )

        rv_match_smoke.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_smoke,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = smokeAdapter.getItem(position)
                        smokeAdapter.selectedList(selectedSmoking)
                        if (selectedSmoking.contains(data)) {
                            selectedSmoking.remove(data)
                        } else {
                            selectedSmoking.add(data)
                        }

                        Log.d(logTAG, "selected education $data array size ${selectedSmoking.size}")
                    }
                })
        )

        rv_match_drink.addOnItemTouchListener(
            RecyclerItemClickListener(this@MatchPreferencesOne, rv_match_drink,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val data = drinkAdapter.getItem(position)
                        drinkAdapter.selectedList(selectedDrinking)
                        if (selectedDrinking.contains(data)) {
                            selectedDrinking.remove(data)
                        } else {
                            selectedDrinking.add(data)
                        }

                        Log.d(
                            logTAG,
                            "selected education $data array size ${selectedDrinking.size}"
                        )
                    }
                })
        )

        tv_next.setOnClickListener {

            when (page_counter) {
                1 -> {
                    if (max_age == "" || min_age == "") {
                        Toast.makeText(this, "Please fill the required field!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        /*val DIFF = max_age.toInt() - min_age.toInt()
                        if (DIFF < AGE_DIFF) {
                            Toast.makeText(
                                this,
                                "Make sure you have at least 6 years difference!",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            ll_match_age_range.visibility = View.GONE
                            ll_match_search_distance.visibility = View.VISIBLE
                            ll_match_search_imp.visibility = View.GONE
                            ll_match_religion.visibility = View.GONE
                            ll_match_mTongue.visibility = View.GONE
                            ll_match_ethnicity.visibility = View.GONE
                            ll_match_education.visibility = View.GONE
                            ll_match_occupation.visibility = View.GONE
                            ll_match_smoke.visibility = View.GONE
                            ll_match_drink.visibility = View.GONE

                            tv_back.visibility = View.VISIBLE

                            page_counter = 2

                            progress_value += 1
                            horizontal_progress_bar.setProgress(10f)
                            horizontal_progress_bar.animate()

                        }*/
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.VISIBLE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 2

                        progress_value += 1
                        horizontal_progress_bar.setProgress(10f)
                        horizontal_progress_bar.animate()
                    }
                    tv_skip.visibility = View.VISIBLE
                }

                2 -> {
                    if (searchDistanceAdapter.getSelectedIndex() == -1) {
                        search_distance = ""
                        Toast.makeText(this, "Please select one!", Toast.LENGTH_SHORT).show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.VISIBLE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 3
                        horizontal_progress_bar.setProgress(20f)
                        horizontal_progress_bar.animate()
                    }
                    tv_skip.visibility = View.VISIBLE
                }

                3 -> {
                    if (searchImportantAdapter.getSelectedIndex() == -1) {
                        search_imp = ""
                        Toast.makeText(this, "Please select one!", Toast.LENGTH_SHORT).show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.VISIBLE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 4

                        horizontal_progress_bar.setProgress(30f)
                        horizontal_progress_bar.animate()
                    }
                }

                4 -> {

                    if (religionAdapter.getSelectedSize()!! == 0) {
                        religion = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {

                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.VISIBLE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 5
                        horizontal_progress_bar.setProgress(40f)
                        horizontal_progress_bar.animate()
                    }
                    tv_skip.visibility = View.VISIBLE
                }

                5 -> {

                    if (mTongueAdapter.getSelectedSize()!! == 0) {
                        mTongue = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        /*if (casteDataList.isEmpty()) {
                            Toast.makeText(
                                this@MatchPreferencesOne, "Denomination is empty. Skipping layout",
                                Toast.LENGTH_SHORT
                            ).show()

                            ll_match_age_range.visibility = View.GONE
                            ll_match_search_distance.visibility = View.GONE
                            ll_match_search_imp.visibility = View.GONE
                            ll_match_ethnicity.visibility = View.GONE
                            ll_match_religion.visibility = View.GONE
                            ll_match_mTongue.visibility = View.GONE
                            ll_match_education.visibility = View.VISIBLE
                            ll_match_occupation.visibility = View.GONE
                            ll_match_smoke.visibility = View.GONE
                            ll_match_drink.visibility = View.GONE

                            tv_back.visibility = View.VISIBLE

                            page_counter = 7

                            horizontal_progress_bar.setProgress(60f)
                            horizontal_progress_bar.animate()


                        } else {*/

                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.VISIBLE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 6

                        horizontal_progress_bar.setProgress(50f)
                        horizontal_progress_bar.animate()
                    }
//                    }
                    tv_skip.visibility = View.VISIBLE
                }

                6 -> {
//                    if (casteDataList.isNotEmpty()) {

                    if (ethnicityAdapter.getSelectedSize()!! == 0) {
                        ethnicity = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.VISIBLE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 7

                        horizontal_progress_bar.setProgress(60f)
                        horizontal_progress_bar.animate()
                    }

                    /* } else {


                         ll_match_age_range.visibility = View.GONE
                         ll_match_search_distance.visibility = View.GONE
                         ll_match_search_imp.visibility = View.GONE
                         ll_match_ethnicity.visibility = View.GONE
                         ll_match_religion.visibility = View.GONE
                         ll_match_mTongue.visibility = View.GONE
                         ll_match_education.visibility = View.VISIBLE
                         ll_match_occupation.visibility = View.GONE
                         ll_match_smoke.visibility = View.GONE
                         ll_match_drink.visibility = View.GONE

                         tv_back.visibility = View.VISIBLE

                         page_counter = 7

                         horizontal_progress_bar.setProgress(60f)
                         horizontal_progress_bar.animate()

                     }*/
                    tv_skip.visibility = View.VISIBLE
                }
                7 -> {
                    if (educationAdapter.getSelectedSize()!! == 0) {
                        education = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.VISIBLE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 8

                        horizontal_progress_bar.setProgress(70f)
                        horizontal_progress_bar.animate()
                    }
                    tv_skip.visibility = View.VISIBLE
                }
                8 -> {
                    if (selectedOccupation.size == 0) {
                        occupation = ""
                        Toast.makeText(
                            this,
                            "Please select your Match's Occupation!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.VISIBLE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 9

                        horizontal_progress_bar.setProgress(80f)
                        horizontal_progress_bar.animate()
                    }
                    tv_skip.visibility = View.VISIBLE
                }
                9 -> {
                    if (smokeAdapter.getSelectedSize()!! == 0) {
                        smoke = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.VISIBLE

                        tv_back.visibility = View.VISIBLE

                        page_counter = 10

                        horizontal_progress_bar.setProgress(90f)
                        horizontal_progress_bar.animate()
                    }
                    Log.d(logTAG, "click size ${educationAdapter.getSelectedSize()!!}")
                    tv_next.text = "Continue"
                    tv_skip.visibility = View.GONE
                }
                10 -> {
                    if (drinkAdapter.getSelectedSize()!! == 0) {
                        drink = ""
                        Toast.makeText(this, "Please select at least one!", Toast.LENGTH_SHORT)
                            .show()
                    } else {

                        horizontal_progress_bar.setProgress(100f)
                        horizontal_progress_bar.animate()

                        ll_match_age_range.visibility = View.GONE
                        ll_match_search_distance.visibility = View.GONE
                        ll_match_search_imp.visibility = View.GONE
                        ll_match_religion.visibility = View.GONE
                        ll_match_mTongue.visibility = View.GONE
                        ll_match_ethnicity.visibility = View.GONE
                        ll_match_education.visibility = View.GONE
                        ll_match_occupation.visibility = View.GONE
                        ll_match_smoke.visibility = View.GONE
                        ll_match_drink.visibility = View.GONE

                        tv_back.visibility = View.GONE

                        progressDialog.show()

                        education = commaSeparatedString(selectedEducation)
                        religion = commaSeparatedString(selectedReligion)
                        mTongue = commaSeparatedString(selectedmTongue)
                        ethnicity = commaSeparatedString(selectedEthnicity)
                        occupation = commaSeparatedString(selectedOccupation)
                        smoke = commaSeparatedString(selectedSmoking)
                        drink = commaSeparatedString(selectedDrinking)

                        Log.d(
                            logTAG,
                            "$search_distance, $search_imp, $ethnicity, $religion, $education, $smoke, $drink," +
                                    "$occupation, $min_age, $max_age, $user_id"
                        )

                        val apiService = ApiInterface.create()
                        val call = apiService.profileMatchApi(
                            user_id!!,
                            ethnicity,
                            religion,
                            mTongue,
                            education,
                            min_age,
                            max_age,
                            search_distance,
                            search_imp,
                            occupation,
                            smoke,
                            drink
                        )
                        Log.d(logTAG, "Api Call Request : " + call.toString())
                        call.enqueue(object : Callback<ProfileMatchResponse> {
                            override fun onFailure(call: Call<ProfileMatchResponse>, t: Throwable) {
                                Log.e(logTAG, "Response error : " + t.toString())
                                Toast.makeText(
                                    this@MatchPreferencesOne,
                                    "Something went wrong. Please try again!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<ProfileMatchResponse>,
                                response: Response<ProfileMatchResponse>
                            ) {

                                Log.d(logTAG, "Response success : ${response.body()}")
                                if (response.body()!!.status == "1") {

                                    if (progressDialog.isShowing) {
                                        progressDialog.dismiss()
                                    }

                                    val match_data = response.body()!!.match_data

                                    if (match_data!!.partner_age_min!!.equals(null)) {
                                        partner_age_min = "0"
                                    } else {
                                        partner_age_min = match_data.partner_age_min!!
                                    }

                                    if (match_data.partner_age_max!!.equals(null)) {
                                        partner_age_max = "0"
                                    } else {
                                        partner_age_max = match_data.partner_age_max
                                    }

                                    if (match_data.partner_match_distance!!.equals(null)) {
                                        partner_match_distance = "0"
                                    } else {
                                        partner_match_distance = match_data.partner_match_distance
                                    }

                                    if (match_data.partner_match_search!!.equals(null)) {
                                        partner_match_search = "0"
                                    } else {
                                        partner_match_search = match_data.partner_match_search
                                    }

                                    if (match_data.partner_ethnicity!!.equals(null)) {
                                        partner_ethnicity = "0"
                                    } else {
                                        partner_ethnicity = match_data.partner_ethnicity
                                    }

                                    if (match_data.partner_religion!!.equals(null)) {
                                        partner_religion = "0"
                                    } else {
                                        partner_religion = match_data.partner_religion
                                    }

                                    if (match_data.partner_mtongue_name!!.equals(null)) {
                                        partner_mTongue = "0"
                                    } else {
                                        partner_mTongue = match_data.partner_mtongue_name
                                    }

                                    /* if (match_data.partner_denomination == null) {
                                         partner_denomination = "0"
                                     } else {
                                         partner_denomination = match_data.partner_denomination
                                     }*/

                                    if (match_data.partner_education!!.equals(null)) {
                                        partner_education = "0"
                                    } else {
                                        partner_education = match_data.partner_education
                                    }

                                    if (match_data.partner_occupation!!.equals(null)) {
                                        partner_occupation = "0"
                                    } else {
                                        partner_occupation = match_data.partner_occupation
                                    }

                                    if (match_data.partner_smoke!!.equals(null)) {
                                        partner_smoke = "0"
                                    } else {
                                        partner_smoke = match_data.partner_smoke
                                    }

                                    if (match_data.partner_drink!!.equals(null)) {
                                        partner_drink = "0"
                                    } else {
                                        partner_drink = match_data.partner_drink
                                    }

                                    if (!match_data.signup_step!!.equals(null)) {
                                        signup_step = match_data.signup_step
                                    }

                                    sessionManager.userMatchPreferences(
                                        partner_age_min,
                                        partner_age_max,
                                        partner_match_distance,
                                        partner_match_search,
                                        partner_ethnicity,
                                        partner_religion,
                                        partner_mTongue,
                                        partner_education,
                                        partner_occupation,
                                        partner_smoke,
                                        partner_drink,
                                        signup_step
                                    )

                                    val intent = Intent(
                                        this@MatchPreferencesOne,
                                        QuizSetupWelcome::class.java
                                    )
                                    startActivity(intent)
                                    this@MatchPreferencesOne.finish()

                                } else {
                                    Toast.makeText(
                                        this@MatchPreferencesOne,
                                        response.body()!!.result,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        })
                    }
                }
            }
        }

        tv_back.setOnClickListener {

            when (page_counter) {
                1 -> {
                    super.onBackPressed()
                }
                2 -> {
                    ll_match_age_range.visibility = View.VISIBLE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 1

                    horizontal_progress_bar.setProgress(0f)
                    horizontal_progress_bar.animate()
                }
                3 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.VISIBLE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 2
                    horizontal_progress_bar.setProgress(10f)
                    horizontal_progress_bar.animate()
                }
                4 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.VISIBLE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 3
                    horizontal_progress_bar.setProgress(20f)
                    horizontal_progress_bar.animate()
                }
                5 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.VISIBLE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    page_counter = 4
                    horizontal_progress_bar.setProgress(30f)
                    horizontal_progress_bar.animate()
                }
                6 -> {

                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.VISIBLE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 5
                    horizontal_progress_bar.setProgress(40f)
                    horizontal_progress_bar.animate()
                }
                7 -> {
                    /*   if (casteDataList.isEmpty()) {
                           Toast.makeText(
                               this@MatchPreferencesOne, "Denomination is empty. Skipping layout!",
                               Toast.LENGTH_SHORT
                           ).show()

                           ll_match_age_range.visibility = View.GONE
                           ll_match_search_distance.visibility = View.GONE
                           ll_match_search_imp.visibility = View.GONE
                           ll_match_ethnicity.visibility = View.GONE
                           ll_match_religion.visibility = View.VISIBLE
                           ll_match_mTongue.visibility = View.GONE
                           ll_match_education.visibility = View.GONE
                           ll_match_occupation.visibility = View.GONE
                           ll_match_smoke.visibility = View.GONE
                           ll_match_drink.visibility = View.GONE

                           tv_back.visibility = View.GONE

                           page_counter = 5
                           horizontal_progress_bar.setProgress(40f)
                           horizontal_progress_bar.animate()

                       } else {*/

                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.VISIBLE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE
                    tv_back.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 6
                    horizontal_progress_bar.setProgress(50f)
                    horizontal_progress_bar.animate()
//                    }
                }
                8 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.VISIBLE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 7
                    horizontal_progress_bar.setProgress(60f)
                    horizontal_progress_bar.animate()
                }
                9 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.VISIBLE
                    ll_match_smoke.visibility = View.GONE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 8
                    horizontal_progress_bar.setProgress(70f)
                    horizontal_progress_bar.animate()
                }
                10 -> {
                    ll_match_age_range.visibility = View.GONE
                    ll_match_search_distance.visibility = View.GONE
                    ll_match_search_imp.visibility = View.GONE
                    ll_match_religion.visibility = View.GONE
                    ll_match_mTongue.visibility = View.GONE
                    ll_match_ethnicity.visibility = View.GONE
                    ll_match_education.visibility = View.GONE
                    ll_match_occupation.visibility = View.GONE
                    ll_match_smoke.visibility = View.VISIBLE
                    ll_match_drink.visibility = View.GONE

                    tv_back.visibility = View.GONE

                    page_counter = 9
                    horizontal_progress_bar.setProgress(80f)
                    horizontal_progress_bar.animate()
                    tv_skip.visibility = View.VISIBLE
                    tv_next.text = "Next"
                }
            }
        }

        tv_skip.setOnClickListener {

            if (page_counter == 10) {

            } else {
                val builder = AlertDialog.Builder(this@MatchPreferencesOne)
                builder.setTitle("Warning!")
                    .setMessage("Do you wish to SKIP?")
                    .setCancelable(false)
                    .setNegativeButton("No", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, id: Int) {
                            dialog!!.dismiss()
                        }
                    })
                    .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, id: Int) {


                            val photo1 = user_details.get(DllSessionManager.PHOTO1)
                            if(photo1.equals("0")){
                                val home_intent = Intent(this@MatchPreferencesOne, ProfilePhotos::class.java)
                                //home_intent.putExtra("from_list","match_setup")
                                startActivity(home_intent)
                                finish()
                            }else {

                                val home_intent = Intent(this@MatchPreferencesOne, DashboardActivity::class.java)
                                home_intent.putExtra("from_list", "match_setup")
                                startActivity(home_intent)
                                finish()
                            }


                            /*val home_intent =
                                Intent(this@MatchPreferencesOne, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "match_setup")
                            startActivity(home_intent)
                            finish()*/
                        }
                    })
                val alertDialog = builder.create()
                alertDialog.show()
            }
        }
    }

    private fun initialize() {
        ll_match_age_range = findViewById(R.id.ll_match_age_range)
        ll_match_search_distance = findViewById(R.id.ll_match_search_distance)
        ll_match_search_imp = findViewById(R.id.ll_match_search_imp)
        ll_match_ethnicity = findViewById(R.id.ll_match_ethnicity)
        ll_match_religion = findViewById(R.id.ll_match_religion)
        ll_match_mTongue = findViewById(R.id.ll_match_mTongue)
        ll_match_education = findViewById(R.id.ll_match_education)
        ll_match_occupation = findViewById(R.id.ll_match_occupation)
        ll_match_smoke = findViewById(R.id.ll_match_smoke)
        ll_match_drink = findViewById(R.id.ll_match_drink)

        sp_match_min = findViewById(R.id.sp_match_min)
        sp_match_max = findViewById(R.id.sp_match_max)

        listView_match_search_distance = findViewById(R.id.listView_match_search_distance)
        listView_match_search_imp = findViewById(R.id.listView_match_search_imp)

        rv_match_ethncity = findViewById(R.id.rv_match_ethncity)
        rv_match_religion = findViewById(R.id.rv_match_religion)
        rv_match_mTongue = findViewById(R.id.rv_match_mTongue)
        rv_match_education = findViewById(R.id.rv_match_education)
        rv_match_smoke = findViewById(R.id.rv_match_smoke)
        rv_match_drink = findViewById(R.id.rv_match_drink)

        match_mTongue_empty = findViewById(R.id.match_mTongue_empty)
        et_match_mTongue = findViewById(R.id.et_match_mTongue)
        tv_skip = findViewById(R.id.tv_skip)

        /*lm_ethnicity = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_religion = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_denomination = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_education = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_smoke = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_drink = LinearLayoutManager(this, LinearLayout.VERTICAL, false)*/

        horizontal_progress_bar = findViewById(R.id.horizontal_progress_bar)
        horizontal_progress_bar.isClickable = false
        horizontal_progress_bar.isFocusable = false
        horizontal_progress_bar.isEnabled = false

        lm_ethnicity = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_religion = GridLayoutManager(this, 2)
        lm_mTongue = GridLayoutManager(this, 2)
        lm_education = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_smoke = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        lm_drink = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        selectedEducation = ArrayList()
        selectedReligion = ArrayList()
        selectedmTongue = ArrayList()
        selectedEthnicity = ArrayList()
        selectedSmoking = ArrayList()
        selectedDrinking = ArrayList()
        religionDataList = ArrayList()
        ethnicityList = ArrayList()
        educationList = ArrayList()
        occupationList = ArrayList()
        incomeList = ArrayList()
        smokeList = ArrayList()
        drinkList = ArrayList()
        questionsList = ArrayList()
        religionDataList = ArrayList()
        motherTongueDataList = ArrayList()
        searchMultiSpinner_occ = findViewById(R.id.searchMultiSpinner_occ)

        progressDialog = Dialog(this)

        min_age_array = ArrayList()
        for (i in MIN_AGE..MAX_AGE) {
            val age = i.toString()
            min_age_array.add(age)
        }

        max_age_array = ArrayList()
        /*for (i in MIN_AGE_mx..MAX_AGE) {
            val age = i.toString()
            max_age_array.add(age)
        }*/
        for (i in MIN_AGE..MAX_AGE) {
            val age = i.toString()
            max_age_array.add(age)
        }


        min_age_adapter =
            object : ArrayAdapter<String>(
                this@MatchPreferencesOne,
                R.layout.item_spinner,
                min_age_array
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return true
                }

                override fun getDropDownView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    return view
                }
            }
        min_age_adapter.setDropDownViewResource(R.layout.item_spinner)

        max_age_adapter =
            object : ArrayAdapter<String>(
                this@MatchPreferencesOne,
                R.layout.item_spinner,
                max_age_array
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return true
                }

                override fun getDropDownView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    return view
                }
            }
        max_age_adapter.setDropDownViewResource(R.layout.item_spinner)

        rv_match_ethncity.setHasFixedSize(true)
        rv_match_religion.setHasFixedSize(true)
        rv_match_mTongue.setHasFixedSize(true)
        rv_match_education.setHasFixedSize(true)
        rv_match_smoke.setHasFixedSize(true)
        rv_match_drink.setHasFixedSize(true)

        rv_match_education.layoutManager = lm_education
        rv_match_ethncity.layoutManager = lm_ethnicity
        rv_match_religion.layoutManager = lm_religion
        rv_match_mTongue.layoutManager = lm_mTongue
        rv_match_smoke.layoutManager = lm_smoke
        rv_match_drink.layoutManager = lm_drink

        searchDistanceAdapter = MatchPrefsSearchDistanceAdapter(
            this@MatchPreferencesOne, resources.getStringArray(R.array.search_distance)
        )
        searchImportantAdapter =
            MatchPrefsImportantAdapter(
                this@MatchPreferencesOne,
                resources.getStringArray(R.array.search_imp)
            )
        mTongueAdapter = MotherTongueAdapter(this@MatchPreferencesOne, motherTongueDataList)

        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })
    }

    private fun commaSeparatedString(list: ArrayList<String>): String {
        var result = ""
        if (list.size > 0) {
            val sb = StringBuilder()
            for (s in list) {
                sb.append(s).append(",")
            }
            result = sb.deleteCharAt(sb.length - 1).toString()
        }
        return result
    }

    override fun onBackPressed() {
        if (page_counter == 1) {
            progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish()
                        progressDialog.dismiss()
                    }
                    return true
                }
            })

        } else {
            val builder = AlertDialog.Builder(this@MatchPreferencesOne)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to head start again?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }
                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        val intent =
                            Intent(this@MatchPreferencesOne, MatchPreferencesWelcome::class.java)
                        startActivity(intent)
                        this@MatchPreferencesOne.finish()
                    }
                })
            val alertDialog = builder.create()
            alertDialog.show()
        }
    }

    private fun getMotherTongueApi() {
        val motherTongueApi = ApiInterface.create()
        val motherTongueCall = motherTongueApi.getMotherTongueApi()
        motherTongueCall.enqueue(object : Callback<MotherTongueResponse> {
            override fun onFailure(call: Call<MotherTongueResponse>, t: Throwable) {
                Log.e(logTAG, "motherTongueResponse : error : $t")
                Toast.makeText(
                    this@MatchPreferencesOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<MotherTongueResponse>, response: Response<MotherTongueResponse>
            ) {
                Log.d(logTAG, "motherTongueResponse: success: ${response.body()!!.data.toString()}")
                if (response.body() != null) {
                    if (response.body()!!.status == "1") {

                        motherTongueDataList = response.body()!!.data!!
                        Log.d(logTAG, "motherTongueResponse: size: ${motherTongueDataList.size}")
                        if (motherTongueDataList.size > 0) {
                            mTongueAdapter =
                                MotherTongueAdapter(this@MatchPreferencesOne, motherTongueDataList)
                            rv_match_mTongue.adapter = mTongueAdapter
                        }
                    } else {
                        Toast.makeText(
                            this@MatchPreferencesOne,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        this@MatchPreferencesOne,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })
    }

    private fun getReligionApi() {
        val religionApi = ApiInterface.create()
        val religionCall = religionApi.getReligionApi()
        religionCall.enqueue(object : Callback<ReligionResponse> {
            override fun onFailure(call: Call<ReligionResponse>, t: Throwable) {
                Log.e(logTAG, "ReligionResponse : error : ${t.toString()}")
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }
                Toast.makeText(
                    this@MatchPreferencesOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<ReligionResponse>, response: Response<ReligionResponse>
            ) {
                Log.d(logTAG, "ReligionResponse: success: ${response.body()!!.data.toString()}")
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                if (response.body()!!.status == "1") {
                    religionDataList = response.body()!!.data!!
                    if (religionDataList.size > 0) {
                        religionAdapter =
                            MatchPrefsReligionAdapter(this@MatchPreferencesOne, religionDataList)
                        rv_match_religion.adapter = religionAdapter
                    }
                }
            }
        })
    }

    private fun getRegisterApi() {
        val api = ApiInterface.create()
        val apiCall = api.registerProfileApi()
        apiCall.enqueue(object : Callback<RegisterProfileSetupResponse> {
            override fun onFailure(call: Call<RegisterProfileSetupResponse>, t: Throwable) {
                Log.e(logTAG, "RegisterProfileSetupResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@MatchPreferencesOne,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<RegisterProfileSetupResponse>,
                response: Response<RegisterProfileSetupResponse>
            ) {
                Log.d(
                    logTAG,
                    "RegisterProfileSetupResponse: success: ${response.body()!!.data.toString()}"
                )
                if (response.body()!!.status == "1") {
                    questionsList = response.body()!!.data!!
                    for (i in 0 until questionsList.size) {
                        if (questionsList.get(i).title == "ethnicity") {
                            ethnicityList = questionsList[i].answer!!
                        }
                        if (questionsList.get(i).title == "education") {
                            educationList = questionsList[i].answer!!
                        }
                        if (questionsList.get(i).title == "occupation") {
                            occupationList = questionsList[i].answer!!
                        }
                        if (questionsList.get(i).title == "income") {
                            incomeList = questionsList[i].answer!!
                        }
                        if (questionsList.get(i).title == "smoke") {
                            smokeList = questionsList[i].answer!!
                        }
                        if (questionsList.get(i).title == "drink") {
                            drinkList = questionsList[i].answer!!
                        }
                    }
                }

                // adapters
                drinkAdapter = MatchPrefsDrinkAdapter(this@MatchPreferencesOne, drinkList)
                educationAdapter =
                    MatchPrefsEducationAdapter(this@MatchPreferencesOne, educationList)
                occupationAdapter =
                    MatchPrefsOccupationAdapter(this@MatchPreferencesOne, occupationList)
                ethnicityAdapter =
                    MatchPrefsEthnicityAdapter(this@MatchPreferencesOne, ethnicityList)
                smokeAdapter = MatchPrefsSmokeAdapter(this@MatchPreferencesOne, smokeList)

                rv_match_ethncity.adapter = ethnicityAdapter
                rv_match_education.adapter = educationAdapter
                rv_match_smoke.adapter = smokeAdapter
                rv_match_drink.adapter = drinkAdapter

                val occupations = ArrayList<String>()
                for (i in 0 until occupationList.size) {
                    occupations.add(occupationList[i].answer)
                }
                val occ = occupations
                val list_occ = ArrayList<KeyPairBoolData>()
                for (i in 0 until occ.size) {
                    val h = KeyPairBoolData()
                    h.id = (i + 1).toLong()
                    h.name = occ[i]
                    h.isSelected = false
                    list_occ.add(h)
                }
                searchMultiSpinner_occ.setItems(list_occ, -1, object : SpinnerListener {
                    override fun onItemsSelected(items: MutableList<KeyPairBoolData>?) {

                        for (i in items!!.indices) {
                            if (items[i].isSelected) {
                                if (!selectedOccupation.contains(items[i].name)) {
                                    selectedOccupation.add(items[i].name)
                                }
                            } else if (!items[i].isSelected && selectedOccupation.contains(items[i].name)) {
                                Log.d(logTAG, " item deselected")
                                selectedOccupation.remove(items[i].name)
                            }
                        }
                        Log.d(logTAG, "item size ${selectedOccupation.size}")
                    }
                })
            }
        })
    }

}
