package divorcelovelounge.com.dll

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import divorcelovelounge.com.dll.TabLayoutHelper.AnimatedTabLayout
import divorcelovelounge.com.dll.TabLayoutHelper.NonSwipeableViewPager
import divorcelovelounge.com.dll.TabLayoutHelper.PagerAdapter
import android.content.Intent
import android.view.Window
import divorcelovelounge.com.dll.Helper.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap
import android.R.attr.start
import android.animation.ObjectAnimator
import android.content.Context
import divorcelovelounge.com.dll.ApiPojo.*
import android.view.animation.AnimationUtils.loadAnimation
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import divorcelovelounge.com.dll.Chat.models.Chat
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatDelegate
import android.view.LayoutInflater
import android.widget.*
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.nanotasks.Completion
import com.nanotasks.Tasks
import divorcelovelounge.com.dll.Chat.Activities.ChatsListsFragment
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession
import divorcelovelounge.com.dll.Fragments.*


class DashboardActivity : AppCompatActivity() {


    lateinit var viewpager: ViewPager
    lateinit var rl_helper_view: RelativeLayout
    lateinit var ll_dashboard_main: LinearLayout
    lateinit var tv_go: CustomTextViewBold
    lateinit var ripple_view: RippleBackground
    private var doubleBackToExitPressedOnce = false
    private var dllSlideManager: DllSlideManager? = null
    private var dllsesstion: DllSessionManager? = null
    lateinit var tv_favorite_count_id: CustomTextView
    lateinit var tv_msg_count_id: CustomTextView
    lateinit var tv_chat_count_id: CustomTextView


    /* lateinit var tv_new_profile_id: CustomTextView
     lateinit var tv_new_favorite_id: CustomTextView
     lateinit var tv_new_discover_id: CustomTextView
     lateinit var tv_new_chat_id: CustomTextView
     lateinit var tv_new_inbox_id: CustomTextView*/

    lateinit var user_details: HashMap<String, String>

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""
    lateinit var chat: Chat

    lateinit var tabLayout: TabLayout

    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView
    lateinit var tabThree: TextView
    lateinit var tabFour: TextView
    lateinit var tabFive: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)


        viewpager = findViewById<NonSwipeableViewPager>(R.id.viewpager)
        // val atl = findViewById<AnimatedTabLayout>(R.id.atl)

        viewpager.offscreenPageLimit = 1

        rl_helper_view = findViewById(R.id.rl_helper_view)
        ll_dashboard_main = findViewById(R.id.ll_dashboard_main)
        tv_go = findViewById(R.id.tv_go)
        ripple_view = findViewById(R.id.ripple_view)
        tv_favorite_count_id = findViewById(R.id.tv_favorite_count_id)
        tv_msg_count_id = findViewById(R.id.tv_msg_count_id)
        tv_chat_count_id = findViewById(R.id.tv_chat_count_id)



        dllSlideManager = DllSlideManager(this)
        dllsesstion = DllSessionManager(this)
        user_details = dllsesstion!!.userDetails
        ripple_view.startRippleAnimation()

        location_details_list = dllsesstion!!.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()



        if (!dllSlideManager!!.isFirstTimeShowed) {
            rl_helper_view.visibility = View.GONE
            ll_dashboard_main.visibility = View.VISIBLE
        } else {
            rl_helper_view.visibility = View.VISIBLE
            ll_dashboard_main.visibility = View.GONE

        }

        tv_go.setOnClickListener {
            dllSlideManager!!.setFirstTimeShowed(false)
            rl_helper_view.visibility = View.GONE
            ll_dashboard_main.visibility = View.VISIBLE
        }

        setupViewPager(viewpager)

        tabLayout = findViewById(R.id.tabLayout) as TabLayout
        tabLayout.setupWithViewPager(viewpager)
        setupTabIcons()
        // val adapter = PagerAdapter(supportFragmentManager, "")
        // viewpager.adapter = adapter

        viewpager.currentItem = 2

        getLoginStatus(user_details.get(DllSessionManager.USER_ID).toString(), "1")
        tabThree.setTextColor(resources.getColor(R.color.red_color))

        tabLayout.addOnTabSelectedListener(object :
            TabLayout.BaseOnTabSelectedListener<TabLayout.Tab> {
            override fun onTabSelected(tab: TabLayout.Tab) {
                Log.e("data_tabs", tab.position.toString() + "")

                 /*Fragment f = mAdapter.getFragment(mPager.getCurrentItem());
                f.loadData();*/

                if (tab!!.getPosition() == 0) {
                    tabOne.setTextColor(resources.getColor(R.color.red_color))
                    tabTwo.setTextColor(resources.getColor(R.color.note_gray))
                    tabThree.setTextColor(resources.getColor(R.color.note_gray))
                    tabFour.setTextColor(resources.getColor(R.color.note_gray))
                    tabFive.setTextColor(resources.getColor(R.color.note_gray))
                } else if (tab.getPosition() == 1) {
                    tabOne.setTextColor(resources.getColor(R.color.note_gray))
                    tabTwo.setTextColor(resources.getColor(R.color.red_color))
                    tabThree.setTextColor(resources.getColor(R.color.note_gray))
                    tabFour.setTextColor(resources.getColor(R.color.note_gray))
                    tabFive.setTextColor(resources.getColor(R.color.note_gray))
                } else if (tab.getPosition() == 2) {
                    tabOne.setTextColor(resources.getColor(R.color.note_gray))
                    tabTwo.setTextColor(resources.getColor(R.color.note_gray))
                    tabThree.setTextColor(resources.getColor(R.color.red_color))
                    tabFour.setTextColor(resources.getColor(R.color.note_gray))
                    tabFive.setTextColor(resources.getColor(R.color.note_gray))

                } else if (tab.getPosition() == 3) {
                    tabOne.setTextColor(resources.getColor(R.color.note_gray))
                    tabTwo.setTextColor(resources.getColor(R.color.note_gray))
                    tabThree.setTextColor(resources.getColor(R.color.note_gray))
                    tabFour.setTextColor(resources.getColor(R.color.red_color))
                    tabFive.setTextColor(resources.getColor(R.color.note_gray))
                } else if (tab.getPosition() == 4) {
                    tabOne.setTextColor(resources.getColor(R.color.note_gray))
                    tabTwo.setTextColor(resources.getColor(R.color.note_gray))
                    tabThree.setTextColor(resources.getColor(R.color.note_gray))
                    tabFour.setTextColor(resources.getColor(R.color.note_gray))
                    tabFive.setTextColor(resources.getColor(R.color.red_color))
                }


                //tabOne.setTextColor(getResources().getColor(R.color.red_color));

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                getMsgCounts(user_details.get(DllSessionManager.USER_ID).toString())
                getCounts(user_details.get(DllSessionManager.USER_ID).toString())


            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                Log.e("data_re", tab.position.toString() + "")
            }
        })

        chat = Chat()
        if (intent.getStringExtra("from_list").equals("profile_photos")) {
            viewpager.currentItem = 0
            tabOne.setTextColor(resources.getColor(R.color.red_color))
            tabTwo.setTextColor(resources.getColor(R.color.note_gray))
            tabThree.setTextColor(resources.getColor(R.color.note_gray))
            tabFour.setTextColor(resources.getColor(R.color.note_gray))
            tabFive.setTextColor(resources.getColor(R.color.note_gray))
        } else if (intent.getStringExtra("from_list").equals("interested_me")) {
            viewpager.currentItem = 1
            tabOne.setTextColor(resources.getColor(R.color.note_gray))
            tabTwo.setTextColor(resources.getColor(R.color.red_color))
            tabThree.setTextColor(resources.getColor(R.color.note_gray))
            tabFour.setTextColor(resources.getColor(R.color.note_gray))
            tabFive.setTextColor(resources.getColor(R.color.note_gray))

        } else if (intent.getStringExtra("from_list").equals("messages_list")) {
            viewpager.currentItem = 4
            tabOne.setTextColor(resources.getColor(R.color.note_gray))
            tabTwo.setTextColor(resources.getColor(R.color.note_gray))
            tabThree.setTextColor(resources.getColor(R.color.note_gray))
            tabFour.setTextColor(resources.getColor(R.color.note_gray))
            tabFive.setTextColor(resources.getColor(R.color.red_color))
        } else if (intent.getStringExtra("from_list").equals("chat_list")) {
            viewpager.currentItem = 3
            tabOne.setTextColor(resources.getColor(R.color.note_gray))
            tabTwo.setTextColor(resources.getColor(R.color.note_gray))
            tabThree.setTextColor(resources.getColor(R.color.note_gray))
            tabFour.setTextColor(resources.getColor(R.color.red_color))
            tabFive.setTextColor(resources.getColor(R.color.note_gray))
        } else if (intent.getStringExtra("from_list") == "notification") {
            viewpager.currentItem = 1
            tabOne.setTextColor(resources.getColor(R.color.note_gray))
            tabTwo.setTextColor(resources.getColor(R.color.red_color))
            tabThree.setTextColor(resources.getColor(R.color.note_gray))
            tabFour.setTextColor(resources.getColor(R.color.note_gray))
            tabFive.setTextColor(resources.getColor(R.color.note_gray))

        }





        //  atl.setupViewPager(viewpager)

        Log.e("chat_count", chat.getUnreadMessagesCount().toString())
        //atl.getTabAt(0).showBadge().setNumber(1)

        getMsgCounts(user_details.get(DllSessionManager.USER_ID).toString())
        getCounts(user_details.get(DllSessionManager.USER_ID).toString())
        // getTalkJsCount(user_details.get(DllSessionManager.USER_ID).toString())


        //Working code

       getChatStatus()


    }



    override fun onPause() {
        // getLoginStatus(user_details.get(DllSessionManager.USER_ID).toString(),"0")
        Log.e("pause_lll", "0")
        super.onPause()
    }

    override fun onStart() {
        // getLoginStatus(user_details.get(DllSessionManager.USER_ID).toString(),"1")
        Log.e("start_lll", "0")
        super.onStart()
    }

    override fun onResume() {
        // getLoginStatus(user_details.get(DllSessionManager.USER_ID).toString(),"1")
        Log.e("resume_lll", "0")
        super.onResume()
    }

    override fun onStop() {
        // getLoginStatus(user_details.get(DllSessionManager.USER_ID).toString(),"0")

        Log.e("stop_lll", "0")
        super.onStop()
    }

    private fun setupTabIcons() {

        tabOne = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabOne.text = "Profile"

        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dash_user_circle, 0, 0)
        tabLayout.getTabAt(0)!!.customView = tabOne


        tabTwo = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabTwo.text = "Favorite"
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dash_star_new, 0, 0)
        tabLayout.getTabAt(1)!!.customView = tabTwo

        tabThree = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabThree.text = "Discover"
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dash_discover_new, 0, 0)
        tabLayout.getTabAt(2)!!.customView = tabThree

        tabFour = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabFour.text = "Chat"
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dash_chat_new, 0, 0)
        tabLayout.getTabAt(3)!!.customView = tabFour

        tabFive = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabFive.text = "Inbox"
        tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dash_inbox_new, 0, 0)
        tabLayout.getTabAt(4)!!.customView = tabFive
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(ProfileFragment(), "Profile")
        adapter.addFrag(NewFavoriteFragment(), "Favorite")
        adapter.addFrag(SecondFragment(), "Discover")
        adapter.addFrag(ChatsListsFragment(), "Chat")
        adapter.addFrag(Inbox(), "Inbox")
        viewPager.adapter = adapter

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentPagerAdapter(manager) {
        private val mFragmentList = java.util.ArrayList<Fragment>()
        private val mFragmentTitleList = java.util.ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onBackPressed() {
        if (!viewpager.currentItem.equals(2)) {
            viewpager.currentItem = 2
            return
        } else if (doubleBackToExitPressedOnce) {
            //super.onBackPressed()
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)

            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


    private fun getMsgCounts(user_id: String) {
        val getCountsApi = ApiInterface.create()
        val religionCall = getCountsApi.getUsermessagesCountsApi(user_id = user_id)
        religionCall.enqueue(object : Callback<MessagecountResponse> {
            override fun onFailure(call: Call<MessagecountResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<MessagecountResponse>, response: Response<MessagecountResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "UserInterestCountResponse: success: ${response.body()!!.status}"
                    )

                    if (response.body()!!.status == "1") {

                        if (response.body()!!.totalcount == "0") {
                            tv_msg_count_id.visibility = View.INVISIBLE
                        } else {
                            tv_msg_count_id.visibility = View.VISIBLE
                            tv_msg_count_id.setText(response.body()!!.totalcount)

                            val anim2 = ObjectAnimator.ofFloat(tv_msg_count_id, "scaleY", 0.9f)
                            anim2.duration = 3000 // duration 3 seconds
                            anim2.start()

                            val anim = ObjectAnimator.ofFloat(tv_msg_count_id, "scaleX", 0.9f)
                            anim.duration = 3000 // duration 3 seconds
                            anim.start()



                            Log.d(
                                "Favourites_count",
                                response.body()!!.totalcount
                            )
                        }


                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCounts(user_id: String) {
        val getCountsApi = ApiInterface.create()
        val religionCall = getCountsApi.getUserCountsApi(user_id = user_id)
        religionCall.enqueue(object : Callback<UserInterestCountResponse> {
            override fun onFailure(call: Call<UserInterestCountResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "UserInterestCountResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<UserInterestCountResponse>, response: Response<UserInterestCountResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "UserInterestCountResponse: success: ${response.body()!!.status}"
                    )
                    if (response.body()!!.status == "1") {
                        val data = response.body()!!.allcounts
                        val interestsMeCount =
                            data!!.interestscount.toInt() + data.favouritedmecount.toInt()

                        Log.d(
                            "FavouritesFragment",
                            interestsMeCount.toString()
                        )

                        if (interestsMeCount.toString().equals("0")) {
                            tv_favorite_count_id.visibility = View.INVISIBLE
                        } else {
                            tv_favorite_count_id.visibility = View.VISIBLE
                            tv_favorite_count_id.setText(interestsMeCount.toString())
                            val anim = ObjectAnimator.ofFloat(tv_msg_count_id, "scaleX", 0.9f)
                            anim.duration = 3000 // duration 3 seconds
                            anim.start()

                            // Make the object height 50%
                            val anim2 = ObjectAnimator.ofFloat(tv_msg_count_id, "scaleY", 0.9f)
                            anim2.duration = 3000 // duration 3 seconds
                            anim2.start()

                        }

                    } else {
                        Toast.makeText(
                            this@DashboardActivity,
                            "Something went wrong. Please try again!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun getLoginStatus(user_id: String, status_login: String) {
        val getCountsApi = ApiInterface.create()
        val religionCall =
            getCountsApi.updateLoginStatusAPI(UiD = user_id, login_status = status_login)
        religionCall.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<ChangePasswordResponse>, response: Response<ChangePasswordResponse>
            ) {
                try {
                    Log.d(
                        "FavouritesFragment",
                        "ChangePasswordResponse: success: ${response.body()!!.status}"
                    )
                    if (response.body()!!.status == "1") {


                    } else {
                        /*Toast.makeText(
                            this@DashboardActivity,
                            "Something went wrong. Please try again!",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }



    private fun getChatStatus() {
        val getChatStatusList = ApiInterface.create()
        val religionCall = getChatStatusList.chatStatusAPI()
        religionCall.enqueue(object : Callback<ChatStatusResponse> {
            override fun onFailure(call: Call<ChatStatusResponse>, t: Throwable) {
                Log.e("ChatStatusResponse", "ChatStatusResponse : error : ${t.toString()}")
            }

            override fun onResponse(
                call: Call<ChatStatusResponse>, response: Response<ChatStatusResponse>
            ) {
                try {

                    if (response.body()!!.status == "1") {
                        if(!response.body()!!.chat_status.equals("0")){
                            loginAndStart(
                                user_details.get(DllSessionManager.USER_NAME_ID).toString(),
                                user_details.get(DllSessionManager.USER_PASSWORD).toString()
                            )
                            dllsesstion!!.getChatStaus("1",response.body()!!.message)
                        }else {
                            dllsesstion!!.getChatStaus("0",response.body()!!.message)
                        }


                    } else {
                        Log.e("chat_status","chat not working")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }



    private fun loginAndStart(userName: String, password: String) {

        Tasks.executeInBackground(this@DashboardActivity, {
            XMPPSession.startService(this@DashboardActivity)
            //((SplashActivity) getApplicationContext()).getService().login(userName, password);
            XMPPSession.getInstance().login(userName.trim(), password)
            Log.e("xmpp_user_id_ll", userName)
            Thread.sleep(XMPPSession.REPLY_TIMEOUT.toLong())
            null
        }, object : Completion<Any> {
            override fun onSuccess(context: Context, result: Any?) {

                //((SplashActivity) getApplicationContext()).startApplication();


            }

            override fun onError(context: Context, e: Exception) {
                Log.e("xmpp_user_id_ll", e.toString())
                XMPPSession.getInstance().xmppConnection.disconnect()
                XMPPSession.clearInstance()
            }
        })
    }

}
