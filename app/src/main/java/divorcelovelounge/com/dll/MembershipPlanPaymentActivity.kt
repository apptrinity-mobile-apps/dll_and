package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.squareup.picasso.Picasso
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList


class MembershipPlanPaymentActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>
    var planid = ""
    var userID = ""
    var paymenttype = "Online"

    lateinit var btnmakepayment: Button

    var online_cash = ""
    var plan_duration_stg = ""
    var plan_type_int = ""
    var duration_id = ""

    lateinit var duration_array: ArrayList<Durations_list>
    lateinit var new_duration_array: ArrayList<Durations_list>


    var plan_name_stg = ""
    var plan_month_amount_stg = ""
    var plan_discount_stg = ""
    var monthly_with_discount = ""

    lateinit var tv_plan_name_id: CustomTextView
    lateinit var tv_plan_amount_id: CustomTextView
    lateinit var tv_plan_discount_id: CustomTextView

    lateinit var tv_main_amount_id: CustomTextViewBold

    //  lateinit var rg_payment_duration: RadioGroup

    lateinit var cv_stripe_id: CardView
    lateinit var card_input_widget: CardInputWidget
    lateinit var rv_duration_id: RecyclerView
    lateinit var ll_main_list: CardView
    lateinit var cv_payment_type_header: CardView
    lateinit var tv_plan_type_header: CustomTextViewBold


    private var back_chat_toolbar: ImageView? = null

    lateinit var progress_bar_id: ProgressBar

    var stripe_token_stg = ""

    var amount_final = 0f
    var adapter_amount_final = 0f

    var get_amount_val = 0f

    var after_promo_amount = 0f
    lateinit var tv_sub_total_id: CustomTextViewBold

    lateinit var ll_free_id: CardView
    lateinit var tv_free_main_amount: CustomTextView

    lateinit var twoDForm: DecimalFormat

    lateinit var et_promo_code_id: EditText
    lateinit var tv_apply_promo_id: CustomTextViewBold

    var offercode_stg = ""
    var offer_id_stg = ""
    var type_stg = ""
    var value_stg = ""

    lateinit var tv_promo_applied_id: CustomTextView
    lateinit var ll_promotion_type: LinearLayout

    lateinit var ll_cash_id: CardView
    lateinit var ll_online_id: CardView

    lateinit var img_cash_id: ImageView
    lateinit var img_online_id: ImageView

    lateinit var progress_dialog:Dialog
    lateinit var success_dialog:Dialog

    lateinit var tv_ok_btn_id:CustomTextViewBold
    lateinit var tv_msg_title_id:CustomTextViewBold
    lateinit var tv_full_msg_id:CustomTextView
    lateinit var img_profile_pic_id:ImageView
    lateinit var iv_close:ImageView

    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    val PUBLISHABLE_KEY = "pk_test_MTLciMArdTGMNKAisIxdcGvk00PTzZsTH5"
    lateinit var card: Card
    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: java.util.ArrayList<AdsListDataResponse>

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memberplan_payment)
        progress_bar_id = findViewById(R.id.progress_bar_id)
        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails
        userID = user_details.get(DllSessionManager.USER_ID).toString()


        btnmakepayment = findViewById(R.id.btnmakepayment) as Button

        back_chat_toolbar = findViewById(R.id.back_chat_toolbar) as ImageView
        back_chat_toolbar!!.setOnClickListener {
            onBackPressed()
        }

        twoDForm = DecimalFormat("#.##")

        tv_plan_name_id = findViewById(R.id.tv_plan_name_id)
        tv_plan_amount_id = findViewById(R.id.tv_plan_amount_id)
        tv_plan_discount_id = findViewById(R.id.tv_plan_discount_id)
        rv_duration_id = findViewById(R.id.rv_duration_id)
        ll_main_list = findViewById(R.id.ll_main_list)
        cv_payment_type_header = findViewById(R.id.cv_payment_type_header)
        tv_plan_type_header = findViewById(R.id.tv_plan_type_header)
        tv_main_amount_id = findViewById(R.id.tv_main_amount_id)

        ll_cash_id = findViewById(R.id.ll_cash_id)
        ll_online_id = findViewById(R.id.ll_online_id)
        img_cash_id = findViewById(R.id.img_cash_id)
        img_online_id = findViewById(R.id.img_online_id)

        cv_stripe_id = findViewById(R.id.cv_stripe_id)
        ll_promotion_type = findViewById(R.id.ll_promotion_type)

        et_promo_code_id = findViewById(R.id.et_promo_code_id)
        tv_apply_promo_id = findViewById(R.id.tv_apply_promo_id)


        tv_promo_applied_id = findViewById(R.id.tv_promo_applied_id)
        tv_sub_total_id = findViewById(R.id.tv_sub_total_id)
        card_input_widget = findViewById(R.id.card_input_widget)

        ll_free_id = findViewById(R.id.ll_free_id)
        tv_free_main_amount = findViewById(R.id.tv_free_main_amount)

        sliderLayout = findViewById(R.id.slider)
        addsarrayList = java.util.ArrayList()

        location_details_list = dllSessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Membership")


        Log.e("MEMBSTATUS",user_details.get(DllSessionManager.MEMBERSHIP_STATUS))

        if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).toString().equals("1")) {
            ll_free_id.visibility = View.GONE
            cv_stripe_id.visibility = View.VISIBLE
            cv_payment_type_header.visibility = View.VISIBLE
            tv_plan_type_header.visibility = View.VISIBLE
            ll_promotion_type.visibility = View.VISIBLE

        } else {
            ll_free_id.visibility = View.VISIBLE
            paymenttype = "Cash"
            cv_stripe_id.visibility = View.GONE
            cv_payment_type_header.visibility = View.GONE
            tv_plan_type_header.visibility = View.GONE
            ll_promotion_type.visibility = View.GONE
        }




        ll_online_id.setCardBackgroundColor(Color.parseColor("#F9A956"))

        img_online_id.setImageDrawable(
            getResources().getDrawable(
                R.drawable.online_select_new_ic,
                getApplicationContext().getTheme()
            ))
       // cv_stripe_id.visibility = View.GONE

       /* ll_cash_id.setOnClickListener {
            paymenttype = "Cash"
            cv_stripe_id.visibility = View.GONE

            ll_cash_id.setCardBackgroundColor(Color.parseColor("#F9A956"))
            ll_online_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))

            img_cash_id.setImageDrawable(
                getResources().getDrawable(
                    R.drawable.cash_select_new_ic,
                    getApplicationContext().getTheme()
                ))

            img_online_id.setImageDrawable(
                getResources().getDrawable(
                    R.drawable.online_new_ic,
                    getApplicationContext().getTheme()
                ))

        }*/

        ll_online_id.setOnClickListener {
            paymenttype = "Online"
            //cv_stripe_id.visibility = View.VISIBLE

            ll_online_id.setCardBackgroundColor(Color.parseColor("#F9A956"))
           // ll_cash_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))

            /*img_cash_id.setImageDrawable(
                getResources().getDrawable(
                    R.drawable.cash_new_ic,
                    getApplicationContext().getTheme()
                ))*/

            img_online_id.setImageDrawable(
                getResources().getDrawable(
                    R.drawable.online_select_new_ic,
                    getApplicationContext().getTheme()
                ))
        }






        planid = intent.getStringExtra("planid")

        plan_name_stg = intent.getStringExtra("plan_name")
        plan_month_amount_stg = intent.getStringExtra("monthly_with_out_discount")
        plan_discount_stg = intent.getStringExtra("plan_discount")
        monthly_with_discount = intent.getStringExtra("monthly_with_discount")
        duration_id = intent.getStringExtra("duration_id")
        //val args = intent.getExtras("BUNDLE")
        //  val args = intent.extras
        //val args = intent.getBundleExtra("BUNDLE")
        new_duration_array = ArrayList()
        //duration_array = intent.getParcelableArrayExtra("duration_array") as ArrayList<Durations_list>
        duration_array = intent.getSerializableExtra("duration_array") as ArrayList<Durations_list>

        tv_plan_name_id.setText(plan_name_stg)
        tv_plan_amount_id.setText("$ " + plan_month_amount_stg + " /month")
        tv_plan_discount_id.setText(plan_discount_stg)

        // val yearly_cal = monthly_with_discount.toFloat() * 12

        /* tv_monthly_id.setText("$ "+plan_month_amount_stg)
         tv_yearly_id.setText("$ "+yearly_cal.toString())*/
        Log.e("duration_arraylll", duration_array.size.toString())


        amount_final =
            plan_month_amount_stg.toFloat() - (plan_month_amount_stg.toFloat() * plan_discount_stg.toFloat() / 100)

        after_promo_amount = amount_final

        for (k in 0 until duration_array.size) {

            new_duration_array.add(duration_array.get(k))
        }


        if (duration_id.equals("Free")) {
            ll_free_id.setCardBackgroundColor(Color.parseColor("#F9A956"))
            ll_main_list.setCardBackgroundColor(Color.parseColor("#FFFFFF"))

            tv_plan_amount_id.setText("$ " + "0" + " /month")
            tv_plan_discount_id.setText("100")


            tv_free_main_amount.setText("2 Months Free")

            tv_free_main_amount.visibility = View.VISIBLE
            tv_main_amount_id.visibility = View.VISIBLE


            tv_main_amount_id.setTextColor(ContextCompat.getColor(this, R.color.gray_border))

            tv_free_main_amount.setTextColor(ContextCompat.getColor(this, R.color.register_btn_color))

            tv_sub_total_id.setText("$ " + "0")
            btnmakepayment.text="Submit"

        } else if (duration_id.equals("one")) {


            ll_free_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            ll_main_list.setCardBackgroundColor(Color.parseColor("#F9A956"))

            tv_plan_amount_id.setText("$ " + plan_month_amount_stg + " /month")
            tv_plan_discount_id.setText(plan_discount_stg)


            tv_main_amount_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(amount_final)).toString())

            tv_sub_total_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(amount_final)).toString())

            tv_main_amount_id.visibility = View.VISIBLE
            tv_free_main_amount.visibility = View.VISIBLE

            tv_main_amount_id.setTextColor(ContextCompat.getColor(this, R.color.register_btn_color))

            tv_free_main_amount.setTextColor(ContextCompat.getColor(this, R.color.gray_border))

            btnmakepayment.text="Make Payment"

        } else {


            ll_free_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            ll_main_list.setCardBackgroundColor(Color.parseColor("#FFFFFF"))

            tv_main_amount_id.visibility = View.VISIBLE
            tv_free_main_amount.visibility = View.VISIBLE

            tv_main_amount_id.setTextColor(ContextCompat.getColor(this, R.color.gray_border))
            tv_free_main_amount.setTextColor(ContextCompat.getColor(this, R.color.gray_border))

            btnmakepayment.text="Make Payment"
        }


        tv_main_amount_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(amount_final)).toString())

        rv_duration_id.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_duration_id.itemAnimator = DefaultItemAnimator()
        rv_duration_id.hasFixedSize()
        val details_adapter = DurationListAdapter(duration_array, this)
        details_adapter.setHasStableIds(true)
        rv_duration_id.adapter = details_adapter
        details_adapter.notifyDataSetChanged()

        Log.e("lllllll", "$userID, $paymenttype, $planid, $duration_id, $stripe_token_stg")

        ll_free_id.setOnClickListener {



            cv_stripe_id.visibility = View.GONE
            cv_payment_type_header.visibility = View.GONE
            tv_plan_type_header.visibility = View.GONE
            ll_promotion_type.visibility = View.GONE
            paymenttype = "Cash"
            duration_id = "Free"

            after_promo_amount = 0f


            ll_free_id.setCardBackgroundColor(Color.parseColor("#F9A956"))
            ll_main_list.setCardBackgroundColor(Color.parseColor("#FFFFFF"))


            tv_plan_amount_id.setText("$ " + "0" + " /month")
            tv_plan_discount_id.setText("100")


            tv_free_main_amount.setText("2 Months Free")

            tv_sub_total_id.setText("$ " + "0")

            tv_free_main_amount.visibility = View.VISIBLE
            tv_main_amount_id.visibility = View.VISIBLE

            tv_main_amount_id.setTextColor(ContextCompat.getColor(this, R.color.gray_border))

            tv_free_main_amount.setTextColor(ContextCompat.getColor(this, R.color.register_btn_color))

            details_adapter.notifyDataSetChanged()

            btnmakepayment.text="Submit"
        }


        ll_main_list.setOnClickListener {

            cv_stripe_id.visibility = View.VISIBLE
            cv_payment_type_header.visibility = View.VISIBLE
            tv_plan_type_header.visibility = View.VISIBLE
            ll_promotion_type.visibility = View.VISIBLE

            duration_id = "one"
            paymenttype = "Online"
            after_promo_amount = amount_final

            ll_free_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            ll_main_list.setCardBackgroundColor(Color.parseColor("#F9A956"))

            tv_plan_amount_id.setText("$ " + plan_month_amount_stg + " /month")
            tv_plan_discount_id.setText(plan_discount_stg)


            tv_main_amount_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(amount_final)).toString())


            tv_sub_total_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(amount_final)).toString())

            tv_main_amount_id.visibility = View.VISIBLE
            tv_free_main_amount.visibility = View.VISIBLE

            tv_free_main_amount.setTextColor(ContextCompat.getColor(this, R.color.gray_border))
            tv_main_amount_id.setTextColor(ContextCompat.getColor(this, R.color.register_btn_color))

            btnmakepayment.text="Make Payment"

            details_adapter.notifyDataSetChanged()
        }


        btnmakepayment.setOnClickListener {

            online_cash = paymenttype

            Log.e("online_cash", online_cash)



            if (online_cash.equals("Online")) {
                /*val intent = Intent(this@MembershipPlanPaymentActivity, StripePaymentGateway::class.java)
                startActivity(intent)*/

                val cardToSave = card_input_widget.getCard()
                if (cardToSave == null) {
                    Toast.makeText(this@MembershipPlanPaymentActivity, "Invalid Card Data", Toast.LENGTH_SHORT).show()
                } else {
                    card = card_input_widget.card!!
                    buy()
                }


            } else {
                //service call cash mode
                stripe_token_stg = ""
                PaymentAPIcall()

            }
        }


        tv_apply_promo_id.setOnClickListener {
            val promo_code_stg = et_promo_code_id.text.toString().trim()

            if (promo_code_stg != "") {
                val apiService = ApiInterface.create()
                val call = apiService.checkPromocodeApi(userID, promo_code_stg)

                call.enqueue(object : Callback<PromoCodeResponse> {
                    override fun onResponse(
                        call: Call<PromoCodeResponse>,
                        response: retrofit2.Response<PromoCodeResponse>?
                    ) {
                        // Log.d("REQUEST", response!!.body()!!.status + "")
                        if (response!!.body()!!.status.equals("1")) {

                            offercode_stg = response!!.body()!!.data!!.offercode
                            offer_id_stg = response!!.body()!!.data!!.offer_id
                            type_stg = response!!.body()!!.data!!.type
                            value_stg = response!!.body()!!.data!!.value

                            Calculationpart(promo_code_stg)

                        } else {
                            Toast.makeText(
                                this@MembershipPlanPaymentActivity,
                                response!!.body()!!.result,
                                Toast.LENGTH_SHORT
                            ).show()

                            offercode_stg = ""
                            offer_id_stg =""
                            type_stg = ""
                            value_stg = ""

                            tv_promo_applied_id.visibility = View.GONE
                        }
                    }

                    override fun onFailure(call: Call<PromoCodeResponse>, t: Throwable) {
                        Log.e("Result_Order_details", t.toString())
                    }
                })
            } else {

            }
        }



        success_dialog = Dialog(this)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        img_profile_pic_id=success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id=success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_full_msg_id=success_dialog.findViewById(R.id.tv_full_msg_id)
        iv_close=success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility=View.GONE
        tv_ok_btn_id=success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            val intent = Intent(applicationContext, MembershipMyPlanActivity::class.java)
            intent.putExtra("from_membership","payment_screen")
            startActivity(intent)
        }

        Picasso.with(this@MembershipPlanPaymentActivity)
            .load(photbaseurl+user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

    }

    fun Calculationpart(promo_code_stg: String) {

        tv_promo_applied_id.visibility = View.VISIBLE

        var final_amount = 0f
        if (type_stg.equals("percent")) {
            final_amount = after_promo_amount - (after_promo_amount * value_stg.toFloat() / 100)

            tv_sub_total_id.text = "$ " + java.lang.Double.valueOf(twoDForm.format(final_amount)).toString()

            tv_promo_applied_id.setText("Promo Code "+promo_code_stg+ " has been Applied for "+value_stg+" % off")

        } else {
            final_amount = after_promo_amount - value_stg.toFloat()
            tv_sub_total_id.text = "$ " + java.lang.Double.valueOf(twoDForm.format(final_amount)).toString()

            tv_promo_applied_id.setText("Promo Code "+promo_code_stg+ " has been Applied for $"+value_stg+" off")
        }
    }

    private fun buy() {
        val validation = card!!.validateCard()
        if (validation) {
            startProgress(/*"Validating Credit Card"*/)
            Stripe(this).createToken(
                card!!,
                PUBLISHABLE_KEY,
                object : TokenCallback {
                    override fun onError(error: Exception) {
                        Log.d("Stripe", error.toString())
                    }

                    override fun onSuccess(token: Token) {
                        finishProgress()
                        // charge(token)
                        Log.e(
                            "cardToken_lll",
                            token.getId() + "--" + token.card!!.customerId + "---" + token.card!!.last4 + "---" + token.card!!.expMonth + "---" + token.card!!.expYear
                        )
                        stripe_token_stg = token.getId()

                        Log.e("kkkkk", "$userID, $paymenttype, $planid, $duration_id, $stripe_token_stg")


                        PaymentAPIcall()


                    }
                })
        } else if (!card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun startProgress() {

      //  progress_bar_id.visibility = View.VISIBLE



        progress_dialog = Dialog(this)
        progress_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progress_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        progress_dialog.setContentView(R.layout.alert_dialog_loading)
        val progress_window = success_dialog.window
        progress_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        progress_dialog.show()

    }

    private fun finishProgress() {
       // progress_bar_id.visibility = View.GONE
        progress_dialog.dismiss()
    }

    fun PaymentAPIcall() {
        startProgress()
        val apiService = ApiInterface.create()
        val call = apiService.makemembershippayment(
            userID,
            paymenttype,
            planid,
            duration_id,
            stripe_token_stg,
            offercode_stg,
            type_stg,
            value_stg
        )

        call.enqueue(object : Callback<UpdateMatchPreferenceResponse> {
            override fun onResponse(
                call: Call<UpdateMatchPreferenceResponse>,
                response: retrofit2.Response<UpdateMatchPreferenceResponse>?
            ) {

                finishProgress()
                // Log.d("REQUEST", response!!.body()!!.status + "")
                if (response!!.body()!!.status.equals("1")) {
                    success_dialog.show()
                }
            }

            override fun onFailure(call: Call<UpdateMatchPreferenceResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    inner class DurationListAdapter(val mServiceList: ArrayList<Durations_list>, val context: Context) :
        RecyclerView.Adapter<ViewHolder1>() {
        var index = -1

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.duration_item_layout, parent, false))

        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10
            holder.ll_duration_list_id.setOnClickListener {


                cv_stripe_id.visibility = View.VISIBLE
                cv_payment_type_header.visibility = View.VISIBLE
                tv_plan_type_header.visibility = View.VISIBLE
                ll_promotion_type.visibility = View.VISIBLE

                paymenttype = "Online"
                // index = position
                notifyDataSetChanged()
                duration_id = mServiceList.get(position).duration_id
                tv_main_amount_id.visibility = View.VISIBLE
                tv_main_amount_id.setTextColor(ContextCompat.getColor(context, R.color.gray_border))

                tv_free_main_amount.setTextColor(ContextCompat.getColor(context, R.color.gray_border))

                btnmakepayment.text="Make Payment"
            }
            /*if (index == position) {
                holder.img_main_id.setImageDrawable(
                    getResources().getDrawable(
                        R.drawable.ic_verification_mark,
                        getApplicationContext().getTheme()
                    )
                )


            } else {
                holder.img_main_id.setImageResource(0)
            }*/


            get_amount_val =
                mServiceList.get(position).amount.toFloat() - (mServiceList.get(position).amount.toFloat() * mServiceList.get(
                    position
                ).discount.toFloat() / 100)

           // after_promo_amount = get_amount_val

            holder.tv_main_after_discount_id.setText(
                "$ " + java.lang.Double.valueOf(
                    twoDForm.format(
                        get_amount_val
                    )
                ).toString()
            )

            if (duration_id.equals(mServiceList.get(position).duration_id)) {

                adapter_amount_final =
                    mServiceList.get(position).amount.toFloat() - (mServiceList.get(position).amount.toFloat() * mServiceList.get(
                        position
                    ).discount.toFloat() / 100)
                after_promo_amount = adapter_amount_final


                holder.cv_main_view_id.setCardBackgroundColor(Color.parseColor("#F9A956"))



                ll_free_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
                ll_main_list.setCardBackgroundColor(Color.parseColor("#FFFFFF"))


                tv_plan_amount_id.setText("$ " + mServiceList.get(position).amount + " /month")
                tv_plan_discount_id.setText(mServiceList.get(position).discount)


                holder.tv_main_after_discount_id.setText(
                    "$ " + java.lang.Double.valueOf(
                        twoDForm.format(
                            adapter_amount_final
                        )
                    ).toString()
                )


                tv_sub_total_id.setText("$ " + java.lang.Double.valueOf(twoDForm.format(adapter_amount_final)).toString())

                holder.tv_main_after_discount_id.visibility = View.VISIBLE
                tv_main_amount_id.visibility = View.VISIBLE

                tv_main_amount_id.setTextColor(ContextCompat.getColor(context, R.color.gray_border))
                tv_free_main_amount.setTextColor(ContextCompat.getColor(context, R.color.gray_border))

                holder.tv_main_after_discount_id.setTextColor(ContextCompat.getColor(context, R.color.register_btn_color))

            } else {
                holder.cv_main_view_id.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
                holder.tv_main_after_discount_id.visibility = View.VISIBLE

                holder.tv_main_after_discount_id.setTextColor(ContextCompat.getColor(context, R.color.gray_border))
            }

            holder.duration_txt_id.setText(mServiceList.get(position).month + " Months")


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        //val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
        val ll_duration_list_id = view.findViewById<LinearLayout>(R.id.ll_duration_list_id)
        val duration_txt_id = view.findViewById<CustomTextView>(R.id.duration_txt_id)
        val cv_main_view_id = view.findViewById<CardView>(R.id.cv_main_view_id)
        val tv_main_after_discount_id = view.findViewById<CustomTextViewBold>(R.id.tv_main_after_discount_id)

    }


    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,userID)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD", PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@MembershipPlanPaymentActivity);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@MembershipPlanPaymentActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString());
                                sliderLayout.addSlider(textSliderView);
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@MembershipPlanPaymentActivity);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }


}
