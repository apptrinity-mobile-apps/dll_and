package divorcelovelounge.com.dll

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import divorcelovelounge.com.dll.ApiPojo.ProfilePhotosResponse
import divorcelovelounge.com.dll.ApiPojo.UploadPhotoResponse
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Utils.Utils
import retrofit2.Call
import retrofit2.Callback
import java.io.IOException
import com.google.android.gms.common.util.IOUtils.toByteArray
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.KeyEvent
import android.view.Window
import android.widget.*
import divorcelovelounge.com.dll.Helper.ImageFilePath
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ProfilePhotos : AppCompatActivity(), View.OnClickListener {


    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>
    var photo1_stg = "nodata"
    var photo2_stg = "nodata"
    var photo3_stg = "nodata"
    var photo4_stg = "nodata"
    var photo5_stg = "nodata"
    var photo6_stg = "nodata"
    var photo7_stg = "nodata"
    var photo8_stg = "nodata"
    var photo9_stg = "nodata"
    var photo10_stg = "nodata"
    private val GALLERY = 1
    private val CAMERA = 2

    lateinit var iv_toolbar_back_id: ImageView

    lateinit var ll_second_row_id: LinearLayout
    lateinit var ll_third_row_id: LinearLayout
    lateinit var ll_four_row_id: LinearLayout
    lateinit var ll_five_row_id: LinearLayout
    lateinit var rl_one_id: RelativeLayout



    lateinit var img_photo_1: ImageView
    lateinit var img_photo_2: ImageView
    lateinit var img_photo_3: ImageView
    lateinit var img_photo_4: ImageView
    lateinit var img_photo_5: ImageView
    lateinit var img_photo_6: ImageView
    lateinit var img_photo_7: ImageView
    lateinit var img_photo_8: ImageView
    lateinit var img_photo_9: ImageView
    lateinit var img_photo_10: ImageView

    lateinit var img_click_1: ImageView
    lateinit var img_click_2: ImageView
    lateinit var img_click_3: ImageView
    lateinit var img_click_4: ImageView
    lateinit var img_click_5: ImageView
    lateinit var img_click_6: ImageView
    lateinit var img_click_7: ImageView
    lateinit var img_click_8: ImageView
    lateinit var img_click_9: ImageView
    lateinit var img_click_10: ImageView

    lateinit var progress_1: ProgressBar
    lateinit var progress_2: ProgressBar
    lateinit var progress_3: ProgressBar
    lateinit var progress_4: ProgressBar
    lateinit var progress_5: ProgressBar
    lateinit var progress_6: ProgressBar
    lateinit var progress_7: ProgressBar
    lateinit var progress_8: ProgressBar
    lateinit var progress_9: ProgressBar
    lateinit var progress_10: ProgressBar

    lateinit var bitmap_data: Bitmap

    var photo_selection = "0"
    var imageFilePath = ""

    lateinit var encodedImage: String
    lateinit var bytearray_img: ByteArray
    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    var imageFileName="test"
     var photo1: String? = null


    val RC_TAKE_PHOTO = 1
    private lateinit var progressDialog: Dialog
    val REQUEST_IMAGE = 100

    private val ALL_PERMISSION_CODE = 55


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_photos)

        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        photo1 = user_details.get(DllSessionManager.PHOTO1)


        iv_toolbar_back_id = findViewById(R.id.iv_toolbar_back_id)

        rl_one_id = findViewById(R.id.rl_one_id)
        ll_second_row_id = findViewById(R.id.ll_second_row_id)
        ll_third_row_id = findViewById(R.id.ll_third_row_id)
        ll_four_row_id = findViewById(R.id.ll_four_row_id)
        ll_five_row_id = findViewById(R.id.ll_five_row_id)

        img_photo_1 = findViewById(R.id.img_photo_1)
        img_photo_2 = findViewById(R.id.img_photo_2)
        img_photo_3 = findViewById(R.id.img_photo_3)
        img_photo_4 = findViewById(R.id.img_photo_4)
        img_photo_5 = findViewById(R.id.img_photo_5)
        img_photo_6 = findViewById(R.id.img_photo_6)
        img_photo_7 = findViewById(R.id.img_photo_7)
        img_photo_8 = findViewById(R.id.img_photo_8)
        img_photo_9 = findViewById(R.id.img_photo_9)
        img_photo_10 = findViewById(R.id.img_photo_10)

        img_click_1 = findViewById(R.id.img_click_1)
        img_click_2 = findViewById(R.id.img_click_2)
        img_click_3 = findViewById(R.id.img_click_3)
        img_click_4 = findViewById(R.id.img_click_4)
        img_click_5 = findViewById(R.id.img_click_5)
        img_click_6 = findViewById(R.id.img_click_6)
        img_click_7 = findViewById(R.id.img_click_7)
        img_click_8 = findViewById(R.id.img_click_8)
        img_click_9 = findViewById(R.id.img_click_9)
        img_click_10 = findViewById(R.id.img_click_10)

        progress_1 = findViewById(R.id.progress_1)
        progress_2 = findViewById(R.id.progress_2)
        progress_3 = findViewById(R.id.progress_3)
        progress_4 = findViewById(R.id.progress_4)
        progress_5 = findViewById(R.id.progress_5)
        progress_6 = findViewById(R.id.progress_6)
        progress_7 = findViewById(R.id.progress_7)
        progress_8 = findViewById(R.id.progress_8)
        progress_9 = findViewById(R.id.progress_9)
        progress_10 = findViewById(R.id.progress_10)

        img_click_1.setOnClickListener(this)
        img_click_2.setOnClickListener(this)
        img_click_3.setOnClickListener(this)
        img_click_4.setOnClickListener(this)
        img_click_5.setOnClickListener(this)
        img_click_6.setOnClickListener(this)
        img_click_7.setOnClickListener(this)
        img_click_8.setOnClickListener(this)
        img_click_9.setOnClickListener(this)
        img_click_10.setOnClickListener(this)
        iv_toolbar_back_id.setOnClickListener { onBackPressed() }

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })




        if (user_details.get(DllSessionManager.PHOTO1).equals(null)) {
            photo1 = "0"
        }

        if(!photo1.equals("0")) {
            rl_one_id.visibility=View.VISIBLE
            ll_second_row_id.visibility=View.VISIBLE
            ll_third_row_id.visibility=View.VISIBLE
            ll_four_row_id.visibility=View.VISIBLE
            ll_five_row_id.visibility=View.VISIBLE
        }
        ProfilePhotosAPI()


        if (isReadStorageAllowed()) {
            return
        }
        //If the app has not the permission then asking for the permission
        requestStoragePermission()

    }
    private fun isReadStorageAllowed(): Boolean {
        //Getting the permission status
        val result_read_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        //  val result_write_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result_camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result_Contacts = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
        val result_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val result_telephone = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)

        //If permission is granted returning true
        //return if (result == PackageManager.PERMISSION_GRANTED) true else false
        return result_read_storage == PackageManager.PERMISSION_GRANTED && result_camera == PackageManager.PERMISSION_GRANTED && result_Contacts == PackageManager.PERMISSION_GRANTED && result_location == PackageManager.PERMISSION_GRANTED && result_telephone == PackageManager.PERMISSION_GRANTED

        //If permission is not granted returning false
    }

    //Requesting permission
    private fun requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ),
            ALL_PERMISSION_CODE
        )
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        //Checking the request code of our request
        if (requestCode == ALL_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show()
                //intents you can use hear
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun ProfilePhotosAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getProfilePhotosApi(user_details.get(DllSessionManager.USER_ID).toString())
        call.enqueue(object : Callback<ProfilePhotosResponse> {
            override fun onResponse(
                call: Call<ProfilePhotosResponse>,
                response: retrofit2.Response<ProfilePhotosResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                    // Log.e("data_2",response!!.body()!!.photos!!.photo2)
                    progressDialog.dismiss()

                    if (response.body()!!.photos!!.photo1 !== null) {
                        photo1_stg = response.body()!!.photos!!.photo1
                        val photo = photbaseurl + photo1_stg
                        dllSessionManager.profile_pic_updated(photo1_stg)


                        Picasso.with(this@ProfilePhotos).load(photo)
                            .into(img_photo_1, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_1.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_1.setVisibility(View.GONE)
                                }
                            })
                        if(photo1.equals("0")) {
                            Log.e("profile_pic_url", photbaseurl + photo1_stg)
                            val intent = Intent(this@ProfilePhotos, DashboardActivity::class.java)
                            intent.putExtra("from_list", "profile_photos")
                            startActivity(intent)

                        }
                        Log.e("profile_if", photbaseurl + photo1_stg)
                        rl_one_id.visibility=View.VISIBLE
                        ll_second_row_id.visibility=View.VISIBLE
                        ll_third_row_id.visibility=View.VISIBLE
                        ll_four_row_id.visibility=View.VISIBLE
                        ll_five_row_id.visibility=View.VISIBLE

                    } else {
                        Log.e("profile_else", photbaseurl + photo1_stg)
                        progress_1.setVisibility(View.GONE)

                        rl_one_id.visibility=View.GONE
                        ll_second_row_id.visibility=View.GONE
                        ll_third_row_id.visibility=View.GONE
                        ll_four_row_id.visibility=View.GONE
                        ll_five_row_id.visibility=View.GONE


                    }


                    if (response.body()!!.photos!!.photo2 !== null/*||response!!.body()!!.photos!!.photo2 !==""||response!!.body()!!.photos!!.photo2 !==" "*/) {
                        photo2_stg = response!!.body()!!.photos!!.photo2
                        val photo = photbaseurl + photo2_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_2, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_2.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_2.setVisibility(View.GONE)
                                }
                            })
                        Log.e("profile_pic_url_2", photbaseurl + photo2_stg)
                        if (photo2_stg.equals("") || photo2_stg.equals(" ")) {
                            photo2_stg = "nodata"
                        }

                    } else {
                        progress_2.setVisibility(View.GONE)
                    }
                    if (response.body()!!.photos!!.photo3 !== null) {
                        photo3_stg = response!!.body()!!.photos!!.photo3

                        val photo = photbaseurl + photo3_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_3, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_3.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_3.setVisibility(View.GONE)
                                }
                            })
                        Log.e("profile_pic_url_3", photbaseurl + photo3_stg)
                        if (photo3_stg.equals("") || photo3_stg.equals(" ")) {
                            photo3_stg = "nodata"
                        }
                    } else {
                        progress_3.setVisibility(View.GONE)
                    }
                    if (response.body()!!.photos!!.photo4 !== null) {
                        photo4_stg = response!!.body()!!.photos!!.photo4
                        val photo = photbaseurl + photo4_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_4, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_4.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_4.setVisibility(View.GONE)
                                }
                            })
                        Log.e("profile_pic_url_4", photbaseurl + photo4_stg)
                        if (photo4_stg.equals("") || photo4_stg.equals(" ")) {
                            photo4_stg = "nodata"
                        }
                    } else {
                        progress_4.setVisibility(View.GONE)
                    }
                    if (response.body()!!.photos!!.photo5 !== null) {
                        photo5_stg = response!!.body()!!.photos!!.photo5
                        val photo = photbaseurl + photo5_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_5, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_5.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_5.setVisibility(View.GONE)
                                }
                            })
                        Log.e("profile_pic_url_5", photbaseurl + photo5_stg)
                        if (photo5_stg.equals("") || photo5_stg.equals(" ")) {
                            photo5_stg = "nodata"
                        }
                    } else {
                        progress_5.setVisibility(View.GONE)
                    }
                    if (response.body()!!.photos!!.photo6 !== null) {
                        photo6_stg = response!!.body()!!.photos!!.photo6
                        val photo = photbaseurl + photo6_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_6, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_6.setVisibility(View.GONE)
                                }

                                override fun onError() {
                                    // Log.e("error_msg",onError().toString()+"---"+"error")
                                    progress_6.setVisibility(View.GONE)
                                }
                            })
                        Log.e("profile_pic_url_6", photbaseurl + photo6_stg)
                        if (photo6_stg.equals("") || photo6_stg.equals(" ")) {
                            photo6_stg = "nodata"
                        }
                    } else {
                        progress_6.setVisibility(View.GONE)
                    }

                    // Photo no 7
                    if (response.body()!!.photos!!.photo7 != null) {
                        photo7_stg = response.body()!!.photos!!.photo7
                        val photo = photbaseurl + photo7_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_7, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_7.visibility = View.GONE
                                }

                                override fun onError() {
                                    progress_7.visibility = View.GONE
                                }
                            })
                        Log.e("profile_pic_url_7", photbaseurl + photo7_stg)
                        if (photo7_stg == "" || photo7_stg == " ") {
                            photo7_stg = "nodata"
                        }
                    } else {
                        progress_7.visibility = View.GONE
                    }

                    // Photo no 8
                    if (response.body()!!.photos!!.photo8 != null) {
                        photo8_stg = response.body()!!.photos!!.photo8
                        val photo = photbaseurl + photo8_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_8, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_8.visibility = View.GONE
                                }

                                override fun onError() {
                                    progress_8.visibility = View.GONE
                                }
                            })
                        Log.e("profile_pic_url_8", photbaseurl + photo8_stg)
                        if (photo8_stg == "" || photo8_stg == " ") {
                            photo8_stg = "nodata"
                        }
                    } else {
                        progress_8.visibility = View.GONE
                    }

                    // Photo no 9
                    if (response.body()!!.photos!!.photo9 != null) {
                        photo9_stg = response.body()!!.photos!!.photo9
                        val photo = photbaseurl + photo9_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_9, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_9.visibility = View.GONE
                                }

                                override fun onError() {
                                    progress_9.visibility = View.GONE
                                }
                            })
                        Log.e("profile_pic_url_9", photbaseurl + photo9_stg)
                        if (photo9_stg == "" || photo9_stg == " ") {
                            photo9_stg = "nodata"
                        }
                    } else {
                        progress_9.visibility = View.GONE
                    }

                    // Photo no 10
                    if (response.body()!!.photos!!.photo10 != null) {
                        photo10_stg = response.body()!!.photos!!.photo10
                        val photo = photbaseurl + photo10_stg
                        Picasso.with(this@ProfilePhotos).load(photo)
                            .error(R.drawable.ic_man_user)
                            .into(img_photo_10, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    progress_10.visibility = View.GONE
                                }

                                override fun onError() {
                                    progress_10.visibility = View.GONE
                                }
                            })

                        Log.e("profile_pic_url_10", photbaseurl + photo10_stg)
                        if (photo10_stg == "" || photo10_stg == " ") {
                            photo10_stg = "nodata"
                        }
                    } else {
                        progress_10.visibility = View.GONE
                    }

                }

                //  }
            }

            override fun onFailure(call: Call<ProfilePhotosResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.img_click_1 -> {
                val popup = PopupMenu(this@ProfilePhotos, img_click_1)
                //inflating menu from xml resource

                if (photo1_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu_edit)
                }

                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()
                                photo_selection = "1"
                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }
            R.id.img_click_2 -> {
                photo_selection = "2"
                val popup = PopupMenu(this@ProfilePhotos, img_click_2)
                //inflating menu from xml resource
                if (photo2_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()

                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }
            R.id.img_click_3 -> {
                photo_selection = "3"
                val popup = PopupMenu(this@ProfilePhotos, img_click_3)
                //inflating menu from xml resource
                if (photo3_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()

                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }
            R.id.img_click_4 -> {
                photo_selection = "4"
                val popup = PopupMenu(this@ProfilePhotos, img_click_4)
                //inflating menu from xml resource
                if (photo4_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()

                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }
            R.id.img_click_5 -> {
                photo_selection = "5"
                val popup = PopupMenu(this@ProfilePhotos, img_click_5)
                //inflating menu from xml resource
                if (photo5_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()

                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }
            R.id.img_click_6 -> {
                photo_selection = "6"
                val popup = PopupMenu(this@ProfilePhotos, img_click_6)
                //inflating menu from xml resource
                if (photo6_stg.equals("nodata")) {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                //adding click listener
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.action_edit_id -> {

                                showImagePickerOptions()

                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                //displaying the popup
                popup.show()
            }

            R.id.img_click_7 -> {
                photo_selection = "7"
                val popup = PopupMenu(this@ProfilePhotos, img_click_7)

                if (photo7_stg == "nodata") {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }

                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.itemId) {
                            R.id.action_edit_id -> {
                                showImagePickerOptions()
                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })

                popup.show()
            }

            R.id.img_click_8 -> {
                photo_selection = "8"
                val popup = PopupMenu(this@ProfilePhotos, img_click_8)

                if (photo8_stg == "nodata") {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.itemId) {
                            R.id.action_edit_id -> {
                                showImagePickerOptions()
                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                popup.show()
            }

            R.id.img_click_9 -> {
                photo_selection = "9"
                val popup = PopupMenu(this@ProfilePhotos, img_click_9)
                if (photo9_stg == "nodata") {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }

                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.itemId) {
                            R.id.action_edit_id -> {
                                showImagePickerOptions()
                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })

                popup.show()
            }

            R.id.img_click_10 -> {
                photo_selection = "10"
                val popup = PopupMenu(this@ProfilePhotos, img_click_10)

                if (photo10_stg == "nodata") {
                    popup.inflate(R.menu.option_menu_edit)
                } else {
                    popup.inflate(R.menu.option_menu)
                }

                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        when (item.itemId) {
                            R.id.action_edit_id -> {
                                showImagePickerOptions()
                            }
                            R.id.action_set_id -> {
                                SetasProfilePic()
                            }
                            R.id.action_delete_id -> {
                                DeleteProfilePic()
                            }
                        }
                        return false
                    }
                })
                popup.show()
            }

        }
    }

    private fun getImageFromAlbum() {
        val pictureDialog = AlertDialog.Builder(this)

        pictureDialog.setTitle("Select Photo from")
        val pictureDialogItems = arrayOf("Gallery", "Camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhoto()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    /*private fun takePhotoFromCamera() {


        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        file = File(
            this.getExternalCacheDir(),
            System.currentTimeMillis().toString() + ".jpg"
        )
        fileUri = Uri.fromFile(file)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, CAMERA)


    }*/





    fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)



        if (intent.resolveActivity(packageManager) != null) {
            //Create a file to store the image
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File

            }

            if (photoFile != null) {
                val photoUri = FileProvider.getUriForFile(this, "$packageName.provider", photoFile!!)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(intent, CAMERA)

            }
        }

    }
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.getDefault()
        ).format(Date())
        imageFileName = "IMG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
        )

        imageFilePath = image.getAbsolutePath()
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*if (resultCode == this) {
            return
        }*/

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                    // loading profile image from local cache
                    //loadProfile(uri.toString())
                    bitmap_data = bitmap
                    // val bm = BitmapFactory.decodeFile("/path/to/image.jpg")
                    val bm = BitmapFactory.decodeFile(ImageFilePath.getPath(this, uri))
                    val baos = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                    bytearray_img = baos.toByteArray()

                    if (photo_selection == "1") {
                        img_photo_1.setImageBitmap(bitmap)

                        Log.e("uplaod_url","-----")
                        UplaodPic()
                    }

                    if (photo_selection == "2") {
                        img_photo_2.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "3") {
                        img_photo_3.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "4") {
                        img_photo_4.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "5") {
                        img_photo_5.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "6") {
                        img_photo_6.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "7") {
                        img_photo_7.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "8") {
                        img_photo_8.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "8") {
                        img_photo_8.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "9") {
                        img_photo_9.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                    if (photo_selection == "10") {
                        img_photo_10.setImageBitmap(bitmap)
                        UplaodPic()
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }

    }

    fun UplaodPic() {
        progressDialog.show()
        encodedImage = ""
        encodedImage = Base64.encodeToString(bytearray_img, Base64.DEFAULT)
        val apiService = ApiInterface.create()
        val call = apiService.changePhotoApi(
            user_details.get(DllSessionManager.USER_ID).toString(),
            photo_selection,
            encodedImage,
            imageFileName+".jpg"
        )
        call.enqueue(object : Callback<UploadPhotoResponse> {
            override fun onResponse(
                call: Call<UploadPhotoResponse>,
                response: retrofit2.Response<UploadPhotoResponse>?
            ) {
                if (progressDialog.isShowing)
                    progressDialog.dismiss()

                Log.e("profile_upload", response!!.body()!!.status
                +"----"+response!!.body()!!.result)
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                    ProfilePhotosAPI()
                }

            }

            override fun onFailure(call: Call<UploadPhotoResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    fun SetasProfilePic() {


        val apiService = ApiInterface.create()
        val call = apiService.setProfilePicApi(user_details.get(DllSessionManager.USER_ID).toString(), photo_selection)
        call.enqueue(object : Callback<UploadPhotoResponse> {
            override fun onResponse(
                call: Call<UploadPhotoResponse>,
                response: retrofit2.Response<UploadPhotoResponse>?
            ) {
                /* if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                 }*/
                ProfilePhotosAPI()

            }

            override fun onFailure(call: Call<UploadPhotoResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    fun DeleteProfilePic() {


        val apiService = ApiInterface.create()

        var call: Call<UploadPhotoResponse>? = null
        if (photo_selection == "2") {
            call = apiService.deletePhotoApi(
                user_details.get(DllSessionManager.USER_ID).toString(),
                photo_selection,
                photo2_stg
            )

        }
        if (photo_selection == "3") {
            call = apiService.deletePhotoApi(
                user_details.get(DllSessionManager.USER_ID).toString(),
                photo_selection,
                photo3_stg
            )

        }
        if (photo_selection == "4") {
            call = apiService.deletePhotoApi(
                user_details.get(DllSessionManager.USER_ID).toString(),
                photo_selection,
                photo4_stg
            )

        }
        if (photo_selection == "5") {
            call = apiService.deletePhotoApi(
                user_details.get(DllSessionManager.USER_ID).toString(),
                photo_selection,
                photo5_stg
            )

        }
        if (photo_selection == "6") {
            call = apiService.deletePhotoApi(
                user_details.get(DllSessionManager.USER_ID).toString(),
                photo_selection,
                photo6_stg
            )

        }

        if (photo_selection == "7") {
            call = apiService.deletePhotoApi(
                user_details[DllSessionManager.USER_ID].toString(),
                photo_selection,
                photo7_stg
            )

        }

        if (photo_selection == "8") {
            call = apiService.deletePhotoApi(
                user_details[DllSessionManager.USER_ID].toString(),
                photo_selection,
                photo8_stg
            )

        }

        if (photo_selection == "9") {
            call = apiService.deletePhotoApi(
                user_details[DllSessionManager.USER_ID].toString(),
                photo_selection,
                photo9_stg
            )

        }

        if (photo_selection == "10") {
            call = apiService.deletePhotoApi(
                user_details[DllSessionManager.USER_ID].toString(),
                photo_selection,
                photo10_stg
            )

        }

        call!!.enqueue(object : Callback<UploadPhotoResponse> {
            override fun onResponse(
                call: Call<UploadPhotoResponse>,
                response: retrofit2.Response<UploadPhotoResponse>?
            ) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {
                    ProfilePhotosAPI()
                }

            }

            override fun onFailure(call: Call<UploadPhotoResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })

    }

    override fun onBackPressed() {
        //super.onBackPressed()


        if(photo1.equals("0")){
            Toast.makeText(this, "Please upload profile picture to continue..", Toast.LENGTH_SHORT).show()

        }else {
            val intent = Intent(this@ProfilePhotos, DashboardActivity::class.java)
            intent.putExtra("from_list", "profile_photos")
            startActivity(intent)
        }


    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object : ImagePickerActivity.PickerOptionListener {
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }
        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@ProfilePhotos, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 400)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 400)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@ProfilePhotos, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

}
