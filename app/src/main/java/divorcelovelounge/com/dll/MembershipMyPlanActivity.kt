package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class MembershipMyPlanActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    lateinit var tv_planname: CustomTextView
    lateinit var tv_plantype: CustomTextView
    lateinit var tv_paidamount: CustomTextView
    lateinit var tv_duration: CustomTextView
    lateinit var tv_startdate: CustomTextView
    lateinit var tv_expiry: CustomTextView
    lateinit var tv_totalprofiles: CustomTextView
    lateinit var tv_remprofiles: CustomTextView
    lateinit var tv_chat: CustomTextView
    lateinit var tv_totalcts: CustomTextView
    lateinit var tv_remcts: CustomTextView
    lateinit var tv_totalintsts: CustomTextView
    lateinit var tv_remintsts: CustomTextView
    lateinit var tv_totalmsgs: CustomTextView
    lateinit var tv_remmsgs: CustomTextView
    lateinit var tv_whoviewdme: CustomTextView
    lateinit var tv_favme: CustomTextView
    lateinit var ll_info_content_id: LinearLayout
    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    var userID = ""

    private var back_chat_toolbar: ImageView? = null

    lateinit var rl_noplans_id:RelativeLayout

    var plan_from=""
    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_myplan)

        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails
        userID = user_details.get(DllSessionManager.USER_ID).toString()


        plan_from=intent.getStringExtra("from_membership")


        back_chat_toolbar = findViewById(R.id.back_chat_toolbar) as ImageView
        back_chat_toolbar!!.setOnClickListener {
            onBackPressed()
        }


        ll_info_content_id = findViewById(R.id.ll_info_content_id)
        tv_planname = findViewById(R.id.tv_planname)
        tv_plantype = findViewById(R.id.tv_plantype)
        tv_paidamount = findViewById(R.id.tv_paidamount)
        tv_duration = findViewById(R.id.tv_duration)
        tv_startdate = findViewById(R.id.tv_startdate)
        tv_expiry = findViewById(R.id.tv_expiry)
        tv_totalprofiles = findViewById(R.id.tv_totalprofiles)
        tv_remprofiles = findViewById(R.id.tv_remprofiles)
        tv_chat = findViewById(R.id.tv_chat)
        tv_totalcts = findViewById(R.id.tv_totalcts)
        tv_remcts = findViewById(R.id.tv_remcts)
        tv_totalintsts = findViewById(R.id.tv_totalintsts)
        tv_remintsts = findViewById(R.id.tv_remintsts)
        tv_totalmsgs = findViewById(R.id.tv_totalmsgs)
        tv_remmsgs = findViewById(R.id.tv_remmsgs)
        rl_noplans_id = findViewById(R.id.rl_noplans_id)
        tv_whoviewdme = findViewById(R.id.tv_whoviewdme)
        tv_favme = findViewById(R.id.tv_favme)


        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()



        location_details_list = dllSessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Membership")


        val apiService = ApiInterface.create()
        val call = apiService.membershipmyplan(userID)

        call.enqueue(object : Callback<MemberMyPlanResponse> {
            override fun onResponse(
                    call: Call<MemberMyPlanResponse>,
                    response: retrofit2.Response<MemberMyPlanResponse>?
            ) {
                Log.d("REQUEST", response!!.body()!!.status + ""+ response!!.body()!!.plandetails)
                if (response!!.body()!!.status.equals("1")) {
                    rl_noplans_id.visibility=View.GONE
                    Log.d("REQUEST_lllll", response!!.body()!!.status + ""+ response!!.body()!!.plandetails!!.user_id)
                    /*tv_planname.setText(response!!.body()!!.plandetails!!.package_type)
                    tv_plantype.setText(response!!.body()!!.plandetails!!.plan_type)
                    tv_paidamount.setText(response!!.body()!!.plandetails!!.plan_amount)
                    tv_duration.setText(response!!.body()!!.plandetails!!.plan_duration+" Days")
                    tv_startdate.setText(response!!.body()!!.plandetails!!.plan_start_date)
                    tv_expiry.setText(response!!.body()!!.plandetails!!.plan_expiry)
                    tv_totalprofiles.setText(response!!.body()!!.plandetails!!.profile)
                    tv_remprofiles.setText(response!!.body()!!.plandetails!!.rem_profiles)
                    tv_chat.setText(response!!.body()!!.plandetails!!.chat)
                    tv_totalcts.setText(response!!.body()!!.plandetails!!.plan_contacts)
                    tv_remcts.setText(response!!.body()!!.plandetails!!.rem_contacts)
                    tv_totalintsts.setText(response!!.body()!!.plandetails!!.interest)
                    tv_remintsts.setText(response!!.body()!!.plandetails!!.rem_interests)
                    tv_totalmsgs.setText(response!!.body()!!.plandetails!!.plan_msg)
                    tv_remmsgs.setText(response!!.body()!!.plandetails!!.rem_msgs)*/



                    tv_planname.setText(response!!.body()!!.plandetails!!.package_type)
                    tv_plantype.setText(response!!.body()!!.plandetails!!.plan_type)
                    tv_paidamount.setText(response!!.body()!!.plandetails!!.plan_amount)
                    tv_duration.setText(response!!.body()!!.plandetails!!.plan_duration+" Days")
                    tv_startdate.setText(editDateFormat(response!!.body()!!.plandetails!!.plan_start_date))
                    tv_expiry.setText(editDateFormat(response!!.body()!!.plandetails!!.plan_expiry))
                    tv_totalprofiles.setText("Unlimited")
                    tv_remprofiles.setText("Unlimited")
                    tv_chat.setText(response!!.body()!!.plandetails!!.chat)
                    tv_whoviewdme.setText(response!!.body()!!.plandetails!!.viewedme)
                    tv_favme.setText(response!!.body()!!.plandetails!!.favoritedme)
                    tv_totalcts.setText("Unlimited")
                    tv_remcts.setText("Unlimited")
                    tv_totalintsts.setText("Unlimited")
                    tv_remintsts.setText("Unlimited")
                    tv_totalmsgs.setText("Unlimited")
                    tv_remmsgs.setText("Unlimited")


                    ll_info_content_id.visibility=View.VISIBLE
                }else{
                    rl_noplans_id.visibility=View.VISIBLE
                    ll_info_content_id.visibility=View.GONE
                }
            }

            override fun onFailure(call: Call<MemberMyPlanResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })


    }
    override fun onBackPressed() {
        //super.onBackPressed()


        if(plan_from.equals("profile_fragment")){
            super.onBackPressed()
        }else {
            val intent = Intent(this@MembershipMyPlanActivity, DashboardActivity::class.java)
            intent.putExtra("from_list", "my_plan")
            startActivity(intent)
        }

    }

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }


    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,userID)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD", PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@MembershipMyPlanActivity);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@MembershipMyPlanActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString())
                                sliderLayout.addSlider(textSliderView)
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@MembershipMyPlanActivity);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

}
