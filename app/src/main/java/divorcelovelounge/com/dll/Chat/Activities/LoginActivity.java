package divorcelovelounge.com.dll.Chat.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nanotasks.BackgroundWork;
import com.nanotasks.Completion;
import com.nanotasks.Tasks;

import butterknife.Bind;
import butterknife.ButterKnife;
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession;
import divorcelovelounge.com.dll.R;


public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.loginUserNameEditText)
    EditText loginUserNameEditText;

    @Bind(R.id.loginJidCompletionEditText)
    EditText loginJidCompletionEditText;

    @Bind(R.id.loginPasswordEditText)
    EditText loginPasswordEditText;

    @Bind(R.id.loginServerEditText)
    EditText loginServerEditText;

    @Bind(R.id.loginButton)
    Button loginButton;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login_dialog);

        ButterKnife.bind(this);

        toolbar.setTitle(getString(R.string.title_login));

       /* String userName = "s2550";
        String password = "abc123";*/
        String userName = "angelina";
        String password = "123456";

        loginUserNameEditText.setText(userName);
        loginUserNameEditText.setSelection(userName.length());
        loginJidCompletionEditText.setText("@" + XMPPSession.SERVICE_NAME);

        loginPasswordEditText.setText(password);
        loginServerEditText.setText(XMPPSession.SERVER_NAME);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginAndStart(loginUserNameEditText.getText().toString(), loginPasswordEditText.getText().toString());
            }
        });
    }

    private void loginAndStart(final String userName, final String password) {
        final ProgressDialog progress = ProgressDialog.show(LoginActivity.this, getString(R.string.loading), null, true);

        Tasks.executeInBackground(LoginActivity.this, new BackgroundWork<Object>() {
            @Override
            public Object doInBackground() throws Exception {
                XMPPSession.startService(LoginActivity.this);
                //((SplashActivity) getApplicationContext()).getService().login(userName, password);
                XMPPSession.getInstance().login(userName, password);
                Thread.sleep(XMPPSession.REPLY_TIMEOUT);
                return null;
            }
        }, new Completion<Object>() {
            @Override
            public void onSuccess(Context context, Object result) {
                progress.dismiss();
                //((SplashActivity) getApplicationContext()).startApplication();
                Intent mainMenuIntent = new Intent(getApplicationContext(), MainMenuActivity_opt.class);
                mainMenuIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainMenuIntent);
                finish();
            }

            @Override
            public void onError(Context context, Exception e) {
                progress.dismiss();
                XMPPSession.getInstance().getXMPPConnection().disconnect();
                XMPPSession.clearInstance();
                Toast.makeText(context, getString(R.string.error_login), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
