package divorcelovelounge.com.dll.Chat.Activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast

import com.nanotasks.BackgroundWork
import com.nanotasks.Completion
import com.nanotasks.Tasks

import org.jivesoftware.smack.packet.Message

import butterknife.Bind
import butterknife.ButterKnife
import com.google.gson.Gson
import de.greenrobot.event.EventBus
import divorcelovelounge.com.dll.ApiPojo.*

import divorcelovelounge.com.dll.Chat.Adapters.ChatListAdapter
import divorcelovelounge.com.dll.Chat.Xmpp.RosterManager
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPUtils
import divorcelovelounge.com.dll.Chat.chat.RoomManager
import divorcelovelounge.com.dll.Chat.chat.RoomManagerListener
import divorcelovelounge.com.dll.Chat.models.Chat
import divorcelovelounge.com.dll.Chat.realm.RealmManager
import divorcelovelounge.com.dll.Chat.ui.itemTouchHelper.SimpleItemTouchHelperCallback
import divorcelovelounge.com.dll.Chat.utils.ChatOrderComparator
import divorcelovelounge.com.dll.Chat.utils.Preferences
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem
import org.jivesoftware.smack.packet.Presence
import org.json.JSONArray
import org.json.JSONObject
import org.jxmpp.jid.Jid
import rx.Subscription
import rx.functions.Action1
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ChatsListsFragment : BaseFragment() {


    lateinit var oneToOneChatsRecyclerView: RecyclerView


    lateinit var chatsLoading: ProgressBar
    lateinit var tv_nodata: CustomTextView

    private var mRoomManager: RoomManager? = null
    private var mOneToOneChats: MutableList<Chat>? = null

    private val mGroupChatsAdapter: ChatListAdapter? = null
    private var mOneToOneChatsAdapter: ChatListAdapter? = null

    lateinit var mMessageSubscription: Subscription
    lateinit var mMessageSentAlertSubscription: Subscription

    internal var mContext: Activity? = null

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    val oneToOneChatsAdapter: ChatListAdapter
        get() = getChatListAdapter(mOneToOneChats)

    lateinit var static_list: ArrayList<Chat>

    var chat_image = ""
    var chat_username = ""
    var chat_status_stg = ""
    var chat_status_msge_stg = ""
    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    lateinit var full_array: ArrayList<Onlyimagename>
    lateinit var swipe_refresh_layout: SwipeRefreshLayout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.recyclerview_layout, container, false)
        ButterKnife.bind(this, view)

        dllSessionManager = DllSessionManager(this!!.context!!)

        user_details = dllSessionManager.userDetails
        full_array = ArrayList()
        full_array.clear()
        Log.e("matches_array_list_chat", dllSessionManager.getImagesData())

        chat_image = dllSessionManager.getImagesData().toString()
        chat_username = dllSessionManager.getNamesData().toString()

        oneToOneChatsRecyclerView = view.findViewById(R.id.oneToOneChatsRecyclerView)
        chatsLoading = view.findViewById(R.id.chatsLoading)
        tv_nodata = view.findViewById(R.id.tv_nodata)

        mContext = activity
        mRoomManager = RoomManager.getInstance(RoomManagerChatListListener(this.context!!))

        swipe_refresh_layout = view!!.findViewById(R.id.loadMessagesSwipeRefreshLayout)

        swipe_refresh_layout.setOnRefreshListener {
            // Initialize a new Runnable
            // Update the text view text with a random number
            XmppUserListAPI()

            swipe_refresh_layout.isRefreshing = false


        }


        try {

            val json_names: JSONArray = JSONArray(chat_username)
            val json_images: JSONArray = JSONArray(chat_image)


            // val list=(Arrays.asList(al_name))

            for (i in 0 until json_images!!.length()) {
                Log.e("names_only", json_images.get(i).toString())

                val only_names = Onlyimagename()
                only_names.username = json_names.get(i).toString()
                only_names.photo1 = json_images.get(i).toString()

                full_array.add(only_names)
            }
            Log.e("names_size", full_array.size.toString())

            XmppUserListAPI()
        } catch (e: Throwable) {
            Log.e("catch_ll", chat_image.toString())
            XmppUserListAPI()
        }

        chat_status_stg = dllSessionManager.chatStatus[DllSessionManager.CHAT_STATUS].toString()
        chat_status_msge_stg = dllSessionManager.chatStatus[DllSessionManager.CHAT_MSG].toString()


        return view
    }


    fun XmppUserListAPI() {

        mOneToOneChats = ArrayList()
        mOneToOneChats!!.clear()
/**/

        val apiService = ApiInterface.create()
        val call =
            apiService.getXmppchatListAPI(user_details.get(DllSessionManager.USER_NAME_ID).toString())

        call.enqueue(object : Callback<XmppListResponse> {
            override fun onResponse(
                call: Call<XmppListResponse>,
                response: retrofit2.Response<XmppListResponse>?
            ) {

                try {
                    swipe_refresh_layout.isRefreshing = false
                    if (response != null) {

                        Log.d("getChatListApiRES", "" + response)
                        if (response!!.body()!!.status.equals("1")) {

                            if (chat_status_stg.equals("0")) {
                                oneToOneChatsRecyclerView.visibility = View.GONE
                                tv_nodata.visibility = View.VISIBLE
                                tv_nodata.setText(chat_status_msge_stg)
                            } else {
                                oneToOneChatsRecyclerView.visibility = View.VISIBLE
                                tv_nodata.visibility = View.GONE

                            }
                            chatsLoading.visibility = View.GONE
                            val user_data = response.body()!!.xmpplist
                            // val favourites_list: ArrayList<All_Matches>? = user_data!!.my_favourites


                            for (i in 0 until user_data!!.size) {
                                val new_name = user_data.get(i).jid
                                val new_array = new_name.split("@").toTypedArray()
                                val new_name_only = new_array[0]
                                for (k in 0 until full_array!!.size) {

                                    if (full_array.get(k).username!!.equals(new_name_only.trim())) {
                                        Log.d(
                                            "dattaa",
                                            "" + full_array.get(k).username + "---" + full_array.get(
                                                k
                                            ).photo1
                                        )
                                        val s = user_data.get(i).jid
                                        val arrayString = s.split("@").toTypedArray()

                                        val name_only = arrayString[0]
                                        var domain_only = arrayString[1]

                                        val chat = Chat()
                                        chat.type = Chat.TYPE_1_T0_1
                                        chat.isShow = true
                                        chat.name = name_only.trim()
                                        chat.imageUrl = photobaseurl + full_array.get(k).photo1
                                        //chat.jid = user_data.get(i).id.trim() + "@divorcelovelounge.com"
                                        chat.jid = user_data.get(i).jid
                                        chat.dateCreated = Date()
                                        RealmManager.getInstance().saveChat(chat)

                                        // mOneToOneChats!!.add(chat)
                                        break

                                    }
                                }
                            }

                            if (mOneToOneChats!!.size.equals(0)) {
                                tv_nodata!!.visibility = View.VISIBLE
                                chatsLoading.visibility = View.GONE
                            }
                            Log.e(
                                "getChatList_array_list",
                                mOneToOneChats.toString() + "---" + mOneToOneChats!!.size
                            )


                        } else if (response!!.body()!!.status.equals("2")) {
                            tv_nodata!!.visibility = View.VISIBLE
                            chatsLoading.visibility = View.GONE
                            oneToOneChatsRecyclerView!!.visibility = View.GONE
                            //Preferences.getInstance().deleteAll()
                            //RealmManager.getInstance().deleteAll()

                        }

                    }



                    initOneToOneChatsRecyclerView()


                } catch (e: Exception) {
                    Log.w("CHATLISTEXCEPTION", e.toString())
                }

            }

            override fun onFailure(call: Call<XmppListResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })



        initOneToOneChatsRecyclerView()

        val preferences = Preferences.getInstance()


        mMessageSubscription = XMPPSession.getInstance().subscribeToMessages { loadChats() }

        mMessageSentAlertSubscription =
            XMPPSession.getInstance().subscribeToMessageSent { loadChats() }

        loadChatsBackgroundTask()
    }


    private fun initOneToOneChatsRecyclerView() {
        mOneToOneChatsAdapter = oneToOneChatsAdapter
        oneToOneChatsRecyclerView!!.setHasFixedSize(true)
        val layoutManagerOneToOneChats = LinearLayoutManager(mContext)
        oneToOneChatsRecyclerView!!.layoutManager = layoutManagerOneToOneChats
        oneToOneChatsRecyclerView!!.adapter = mOneToOneChatsAdapter

        val callback = SimpleItemTouchHelperCallback(mOneToOneChatsAdapter)
        val mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(oneToOneChatsRecyclerView)
    }

    private fun getChatListAdapter(chats: List<Chat>?): ChatListAdapter {
        return ChatListAdapter(chats, mContext,
            ChatListAdapter.ChatClickListener { chat ->
                val intent = Intent(mContext, ChatActivity::class.java)
                intent.putExtra(ChatActivity.CHAT_JID_PARAMETER, chat.jid)
                intent.putExtra(ChatActivity.CHAT_NAME_PARAMETER, XMPPUtils.getChatName(chat))
                intent.putExtra(ChatActivity.CHAT_IMAGEURL, chat.imageUrl)
                intent.putExtra(ChatActivity.IS_NEW_CHAT_PARAMETER, false)
                mContext!!.startActivity(intent)
            })
    }

    fun loadChats() {
        try {
            if (chatsLoading != null) {
                chatsLoading!!.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
        }

        if (mContext == null) {
            changeChatsList()
            Log.d("loadChats", "")
        } else {
            mContext!!.runOnUiThread { changeChatsList() }
            Log.d("loadChats_else", "")
        }
    }

    private fun changeChatsList() {
        synchronized(SYNC_CHATS) {
            updateChatsList(mOneToOneChats, RealmManager.getInstance().get1to1Chats())

            Collections.sort(mOneToOneChats, ChatOrderComparator())

            if (mOneToOneChatsAdapter == null) {
                mOneToOneChatsAdapter = oneToOneChatsAdapter
            }



            mOneToOneChatsAdapter!!.notifyDataSetChanged()

            if (chatsLoading != null) {
                chatsLoading!!.visibility = View.GONE
            }
        }
    }

    private fun updateChatsList(chatsList: MutableList<Chat>?, updaterList: List<Chat>) {
        val deletedOneToOneChat = deletedChat(chatsList!!)
        if (deletedOneToOneChat) {
            chatsList.clear()
            chatsList.addAll(updaterList)
        } else {
            refineChatsList(chatsList, updaterList)
        }
    }

    private fun deletedChat(chats: List<Chat>): Boolean {
        for (chat in chats) {
            if (!chat.isValid) {
                return true
            }
        }
        return false
    }

    private fun refineChatsList(list: MutableList<Chat>, refiner: List<Chat>) {
        for (chat in refiner) {
            if (!list.contains(chat)) {
                list.add(chat)
            }
        }
    }

    private fun loadChatsAfterRoomLeft() {
        Log.d("loadChatsAfterRoomLeft", "")
        loadChatsBackgroundTask()

    }

    fun loadChatsBackgroundTask() {
        try {
            if (mRoomManager == null) {
                return
            }

            Tasks.executeInBackground(mContext, {
                mRoomManager!!.loadRosterContactsChats() // load 1 to 1 chats from contacts
                mRoomManager!!.loadMUCLightRooms() // load group chats
                null
            }, object : Completion<Any> {
                override fun onSuccess(context: Context, `object`: Any) {
                    mContext!!.runOnUiThread { loadChats() }

                    Log.d("loadChatsBackgroundTask", "")
                }

                override fun onError(context: Context, e: Exception) {
                    mContext!!.runOnUiThread { loadChats() }
                }
            })
        } catch (e: Exception) {
            Log.e("mroommanger", e.toString())

        }
    }

    /*override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }*/

    // receives events from EventBus


    private inner class RoomManagerChatListListener(context: Context) :
        RoomManagerListener(context) {

        override fun onRoomLeft() {
            loadChatsAfterRoomLeft()
            Log.d("onRoomLeft", "")
        }

        override fun onRoomsLoaded() {
            loadChats()
            Log.d("onRoomsLoaded", "")
        }
    }

    companion object {

        private val SYNC_CHATS = object : Any() {

        }
    }


}
