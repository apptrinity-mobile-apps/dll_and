package divorcelovelounge.com.dll.Chat.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.nanotasks.BackgroundWork;
import com.nanotasks.Completion;
import com.nanotasks.Tasks;
import divorcelovelounge.com.dll.Chat.Adapters.ChatListAdapter;
import divorcelovelounge.com.dll.Chat.Notifications.RosterNotifications;
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPUtils;
import divorcelovelounge.com.dll.Chat.chat.RoomManager;
import divorcelovelounge.com.dll.Chat.chat.RoomManagerListener;
import divorcelovelounge.com.dll.Chat.models.Chat;
import divorcelovelounge.com.dll.Chat.models.Event;
import divorcelovelounge.com.dll.Chat.realm.RealmManager;
import divorcelovelounge.com.dll.Chat.ui.itemTouchHelper.SimpleItemTouchHelperCallback;
import divorcelovelounge.com.dll.Chat.utils.ChatOrderComparator;
import divorcelovelounge.com.dll.R;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class MainMenuActivity_opt extends BaseActivity {

    @Bind(R.id.oneToOneChatsRecyclerView)
    RecyclerView oneToOneChatsRecyclerView;
    @Bind(R.id.chatsLoading)
    ProgressBar chatsLoading;

    public boolean mRoomsLoaded = false;

    public static String NEW_BLOG_POST = "newBlogPost";
    public static String NEW_ROSTER_REQUEST = "newRosterRequest";
    public static String NEW_ROSTER_REQUEST_SENDER = "newRosterRequestSender";

    Context mContext;
    private RoomManager mRoomManager;
    private List<Chat> mOneToOneChats;
    private ChatListAdapter mOneToOneChatsAdapter;
    private static final Object SYNC_CHATS = new Object() {
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu_opt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       ButterKnife.bind(this);
        mContext = getApplicationContext();





        mRoomManager = RoomManager.getInstance(new RoomManagerChatListListener(mContext));

        mOneToOneChats = new ArrayList<>();
        Chat chat = new Chat();
        chat.setType(Chat.TYPE_1_T0_1);
        chat.setShow(true);
        chat.setName("jntu");
        chat.setJid("jntu@35.154.208.171");
        chat.setDateCreated(new Date());
        RealmManager.getInstance().saveChat(chat);

        mOneToOneChats.add(chat);
        initOneToOneChatsRecyclerView();
       // manageCallFromRosterRequestNotification();
    }
    private void initOneToOneChatsRecyclerView() {
        mOneToOneChatsAdapter = getOneToOneChatsAdapter();
        oneToOneChatsRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManagerOneToOneChats = new LinearLayoutManager(mContext);
        oneToOneChatsRecyclerView.setLayoutManager(layoutManagerOneToOneChats);
        oneToOneChatsRecyclerView.setAdapter(mOneToOneChatsAdapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mOneToOneChatsAdapter);
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(oneToOneChatsRecyclerView);
    }

    public ChatListAdapter getOneToOneChatsAdapter() {
        return getChatListAdapter(mOneToOneChats);
    }

    private ChatListAdapter getChatListAdapter(List<Chat> chats) {
        return new ChatListAdapter(chats, mContext,
                new ChatListAdapter.ChatClickListener() {
                    @Override
                    public void onChatClicked(Chat chat) {
                        Intent intent = new Intent(mContext, ChatActivity.class);
                        intent.putExtra(ChatActivity.CHAT_JID_PARAMETER, chat.getJid());
                        intent.putExtra(ChatActivity.CHAT_NAME_PARAMETER, XMPPUtils.getChatName(chat));
                        intent.putExtra(ChatActivity.IS_NEW_CHAT_PARAMETER, false);
                        mContext.startActivity(intent);
                    }
                });
    }
    private void manageCallFromRosterRequestNotification() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean newRosterRequest = bundle.getBoolean(NEW_ROSTER_REQUEST, false);
            if (newRosterRequest) {
                try {
                    String sender = bundle.getString(NEW_ROSTER_REQUEST_SENDER);
                    Jid jid = JidCreate.from(sender);
                    RosterNotifications.cancelRosterRequestNotification(this, sender);
                    answerSubscriptionRequest(jid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }





    // receives events from EventBus
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event.getType()) {
            case ROOMS_LOADED:
                mRoomsLoaded = true;
                break;

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //reloadChats();
    }

    private class RoomManagerChatListListener extends RoomManagerListener {

        public RoomManagerChatListListener(Context context) {
            super(context);
        }

        @Override
        public void onRoomLeft() {
            loadChatsAfterRoomLeft();
        }

        @Override
        public void onRoomsLoaded() {
            loadChats();
        }
    }


    private void loadChatsAfterRoomLeft() {
        loadChatsBackgroundTask();
    }

    public void loadChatsBackgroundTask() {
        if (mRoomManager == null) {
            return;
        }

        Tasks.executeInBackground(mContext, new BackgroundWork<Object>() {
            @Override
            public Object doInBackground() throws Exception {
                mRoomManager.loadRosterContactsChats(); // load 1 to 1 chats from contacts
                mRoomManager.loadMUCLightRooms(); // load group chats
                return null;
            }
        }, new Completion<Object>() {
            @Override
            public void onSuccess(Context context, Object object) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadChats();
                    }
                });
            }

            @Override
            public void onError(Context context, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadChats();
                    }
                });
            }
        });
    }

    public void loadChats() {
        try {
            if (chatsLoading != null) {
                chatsLoading.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            chatsLoading.setVisibility(View.GONE);
        }

        if (mContext == null) {
            changeChatsList();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    changeChatsList();
                }
            });
        }
    }

    private void changeChatsList() {
        synchronized (SYNC_CHATS) {
            updateChatsList(mOneToOneChats, RealmManager.getInstance().get1to1Chats());

            Collections.sort(mOneToOneChats, new ChatOrderComparator());

            if (mOneToOneChatsAdapter == null) {
                mOneToOneChatsAdapter = getOneToOneChatsAdapter();
            }


            mOneToOneChatsAdapter.notifyDataSetChanged();



                chatsLoading.setVisibility(View.GONE);

        }
    }

    private void updateChatsList(List<Chat> chatsList, List<Chat> updaterList) {
        boolean deletedOneToOneChat = deletedChat(chatsList);
        if (deletedOneToOneChat) {
            chatsList.clear();
            chatsList.addAll(updaterList);
        } else {
            refineChatsList(chatsList, updaterList);
        }
    }

    private boolean deletedChat(List<Chat> chats) {
        for (Chat chat : chats) {
            if (!chat.isValid()) {
                return true;
            }
        }
        return false;
    }

    private void refineChatsList(List<Chat> list, List<Chat> refiner) {
        for (Chat chat : refiner) {
            if (!list.contains(chat)) {
                list.add(chat);
            }
        }
    }




}
