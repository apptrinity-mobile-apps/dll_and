package divorcelovelounge.com.dll.Chat.services;

import android.content.Context;
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPSession;
import divorcelovelounge.com.dll.Chat.utils.MangostaApplication;
import divorcelovelounge.com.dll.Chat.utils.Preferences;

import java.util.TimerTask;



public class XMPPReconnectTask extends TimerTask {

    private Context mContext;
    private static final Object LOCK_1 = new Object() {
    };

    public XMPPReconnectTask(Context context) {
        this.mContext = context;
    }

    @Override
    public void run() {
        synchronized (LOCK_1) {
            // if app is closed
            if (MangostaApplication.getInstance().isClosed()) {

                Preferences preferences = Preferences.getInstance();
                XMPPSession xmppSession = XMPPSession.getInstance();

                if (preferences.isLoggedIn()) {
                    try {
                        xmppSession.relogin();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

}
