package divorcelovelounge.com.dll.Chat.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import divorcelovelounge.com.dll.Chat.Xmpp.RosterManager;
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPUtils;
import divorcelovelounge.com.dll.Chat.models.Chat;
import divorcelovelounge.com.dll.Chat.models.ChatMessage;
import divorcelovelounge.com.dll.Chat.realm.RealmManager;
import divorcelovelounge.com.dll.Chat.ui.ViewHolderType;
import divorcelovelounge.com.dll.Chat.ui.itemTouchHelper.ItemTouchHelperAdapter;
import divorcelovelounge.com.dll.Chat.ui.itemTouchHelper.ItemTouchHelperViewHolder;
import divorcelovelounge.com.dll.FullProfile;
import divorcelovelounge.com.dll.Helper.DllSessionManager;
import divorcelovelounge.com.dll.R;

import org.jivesoftware.smack.packet.Presence;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder>
        implements ItemTouchHelperAdapter {

    private List<Chat> mChats;
    private Context mContext;
    private ChatClickListener mChatClickListener;

    DllSessionManager dllSessionManager;

    private HashMap<String, Integer> user_details;
    private HashMap<String, String> user_session;

    public ChatListAdapter(List<Chat> chats, Context context, ChatClickListener chatClickListener) {
        this.mChats = chats;
        this.mContext = context;
        this.mChatClickListener = chatClickListener;
        dllSessionManager = new DllSessionManager(mContext);
        user_details = new HashMap<>();
        user_session = new HashMap<>();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mChats, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        RealmManager.getInstance().updateChatsSortPosition(mChats);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        mChats.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }
    }

    public class ChatViewHolder extends ViewHolder
            implements ItemTouchHelperViewHolder {

        @Bind(R.id.chatNameTextView)
        TextView chatNameTextView;

        @Bind(R.id.chatMessageTextView)
        TextView chatMessageTextView;

        @Bind(R.id.chatImageView)
        ImageView chatImageView;

        @Bind(R.id.connectionStatusImageView)
        ImageView connectionStatusImageView;

        @Bind(R.id.unreadMessageIndicatorTextView)
        TextView unreadMessageIndicatorTextView;

        private Context mContext;
        private View mView;
        private ChatClickListener mChatClickListener;

        public ChatViewHolder(View view, Context context, ChatClickListener chatClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.mContext = context;
            this.mView = view;
            this.mChatClickListener = chatClickListener;
        }

        public void bind(final Chat chat) {

            if (chat == null || !chat.isValid()) {
                return;
            }

            chatNameTextView.setText(XMPPUtils.getChatName(chat));

            String jid = chat.getJid();

            Log.e("jid", jid);

            ChatMessage chatMessage = RealmManager.getInstance().getLastMessageForChat(jid);

            if (chatMessage != null) {
                manageLastMessage(chatMessage);
            } else {
                chatMessageTextView.setText("");
            }

            if (chat.getType() == Chat.TYPE_1_T0_1) { // 1 to 1
                assignChatImage(chat);
                connectionStatusImageView.setVisibility(View.VISIBLE);
                assignConnectionStatusImage(chat);
            } else { // group chat
                Picasso.with(mContext).load(R.mipmap.ic_groupchat).noFade().fit().into(chatImageView);
                connectionStatusImageView.setVisibility(View.GONE);
            }

            this.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mChatClickListener != null) {
                        mChatClickListener.onChatClicked(chat);
                    }
                }
            });
            Log.e("chat cha", "" + chat.getUnreadMessagesCount());


            manageUnreadMessagesCount(chat);
        }

        private void manageUnreadMessagesCount(Chat chat) {
            int unreadMessagesCount = chat.getUnreadMessagesCount();


            if (unreadMessagesCount == 0) {
                unreadMessageIndicatorTextView.setVisibility(View.GONE);
                Log.e("chat_gone", "" + getAdapterPosition());
                user_details.put(String.valueOf(getAdapterPosition()),0);
                //dllSessionManager.XmppChatCount("0");
            } else {
                unreadMessageIndicatorTextView.setVisibility(View.VISIBLE);

                Log.e("chat_visible", "" + getAdapterPosition());
                user_details.put(String.valueOf(getAdapterPosition()),1);

                int sum =0;
                for (float f : user_details.values()) { sum += f; }

                Log.e("chat_count_new", "" + sum);
                dllSessionManager.XmppChatCount(String.valueOf(sum));
                Log.e("chat_count_new111", "" + sum);
                if (unreadMessagesCount > 99) {
                    unreadMessageIndicatorTextView.setText("+99");
                } else {
                    unreadMessageIndicatorTextView.setText(String.valueOf(unreadMessagesCount));
                }
            }
        }

        private void assignConnectionStatusImage(Chat chat) {
            String userName = XMPPUtils.fromJIDToUserName(chat.getJid());
            if (RosterManager.getInstance().getStatusFromContact(userName).equals(Presence.Type.available)) {
                connectionStatusImageView.setImageResource(R.mipmap.ic_connected);
            } else {
                //connectionStatusImageView.setImageResource(R.mipmap.ic_disconnected);
            }
        }

        private void assignChatImage(Chat chat) {
            if (chat.getImageUrl() != null) {
                Picasso.with(mContext).load(chat.getImageUrl()).noFade().transform(new FullProfile.CircleTransform()).fit().into(chatImageView);

                Log.e("chat.getImageUrl()", "" + chat.getImageUrl());

            } else {
                Picasso.with(mContext).load(R.mipmap.ic_user).noFade().fit().into(chatImageView);
            }
        }

        private void manageLastMessage(ChatMessage chatMessage) {
            switch (chatMessage.getType()) {
                case ChatMessage.TYPE_CHAT:
                    if (chatMessage.isMeMessage()) {
                        chatMessageTextView.setText(chatMessage.getMeContent());
                    } else {
                        /*chatMessageTextView.setText(String.format(Locale.getDefault(), mContext.getString(R.string.chat_last_message),
                                chatMessage.getUserSender(), chatMessage.getContent()));*/
                        chatMessageTextView.setText(chatMessage.getContent());
                    }
                    break;

                case ChatMessage.TYPE_STICKER:
                    chatMessageTextView.setText(String.format(Locale.getDefault(), mContext.getString(R.string.chat_last_message_sticker),
                            chatMessage.getUserSender()));
                    break;
            }

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(mContext.getResources().getColor(R.color.background_chat));
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItemChat = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_item_chat, parent, false);
        return new ChatViewHolder(viewItemChat, mContext, mChatClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatViewHolder chatViewHolder = (ChatViewHolder) holder;
        chatViewHolder.bind(mChats.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return ViewHolderType.VIEW_TYPE_CHAT_ITEM;
    }

    @Override
    public int getItemCount() {
        return mChats.size();
    }

    public interface ChatClickListener {
        void onChatClicked(Chat chat);
    }

}
