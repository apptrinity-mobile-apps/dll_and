package divorcelovelounge.com.dll

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl

class PageFragment : Fragment() {

    private var imageResource: String? = null
    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageResource = arguments!!.getString("image_source")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imageView = view.findViewById<View>(R.id.image) as ImageView

        Log.e("images_only", imageResource)

        /* BitmapFactory.Options o = new BitmapFactory.Options();
        o.inSampleSize = 4;
        o.inDither = false;
        bitmap = BitmapFactory.decodeResource(getResources(), imageResource, o);
        imageView.setImageBitmap(bitmap);*/




        if(imageResource!!.equals("0")){
            imageView.visibility=View.GONE
        }else{
            imageView.visibility=View.VISIBLE
            Picasso.with(context)
                .load(photobaseurl + imageResource!!)
                //   .error(R.drawable.ic_man_user)
                .into(imageView)
        }

    }



    companion object {
        // String photobaseurl = new PhotoBaseUrl().BASE_PHOTO_URL;
        fun getInstance(resourceID: String): PageFragment {
            val f = PageFragment()
            val args = Bundle()
            args.putString("image_source", resourceID)
            f.arguments = args
            return f
        }
    }
}
