package divorcelovelounge.com.dll

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fm: FragmentManager, private val images: List<Int>, private val images_only: List<String>) :
    FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return PageFragment.getInstance(images_only[position])
    }

    override fun getCount(): Int {
        return images.size
    }
}
