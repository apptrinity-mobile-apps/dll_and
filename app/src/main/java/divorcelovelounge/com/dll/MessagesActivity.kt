package divorcelovelounge.com.dll

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import divorcelovelounge.com.dll.Adapters.MessagesAdapter
import divorcelovelounge.com.dll.TabLayoutHelper.NonSwipeableViewPager

class MessagesActivity : AppCompatActivity() {
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: NonSwipeableViewPager
    private lateinit var rl_prefs: RelativeLayout
    private lateinit var iv_prefs_back: ImageView
    private lateinit var iv_compose_mail_id: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messages)
        initialize()

    }

    private fun initialize() {

        rl_prefs = findViewById(R.id.rl_prefs)
        iv_prefs_back = findViewById(R.id.iv_prefs_back)
        iv_compose_mail_id = findViewById(R.id.iv_compose_mail_id)
        iv_prefs_back.setOnClickListener {
            onBackPressed()
        }
        rl_prefs.visibility=View.VISIBLE
        iv_compose_mail_id.setOnClickListener {
            val intent =Intent(this@MessagesActivity,ComposeMessageActivity::class.java)
            intent.putExtra("from_stg","compose")
            startActivity(intent)
        }
        tabLayout=findViewById(R.id.tabLayout)
        viewPager=findViewById(R.id.viewPager_id)

        tabLayout.addTab(tabLayout.newTab().setText("Inbox"))
        tabLayout.addTab(tabLayout.newTab().setText("Sent"))

        tabLayout.addTab(tabLayout.newTab().setText("Important"))
        tabLayout.addTab(tabLayout.newTab().setText("Trash"))
        // tabLayout.setTabGravity(TabLayout.GRAVITY_FILL)

        val adapter = MessagesAdapter(this!!.supportFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })


    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val intent = Intent(this@MessagesActivity, DashboardActivity::class.java)
        intent.putExtra("from_list","profile_photos")
        startActivity(intent)

    }


}
