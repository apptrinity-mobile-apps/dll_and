package divorcelovelounge.com.dll

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.util.Pair
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.mzelzoghbi.zgallery.ZGallery
import com.mzelzoghbi.zgallery.entities.ZColor
import com.nanotasks.BackgroundWork
import com.nanotasks.Completion
import com.nanotasks.Tasks
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Chat.Activities.ChatActivity
import divorcelovelounge.com.dll.Chat.Xmpp.RosterManager
import divorcelovelounge.com.dll.Chat.Xmpp.XMPPUtils
import divorcelovelounge.com.dll.Chat.chat.RoomsListManager
import divorcelovelounge.com.dll.Chat.models.Chat
import divorcelovelounge.com.dll.Chat.models.Event
import divorcelovelounge.com.dll.Chat.models.User
import divorcelovelounge.com.dll.Chat.realm.RealmManager
import divorcelovelounge.com.dll.Chat.utils.Preferences
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import divorcelovelounge.com.dll.SparkButtonHelper.SparkEventListener
import org.jivesoftware.smack.packet.Presence
import org.jxmpp.jid.Jid
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FullProfile : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    lateinit var collapsing_toolbar: CollapsingToolbarLayout
    lateinit var iv_toolbar_back_id: ImageView

    var user_id = ""
    var user_date_id = ""
    var user_gender = ""
    var user_looking_for = ""
    var user_first_name = ""
    var user_email = ""
    var user_zipcode = ""
    var user_state = ""
    var user_referal_from = ""
    var user_children = ""
    var user_city = ""
    var user_dob = ""
    var user_phone_number = ""
    var user_ethnicity = ""
    var user_religion = ""
    var user_denomination = ""
    var user_education = ""
    var user_occupation = ""
    var user_income = ""
    var user_smoke = ""
    var user_drink = ""
    var user_height_ft = ""
    var user_height_inc = ""
    var user_height_cms = ""
    var user_passionate = ""
    var user_leisure_time = ""
    var user_thankful_1 = ""
    var user_thankful_2 = ""
    var user_thankful_3 = ""
    var user_partner_age_min = ""
    var user_partner_age_max = ""
    var user_partner_match_distance = ""
    var user_partner_match_search = ""
    var user_partner_ethnicity = ""
    var user_partner_religion = ""
    var user_partner_denomination = ""
    var user_partner_education = ""
    var user_partner_occupation = ""
    var user_partner_smoke = ""
    var user_partner_drink = ""
    var user_photo1 = ""
    var user_photo2 = ""
    var user_photo3 = ""
    var user_photo4 = ""
    var user_photo5 = ""
    var user_photo6 = ""
    var user_photo7 = ""
    var user_photo8 = ""
    var user_photo9 = ""
    var user_photo10 = ""
    var user_photo1_approve = ""
    var user_photo2_approve = ""
    var user_photo3_approve = ""
    var user_photo4_approve = ""
    var user_photo5_approve = ""
    var user_photo6_approve = ""
    var user_photo7_approve = ""
    var user_photo8_approve = ""
    var user_photo9_approve = ""
    var user_photo10_approve = ""
    var user_status = ""
    var user_signup_step = ""
    var user_fav_id = ""
    var user_fav_from = ""
    var user_fav_to = ""
    var user_interest_from = ""
    var user_interest_to = ""
    var interest_status = ""
    var from_screen = ""
    var partner_mtongue_name = ""
    var mtongue_name = ""
    var user_name_stg = ""


    lateinit var ll_full_list_id: LinearLayout
    lateinit var tv_user_name_id: CustomTextView
    lateinit var tv_user_age_id: CustomTextView
    lateinit var tv_info_age_id: CustomTextView
    lateinit var tv_user_distance_id: CustomTextView
    lateinit var tv_height_id: CustomTextView
    lateinit var tv_ethnicity_id: CustomTextView
    lateinit var tv_match_date_id: CustomTextView
    lateinit var tv_smoking_id: CustomTextView
    lateinit var tv_drinking_id: CustomTextView
    lateinit var tv_kids_id: CustomTextView
    lateinit var tv_religion_id: CustomTextView
    lateinit var tv_mother_tongue_id: CustomTextView
    lateinit var tv_city_id: CustomTextView
    lateinit var tv_state_id: CustomTextView
    lateinit var tv_zipcode_id: CustomTextView
    lateinit var tv_occupation_id: CustomTextView
    lateinit var tv_education_id: CustomTextView
    lateinit var tv_income_id: CustomTextView
    lateinit var ll_request_id: LinearLayout
    lateinit var tv_accept_id: CustomTextView
    lateinit var tv_Reject_id: CustomTextView
    lateinit var tv_about_me: CustomTextView
    lateinit var btn_interest_id: SparkButton
    lateinit var btn_like_id: SparkButton
    lateinit var btn_ic_msg: ImageView
    lateinit var img_profile_pic: ImageView
    lateinit var img_profile_small: ImageView
    lateinit var img_gender_id: ImageView
    lateinit var btn_ic_block: ImageView
    lateinit var iv_phone: ImageView
    lateinit var tv_block_id: CustomTextViewBold


    private val PERMISSION_CODE = 100

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>
    lateinit var location_details_list: HashMap<String, String>

    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL

    lateinit var rl_main_id: RelativeLayout


    lateinit var success_dialog: Dialog

    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id: CustomTextView
    lateinit var img_profile_pic_id: ImageView
    lateinit var iv_close: ImageView

    private var mOneToOneChats: ArrayList<Chat>? = null


    var name_stg = ""
    var gender_stg = ""
    var age_stg = ""
    var location_stg = ""

    private lateinit var addsarrayList: java.util.ArrayList<AdsListDataResponse>
    private lateinit var sliderLayout: SliderLayout


    var gps_county_stg = ""
    var gps_state_stg = ""
    var gps_city_stg = ""

    var chat_status_stg = ""

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_profile)
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar)
        iv_toolbar_back_id = findViewById(R.id.iv_toolbar_back_id)
        // collapsing_toolbar.title="Emma Watson"
        //collapsing_toolbar.setExpandedTitleColor(resources.getColor(android.R.color.transparent))
        //printHashKey(this)
        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails


        ll_full_list_id = findViewById(R.id.ll_full_list_id)
        tv_user_name_id = findViewById(R.id.tv_user_name_id)
        tv_user_age_id = findViewById(R.id.tv_user_age_id)
        tv_info_age_id = findViewById(R.id.tv_info_age_id)
        tv_user_distance_id = findViewById(R.id.tv_user_distance_id)
        tv_height_id = findViewById(R.id.tv_height_id)
        tv_ethnicity_id = findViewById(R.id.tv_ethnicity_id)
        tv_match_date_id = findViewById(R.id.tv_match_date_id)
        tv_smoking_id = findViewById(R.id.tv_smoking_id)
        tv_drinking_id = findViewById(R.id.tv_drinking_id)
        tv_kids_id = findViewById(R.id.tv_kids_id)
        tv_religion_id = findViewById(R.id.tv_religion_id)
        tv_mother_tongue_id = findViewById(R.id.tv_mother_tongue_id)
        tv_city_id = findViewById(R.id.tv_city_id)
        tv_state_id = findViewById(R.id.tv_state_id)
        tv_zipcode_id = findViewById(R.id.tv_zipcode_id)
        tv_occupation_id = findViewById(R.id.tv_occupation_id)
        tv_education_id = findViewById(R.id.tv_education_id)
        tv_income_id = findViewById(R.id.tv_income_id)
        tv_about_me = findViewById(R.id.tv_about_me)

        ll_request_id = findViewById(R.id.ll_request_id)
        tv_accept_id = findViewById(R.id.tv_accept_id)
        tv_Reject_id = findViewById(R.id.tv_Reject_id)
        btn_interest_id = findViewById(R.id.btn_interest_id)
        btn_like_id = findViewById(R.id.btn_like_id)
        btn_ic_msg = findViewById(R.id.btn_ic_msg)
        img_profile_pic = findViewById(R.id.img_profile_pic)
        img_profile_small = findViewById(R.id.img_profile_small)
        img_gender_id = findViewById(R.id.img_gender_id)
        rl_main_id = findViewById(R.id.rl_main_id)
        btn_ic_block = findViewById(R.id.btn_ic_block)
        tv_block_id = findViewById(R.id.tv_block_id)
        iv_phone = findViewById(R.id.iv_phone)
        ll_full_list_id.visibility = View.GONE
        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()


        location_details_list = dllSessionManager.locationDetails
        gps_county_stg = location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg = location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg = location_details_list[DllSessionManager.USER_GPS_CITY].toString()


        chat_status_stg = dllSessionManager.chatStatus[DllSessionManager.CHAT_STATUS].toString()



        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Profile")

        iv_toolbar_back_id.setOnClickListener {
            /*onBackPressed()*/
            onBackPressed()
        }


        val event = Event()


        //Log.e("Jid_outside", event.getJidSender().toString())
        /*if (!RosterManager.getInstance().isContact(event.getJidSender())) {
            RosterManager.getInstance().addContact("LINDA@diorcelovelounge.com")

            Log.e("Jid_isconnectedif", event.getJidSender().toString())
        }else{*/
        Log.e("Jid_isconnected", "----")

        //  }


        from_screen = intent.getStringExtra("from_screen")
        user_id = intent.getStringExtra("id")
        user_photo1 = intent.getStringExtra("photo1")
        name_stg = intent.getStringExtra("name_stg")
        gender_stg = intent.getStringExtra("gender_stg")
        age_stg = intent.getStringExtra("age_stg")
        location_stg = intent.getStringExtra("location_stg")


        tv_user_name_id.text = name_stg

        if (gender_stg.equals("Woman")) {
            img_gender_id.setImageResource(R.drawable.new_female_ic)
        } else {
            img_gender_id.setImageResource(R.drawable.new_male_ic)
        }


        if (age_stg != "0") {
            var date_only: Date? = null
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            try {
                date_only = sdf.parse(age_stg)
            } catch (e: ParseException) {
                e.printStackTrace()
            }


            val c = Calendar.getInstance()
            //Set time in milliseconds
            // c.setTimeInMillis(user_date_id.toLong())
            c.time = date_only
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)


            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()

            dob.set(mYear, mMonth, mDay)

            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }

            val ageInt = age

            tv_user_age_id.text = ageInt.toString()
        } else {
            tv_user_age_id.text = "0"
        }
        tv_user_distance_id.text = location_stg


        Picasso.with(this@FullProfile)
            .load(photobaseurl + user_photo1)
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic)


        Picasso.with(this@FullProfile)
            .load(photobaseurl + user_photo1)
            .error(R.drawable.ic_man_user)
            .transform(CircleTransform())
            .into(img_profile_small)


        val apiService = ApiInterface.create()
        val call = apiService.getPartnerProfile(
            user_details[DllSessionManager.USER_ID].toString(),
            user_id
        )

        call.enqueue(object : Callback<PartnerProfileResponse> {
            override fun onResponse(
                call: Call<PartnerProfileResponse>,
                response: retrofit2.Response<PartnerProfileResponse>?
            ) {
                // Log.d("CardPagerAdapter", "partner_details success ${response!!.body()!!.result}")

                ll_full_list_id.visibility = View.VISIBLE

                if (response!!.body()!!.result.equals("success")) {
                    if (response!!.body()!!.status == "1") {
                        val user_info = response.body()!!.user_info

                        if (user_info!!.id == null || user_info.id == "") {
                            user_id = "0"
                        } else {
                            user_id = user_info.id!!
                        }
                        if (user_info.date_id == null || user_info.date_id == "") {
                            user_date_id = "0"
                        } else {
                            user_date_id = user_info.date_id!!
                        }

                        if (user_info.looking_for == null || user_info.looking_for == "") {
                            user_looking_for = "0"
                        } else {
                            user_looking_for = user_info.looking_for!!
                        }

                        if (user_info.gender == null || user_info.gender == "") {
                            user_gender = "0"
                        } else {
                            user_gender = user_info.gender!!
                        }
                        if (user_info.first_name == null || user_info.first_name == "") {
                            user_first_name = "0"
                        } else {
                            user_first_name = user_info.first_name!!
                        }
                        if (user_info.email == null || user_info.email == "") {
                            user_email = "0"
                        } else {
                            user_email = user_info.email!!
                        }
                        if (user_info.zipcode == null || user_info.zipcode == "") {
                            user_zipcode = "0"
                        } else {
                            user_zipcode = user_info.zipcode!!
                        }
                        if (user_info.state == null || user_info.state == "") {
                            user_state = "0"
                        } else {
                            user_state = user_info.state!!
                        }
                        if (user_info.referal_from == null || user_info.referal_from == "") {
                            user_referal_from = "0"
                        } else {
                            user_referal_from = user_info.referal_from!!
                        }
                        if (user_info.children == null || user_info.children == "") {
                            user_children = "0"
                        } else {
                            user_children = user_info.children!!
                        }

                        if (user_info.city == null || user_info.city == "") {
                            user_city = "0"
                        } else {
                            user_city = user_info.city!!
                        }
                        if (user_info.dob == null || user_info.dob == "") {
                            user_dob = "0"
                        } else {
                            user_dob = user_info.dob!!
                        }

                        if (user_info.phone_number == null || user_info.phone_number == "") {
                            user_phone_number = "0"
                        } else {
                            user_phone_number = user_info.phone_number!!
                        }
                        if (user_info.ethnicity == null || user_info.ethnicity == "") {
                            user_ethnicity = "0"
                        } else {
                            user_ethnicity = user_info.ethnicity!!
                        }
                        if (user_info.religion == null || user_info.religion == "") {
                            user_religion = "0"
                        } else {
                            user_religion = user_info.religion!!
                        }
                        if (user_info.denomination == null || user_info.denomination == "") {
                            user_denomination = "0"
                        } else {
                            user_denomination = user_info.denomination!!
                        }
                        if (user_info.education == null || user_info.education == "") {
                            user_education = "0"
                        } else {
                            user_education = user_info.education!!
                        }
                        if (user_info.occupation == null || user_info.occupation == "") {
                            user_occupation = "0"
                        } else {
                            user_occupation = user_info.occupation!!
                        }
                        if (user_info.income == null || user_info.income == "") {
                            user_income = "0"
                        } else {
                            user_income = user_info.income!!
                        }
                        if (user_info.smoke == null || user_info.smoke == "") {
                            user_smoke = "0"
                        } else {
                            user_smoke = user_info.smoke!!
                        }
                        if (user_info.drink == null || user_info.drink == "") {
                            user_drink = "0"
                        } else {
                            user_drink = user_info.drink!!
                        }
                        if (user_info.height_ft == null || user_info.height_ft == "") {
                            user_height_ft = "0"
                        } else {
                            user_height_ft = user_info.height_ft!!
                        }
                        if (user_info.height_inc == null || user_info.height_inc == "") {
                            user_height_inc = "0"
                        } else {
                            user_height_inc = user_info.height_inc!!
                        }
                        if (user_info.height_cms == null || user_info.height_cms == "") {
                            user_height_cms = "0"
                        } else {
                            user_height_cms = user_info.height_cms!!
                        }
                        if (user_info.passionate == null || user_info.passionate == "") {
                            user_passionate = "0"
                        } else {
                            user_passionate = user_info.passionate!!
                        }
                        if (user_info.leisure_time == null || user_info.leisure_time == "") {
                            user_leisure_time = "0"
                        } else {
                            user_leisure_time = user_info.leisure_time!!
                        }
                        if (user_info.thankful_1 == null || user_info.thankful_1 == "") {
                            user_thankful_1 = "0"
                        } else {
                            user_thankful_1 = user_info.thankful_1!!
                        }
                        if (user_info.thankful_2 == null || user_info.thankful_2 == "") {
                            user_thankful_2 = "0"
                        } else {
                            user_thankful_2 = user_info.thankful_2!!
                        }
                        if (user_info.thankful_3 == null || user_info.thankful_3 == "") {
                            user_thankful_3 = "0"
                        } else {
                            user_thankful_3 = user_info.thankful_3!!
                        }
                        if (user_info.partner_age_min == null || user_info.partner_age_min == "") {
                            user_partner_age_min = "0"
                        } else {
                            user_partner_age_min = user_info.partner_age_min!!
                        }
                        if (user_info.partner_age_max == null || user_info.partner_age_max == "") {
                            user_partner_age_max = "0"
                        } else {
                            user_partner_age_max = user_info.partner_age_max!!
                        }
                        if (user_info.partner_match_distance == null || user_info.partner_match_distance == "") {
                            user_partner_match_distance = "0"
                        } else {
                            user_partner_match_distance = user_info.partner_match_distance!!
                        }
                        if (user_info.partner_match_search == null || user_info.partner_match_search == "") {
                            user_partner_match_search = "0"
                        } else {
                            user_partner_match_search = user_info.partner_match_search!!
                        }
                        if (user_info.partner_ethnicity == null || user_info.partner_ethnicity == "") {
                            user_partner_ethnicity = "0"
                        } else {
                            user_partner_ethnicity = user_info.partner_ethnicity!!
                        }
                        if (user_info.partner_religion == null || user_info.partner_religion == "") {
                            user_partner_religion = "0"
                        } else {
                            user_partner_religion = user_info.partner_religion!!
                        }
                        if (user_info.partner_denomination == null || user_info.partner_denomination == "") {
                            user_partner_denomination = "0"
                        } else {
                            user_partner_denomination = user_info.partner_denomination!!
                        }
                        if (user_info.partner_education == null || user_info.partner_education == "") {
                            user_partner_education = "0"
                        } else {
                            user_partner_education = user_info.partner_education!!
                        }
                        if (user_info.partner_occupation == null || user_info.partner_occupation == "") {
                            user_partner_occupation = "0"
                        } else {
                            user_partner_occupation = user_info.partner_occupation!!
                        }
                        if (user_info.partner_smoke == null || user_info.partner_smoke == "") {
                            user_partner_smoke = "0"
                        } else {
                            user_partner_smoke = user_info.partner_smoke!!
                        }
                        if (user_info.partner_drink == null || user_info.partner_drink == "") {
                            user_partner_drink = "0"
                        } else {
                            user_partner_drink = user_info.partner_drink!!
                        }
                        if (user_info.photo1 == null || user_info.photo1 == "") {
                            user_photo1 = "0"
                        } else {
                            user_photo1 = user_info.photo1!!
                        }
                        if (user_info.photo2 == null || user_info.photo2 == "") {
                            user_photo2 = "0"
                        } else {
                            user_photo2 = user_info.photo2!!
                        }
                        if (user_info.photo3 == null || user_info.photo3 == "") {
                            user_photo3 = "0"
                        } else {
                            user_photo3 = user_info.photo3!!
                        }
                        if (user_info.photo4 == null || user_info.photo4 == "") {
                            user_photo4 = "0"
                        } else {
                            user_photo4 = user_info.photo4!!
                        }
                        if (user_info.photo5 == null || user_info.photo5 == "") {
                            user_photo5 = "0"
                        } else {
                            user_photo5 = user_info.photo5!!
                        }
                        if (user_info.photo6 == null || user_info.photo6 == "") {
                            user_photo6 = "0"
                        } else {
                            user_photo6 = user_info.photo6!!
                        }
                        if (user_info.photo7 == null || user_info.photo7 == "") {
                            user_photo7 = "0"
                        } else {
                            user_photo7 = user_info.photo7!!
                        }
                        if (user_info.photo8 == null || user_info.photo8 == "") {
                            user_photo8 = "0"
                        } else {
                            user_photo8 = user_info.photo8!!
                        }
                        if (user_info.photo9 == null || user_info.photo9 == "") {
                            user_photo9 = "0"
                        } else {
                            user_photo9 = user_info.photo9!!
                        }
                        if (user_info.photo10 == null || user_info.photo10 == "") {
                            user_photo10 = "0"
                        } else {
                            user_photo10 = user_info.photo10!!
                        }

                        if (user_info.photo1_approve == null || user_info.photo1_approve == "") {
                            user_photo1_approve = "0"
                        } else {
                            user_photo1_approve = user_info.photo1_approve!!
                        }
                        if (user_info.photo2_approve == null || user_info.photo2_approve == "") {
                            user_photo2_approve = "0"
                        } else {
                            user_photo2_approve = user_info.photo2_approve!!
                        }
                        if (user_info.photo3_approve == null || user_info.photo3_approve == "") {
                            user_photo3_approve = "0"
                        } else {
                            user_photo3_approve = user_info.photo3_approve!!
                        }
                        if (user_info.photo4_approve == null || user_info.photo4_approve == "") {
                            user_photo4_approve = "0"
                        } else {
                            user_photo4_approve = user_info.photo4_approve!!
                        }
                        if (user_info.photo5_approve == null || user_info.photo5_approve == "") {
                            user_photo5_approve = "0"
                        } else {
                            user_photo5_approve = user_info.photo5_approve!!
                        }
                        if (user_info.photo6_approve == null || user_info.photo6_approve == "") {
                            user_photo6_approve = "0"
                        } else {
                            user_photo6_approve = user_info.photo6_approve!!
                        }
                        if (user_info.photo7_approve == null || user_info.photo7_approve == "") {
                            user_photo7_approve = "0"
                        } else {
                            user_photo7_approve = user_info.photo7_approve!!
                        }
                        if (user_info.photo8_approve == null || user_info.photo8_approve == "") {
                            user_photo8_approve = "0"
                        } else {
                            user_photo8_approve = user_info.photo8_approve!!
                        }
                        if (user_info.photo9_approve == null || user_info.photo9_approve == "") {
                            user_photo9_approve = "0"
                        } else {
                            user_photo9_approve = user_info.photo9_approve!!
                        }
                        if (user_info.photo10_approve == null || user_info.photo10_approve == "") {
                            user_photo10_approve = "0"
                        } else {
                            user_photo10_approve = user_info.photo10_approve!!
                        }
                        if (user_info.status == null || user_info.status == "") {
                            user_status = "0"
                        } else {
                            user_status = user_info.status!!
                        }
                        if (user_info.signup_step == null || user_info.signup_step == "") {
                            user_signup_step = "0"
                        } else {
                            user_signup_step = user_info.signup_step!!
                        }
                        if (user_info.fav_id == null || user_info.fav_id == "") {
                            user_fav_id = "0"
                        } else {
                            user_fav_id = user_info.fav_id!!
                        }
                        if (user_info.fav_from == null || user_info.fav_from == "") {
                            user_fav_from = "0"
                        } else {
                            user_fav_from = user_info.fav_from!!
                        }
                        if (user_info.fav_to == null || user_info.fav_to == "") {
                            user_fav_to = "0"
                        } else {
                            user_fav_to = user_info.fav_to!!
                        }
                        if (user_info.interest_from == null || user_info.interest_from == "") {
                            user_interest_from = "0"
                        } else {
                            user_interest_from = user_info.interest_from!!
                        }
                        if (user_info.interest_status == null || user_info.interest_status == "") {
                            user_interest_to = "0"
                        } else {
                            user_interest_to = user_info.interest_to!!
                        }
                        if (user_info.interest_status == null || user_info.interest_status == "") {
                            interest_status = "0"
                        } else {
                            interest_status = user_info.interest_status!!
                        }
                        if (user_info.partner_mtongue_name == null || user_info.partner_mtongue_name == "") {
                            partner_mtongue_name = "0"
                        } else {
                            partner_mtongue_name = user_info.partner_mtongue_name!!
                        }
                        if (user_info.mtongue_name == null || user_info.mtongue_name == "") {
                            mtongue_name = "0"
                        } else {
                            mtongue_name = user_info.mtongue_name!!
                        }

                        if (user_info.username == null || user_info.username == "") {
                            user_name_stg = "0"
                        } else {
                            user_name_stg = user_info.username!!
                        }


                        Log.e(
                            "user_gender_STATUS",
                            user_gender + "-----" + interest_status + "---" + user_name_stg
                        )


                        if (user_gender.equals("Woman")) {
                            img_gender_id.setImageResource(R.drawable.new_female_ic)
                        } else {
                            img_gender_id.setImageResource(R.drawable.new_male_ic)
                        }

                        if (user_interest_from == user_details.get(DllSessionManager.USER_ID).toString()) {
                            btn_interest_id.isChecked = true
                            btn_interest_id.isEnabled = false
                        } else {
                            btn_interest_id.isChecked = false

                        }

                        if (user_fav_from == user_details.get(DllSessionManager.USER_ID).toString()) {
                            btn_like_id.isChecked = true
                        } else {
                            btn_like_id.isChecked = false
                        }

                        if (user_photo1 == "0") {
                            Picasso.with(this@FullProfile)
                                .load(photobaseurl + user_photo1)
                                .error(R.drawable.ic_man_user)
                                .into(img_profile_pic)
                            img_profile_pic.scaleType = ImageView.ScaleType.CENTER_INSIDE

                            Picasso.with(this@FullProfile)
                                .load(photobaseurl + user_photo1)
                                .error(R.drawable.ic_man_user)
                                .transform(CircleTransform())
                                .into(img_profile_small)

                        } else {
                            if (user_photo1_approve == "UNAPPROVED") {
                                Picasso.with(this@FullProfile)
                                    .load(photobaseurl + "0")
                                    .error(R.drawable.ic_man_user)
                                    .into(img_profile_pic)


                                Picasso.with(this@FullProfile)
                                    .load(photobaseurl + "0")
                                    .error(R.drawable.ic_man_user)
                                    .transform(CircleTransform())
                                    .into(img_profile_small)
                            } else {
                                Picasso.with(this@FullProfile)
                                    .load(photobaseurl + user_photo1)
                                    .error(R.drawable.ic_man_user)
                                    .into(img_profile_pic)


                                Picasso.with(this@FullProfile)
                                    .load(photobaseurl + user_photo1)
                                    .error(R.drawable.ic_man_user)
                                    .transform(CircleTransform())
                                    .into(img_profile_small)
                            }
                        }

                        /*if (dllSessionManager.userDetails[DllSessionManager.MEMBERSHIP_STATUS]!! == "1") {
                            if (dllSessionManager.userDetails[DllSessionManager.MEMBERSHIP_CHAT_STATUS]!! == "Yes"  && interest_status!! == "1") {
                                btn_ic_msg.isEnabled = true
                            } else {
                                btn_ic_msg.isEnabled = false
                                val matrix = ColorMatrix()
                                matrix.setSaturation(0f)
                                val filter = ColorMatrixColorFilter(matrix)
                                btn_ic_msg.colorFilter = filter
                            }
                        } else {
                            btn_ic_msg.isEnabled = false
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            val filter = ColorMatrixColorFilter(matrix)
                            btn_ic_msg.colorFilter = filter
                        }*/

                        if (dllSessionManager.userDetails[DllSessionManager.MEMBERSHIP_STATUS]!! == "1") {
                            if (dllSessionManager.userDetails[DllSessionManager.MEMBERSHIP_CHAT_STATUS]!! == "Yes" && interest_status!! == "1") {
                                btn_ic_msg.isEnabled = true
                            } else {
                                btn_ic_msg.isEnabled = true
                                /*btn_ic_msg.isEnabled = false
                                val matrix = ColorMatrix()
                                matrix.setSaturation(0f)
                                val filter = ColorMatrixColorFilter(matrix)
                                btn_ic_msg.colorFilter = filter*/
                            }
                        } else {
                            btn_ic_msg.isEnabled = true
                            /*btn_ic_msg.isEnabled = false
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            val filter = ColorMatrixColorFilter(matrix)
                            btn_ic_msg.colorFilter = filter*/
                        }
                        if (user_details.get(DllSessionManager.PHOTO1_APPROVE).toString().equals("UNAPPROVED")) {

                            btn_interest_id.isEnabled = false
                            btn_like_id.isEnabled = false
                            btn_ic_msg.isEnabled = false
                            iv_phone.isEnabled = false
                            btn_ic_block.isEnabled = false
                            tv_accept_id.isEnabled = false

                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            val filter = ColorMatrixColorFilter(matrix)
                            btn_ic_msg.colorFilter = filter
                            btn_ic_block.colorFilter = filter

                        }

                        if(chat_status_stg.equals("0")){
                            btn_ic_msg.isEnabled = false
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            val filter = ColorMatrixColorFilter(matrix)
                            btn_ic_msg.colorFilter = filter
                        }


                        Log.e("url_pic_id", photobaseurl + user_photo1)

                        if (user_dob != "0") {
                            var date_only: Date? = null
                            val sdf = SimpleDateFormat("yyyy-MM-dd")
                            try {
                                date_only = sdf.parse(user_dob)
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }


                            val c = Calendar.getInstance()
                            //Set time in milliseconds
                            // c.setTimeInMillis(user_date_id.toLong())
                            c.time = date_only
                            val mYear = c.get(Calendar.YEAR)
                            val mMonth = c.get(Calendar.MONTH)
                            val mDay = c.get(Calendar.DAY_OF_MONTH)


                            val dob = Calendar.getInstance()
                            val today = Calendar.getInstance()

                            dob.set(mYear, mMonth, mDay)

                            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

                            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                                age--
                            }

                            val ageInt = age

                            DataIntegration(ageInt.toString())
                        } else {
                            DataIntegration("0")
                        }

                        // DataIntegration("22")
                        /*   if (isTelephonePermissionAllowed()) {
                               Toast.makeText(this@FullProfile, "Permission granted!", Toast.LENGTH_LONG).show()
                           }*/

                        tv_block_id.setOnClickListener {

                            // val builder = AlertDialog.Builder(this@FullProfile)

                            if (from_screen.equals("blocked")) {


                                success_dialog = Dialog(this@FullProfile)
                                success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                                success_dialog.setContentView(R.layout.success_dialog_layout)
                                val success_window = success_dialog.window
                                success_window!!.setLayout(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                                img_profile_pic_id =
                                    success_dialog.findViewById(R.id.img_profile_pic_id)
                                tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
                                tv_msg_title_id.text = "Alert!"
                                tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
                                tv_full_msg_id.text =
                                    "Are you sure you want to Unblock $user_first_name?"
                                iv_close = success_dialog.findViewById(R.id.iv_close)
                                iv_close.visibility = View.VISIBLE
                                tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
                                tv_ok_btn_id.setOnClickListener {
                                    success_dialog.dismiss()
                                    val api = ApiInterface.create()
                                    val blockUserApi = api.blockUserApi(
                                        user_details[DllSessionManager.USER_ID]!!,
                                        user_id
                                    )
                                    blockUserApi.enqueue(object : Callback<BlockUserResponse> {
                                        override fun onFailure(
                                            call: Call<BlockUserResponse>,
                                            t: Throwable
                                        ) {
                                            Log.e("@6", "new")
                                            /* Toast.makeText(
                                                 this@FullProfile,
                                                 "Something went wrong!",
                                                 Toast.LENGTH_SHORT
                                             )
                                                 .show()*/
                                        }

                                        override fun onResponse(
                                            call: Call<BlockUserResponse>,
                                            response: Response<BlockUserResponse>
                                        ) {
                                            Log.d("FullProfile", response.body()!!.result)
                                            if (response != null) {

                                                if (response.body() != null) {

                                                    if (response.body()!!.status == "1") {
                                                        Toast.makeText(
                                                            this@FullProfile,
                                                            "Blocked",
                                                            Toast.LENGTH_SHORT
                                                        )
                                                            .show()
                                                        val home_intent =
                                                            Intent(
                                                                this@FullProfile,
                                                                DashboardActivity::class.java
                                                            )
                                                        home_intent.putExtra(
                                                            "from_list",
                                                            "quiz_setup"
                                                        )
                                                        startActivity(home_intent)
                                                        finish()
                                                    } else {
                                                        Log.e("@5", "new")
                                                        /*Toast.makeText(
                                                            this@FullProfile,
                                                            "Something went wrong",
                                                            Toast.LENGTH_SHORT
                                                        )
                                                            .show()*/
                                                    }

                                                } else {
                                                    Toast.makeText(
                                                        this@FullProfile,
                                                        "Please try again!",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }

                                            } else {
                                                Log.e("@4", "new")
                                                /*Toast.makeText(
                                                    this@FullProfile,
                                                    "Something went wrong!",
                                                    Toast.LENGTH_SHORT
                                                ).show()*/

                                            }

                                        }

                                    })
                                }




                                if (user_photo1 == "0") {
                                    Picasso.with(this@FullProfile)
                                        .load(photobaseurl + user_photo1)
                                        .error(R.drawable.ic_man_user)
                                        .into(img_profile_pic_id)
                                    img_profile_pic_id.scaleType = ImageView.ScaleType.CENTER_INSIDE


                                } else {
                                    if (user_photo1_approve == "UNAPPROVED") {
                                        Picasso.with(this@FullProfile)
                                            .load(photobaseurl + "0")
                                            .error(R.drawable.ic_man_user)
                                            .into(img_profile_pic_id)

                                    } else {
                                        Picasso.with(this@FullProfile)
                                            .load(photobaseurl + user_photo1)
                                            .error(R.drawable.ic_man_user)
                                            .into(img_profile_pic_id)

                                    }
                                }


                                iv_close.setOnClickListener {
                                    success_dialog.dismiss()
                                }

                                success_dialog.show()


                            } else {

                                success_dialog = Dialog(this@FullProfile)
                                success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                                success_dialog.setContentView(R.layout.success_dialog_layout)
                                val success_window = success_dialog.window
                                success_window!!.setLayout(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )
                                img_profile_pic_id =
                                    success_dialog.findViewById(R.id.img_profile_pic_id)
                                tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
                                tv_msg_title_id.text = "Alert!"
                                tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
                                tv_full_msg_id.text =
                                    "Are you sure you want to Block $user_first_name?"
                                iv_close = success_dialog.findViewById(R.id.iv_close)
                                iv_close.visibility = View.VISIBLE
                                tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
                                tv_ok_btn_id.setOnClickListener {
                                    success_dialog.dismiss()
                                    val api = ApiInterface.create()
                                    val blockUserApi = api.blockUserApi(
                                        user_details[DllSessionManager.USER_ID]!!,
                                        user_id
                                    )
                                    blockUserApi.enqueue(object : Callback<BlockUserResponse> {
                                        override fun onFailure(
                                            call: Call<BlockUserResponse>,
                                            t: Throwable
                                        ) {
                                            Log.e("@3", "new")
                                            /*Toast.makeText(
                                                this@FullProfile,
                                                "Something went wrong!",
                                                Toast.LENGTH_SHORT
                                            )
                                                .show()*/
                                        }

                                        override fun onResponse(
                                            call: Call<BlockUserResponse>,
                                            response: Response<BlockUserResponse>
                                        ) {
                                            Log.d("FullProfile", response.body()!!.result)
                                            if (response != null) {

                                                if (response.body() != null) {

                                                    if (response.body()!!.status == "1") {
                                                        Toast.makeText(
                                                            this@FullProfile,
                                                            "Blocked",
                                                            Toast.LENGTH_SHORT
                                                        )
                                                            .show()
                                                        val home_intent =
                                                            Intent(
                                                                this@FullProfile,
                                                                DashboardActivity::class.java
                                                            )
                                                        home_intent.putExtra(
                                                            "from_list",
                                                            "quiz_setup"
                                                        )
                                                        startActivity(home_intent)
                                                        finish()
                                                    } else {
                                                        Log.e("@1", "new")

                                                        /* Toast.makeText(
                                                             this@FullProfile,
                                                             "Something went wrong",
                                                             Toast.LENGTH_SHORT
                                                         )
                                                             .show()*/
                                                    }

                                                } else {
                                                    Toast.makeText(
                                                        this@FullProfile,
                                                        "Please try again!",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }

                                            } else {
                                                Log.e("@2", "new")
                                                /*Toast.makeText(
                                                    this@FullProfile,
                                                    "Something went wrong!",
                                                    Toast.LENGTH_SHORT
                                                ).show()*/

                                            }

                                        }

                                    })
                                }

                                if (user_photo1 == "0") {
                                    Picasso.with(this@FullProfile)
                                        .load(photobaseurl + user_photo1)
                                        .error(R.drawable.ic_man_user)
                                        .into(img_profile_pic_id)
                                    img_profile_pic_id.scaleType = ImageView.ScaleType.CENTER_INSIDE


                                } else {
                                    if (user_photo1_approve == "UNAPPROVED") {
                                        Picasso.with(this@FullProfile)
                                            .load(photobaseurl + "0")
                                            .error(R.drawable.ic_man_user)
                                            .into(img_profile_pic_id)

                                    } else {
                                        Picasso.with(this@FullProfile)
                                            .load(photobaseurl + user_photo1)
                                            .error(R.drawable.ic_man_user)
                                            .into(img_profile_pic_id)

                                    }
                                }


                                iv_close.setOnClickListener {
                                    success_dialog.dismiss()
                                }

                                success_dialog.show()

                            }


                        }



                        img_profile_pic.setOnClickListener {
                            Log.e("lllll", "click")


                            val images_only: ArrayList<String> = ArrayList()

                            if (user_photo1 != "0") {
                                if (user_photo1_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo1)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo2 != "0") {
                                if (user_photo2_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo2)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo3 != "0") {
                                if (user_photo3_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo3)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo4 != "0") {
                                if (user_photo4_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo4)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo5 != "0") {
                                if (user_photo5_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo5)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo5 != "0") {
                                if (user_photo5_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo5)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo6 != "0") {
                                if (user_photo6_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo6)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo7 != "0") {
                                if (user_photo7_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo7)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo8 != "0") {
                                if (user_photo8_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo8)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo9 != "0") {
                                if (user_photo9_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo9)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }
                            if (user_photo10 != "0") {
                                if (user_photo10_approve != "UNAPPROVED") {
                                    images_only.add(photobaseurl + user_photo10)
                                } else {
                                    // do not add photos
                                }
                            } else {
                                // don't display image
                            }

                            if (images_only.size > 0) {
                                ZGallery.with(this@FullProfile, images_only)
                                    .setToolbarTitleColor(ZColor.WHITE)
                                    .setGalleryBackgroundColor(ZColor.BLACK)
                                    .setToolbarColorResId(R.color.transparent)
                                    .setTitle("")
                                    .show()
                            }

                        }


                    } else {
                        Toast.makeText(
                            this@FullProfile,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.e("@8", "new" + "--" + response.body()!!.result)
                        memberShipAlertDialog()
                    }

                } else {
                    val e: java.lang.Exception = java.lang.Exception()
                    Log.e("exception", e.toString())
                    Log.e("@8", "new")
                    // Toast.makeText(this@FullProfile, "Something went wrong!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<PartnerProfileResponse>, t: Throwable) {
                Log.d("CardPagerAdapter", "partner_details error $t")
                Log.e("@9", "new")
                //Toast.makeText(this@FullProfile, "Something went wrong!", Toast.LENGTH_SHORT).show()
            }
        })

        /*if (user_details.get(DllSessionManager.PHOTO1_APPROVE).toString().equals("UNAPPROVED")) {

            btn_interest_id.isEnabled = false
            btn_like_id.isEnabled = false
            btn_ic_msg.isEnabled = false
            iv_phone.isEnabled = false
            btn_ic_block.isEnabled = false
            tv_accept_id.isEnabled = false

        }*/

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun DataIntegration(age_id: String) {
        tv_user_name_id.setText(user_first_name)
        tv_user_age_id.setText(age_id)
        tv_info_age_id.setText(age_id)
        tv_user_distance_id.setText(user_city)
        val height = String.format(
            "%s'%s\"",
            user_height_ft, user_height_inc
        )

        tv_height_id.setText("$height/$user_height_cms")
        tv_ethnicity_id.setText(user_ethnicity)
        tv_match_date_id.setText(user_looking_for)
        tv_smoking_id.setText(user_smoke)
        tv_drinking_id.setText(user_drink)
        tv_kids_id.setText(user_children)
        tv_religion_id.setText(user_religion)
        tv_mother_tongue_id.setText(mtongue_name)
        tv_city_id.setText(user_city)
        tv_state_id.setText(user_state)
        tv_zipcode_id.setText(user_zipcode)
        tv_occupation_id.setText(user_occupation)
        tv_education_id.setText(user_education)
        tv_income_id.setText(user_income)
        tv_about_me.setText(user_passionate)



        btn_ic_msg!!.setOnClickListener {
            mOneToOneChats = ArrayList()
            mOneToOneChats!!.clear()

            RosterManager.getInstance().addContact(user_name_stg + "@divorcelovelounge.com")

            val chat = Chat()
            chat.type = Chat.TYPE_1_T0_1
            chat.isShow = true
            chat.name = user_name_stg
            chat.jid = user_name_stg + "@divorcelovelounge.com"
            chat.dateCreated = Date()
            RealmManager.getInstance().saveChat(chat)

            mOneToOneChats!!.add(chat)

            /*val intent = Intent(this@FullProfile, ChatwebActivity::class.java)
            intent.putExtra("tuser_id", user_id)
            intent.putExtra("tusername", user_first_name)
            intent.putExtra("tuseremail", user_email)
            intent.putExtra("tphoto1", user_photo1)

            startActivity(intent)*/


            val intent = Intent(this@FullProfile, ChatActivity::class.java)
            intent.putExtra(ChatActivity.CHAT_JID_PARAMETER, chat.jid)
            intent.putExtra(ChatActivity.CHAT_NAME_PARAMETER, XMPPUtils.getChatName(chat))
            intent.putExtra(ChatActivity.IS_NEW_CHAT_PARAMETER, false)
            startActivity(intent)

        }


        if (from_screen.equals("interested_me")) {

            ll_request_id.visibility = View.VISIBLE

            if (interest_status.equals("0")) {
                tv_accept_id.visibility = View.VISIBLE
                tv_Reject_id.visibility = View.VISIBLE

                tv_accept_id.setOnClickListener {

                    //iterestAcceptApi

                    val apiService = ApiInterface.create()
                    val call =
                        apiService.iterestAcceptApi(
                            user_details.get(DllSessionManager.USER_ID).toString(),
                            user_id
                        )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                /*Toast.makeText(
                                    this@FullProfile,
                                    response.body()!!.result.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()*/

                                tv_accept_id.setText("Accepted")
                                tv_Reject_id.visibility = View.GONE
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }
                tv_Reject_id.setOnClickListener {

                    val apiService = ApiInterface.create()
                    val call =
                        apiService.iterestRejectApi(
                            user_details.get(DllSessionManager.USER_ID).toString(),
                            user_id
                        )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                /*Toast.makeText(
                                    this@FullProfile,
                                    response.body()!!.result.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()*/
                                tv_Reject_id.text = "Rejected"
                                tv_accept_id.visibility = View.GONE
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }

            } else if (interest_status.equals("1")) {
                tv_accept_id.visibility = View.VISIBLE
                tv_accept_id.setText("Accepted")

                tv_Reject_id.visibility = View.GONE
            } else if (interest_status.equals("2")) {
                tv_accept_id.visibility = View.GONE
                tv_Reject_id.visibility = View.VISIBLE
                tv_Reject_id.setText("Rejected")
            }


        } else if (from_screen.equals("my_interest")) {

            ll_request_id.visibility = View.VISIBLE

            if (interest_status.equals("0")) {
                tv_accept_id.visibility = View.GONE
                tv_Reject_id.visibility = View.VISIBLE
                tv_Reject_id.setText("Pending")
                tv_Reject_id.setBackgroundTintList(getResources().getColorStateList(R.color.pendding_yellow_color))
            } else if (interest_status.equals("1")) {
                tv_accept_id.visibility = View.VISIBLE
                tv_Reject_id.visibility = View.GONE
                tv_accept_id.setText("Accepted")

            } else if (interest_status.equals("2")) {
                tv_accept_id.visibility = View.GONE
                tv_Reject_id.visibility = View.VISIBLE
                tv_Reject_id.setText("Rejected")
                tv_Reject_id.setBackgroundTintList(getResources().getColorStateList(R.color.reject_red_color))
            }

        } else if (from_screen.equals("matches")) {
            ll_request_id.visibility = View.GONE

        } else if (from_screen == "blocked") {
            ll_request_id.visibility = View.GONE
            btn_interest_id.isEnabled = false
            btn_interest_id.isChecked = false
            btn_like_id.isEnabled = false
            btn_like_id.isChecked = false
            btn_ic_msg.isEnabled = false
            iv_phone.isEnabled = false
            iv_phone.isFocusable = false
            iv_phone.isClickable = false
            iv_phone.setColorFilter(
                ContextCompat.getColor(this, R.color.gray_light),
                android.graphics.PorterDuff.Mode.SRC_IN
            );
            tv_block_id.text = "Unblock"

            // For grey scale image view
            val matrix = ColorMatrix()
            matrix.setSaturation(0f)
            val filter = ColorMatrixColorFilter(matrix)
            val filter_1 = ColorMatrixColorFilter(matrix)
            btn_ic_msg.colorFilter = filter
            // iv_phone.colorFilter = filter_1


        } else if (from_screen.equals("Contacted")) {
            ll_request_id.visibility = View.GONE

        } else {
            ll_request_id.visibility = View.GONE
        }

        btn_ic_block.setOnClickListener {

            if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1") {
                val intent = Intent(this, ComposeMessageActivity::class.java)
                intent.putExtra("msg_from_id", user_id)
                intent.putExtra("first_name_stg", user_first_name)
                intent.putExtra("from_stg", "dashboard")
                startActivity(intent)
            } else if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "2") {

                memberShipAlertDialog()

            } else {
                memberShipAlertDialog()
            }
        }


        iv_phone.setOnClickListener {

            if (isTelephonePermissionAllowed()) {
                if (user_phone_number != "0") {

                    val api = ApiInterface.create()
                    val blockUserApi =
                        api.viewContactApi(user_details[DllSessionManager.USER_ID]!!, user_id)
                    blockUserApi.enqueue(object : Callback<BlockUserResponse> {
                        override fun onFailure(call: Call<BlockUserResponse>, t: Throwable) {
                            Log.e("@7", "new")
                            /*Toast.makeText(
                                this@FullProfile,
                                "Something went wrong!",
                                Toast.LENGTH_SHORT
                            )
                                .show()*/
                        }

                        override fun onResponse(
                            call: Call<BlockUserResponse>,
                            response: Response<BlockUserResponse>
                        ) {
                            //Log.d("FullProfile", response.body()!!.result)

                            if (response.body() != null) {

                                if (response.body()!!.status == "1") {
                                    try {
                                        val phone_no = user_phone_number
                                        val call = Uri.parse("tel:$phone_no")
                                        val call_intent = Intent(Intent.ACTION_CALL, call)
                                        startActivity(call_intent)
                                    } catch (e: SecurityException) {
                                        e.printStackTrace()
                                        Log.d("FullProf_llll", e.toString())
                                    }
                                } else if (response.body()!!.status == "3") {
                                    memberShipAlertDialog()
                                } else {
                                    Toast.makeText(
                                        this@FullProfile,
                                        "Please try again!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                            } else {
                                Toast.makeText(
                                    this@FullProfile,
                                    "Please try again!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    })

                } else {
                    Toast.makeText(
                        this@FullProfile,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            } else {
                Toast.makeText(
                    this@FullProfile,
                    "Requires permission to make call!",
                    Toast.LENGTH_LONG
                )
                    .show()
                requestTelephonePermission()
            }

        }




        btn_interest_id.setEventListener(object : SparkEventListener {
            override fun onEvent(button: ImageView, buttonState: Boolean) {
                /*if (plan_status == "1") {*/
                if (user_interest_from == user_details.get(DllSessionManager.USER_ID).toString()) {
                    btn_interest_id.setEventListener(null)
                    btn_interest_id.isEnabled = false
                    btn_interest_id.isClickable = false
                    button.setImageDrawable(this@FullProfile.getDrawable(R.drawable.ic_yellow_interest))
                } else {

                    if (buttonState) {
                        //Toast.makeText(context, buttonState.toString(), Toast.LENGTH_SHORT).show()
                        val apiService = ApiInterface.create()
                        val call = apiService.sendInterestApi(
                            user_details.get(DllSessionManager.USER_ID).toString(),
                            user_id
                        )
                        Log.d("REQUEST", call.toString() + "")
                        call.enqueue(object : Callback<ContactusResponse> {
                            override fun onResponse(
                                call: Call<ContactusResponse>,
                                response: retrofit2.Response<ContactusResponse>?
                            ) {
                                //  if (response != null) {
                                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                        "success"
                                    )
                                ) {
                                    val user_data = response.body()!!.data
                                    Toast.makeText(
                                        this@FullProfile,
                                        "Interest Sent Successfully.",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    user_interest_from == user_details.get(DllSessionManager.USER_ID).toString()
                                    btn_interest_id.isEnabled = false
                                    btn_interest_id.isClickable = false
                                    btn_interest_id.setEventListener(null)

                                } else if (response!!.body()!!.status.equals("2")) {
                                    btn_interest_id.setEventListener(null)
                                    btn_interest_id.isEnabled = false
                                    btn_interest_id.isClickable = false
                                    btn_interest_id.setEventListener(null)
                                }
                                //  }
                            }

                            override fun onFailure(
                                call: Call<ContactusResponse>,
                                t: Throwable
                            ) {
                                Log.w("Result_Order_details", t.toString())
                            }
                        })


                    } else {
                        btn_interest_id.isEnabled = false
                        btn_interest_id.isClickable = false

                        val apiService = ApiInterface.create()
                        val call =
                            apiService.sendInterestApi(
                                user_details.get(DllSessionManager.USER_ID).toString(),
                                user_id
                            )
                        Log.d("REQUEST", call.toString() + "")
                        call.enqueue(object : Callback<ContactusResponse> {
                            override fun onResponse(
                                call: Call<ContactusResponse>,
                                response: retrofit2.Response<ContactusResponse>?
                            ) {

                                //  if (response != null) {
                                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                        "success"
                                    )
                                ) {

                                    val user_data = response.body()!!.data
                                    Toast.makeText(
                                        this@FullProfile,
                                        response.body()!!.result.toString(),
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    btn_interest_id.isEnabled = false
                                }

                                //  }
                            }

                            override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                                Log.w("Result_Order_details", t.toString())
                            }
                        })


                    }


                }

            }


            override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
            }

            override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
            }
        })




        btn_like_id.setEventListener(object : SparkEventListener {

            override fun onEvent(button: ImageView, buttonState: Boolean) {
                if (buttonState) {
                    // Toast.makeText(this@FullProfile,buttonState.toString(),Toast.LENGTH_SHORT).show()

                    val apiService = ApiInterface.create()
                    val call = apiService.likeApi(
                        user_details.get(DllSessionManager.USER_ID).toString(),
                        user_id
                    )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                Toast.makeText(
                                    this@FullProfile,
                                    response.body()!!.result.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                                Log.e("@10", "new" + "--" + response.body()!!.result)
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })

                } else {

                    val apiService = ApiInterface.create()
                    val call = apiService.likeApi(
                        user_details.get(DllSessionManager.USER_ID).toString(),
                        user_id
                    )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                Toast.makeText(
                                    this@FullProfile,
                                    response.body()!!.result.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                                Log.e("@11", "new" + "--" + response.body()!!.result)
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }
            }

            override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {

            }

            override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {

            }
        })

    }

    fun printHashKey(pContext: Context) {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("key_hash", "printHashKey() Hash Key: $hashKey")
            }
        } /**/ catch (e: Exception) {
            Log.e("no_hash", "printHashKey()", e)
        }
    }

    internal class CircleTransform : Transformation {

        var mCircleSeparator = false

        constructor() {}

        constructor(circleSeparator: Boolean) {
            mCircleSeparator = circleSeparator
        }

        override fun transform(source: Bitmap): Bitmap {
            val size = Math.min(source.width, source.height)
            val x = (source.width - size) / 2
            val y = (source.height - size) / 2
            val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
            if (squaredBitmap != source) {
                source.recycle()
            }
            val bitmap = Bitmap.createBitmap(size, size, source.config)
            val canvas = Canvas(bitmap)
            val shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            val paint =
                Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG or Paint.FILTER_BITMAP_FLAG)
            paint.setShader(shader)
            val r = size / 2f
            canvas.drawCircle(r, r, r - 1, paint)
            // Make the thin border:
            val paintBorder = Paint()
            paintBorder.setStyle(Paint.Style.STROKE)
            paintBorder.setColor(Color.argb(84, 0, 0, 0))
            paintBorder.setAntiAlias(true)
            paintBorder.setStrokeWidth(1F)
            canvas.drawCircle(r, r, r - 1, paintBorder)

            // Optional separator for stacking:
            if (mCircleSeparator) {
                val paintBorderSeparator = Paint()
                paintBorderSeparator.setStyle(Paint.Style.STROKE)
                paintBorderSeparator.setColor(Color.parseColor("#ffffff"))
                paintBorderSeparator.setAntiAlias(true)
                paintBorderSeparator.setStrokeWidth(4F)
                canvas.drawCircle(r, r, r + 1, paintBorderSeparator)
            }
            squaredBitmap.recycle()
            return bitmap
        }


        override fun key(): String {
            return "circle"
        }
    }

    private fun isTelephonePermissionAllowed(): Boolean {

        val result_telephone =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return result_telephone == PackageManager.PERMISSION_GRANTED

    }

    private fun requestTelephonePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CALL_PHONE
            )
        ) {

            val builder = AlertDialog.Builder(this@FullProfile)
            builder.setTitle("Alert!")
                .setMessage("App needs permissions to make calls.")
                .setCancelable(false)
                .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                        ActivityCompat.requestPermissions(
                            this@FullProfile, arrayOf(Manifest.permission.CALL_PHONE),
                            PERMISSION_CODE
                        )
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()
        } else {
            ActivityCompat.requestPermissions(
                this@FullProfile, arrayOf(Manifest.permission.CALL_PHONE),
                PERMISSION_CODE
            )
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == PERMISSION_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                    this,
                    "Permission granted now you can make calls now",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(this, "Permission denied. You cannot make calls.", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    fun memberShipAlertDialog() {

        success_dialog = Dialog(this)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        img_profile_pic_id = success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text = "Alert!"
        tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text =
            "You need an active membership plan to view the Contact Details. Proceed to membership plans?"
        iv_close = success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility = View.VISIBLE
        tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(this, MembershipPlanActivity::class.java)
            startActivity(intent)
        }

        Picasso.with(this)
            .load(photobaseurl + user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()
    }

    override fun onBackPressed() {
        if (from_screen.equals("interested_me")) {
            val home_intent = Intent(this@FullProfile, DashboardActivity::class.java)
            home_intent.putExtra("from_list", "interested_me")
            startActivity(home_intent)
        } else {
            super.onBackPressed()
        }
    }


    private fun createChat(memberUsers: List<User>) {

        val chatJid = XMPPUtils.fromUserNameToJID(memberUsers[0].getLogin())
        RoomsListManager.getInstance().createCommonChat(chatJid)

        /*if (memberUsers.isEmpty()) {
            Toast.makeText(this, getString(R.string.add_people_to_create_chat), Toast.LENGTH_LONG)
                .show()

        } else if (memberUsers.size == 1) {   // 1 to 1 chat
            val chatJid = XMPPUtils.fromUserNameToJID(memberUsers[0].getLogin())
            RoomsListManager.getInstance().createCommonChat(chatJid)
            //NavigateToChat.go(chatJid, XMPPUtils.fromJIDToUserName(chatJid), this)

        } else {    // muc or muc light
            //showRoomNameDialog(memberUsers)
        }*/

    }

    private fun addContact(user: User) {
        val progress = ProgressDialog.show(this, getString(R.string.loading), null, true)

        Tasks.executeInBackground(this, {
            RosterManager.getInstance().addContact(user)
            null
        }, object : Completion<Any> {
            override fun onSuccess(context: Context, result: Any) {
                progress?.dismiss()

                /*if (manageContactsUsersRemoveAllContactsButton != null) {
                    mContacts.add(user)
                    mContactsAdapter.notifyDataSetChanged()
                    manageContactsUsersRemoveAllContactsButton.setVisibility(View.VISIBLE)
                    mSearchUsers.clear()
                    mSearchAdapter.notifyDataSetChanged()
                }*/

                Event(Event.Type.CONTACTS_CHANGED).post()
            }

            override fun onError(context: Context, e: Exception) {
                progress?.dismiss()

                if (!Preferences.isTesting()) {
                    Toast.makeText(this@FullProfile, R.string.error, Toast.LENGTH_SHORT)
                        .show()
                }

                e.printStackTrace()
            }
        })

    }

    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall =
            getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg, user_id)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility = View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e(
                                    "IMAGEADD",
                                    PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(i).add_image
                                )

                                val textSliderView = TextSliderView(this@FullProfile);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(
                                        PhotoBaseUrl().BASE_ADS_URL + response.body()!!.data!!.get(
                                            i
                                        ).add_image
                                    )
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@FullProfile);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString(
                                        "extra",
                                        response.body()!!.data!!.get(i).add_link.toString()
                                    )
                                sliderLayout.addSlider(textSliderView)
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@FullProfile);


                        } else {
                            sliderLayout.visibility = View.GONE
                            Log.e("date_only", "No Data")
                        }
                    } else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(slider!!.bundle.get("extra").toString())
            )
        );
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }
}