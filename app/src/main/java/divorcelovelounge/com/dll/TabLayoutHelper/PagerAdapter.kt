package divorcelovelounge.com.dll.TabLayoutHelper

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import divorcelovelounge.com.dll.Chat.Activities.ChatsListsFragment
import divorcelovelounge.com.dll.Fragments.*


class PagerAdapter(fm: FragmentManager?, var from_stg: String) : FragmentStatePagerAdapter(fm) {
    var from = ""

    init {
        from = this.from_stg
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getItem(position: Int): Fragment {

        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = ProfileFragment()
            1 -> {
                fragment = FavouritesFragment()
                val bundle = Bundle()
                if (from != "notification") {
                    bundle.putString("message", "1")
                } else {
                    bundle.putString("message", "interest_me")
                }
                fragment.arguments = bundle
            }
            2 -> fragment = SecondFragment()
           // 3 -> fragment = ConversationsFragment()
            3 -> fragment = ChatsListsFragment()
            4 -> fragment = Inbox()
        }


        return fragment!!
    }

}