package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import divorcelovelounge.com.dll.Adapters.*
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.Helper.RecyclerItemClickListener
import divorcelovelounge.com.dll.Pojos.EditPrefsPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class EditMatchPreferencesActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    /* UI Components */
    private lateinit var match_prefs_ll: LinearLayout
    private lateinit var ll_prefs_main: LinearLayout
    private lateinit var ll_prefs_profile_main: LinearLayout
    private lateinit var ll_prefs_profile_header: LinearLayout
    private lateinit var ll_prefs_profile_1: LinearLayout
    private lateinit var ll_prefs_profile_2: LinearLayout
    private lateinit var ll_prefs_matches_main: LinearLayout
    private lateinit var ll_prefs_matches_header: LinearLayout
    private lateinit var ll_prefs_matches_1: LinearLayout
    private lateinit var ll_prefs_matches_2: LinearLayout
    private lateinit var rl_prefs: RelativeLayout
    private lateinit var ll_dialog_prefs: LinearLayout

    private lateinit var tv_prefs_profile_qstn_1: CustomTextView
    private lateinit var tv_prefs_profile_ans_1: CustomTextView
    private lateinit var tv_prefs_profile_qstn_2: CustomTextView
    private lateinit var tv_prefs_profile_ans_2: CustomTextView
    private lateinit var tv_prefs_matches_header_1: CustomTextView
    private lateinit var tv_prefs_matches_hint_1: CustomTextView
    private lateinit var tv_prefs_matches_selected_1: CustomTextView
    private lateinit var tv_prefs_matches_ans_1: CustomTextView
    private lateinit var tv_prefs_matches_header_2: CustomTextView
    private lateinit var tv_prefs_matches_hint_2: CustomTextView
    private lateinit var tv_prefs_matches_selected_2: CustomTextView
    private lateinit var tv_prefs_matches_ans_2: CustomTextView
    private lateinit var tv_dialog_prefs_done: CustomTextViewBold
    private lateinit var tv_city_alert_header: CustomTextView
    private lateinit var tv_country_alert_header: CustomTextView
    private lateinit var tv_state_alert_header: CustomTextView
    private lateinit var city_searchEmpty: CustomTextView
    private lateinit var country_searchEmpty: CustomTextView
    private lateinit var state_searchEmpty: CustomTextView

    private lateinit var tv_dialog_prefs_header: CustomTextViewBold
    private lateinit var tv_prefs_header: CustomTextViewBold
    private lateinit var tv_city_alert_done: CustomTextViewBold
    private lateinit var tv_country_alert_done: CustomTextViewBold
    private lateinit var tv_state_alert_done: CustomTextViewBold

    private lateinit var iv_prefs_back: ImageView
    private lateinit var iv_dialog_prefs_close: ImageView
    private lateinit var iv_city_alert_close: ImageView
    private lateinit var iv_country_alert_close: ImageView
    private lateinit var iv_state_alert_close: ImageView

    private lateinit var city_searchEditText: EditText
    private lateinit var country_searchEditText: EditText
    private lateinit var state_searchEditText: EditText

    private lateinit var recyclerView_prefs: RecyclerView
    private lateinit var city_searchListView: ListView
    private lateinit var country_searchListView: ListView
    private lateinit var state_searchListView: ListView
    private lateinit var sliderLayout: SliderLayout

    private lateinit var prefsLayoutManager: LinearLayoutManager

    private lateinit var editPrefsAdapter: EditPrefsAdapter

    var recycle_images: IntArray? = null
    var recycle_name: Array<String>? = null
    var recycle_items: ArrayList<EditPrefsPojo>? = null

    private val logTAG = "EditMatchPreferences"
    private val DEFAULT_POSITION = 0

    private lateinit var sessionManager: DllSessionManager
    private lateinit var userId_session: String

    private var user_zipCode_session = ""
    private var user_state_session = ""
    private var user_city_session = ""
    private var user_smoke_session = ""
    private var user_drink_session = ""
    private var user_ethnicity_session = ""
    private var user_age = ""
    private var user_dob_session = ""
    private var user_religion_session = ""
    private var user_mTongue_session = ""
    private var user_education_session = ""
    private var user_occupation_session = ""
    private var partner_distance_session = ""
    private var partner_search_session = ""
    private var partner_smoke_session = ""
    private var partner_drink_session = ""
    private var partner_ethnicity_session = ""
    private var partner_age_min_session = ""
    private var partner_age_max_session = ""
    private var partner_religion_session = ""
    private var partner_mTongue_session = ""
    private var partner_education_session = ""
    private var partner_occupation_session = ""

    private var min_age = ""
    private var max_age = ""
    private val MIN_AGE = 18
    private val MIN_AGE_mx = 24
    private val MAX_AGE = 99
    private val AGE_DIFF = 6
    private var provider = ""

    private lateinit var mDialog: Dialog
    private lateinit var progressBar: Dialog
    //    private lateinit var builder: AlertDialog.Builder
    private lateinit var city_search_dialog: Dialog
    private lateinit var country_search_dialog: Dialog
    private lateinit var state_search_dialog: Dialog


    private lateinit var selectedSmoking: ArrayList<String>
    private lateinit var selectedDrinking: ArrayList<String>
    private lateinit var selectedEthnicity: ArrayList<String>
    private lateinit var selectedReligion: ArrayList<String>
    private lateinit var selectedmTongue: ArrayList<String>
    private lateinit var selectedEducation: ArrayList<String>
    private lateinit var selectedOccupation: ArrayList<String>
    private lateinit var min_age_array: ArrayList<String>
    private lateinit var max_age_array: ArrayList<String>
    private lateinit var citiesList: ArrayList<String>
    private lateinit var religionsList: ArrayList<String>
    private lateinit var religionsList1: ArrayList<String>
    private lateinit var countriesList: ArrayList<String>
    private lateinit var statesNamesList: ArrayList<String>
    private lateinit var countriesDataList: ArrayList<CountriesDataResponse>
    private lateinit var citiesDataList: ArrayList<CitiesDataResponse>
    private lateinit var ethnicityList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var educationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var occupationList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var incomeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var smokeList: ArrayList<RegisterProfileSetupAnswer>
    private lateinit var drinkList: ArrayList<RegisterProfileSetupAnswer>
    private var questionsList: ArrayList<RegisterProfileSetupData>? = null
    private lateinit var motherTongueDataList: ArrayList<MotherTongueData>
    private var religionDataList: ArrayList<ReligionData>? = null
    private var religionDataListtest: ArrayList<String>? = null
    private var religionDataList1: ArrayList<ReligionData>? = null

    private lateinit var min_age_adapter: ArrayAdapter<String>
    private lateinit var max_age_adapter: ArrayAdapter<String>
    private lateinit var city_SearchAdapter: LocationSearchAdapter
    private lateinit var country_SearchAdapter: LocationSearchAdapter
    private lateinit var state_SearchAdapter: LocationSearchAdapter

    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""
    @SuppressLint("WrongConstant", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_match_preferences)

        sessionManager = DllSessionManager(this@EditMatchPreferencesActivity)
        if (!sessionManager.userDetails[DllSessionManager.USER_ID].equals("")) {
            userId_session = sessionManager.userDetails[DllSessionManager.USER_ID]!!
            user_zipCode_session = sessionManager.userDetails[DllSessionManager.USER_ZIP]!!
            user_state_session = sessionManager.userDetails[DllSessionManager.USER_STATE]!!
            user_city_session = sessionManager.userDetails[DllSessionManager.USER_CITY]!!
            user_smoke_session = sessionManager.userDetails[DllSessionManager.USER_SMOKE]!!
            user_drink_session = sessionManager.userDetails[DllSessionManager.USER_DRINK]!!
            user_ethnicity_session = sessionManager.userDetails[DllSessionManager.USER_ETHNICITY]!!
            user_dob_session = sessionManager.userDetails[DllSessionManager.USER_DOB]!!
            user_religion_session = sessionManager.userDetails[DllSessionManager.USER_RELIGION]!!
            user_mTongue_session =
                sessionManager.userDetails[DllSessionManager.USER_MOTHER_TONGUE]!!
            user_education_session = sessionManager.userDetails[DllSessionManager.USER_EDUCATION]!!
            user_occupation_session =
                sessionManager.userDetails[DllSessionManager.USER_OCCUPATION]!!
            partner_distance_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!
            partner_search_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!
            partner_smoke_session = sessionManager.userDetails[DllSessionManager.PARTNER_smoke]!!
            partner_drink_session = sessionManager.userDetails[DllSessionManager.PARTNER_drink]!!
            partner_ethnicity_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_ethnicity]!!
            partner_age_min_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MIN]!!
            partner_age_max_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MAX]!!
            partner_religion_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_religion]!!
            partner_mTongue_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_MOTHER_TONGUE]!!
            partner_education_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_education]!!
            partner_occupation_session =
                sessionManager.userDetails[DllSessionManager.PARTNER_occupation]!!
            provider = sessionManager.userDetails[DllSessionManager.USER_PROVIDER]!!
        }

        initialize()
        progressBar.show()

        getCountries()
        getRegisterApi()
        getReligionApi()
        getMotherTongueApi()

        location_details_list = sessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Profile")


        if (user_dob_session != "0") {

            user_age = getAge(user_dob_session).toString()

        } else {
            user_age = "0"
        }

        val state_codes = resources.getStringArray(R.array.state_codes)
        val state_names = resources.getStringArray(R.array.state_names)



        for (i in 0 until state_names.size) {
            statesNamesList.add(state_names[i])
        }

        state_SearchAdapter =
            LocationSearchAdapter(this@EditMatchPreferencesActivity, statesNamesList)

        iv_prefs_back.setOnClickListener {
            onBackPressed()
        }

        iv_dialog_prefs_close.setOnClickListener {
            mDialog.dismiss()
        }


        val recyclerViewLayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        recyclerViewLayoutParams.setMargins(
            convertToDp(0),
            convertToDp(0),
            convertToDp(0),
            convertToDp(0)
        )

        val editTextLayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        editTextLayoutParams.setMargins(
            convertToDp(5),
            convertToDp(5),
            convertToDp(5),
            convertToDp(5)
        )

        val paddingTop = convertToDp(8)
        val paddingBottom = convertToDp(8)
        val paddingLeft = convertToDp(15)
        val paddingRight = convertToDp(15)

        // default position & data
        editPrefsAdapter.setSelectedIndex(DEFAULT_POSITION)
        if (editPrefsAdapter.getSelectedIndex() == DEFAULT_POSITION) {

            tv_dialog_prefs_header.visibility = View.VISIBLE

            tv_prefs_profile_qstn_1.text = resources.getString(R.string.zip_code)
            tv_prefs_profile_ans_1.text = sessionManager.userDetails[DllSessionManager.USER_ZIP]!!
            tv_prefs_profile_qstn_2.text = resources.getString(R.string.state)
            tv_prefs_profile_ans_2.text =
                sessionManager.userDetails[DllSessionManager.USER_CITY]!! + ", " +
                        sessionManager.userDetails[DllSessionManager.USER_STATE]!!

            tv_prefs_matches_header_1.text = resources.getString(R.string.match_distance)
            tv_prefs_matches_hint_1.text =
                resources.getString(R.string.how_far_should_we_search_for_your_matches)
            tv_prefs_matches_selected_1.text =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!
            tv_prefs_matches_ans_1.text =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!

            tv_prefs_matches_header_2.text = getString(R.string.match_distance_importance)
            tv_prefs_matches_hint_2.text =
                resources.getString(R.string.how_important_is_the_distance_of_your_match)
            tv_prefs_matches_selected_2.text =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!
            tv_prefs_matches_ans_2.text =
                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!

            tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.ic_arrow_drop_down, 0
            )
            tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                0, 0, R.drawable.ic_arrow_drop_down, 0
            )

            tv_prefs_profile_ans_1.setOnClickListener {
                mDialog.show()
                ll_dialog_prefs.removeAllViews()
                tv_dialog_prefs_header.text = getString(R.string.zip_code)
                val et_zip_code = EditText(mDialog.context)
                et_zip_code.setBackgroundResource(R.drawable.edittext_bg)

                et_zip_code.layoutParams = editTextLayoutParams
                et_zip_code.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)
                et_zip_code.hint = "Zip Code"
                et_zip_code.imeOptions = EditorInfo.IME_ACTION_DONE
                et_zip_code.inputType = InputType.TYPE_CLASS_NUMBER
                et_zip_code.setText(sessionManager.userDetails[DllSessionManager.USER_ZIP]!!)

                ll_dialog_prefs.addView(et_zip_code)

                tv_dialog_prefs_done.setOnClickListener {
                    val zip_code_update = et_zip_code.text.toString().trim()
                    updatePreferences(
                        userId_session,
                        zip_code_update,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                    mDialog.dismiss()
                }
            }

            tv_prefs_profile_ans_2.setOnClickListener {
                mDialog.show()
                ll_dialog_prefs.removeAllViews()
                tv_dialog_prefs_header.text = resources.getString(R.string.state_city)
                var state = ""
                var state_code = ""
                var city = ""
                var country = ""


                val ll_et_country = LinearLayout(mDialog.context)
                val tv_country = CustomTextView(mDialog.context)
                val et_country = EditText(mDialog.context)
                val ll_et_state = LinearLayout(mDialog.context)
                val tv_state = CustomTextView(mDialog.context)
                val et_state = EditText(mDialog.context)
                val ll_et_city = LinearLayout(mDialog.context)
                val tv_city = CustomTextView(mDialog.context)
                val et_city = EditText(mDialog.context)

                et_state.setText(sessionManager.userDetails[DllSessionManager.USER_STATE]!!)
                et_city.setText(sessionManager.userDetails[DllSessionManager.USER_CITY]!!)
                et_country.setText(sessionManager.userDetails[DllSessionManager.USER_COUNTRY]!!)

                val linearLayoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                linearLayoutParams.setMargins(
                    convertToDp(3), convertToDp(3), convertToDp(3), convertToDp(3)
                )

/* Alert dialog for country search - Start */

                country_search_dialog = Dialog(this@EditMatchPreferencesActivity)
                country_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                country_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                country_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                val filter_window_country = country_search_dialog.window
                filter_window_country!!.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                tv_country_alert_done = country_search_dialog.findViewById(R.id.tv_alert_done)
                iv_country_alert_close = country_search_dialog.findViewById(R.id.iv_alert_close)
                tv_country_alert_header = country_search_dialog.findViewById(R.id.tv_alert_header)

                country_searchEditText = country_search_dialog.findViewById(R.id.searchEditText)
                country_searchListView = country_search_dialog.findViewById(R.id.searchListView)
                country_searchEmpty = country_search_dialog.findViewById(R.id.searchEmpty)

                tv_country_alert_header.text = "Select your Country"
                country_searchEditText.hint = "Select your Country"

                country_searchListView.adapter = country_SearchAdapter

                if (sessionManager.userDetails[DllSessionManager.USER_COUNTRY] != "0") {
                    val pos =
                        countriesList.indexOf(sessionManager.userDetails[DllSessionManager.USER_COUNTRY])
                    country_SearchAdapter.setSelectedIndex(pos)
                    country = countriesList[pos]
                }


                country_searchEditText.addTextChangedListener(object : TextWatcher {
                    override fun onTextChanged(
                        char: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (char.isNotEmpty()) {
                            country_SearchAdapter.filter.filter(char.toString())
                        } else {
                            country_SearchAdapter.filter.filter("")
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                country_searchListView.emptyView = country_searchEmpty
                country_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                    try {
                        country = country_SearchAdapter.getItem(position).toString()
                        country_SearchAdapter.setSelectedIndex(position)
                        Log.d("SecondFragment", "$country ")
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }

                }

                tv_country_alert_done.setOnClickListener {
                    et_country.setText(country)
                    if (country == "USA") {
                        et_state.isFocusable = false
                        et_city.isFocusable = false
                        et_state.hint = "Select your State"
                        et_city.hint = "Select your City"
                    } else {
                        et_state.hint = "Enter your State"
                        et_city.hint = "Enter your City"
                        et_state.setText("")
                        et_city.setText("")
                        et_state.isFocusable = true
                        et_city.isFocusable = true
                        et_state.isClickable = false
                        et_city.isClickable = false
                        et_state.isFocusableInTouchMode = true
                        et_city.isFocusableInTouchMode = true
                    }
                    country_search_dialog.dismiss()
                }

                iv_country_alert_close.setOnClickListener {
                    country_search_dialog.dismiss()
                }

/* Alert dialog for country search - End */

/* Alert dialog for state search - Start */

                state_search_dialog = Dialog(this@EditMatchPreferencesActivity)
                state_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                state_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                state_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                val filter_window_state = state_search_dialog.window
                filter_window_state!!.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                tv_state_alert_done = state_search_dialog.findViewById(R.id.tv_alert_done)
                iv_state_alert_close = state_search_dialog.findViewById(R.id.iv_alert_close)
                tv_state_alert_header = state_search_dialog.findViewById(R.id.tv_alert_header)

                state_searchEditText = state_search_dialog.findViewById(R.id.searchEditText)
                state_searchListView = state_search_dialog.findViewById(R.id.searchListView)
                state_searchEmpty = state_search_dialog.findViewById(R.id.searchEmpty)

                tv_state_alert_header.text = "Select your State"
                state_searchEditText.hint = "Select your State"

                state_searchListView.adapter = state_SearchAdapter

                state_searchEditText.addTextChangedListener(object : TextWatcher {
                    override fun onTextChanged(
                        char: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (char.isNotEmpty()) {
                            state_SearchAdapter.filter.filter(char.toString())
                        } else {
                            state_SearchAdapter.filter.filter("")
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                state_searchListView.emptyView = state_searchEmpty
                state_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                    try {
                        state = state_SearchAdapter.getItem(position).toString()
                        state_SearchAdapter.setSelectedIndex(position)
                        state_code = state_codes[position].toString()
                        Log.d("SecondFragment", "$state $state_code")
                       // et_city.setText("")
                        getCities(state_code)
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }

                }

                tv_state_alert_done.setOnClickListener {
                    et_state.setText(state)
                    state_search_dialog.dismiss()
                }

                iv_state_alert_close.setOnClickListener {
                    state_search_dialog.dismiss()
                }

/* Alert dialog for state search - End */

/* Alert dialog for city search - Start */

                city_search_dialog = Dialog(this@EditMatchPreferencesActivity)
                city_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                city_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                city_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                val filter_window = city_search_dialog.window
                filter_window!!.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                tv_city_alert_done = city_search_dialog.findViewById(R.id.tv_alert_done)
                iv_city_alert_close = city_search_dialog.findViewById(R.id.iv_alert_close)
                tv_city_alert_header = city_search_dialog.findViewById(R.id.tv_alert_header)

                city_searchEditText = city_search_dialog.findViewById(R.id.searchEditText)
                city_searchListView = city_search_dialog.findViewById(R.id.searchListView)
                city_searchEmpty = city_search_dialog.findViewById(R.id.searchEmpty)

                tv_city_alert_header.text = "Select your City"
                city_searchEditText.hint = "Select your City"

                getCities(user_state_session)
                try {
                    if(user_state_session!=""||user_state_session!=null||user_state_session!="0"){
                        for (i in 0 until state_codes.size){
                            if(user_state_session.equals(state_names.get(i))){
                                getCities(state_codes.get(i))
                            }
                        }


                    }
                }catch (e:Throwable){

                }


                city_searchListView.adapter = city_SearchAdapter

                city_searchEditText.addTextChangedListener(object : TextWatcher {
                    override fun onTextChanged(
                        char: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (char.isNotEmpty()) {
                            city_SearchAdapter.filter.filter(char.toString())
                        } else {
                            city_SearchAdapter.filter.filter("")
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                city_searchListView.emptyView = city_searchEmpty
                city_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                    try {
                        city = city_SearchAdapter.getItem(position).toString()
                        city_SearchAdapter.setSelectedIndex(position)
                        Log.d("SecondFragment", "$city ")
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }

                }

                tv_city_alert_done.setOnClickListener {
                    et_city.setText(city)
                    city_search_dialog.dismiss()
                }

                iv_city_alert_close.setOnClickListener {
                    city_search_dialog.dismiss()
                }

/* Alert dialog for city search - End */

                val etLayoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                val tvLayoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                etLayoutParams.setMargins(0, 0, 0, convertToDp(3))
                tvLayoutParams.setMargins(convertToDp(8), 0, 0, 0)

                ll_et_city.layoutParams = etLayoutParams
                ll_et_country.layoutParams = etLayoutParams
                ll_et_state.layoutParams = etLayoutParams

                tv_country.layoutParams = tvLayoutParams
                tv_state.layoutParams = tvLayoutParams
                tv_city.layoutParams = tvLayoutParams

                tv_country.text = "Country"
                tv_state.text = "State"
                tv_city.text = "City"

                ll_et_country.orientation = LinearLayoutManager.VERTICAL
                ll_et_state.orientation = LinearLayoutManager.VERTICAL
                ll_et_city.orientation = LinearLayoutManager.VERTICAL

                et_country.setBackgroundResource(R.drawable.edittext_bg)
                et_country.layoutParams = editTextLayoutParams
                et_country.isFocusable = false
                et_country.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)
                et_country.hint = "Select Country"
                et_country.setOnClickListener {
                    country_search_dialog.show()
                }

                et_state.setBackgroundResource(R.drawable.edittext_bg)
                et_state.layoutParams = editTextLayoutParams
                //et_state.isFocusable = false
                et_state.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)
                et_state.hint = "Select State"

                et_state.setOnClickListener {
                    Log.d("STATEIF", "country : $country")
                    if (country == "USA") {
                        state_search_dialog.show()
                        et_state.isFocusable = false
                        et_city.isFocusable = false
                        et_state.isClickable = false
                        et_state.isFocusableInTouchMode = true
                        et_city.isFocusableInTouchMode = true
                    } else {
                        Log.d("STATEELSE", "country : $country")
                        state_search_dialog.dismiss()
                        et_state.isFocusable = true
                        et_state.isClickable = true
                        et_city.isFocusable = true
                        et_city.isClickable = true
                    }
                    // state_search_dialog.show()
                }

                et_city.setBackgroundResource(R.drawable.edittext_bg)
                et_city.layoutParams = editTextLayoutParams
                // et_city.isFocusable = false
                et_city.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)
                et_city.hint = "Select City"

                et_city.setOnClickListener {
                    Log.d("CITYIF", "country : $country")
                    if (country == "USA") {
                        city_search_dialog.show()
                        et_state.isFocusable = false
                        et_city.isFocusable = false
                        et_city.isClickable = false
                        et_state.isFocusableInTouchMode = true
                        et_city.isFocusableInTouchMode = true
                    } else {
                        Log.d("STATEELSE", "country : $country")
                        state_search_dialog.dismiss()
                        city_search_dialog.dismiss()
                        et_state.isFocusable = true
                        et_state.isClickable = true
                        et_city.isFocusable = true
                        et_city.isClickable = true
                    }
                }

                tv_country.text = "Country"
                tv_state.text = "State"
                tv_city.text = "City"

                ll_et_country.addView(tv_country)
                ll_et_country.addView(et_country)
                ll_et_state.addView(tv_state)
                ll_et_state.addView(et_state)
                ll_et_city.addView(tv_city)
                ll_et_city.addView(et_city)

                ll_dialog_prefs.addView(ll_et_country)
                ll_dialog_prefs.addView(ll_et_state)
                ll_dialog_prefs.addView(ll_et_city)

                tv_dialog_prefs_done.setOnClickListener {
                    val city_update = et_city.text.toString().trim()
                    var state_update = ""
                    if (country == "USA") {
                        state_update = state_code
                    } else {
                        state_update = et_state.text.toString()
                    }
                    updatePreferences(
                        userId_session,
                        "",
                        state_update,
                        city_update,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        country,
                        "",
                        "",
                        ""
                    )
                    mDialog.dismiss()
                }

            }

            tv_prefs_matches_ans_1.setOnClickListener {
                mDialog.show()
                ll_dialog_prefs.removeAllViews()
                var data = ""
                tv_dialog_prefs_header.text = resources.getString(R.string.match_distance)
                val distanceAdapter = EditPrefsDistanceAdapter(
                    this@EditMatchPreferencesActivity,
                    resources.getStringArray(R.array.search_distance)
                )
                val recyclerView = RecyclerView(mDialog.context)
                recyclerView.setHasFixedSize(true)
                recyclerView.layoutParams = recyclerViewLayoutParams
                recyclerView.layoutManager =
                    LinearLayoutManager(mDialog.context, LinearLayout.VERTICAL, false)
                recyclerView.adapter = distanceAdapter

                recyclerView.addOnItemTouchListener(
                    RecyclerItemClickListener(this@EditMatchPreferencesActivity, recyclerView,
                        object : RecyclerItemClickListener.OnItemClickListener {

                            override fun onItemClick(view: View, position: Int) {

                                data = distanceAdapter.getItem(position)
                                distanceAdapter.setSelectedIndex(position)

                                Log.d(logTAG, "selected distance $data")
                            }
                        })
                )

                ll_dialog_prefs.addView(recyclerView)
                tv_dialog_prefs_done.setOnClickListener {
                    val partner_distance_update = data
                    updatePreferences(
                        userId_session,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        partner_distance_update,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    )
                    mDialog.dismiss()
                }
            }

            tv_prefs_matches_ans_2.setOnClickListener {
                mDialog.show()
                ll_dialog_prefs.removeAllViews()
                var data = ""
                tv_dialog_prefs_header.text =
                    resources.getString(R.string.match_distance_importance)
                val distanceAdapter = EditPrefsDistanceAdapter(
                    this@EditMatchPreferencesActivity, resources.getStringArray(R.array.search_imp)
                )
                val recyclerView = RecyclerView(mDialog.context)
                recyclerView.setHasFixedSize(true)
                recyclerView.layoutParams = recyclerViewLayoutParams
                recyclerView.layoutManager =
                    LinearLayoutManager(mDialog.context, LinearLayout.VERTICAL, false)
                recyclerView.adapter = distanceAdapter

                recyclerView.addOnItemTouchListener(
                    RecyclerItemClickListener(this@EditMatchPreferencesActivity, recyclerView,
                        object : RecyclerItemClickListener.OnItemClickListener {

                            override fun onItemClick(view: View, position: Int) {

                                data = distanceAdapter.getItem(position)
                                distanceAdapter.setSelectedIndex(position)

                                Log.d(logTAG, "selected importance $data")
                            }
                        })
                )

                ll_dialog_prefs.addView(recyclerView)
                tv_dialog_prefs_done.setOnClickListener {
                    val partner_search_update = data
                    updatePreferences(
                        userId_session,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        partner_search_update,
                        "",
                        "",
                        "",
                        ""
                    )
                    mDialog.dismiss()
                }
            }

        }


        recyclerView_prefs.addOnItemTouchListener(
            RecyclerItemClickListener(
                this@EditMatchPreferencesActivity, recyclerView_prefs,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val item = editPrefsAdapter.getItem(position)
                        editPrefsAdapter.setSelectedIndex(position)
                        when (position) {
                            0 -> {

                                tv_prefs_profile_qstn_2.visibility = View.VISIBLE
                                tv_prefs_profile_ans_2.visibility = View.VISIBLE

                                tv_dialog_prefs_header.visibility = View.VISIBLE

                                tv_prefs_matches_header_2.visibility = View.VISIBLE
                                tv_prefs_matches_hint_2.visibility = View.VISIBLE
                                tv_prefs_matches_selected_2.visibility = View.VISIBLE
                                tv_prefs_matches_ans_2.visibility = View.VISIBLE

                                tv_prefs_profile_qstn_1.text =
                                    resources.getString(R.string.zip_code)
                                tv_prefs_profile_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.USER_ZIP]!!
                                tv_prefs_profile_qstn_2.text = resources.getString(R.string.state)
                                tv_prefs_profile_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.USER_CITY]!! + ", " +
                                            sessionManager.userDetails[DllSessionManager.USER_STATE]!!

                                tv_prefs_matches_header_1.text =
                                    resources.getString(R.string.match_distance)
                                tv_prefs_matches_hint_1.text =
                                    resources.getString(R.string.how_far_should_we_search_for_your_matches)
                                tv_prefs_matches_selected_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!
                                tv_prefs_matches_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!

                                tv_prefs_matches_header_2.text =
                                    getString(R.string.match_distance_importance)
                                tv_prefs_matches_hint_2.text =
                                    resources.getString(R.string.how_important_is_the_distance_of_your_match)
                                tv_prefs_matches_selected_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!
                                tv_prefs_matches_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!

                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0,
                                    0,
                                    R.drawable.ic_arrow_drop_down,
                                    0
                                )

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    tv_dialog_prefs_header.text = getString(R.string.zip_code)
                                    val et_zip_code = EditText(mDialog.context)
                                    et_zip_code.setBackgroundResource(R.drawable.edittext_bg)

                                    et_zip_code.layoutParams = editTextLayoutParams
                                    et_zip_code.setPadding(
                                        paddingLeft,
                                        paddingTop,
                                        paddingRight,
                                        paddingBottom
                                    )
                                    et_zip_code.hint = "Zip Code"
                                    et_zip_code.imeOptions = EditorInfo.IME_ACTION_DONE
                                    et_zip_code.inputType = InputType.TYPE_CLASS_NUMBER
                                    et_zip_code.setText(sessionManager.userDetails[DllSessionManager.USER_ZIP]!!)

                                    ll_dialog_prefs.addView(et_zip_code)

                                    tv_dialog_prefs_done.setOnClickListener {
                                        val zip_code_update = et_zip_code.text.toString().trim()
                                        updatePreferences(
                                            userId_session,
                                            zip_code_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_profile_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.state_city)
                                    var state = ""
                                    var state_code = ""
                                    var country = ""
                                    var city = ""

                                    val ll_et_country = LinearLayout(mDialog.context)
                                    val tv_country = CustomTextView(mDialog.context)
                                    val et_country = EditText(mDialog.context)
                                    val ll_et_state = LinearLayout(mDialog.context)
                                    val tv_state = CustomTextView(mDialog.context)
                                    val et_state = EditText(mDialog.context)
                                    val ll_et_city = LinearLayout(mDialog.context)
                                    val tv_city = CustomTextView(mDialog.context)
                                    val et_city = EditText(mDialog.context)

                                    et_state.setText(sessionManager.userDetails[DllSessionManager.USER_STATE]!!)
                                    et_city.setText(sessionManager.userDetails[DllSessionManager.USER_CITY]!!)
                                    et_country.setText(sessionManager.userDetails[DllSessionManager.USER_COUNTRY]!!)

                                    val linearLayoutParams =
                                        LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT
                                        )
                                    linearLayoutParams.setMargins(
                                        convertToDp(3),
                                        convertToDp(3),
                                        convertToDp(3),
                                        convertToDp(3)
                                    )

/* Alert dialog for country search - Start */

                                    country_search_dialog =
                                        Dialog(this@EditMatchPreferencesActivity)
                                    country_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    country_search_dialog.window!!.setBackgroundDrawableResource(
                                        android.R.color.transparent
                                    )

                                    country_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                                    val filter_window_country = country_search_dialog.window
                                    filter_window_country!!.setLayout(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                    )

                                    tv_country_alert_done =
                                        country_search_dialog.findViewById(R.id.tv_alert_done)
                                    iv_country_alert_close =
                                        country_search_dialog.findViewById(R.id.iv_alert_close)
                                    tv_country_alert_header =
                                        country_search_dialog.findViewById(R.id.tv_alert_header)

                                    country_searchEditText =
                                        country_search_dialog.findViewById(R.id.searchEditText)
                                    country_searchListView =
                                        country_search_dialog.findViewById(R.id.searchListView)
                                    country_searchEmpty =
                                        country_search_dialog.findViewById(R.id.searchEmpty)

                                    tv_country_alert_header.text = "Select your Country"
                                    country_searchEditText.hint = "Select your Country"

                                    country_searchListView.adapter = country_SearchAdapter

                                    val pos =
                                        countriesList.indexOf(sessionManager.userDetails[DllSessionManager.USER_COUNTRY])
                                    country_SearchAdapter.setSelectedIndex(pos)
                                    country = countriesList[pos]

                                    country_searchEditText.addTextChangedListener(object :
                                        TextWatcher {
                                        override fun onTextChanged(
                                            char: CharSequence,
                                            start: Int,
                                            before: Int,
                                            count: Int
                                        ) {
                                            if (char.isNotEmpty()) {
                                                country_SearchAdapter.filter.filter(char.toString())
                                            } else {
                                                country_SearchAdapter.filter.filter("")
                                            }
                                        }

                                        override fun beforeTextChanged(
                                            s: CharSequence,
                                            start: Int,
                                            count: Int,
                                            after: Int
                                        ) {
                                        }

                                        override fun afterTextChanged(s: Editable) {
                                        }
                                    })

                                    country_searchListView.emptyView = country_searchEmpty
                                    country_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                                        try {
                                            country =
                                                country_SearchAdapter.getItem(position).toString()
                                            country_SearchAdapter.setSelectedIndex(position)
                                            Log.d("SecondFragment", "$country ")
                                        } catch (exception: Exception) {
                                            exception.printStackTrace()
                                        }

                                    }

                                    tv_country_alert_done.setOnClickListener {
                                        et_country.setText(country)
                                        if (country == "USA") {
                                            et_state.isFocusable = false
                                            et_city.isFocusable = false
                                            et_state.hint = "Select your State"
                                            et_city.hint = "Select your City"
                                        } else {
                                            et_state.hint = "Enter your State"
                                            et_city.hint = "Enter your City"
                                            et_state.setText("")
                                            et_city.setText("")
                                            et_state.isFocusable = true
                                            et_city.isFocusable = true
                                            et_state.isClickable = false
                                            et_city.isClickable = false
                                            et_state.isFocusableInTouchMode = true
                                            et_city.isFocusableInTouchMode = true
                                        }
                                        country_search_dialog.dismiss()
                                    }

                                    iv_country_alert_close.setOnClickListener {
                                        country_search_dialog.dismiss()
                                    }

/* Alert dialog for country search - End */

/* Alert dialog for state search - Start */

                                    state_search_dialog = Dialog(this@EditMatchPreferencesActivity)
                                    state_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    state_search_dialog.window!!.setBackgroundDrawableResource(
                                        android.R.color.transparent
                                    )

                                    state_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                                    val filter_window_state = state_search_dialog.window
                                    filter_window_state!!.setLayout(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                    )

                                    tv_state_alert_done =
                                        state_search_dialog.findViewById(R.id.tv_alert_done)
                                    iv_state_alert_close =
                                        state_search_dialog.findViewById(R.id.iv_alert_close)
                                    tv_state_alert_header =
                                        state_search_dialog.findViewById(R.id.tv_alert_header)

                                    state_searchEditText =
                                        state_search_dialog.findViewById(R.id.searchEditText)
                                    state_searchListView =
                                        state_search_dialog.findViewById(R.id.searchListView)
                                    state_searchEmpty =
                                        state_search_dialog.findViewById(R.id.searchEmpty)

                                    tv_state_alert_header.text = "Select your State"
                                    state_searchEditText.hint = "Select your State"

                                    state_searchListView.adapter = state_SearchAdapter

                                    state_searchEditText.addTextChangedListener(object :
                                        TextWatcher {
                                        override fun onTextChanged(
                                            char: CharSequence,
                                            start: Int,
                                            before: Int,
                                            count: Int
                                        ) {
                                            if (char.isNotEmpty()) {
                                                state_SearchAdapter.filter.filter(char.toString())
                                            } else {
                                                state_SearchAdapter.filter.filter("")
                                            }
                                        }

                                        override fun beforeTextChanged(
                                            s: CharSequence,
                                            start: Int,
                                            count: Int,
                                            after: Int
                                        ) {
                                        }

                                        override fun afterTextChanged(s: Editable) {
                                        }
                                    })

                                    state_searchListView.emptyView = state_searchEmpty
                                    state_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                                        try {
                                            state = state_SearchAdapter.getItem(position).toString()
                                            state_SearchAdapter.setSelectedIndex(position)
                                            state_code = state_codes[position].toString()
                                            Log.d("SecondFragment", "$state $state_code")
                                        } catch (exception: Exception) {
                                            exception.printStackTrace()
                                        }

                                    }

                                    tv_state_alert_done.setOnClickListener {
                                        et_state.setText(state)
                                        state_search_dialog.dismiss()
                                    }

                                    iv_state_alert_close.setOnClickListener {
                                        state_search_dialog.dismiss()
                                    }

/* Alert dialog for state search - End */

/* Alert dialog for city search - Start */
                                    city_search_dialog = Dialog(this@EditMatchPreferencesActivity)
                                    city_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    city_search_dialog.window!!.setBackgroundDrawableResource(
                                        android.R.color.transparent
                                    )

                                    city_search_dialog.setContentView(R.layout.alert_dialog_location_search)
                                    val filter_window = city_search_dialog.window
                                    filter_window!!.setLayout(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                    )

                                    tv_city_alert_done =
                                        city_search_dialog.findViewById(R.id.tv_alert_done)
                                    iv_city_alert_close =
                                        city_search_dialog.findViewById(R.id.iv_alert_close)
                                    tv_city_alert_header =
                                        city_search_dialog.findViewById(R.id.tv_alert_header)

                                    city_searchEditText =
                                        city_search_dialog.findViewById(R.id.searchEditText)
                                    city_searchListView =
                                        city_search_dialog.findViewById(R.id.searchListView)
                                    city_searchEmpty =
                                        city_search_dialog.findViewById(R.id.searchEmpty)

                                    tv_city_alert_header.text = "Select your City"
                                    city_searchEditText.hint = "Select your City"

                                    city_searchListView.adapter = city_SearchAdapter

                                    city_searchEditText.addTextChangedListener(object :
                                        TextWatcher {
                                        override fun onTextChanged(
                                            char: CharSequence,
                                            start: Int,
                                            before: Int,
                                            count: Int
                                        ) {
                                            if (char.isNotEmpty()) {
                                                city_SearchAdapter.filter.filter(char.toString())
                                            } else {
                                                city_SearchAdapter.filter.filter("")
                                            }
                                        }

                                        override fun beforeTextChanged(
                                            s: CharSequence,
                                            start: Int,
                                            count: Int,
                                            after: Int
                                        ) {
                                        }

                                        override fun afterTextChanged(s: Editable) {
                                        }
                                    })

                                    city_searchListView.emptyView = city_searchEmpty
                                    city_searchListView.setOnItemClickListener { adapterView, view, position, l ->

                                        try {
                                            city = city_SearchAdapter.getItem(position).toString()
                                            city_SearchAdapter.setSelectedIndex(position)
                                            Log.d("SecondFragment", "$city ")
                                        } catch (exception: Exception) {
                                            exception.printStackTrace()
                                        }

                                    }

                                    tv_city_alert_done.setOnClickListener {
                                        et_city.setText(city)

                                        city_search_dialog.dismiss()
                                    }

                                    iv_city_alert_close.setOnClickListener {
                                        city_search_dialog.dismiss()
                                    }
/* Alert dialog for city search - End */

                                    val etLayoutParams =
                                        LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT
                                        )

                                    val tvLayoutParams =
                                        LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT
                                        )

                                    etLayoutParams.setMargins(0, 0, 0, convertToDp(3))
                                    tvLayoutParams.setMargins(convertToDp(8), 0, 0, 0)

                                    ll_et_city.layoutParams = etLayoutParams
                                    ll_et_country.layoutParams = etLayoutParams
                                    ll_et_state.layoutParams = etLayoutParams

                                    tv_country.layoutParams = tvLayoutParams
                                    tv_state.layoutParams = tvLayoutParams
                                    tv_city.layoutParams = tvLayoutParams

                                    tv_country.text = "Country"
                                    tv_state.text = "State"
                                    tv_city.text = "City"

                                    ll_et_country.orientation = LinearLayoutManager.VERTICAL
                                    ll_et_state.orientation = LinearLayoutManager.VERTICAL
                                    ll_et_city.orientation = LinearLayoutManager.VERTICAL

                                    et_country.setBackgroundResource(R.drawable.edittext_bg)
                                    et_country.layoutParams = editTextLayoutParams
                                    et_country.isFocusable = false
                                    et_country.setPadding(
                                        paddingLeft,
                                        paddingTop,
                                        paddingRight,
                                        paddingBottom
                                    )
                                    et_country.hint = "Select Country"
                                    et_country.setOnClickListener {
                                        country_search_dialog.show()
                                    }

                                    et_state.setBackgroundResource(R.drawable.edittext_bg)
                                    et_state.layoutParams = editTextLayoutParams
                                    // et_state.isFocusable = false
                                    et_state.setPadding(
                                        paddingLeft,
                                        paddingTop,
                                        paddingRight,
                                        paddingBottom
                                    )
                                    et_state.hint = "Select State"
                                    et_state.setOnClickListener {

                                        Log.d("STATEIF", "country : $country")
                                        if (country == "USA") {
                                            state_search_dialog.show()
                                            et_state.isFocusable = false
                                            et_city.isFocusable = false
                                            et_state.isFocusableInTouchMode = true
                                            et_city.isFocusableInTouchMode = true
                                        } else {
                                            Log.d("STATEELSE", "country : $country")
                                            state_search_dialog.dismiss()
                                            et_state.isFocusable = true
                                            et_state.isClickable = true
                                            et_city.isFocusable = true
                                            et_city.isClickable = true
                                        }
                                    }

                                    et_city.setBackgroundResource(R.drawable.edittext_bg)
                                    et_city.layoutParams = editTextLayoutParams
                                    // et_city.isFocusable = false
                                    et_city.setPadding(
                                        paddingLeft,
                                        paddingTop,
                                        paddingRight,
                                        paddingBottom
                                    )
                                    et_city.hint = "Select City"
                                    et_city.setOnClickListener {

                                        Log.d("CITYIF", "country : $country")
                                        if (country == "USA") {
                                            city_search_dialog.show()
                                            et_state.isFocusable = false
                                            et_city.isFocusable = false
                                            et_city.isClickable = false
                                            et_state.isFocusableInTouchMode = true
                                            et_city.isFocusableInTouchMode = true
                                        } else {
                                            Log.d("STATEELSE", "country : $country")
                                            state_search_dialog.dismiss()
                                            city_search_dialog.dismiss()
                                            et_state.isFocusable = true
                                            et_state.isClickable = true
                                            et_city.isFocusable = true
                                            et_city.isClickable = true
                                        }
                                    }

                                    tv_country.text = "Country"
                                    tv_state.text = "State"
                                    tv_city.text = "City"

                                    ll_et_country.addView(tv_country)
                                    ll_et_country.addView(et_country)
                                    ll_et_state.addView(tv_state)
                                    ll_et_state.addView(et_state)
                                    ll_et_city.addView(tv_city)
                                    ll_et_city.addView(et_city)

                                    ll_dialog_prefs.addView(ll_et_country)
                                    ll_dialog_prefs.addView(ll_et_state)
                                    ll_dialog_prefs.addView(ll_et_city)

                                    /* ll_dialog_prefs.addView(ll_et_country)
                                     ll_dialog_prefs.addView(ll_et_state)
                                     ll_dialog_prefs.addView(ll_et_city)*/

                                    tv_dialog_prefs_done.setOnClickListener {
                                        val city_update = et_city.text.toString().trim()
                                        var state_update = ""
                                        if (country == "USA") {
                                            state_update = state
                                        } else {
                                            state_update = et_state.text.toString()
                                        }
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            state_update,
                                            city_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            country,
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.match_distance)
                                    val distanceAdapter = EditPrefsDistanceAdapter(
                                        this@EditMatchPreferencesActivity,
                                        resources.getStringArray(R.array.search_distance)
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = distanceAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = distanceAdapter.getItem(position)
                                                    distanceAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected distance $data")
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_distance_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_distance_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_matches_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.match_distance_importance)
                                    val distanceAdapter = EditPrefsDistanceAdapter(
                                        this@EditMatchPreferencesActivity,
                                        resources.getStringArray(R.array.search_imp)
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = distanceAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = distanceAdapter.getItem(position)
                                                    distanceAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected importance $data")
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_search_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_search_update,
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                            }
                            1 -> {
                                tv_prefs_profile_qstn_2.visibility = View.VISIBLE
                                tv_prefs_profile_ans_2.visibility = View.VISIBLE

                                tv_prefs_matches_header_2.visibility = View.VISIBLE
                                tv_prefs_matches_hint_2.visibility = View.VISIBLE
                                tv_prefs_matches_selected_2.visibility = View.VISIBLE
                                tv_prefs_matches_ans_2.visibility = View.VISIBLE


                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )
                                tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )

                                tv_prefs_profile_qstn_1.text =
                                    resources.getString(R.string.how_often_do_you_smoke)
                                tv_prefs_profile_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.USER_SMOKE]!!
                                tv_prefs_profile_qstn_2.text =
                                    resources.getString(R.string.how_often_do_you_drink)
                                tv_prefs_profile_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.USER_DRINK]!!

                                tv_prefs_matches_header_1.text =
                                    resources.getString(R.string.smoking)
                                tv_prefs_matches_hint_1.text =
                                    getString(R.string.please_indicate_partner_smoke)
                                tv_prefs_matches_selected_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_smoke]!!
                                tv_prefs_matches_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_smoke]!!
                                tv_prefs_matches_header_2.text =
                                    resources.getString(R.string.drinking)
                                tv_prefs_matches_hint_2.text =
                                    getString(R.string.please_indicate_partner_drink)
                                tv_prefs_matches_selected_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_drink]!!
                                tv_prefs_matches_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_drink]!!

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.smoking)
                                    val smokingAdapter = EditPrefsSmokingAdapter(
                                        this@EditMatchPreferencesActivity, smokeList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = smokingAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = smokingAdapter.getItem(position)
                                                    smokingAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected smoking $data")
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val smoking_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            smoking_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                                tv_prefs_profile_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.drinking)
                                    val drinkingAdapter = EditPrefsDrinkingAdapter(
                                        this@EditMatchPreferencesActivity, drinkList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = drinkingAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = drinkingAdapter.getItem(position)
                                                    drinkingAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected drinking $data")
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val drinking_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            drinking_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()

                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.smoking)
                                    val smokingAdapter = EditPrefsSmokingAdapter(
                                        this@EditMatchPreferencesActivity, smokeList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = null
                                    recyclerView.adapter = smokingAdapter
                                    selectedSmoking.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = smokingAdapter.getItem(position)

                                                    smokingAdapter.selectedList(selectedSmoking)
                                                    if (selectedSmoking.contains(data)) {
                                                        selectedSmoking.remove(data)
                                                    } else {
                                                        selectedSmoking.add(data)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected smoking $data size ${selectedSmoking.size}"
                                                    )
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)

                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_smoking_update =
                                            commaSeparatedString(selectedSmoking)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_smoking_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                                tv_prefs_matches_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.drinking)
                                    val drinkingAdapter = EditPrefsDrinkingAdapter(
                                        this@EditMatchPreferencesActivity, drinkList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = drinkingAdapter
                                    selectedDrinking.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = drinkingAdapter.getItem(position)
                                                    drinkingAdapter.selectedList(selectedDrinking)
                                                    if (selectedDrinking.contains(data)) {
                                                        selectedDrinking.remove(data)
                                                    } else {
                                                        selectedDrinking.add(data)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected drinking $data size ${selectedDrinking.size}"
                                                    )
                                                }
                                            })
                                    )

                                    ll_dialog_prefs.addView(recyclerView)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_drinking_update =
                                            commaSeparatedString(selectedDrinking)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_drinking_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                            }
                            2 -> {
                                tv_prefs_profile_qstn_2.visibility = View.GONE
                                tv_prefs_profile_ans_2.visibility = View.GONE

                                tv_prefs_matches_header_2.visibility = View.GONE
                                tv_prefs_matches_hint_2.visibility = View.GONE
                                tv_prefs_matches_selected_2.visibility = View.GONE
                                tv_prefs_matches_ans_2.visibility = View.GONE


                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )
                                tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )

                                tv_prefs_profile_qstn_1.text =
                                    resources.getString(R.string.ethnicity)
                                tv_prefs_profile_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.USER_ETHNICITY]!!

                                tv_prefs_matches_header_1.text =
                                    resources.getString(R.string.ethnicity)
                                tv_prefs_matches_hint_1.text =
                                    resources.getString(R.string.ethnicity_you_are_looking_for)
                                tv_prefs_matches_selected_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_ethnicity]!!
                                tv_prefs_matches_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_ethnicity]!!

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.ethnicity)
                                    val ethnicityAdapter = EditPrefsEthnicityAdapter(
                                        this@EditMatchPreferencesActivity, ethnicityList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = ethnicityAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = ethnicityAdapter.getItem(position)
                                                    ethnicityAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected ethnicity $data")
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (ethnicityList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val ethnicity_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            ethnicity_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.ethnicity)
                                    val ethnicityAdapter = EditPrefsEthnicityAdapter(
                                        this@EditMatchPreferencesActivity, ethnicityList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = ethnicityAdapter
                                    selectedEthnicity.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = ethnicityAdapter.getItem(position)

                                                    ethnicityAdapter.selectedList(selectedEthnicity)
                                                    if (selectedEthnicity.contains(data)) {
                                                        selectedEthnicity.remove(data)
                                                    } else {
                                                        selectedEthnicity.add(data)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected ethnicity $data size ${selectedEthnicity.size}"
                                                    )
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (ethnicityList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_ethnicity_update =
                                            commaSeparatedString(selectedEthnicity)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_ethnicity_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                            }
                            3 -> {
                                tv_prefs_profile_qstn_2.visibility = View.VISIBLE
                                tv_prefs_profile_ans_2.visibility = View.VISIBLE

                                tv_prefs_matches_header_2.visibility = View.GONE
                                tv_prefs_matches_hint_2.visibility = View.GONE
                                tv_prefs_matches_selected_2.visibility = View.GONE
                                tv_prefs_matches_ans_2.visibility = View.GONE
                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0,
                                    0,
                                    R.drawable.ic_arrow_drop_down,
                                    0
                                )
                                tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                                    0,
                                    0,
                                    0,
                                    0
                                )

                                tv_prefs_profile_qstn_2.text = resources.getString(R.string.my_age)
                                tv_prefs_profile_ans_2.text = user_age
                                tv_prefs_profile_qstn_1.text = "Date of Birth"
                                tv_prefs_profile_ans_1.text =
                                    editDateFormatUi(sessionManager.userDetails[DllSessionManager.USER_DOB]!!)

                                tv_prefs_matches_header_1.text = resources.getString(R.string.age)
                                tv_prefs_matches_hint_1.text =
                                    resources.getString(R.string.i_m_looking_for_someone_between_the_ages_of_make_sure)
                                tv_prefs_matches_selected_1.text =
                                    "${sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MIN]!!} to ${sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MAX]!!}"
                                tv_prefs_matches_ans_1.text =
                                    "${sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MIN]!!} to ${sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MAX]!!}"


                                tv_prefs_profile_ans_2.setOnClickListener {
                                    // do nothing
                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text = resources.getString(R.string.age)
                                    val ll_spinner_min = LinearLayout(mDialog.context)
                                    val ll_spinner_max = LinearLayout(mDialog.context)
                                    val linearLayoutParams =
                                        LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT
                                        )
                                    linearLayoutParams.setMargins(
                                        convertToDp(3),
                                        convertToDp(3),
                                        convertToDp(3),
                                        convertToDp(3)
                                    )
                                    ll_spinner_max.layoutParams = linearLayoutParams
                                    ll_spinner_min.layoutParams = linearLayoutParams

                                    ll_spinner_max.setBackgroundResource(R.drawable.edittext_bg)
                                    ll_spinner_min.setBackgroundResource(R.drawable.edittext_bg)

                                    val age_min_sp = Spinner(mDialog.context)
                                    val age_max_sp = Spinner(mDialog.context)

                                    age_min_sp.adapter = min_age_adapter
                                    age_max_sp.adapter = max_age_adapter

                                    val layoutParams =
                                        LinearLayout.LayoutParams(
                                            ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT
                                        )

                                    age_min_sp.layoutParams = layoutParams
                                    age_max_sp.layoutParams = layoutParams

                                    age_min_sp.setPadding(paddingLeft, 0, paddingRight, 0)
                                    age_max_sp.setPadding(paddingLeft, 0, paddingRight, 0)

                                    age_min_sp.onItemSelectedListener =
                                        object : AdapterView.OnItemSelectedListener {
                                            override fun onItemSelected(
                                                parentView: AdapterView<*>,
                                                selectedItemView: View,
                                                position: Int,
                                                id: Long
                                            ) {
                                                min_age = parentView.getItemAtPosition(position)
                                                    .toString()
                                                Log.e(logTAG, "min_age : $min_age")
                                            }

                                            override fun onNothingSelected(parentView: AdapterView<*>) {
                                                // do nothing
                                            }

                                        }

                                    age_max_sp.onItemSelectedListener =
                                        object : AdapterView.OnItemSelectedListener {
                                            override fun onItemSelected(
                                                parentView: AdapterView<*>,
                                                selectedItemView: View,
                                                position: Int,
                                                id: Long
                                            ) {
                                                max_age = parentView.getItemAtPosition(position)
                                                    .toString()
                                                Log.e(logTAG, "max_age :$max_age")
                                            }

                                            override fun onNothingSelected(parentView: AdapterView<*>) {
                                                // do nothing
                                            }

                                        }

                                    ll_spinner_max.addView(age_max_sp)
                                    ll_spinner_min.addView(age_min_sp)

                                    ll_dialog_prefs.addView(ll_spinner_min)
                                    ll_dialog_prefs.addView(ll_spinner_max)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_age_min = min_age
                                        val partner_age_max = max_age
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_age_min,
                                            partner_age_max,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    tv_dialog_prefs_header.text = "Date of Birth"
                                    var dob = ""

                                    val et_dob = EditText(mDialog.context)
                                    et_dob.setBackgroundResource(R.drawable.edittext_bg)

                                    et_dob.layoutParams = editTextLayoutParams
                                    et_dob.setPadding(
                                        paddingLeft,
                                        paddingTop,
                                        paddingRight,
                                        paddingBottom
                                    )
                                    et_dob.hint = "Date of Birth"
                                    et_dob.isFocusable = false

                                    if (sessionManager.userDetails[DllSessionManager.USER_DOB]!! != "0") {
                                        val date_birth =
                                            editDateFormatUi(sessionManager.userDetails[DllSessionManager.USER_DOB]!!)
                                        dob = date_birth
                                        et_dob.setText(date_birth)
                                    }
                                    et_dob.setOnClickListener {
                                        var date2 = ""
                                        val currentDate = Calendar.getInstance()
                                        val date = Calendar.getInstance()
                                        currentDate.add(Calendar.YEAR, -18)
                                        currentDate.add(Calendar.DATE, -1)
                                        val datePickerDialog =
                                            DatePickerDialog(
                                                this@EditMatchPreferencesActivity,
                                                DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
                                                    date.set(year, monthofyear, dayofmonth)

                                                    val myFormat = "MM-dd-yyyy"
                                                    val sdf = SimpleDateFormat(myFormat, Locale.US)
                                                    date2 = sdf.format(date.time)
                                                    dob = date2
                                                    Log.d(
                                                        logTAG,
                                                        "calendar date : ${date.time} ===== formatted date : $date2"
                                                    )

                                                    et_dob.setText(date2)

                                                },
                                                currentDate.get(Calendar.YEAR),
                                                currentDate.get(Calendar.MONTH),
                                                currentDate.get(Calendar.DATE)
                                            )

                                        datePickerDialog.getDatePicker()
                                            .setMaxDate(System.currentTimeMillis() - 568025136000L);
                                        datePickerDialog.show()
                                    }

                                    ll_dialog_prefs.addView(et_dob)

                                    tv_dialog_prefs_done.setOnClickListener {
                                        val dob_update = editDateFormatServer(dob)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            dob_update
                                        )
                                        mDialog.dismiss()
                                    }

                                }


                            }
                            4 -> {
                                tv_prefs_profile_qstn_2.visibility = View.VISIBLE
                                tv_prefs_profile_ans_2.visibility = View.VISIBLE

                                tv_prefs_matches_header_2.visibility = View.VISIBLE
                                tv_prefs_matches_hint_2.visibility = View.VISIBLE
                                tv_prefs_matches_selected_2.visibility = View.VISIBLE
                                tv_prefs_matches_ans_2.visibility = View.VISIBLE


                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0,
                                    0,
                                    R.drawable.ic_arrow_drop_down,
                                    0
                                )
                                tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                                    0,
                                    0,
                                    R.drawable.ic_arrow_drop_down,
                                    0
                                )

                                if (religionDataList1!!.size > 0) {
                                    for (i in 0 until religionDataList1!!.size) {
                                        religionsList1.add(religionDataList1!![i].religion_name)
                                    }

                                }



                                if (religionDataList!!.size > 0) {
                                    for (i in 0 until religionDataList!!.size) {
                                        religionsList.add(religionDataList!![i].religion_name)
                                    }

                                }



                                tv_prefs_profile_qstn_1.text =
                                    resources.getString(R.string.religion)
                                tv_prefs_profile_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.USER_RELIGION]!!
                                tv_prefs_profile_qstn_2.text = "Mother tongue"
                                tv_prefs_profile_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.USER_MOTHER_TONGUE]!!

                                tv_prefs_matches_header_1.text =
                                    resources.getString(R.string.religion)
                                tv_prefs_matches_hint_1.text =
                                    resources.getString(R.string.what_best_describes_your_partner_s_religious_beliefs_or_spirituality)
                                tv_prefs_matches_selected_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_religion]!!
                                tv_prefs_matches_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_religion]!!
                                tv_prefs_matches_header_2.text = "Mother tongue"
                                tv_prefs_matches_hint_2.text =
                                    "Please choose your partner's Mother tongue"
                                tv_prefs_matches_selected_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MOTHER_TONGUE]!!
                                tv_prefs_matches_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_MOTHER_TONGUE]!!

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    /* if (religionDataList!!.size > 0) {
                                         for (i in 0 until religionDataList!!.size) {
                                             if (religionDataList!![i].religion_name == "All") {
                                                 religionDataList!!.removeAt(i)
                                                 break
                                             }
                                         }
                                     }*/
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.religion)
                                    val religionAdapter = EditPrefsReligion_WithoutAll_Adapter(
                                        this@EditMatchPreferencesActivity, religionDataListtest!!
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = religionAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {
                                                    data = religionAdapter.getItem(position)
                                                    religionAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected religion $data")

                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (religionsList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val religion_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            religion_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_profile_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text = "Mother Tongue"

                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    val mTongueAdapter =
                                        EditPrefsMotherTongueAdapter(
                                            this@EditMatchPreferencesActivity,
                                            motherTongueDataList
                                        )
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = mTongueAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = mTongueAdapter.getItem(position)
                                                    mTongueAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected denomination $data")
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (motherTongueDataList!!.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val mTongue_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            mTongue_update,
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.religion)
                                    val religionAdapter = EditPrefsReligionAdapter(
                                        this@EditMatchPreferencesActivity, religionDataList1!!
                                    )

                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = religionAdapter
                                    selectedReligion.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {
                                                    data = religionAdapter.getItem(position)

                                                    if (data == "All") {
                                                        if (selectedReligion.isEmpty()) {
                                                            for (i in 0 until religionDataList1!!.size) {
                                                                selectedReligion.add(
                                                                    religionDataList1!![i].religion_name
                                                                )
                                                            }
                                                            religionAdapter.selectedList(
                                                                selectedReligion
                                                            )
                                                        } else if (!selectedReligion.contains("All")) {
                                                            selectedReligion.clear()
                                                            for (i in 0 until religionDataList1!!.size) {
                                                                selectedReligion.add(
                                                                    religionDataList1!![i].religion_name
                                                                )
                                                            }
                                                            religionAdapter.selectedList(
                                                                selectedReligion
                                                            )
                                                        } else if (selectedReligion.contains("All")) {
                                                            selectedReligion.clear()
                                                            religionAdapter.selectedList(
                                                                selectedReligion
                                                            )
                                                        }

                                                    } else {
                                                        if (selectedReligion.contains("All")) {
                                                            selectedReligion.remove("All")
                                                        }
                                                        if (selectedReligion.contains(data)) {
                                                            selectedReligion.remove(data)
                                                        } else {
                                                            selectedReligion.add(data)
                                                        }
                                                        religionAdapter.selectedList(
                                                            selectedReligion
                                                        )
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected religion $data size ${selectedReligion.size}"
                                                    )
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (religionsList1.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_religion_update =
                                            commaSeparatedString(selectedReligion)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_religion_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                                tv_prefs_matches_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text = "Mother Tongue"

                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    val mTongueAdapter =
                                        EditPrefsMotherTongueAdapter(
                                            this@EditMatchPreferencesActivity,
                                            motherTongueDataList
                                        )
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = mTongueAdapter
                                    selectedmTongue.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = mTongueAdapter.getItem(position)
                                                    mTongueAdapter.selectedList(selectedmTongue)
                                                    if (selectedmTongue.contains(data)) {
                                                        selectedmTongue.remove(data)
                                                    } else {
                                                        selectedmTongue.add(data)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected mTongue $data size ${selectedmTongue.size}"
                                                    )
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (motherTongueDataList!!.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_mTongue_update =
                                            commaSeparatedString(selectedmTongue)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_mTongue_update,
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                            }
                            5 -> {

                                tv_prefs_profile_qstn_2.visibility = View.VISIBLE
                                tv_prefs_profile_ans_2.visibility = View.VISIBLE

                                tv_prefs_matches_header_2.visibility = View.VISIBLE
                                tv_prefs_matches_hint_2.visibility = View.VISIBLE
                                tv_prefs_matches_selected_2.visibility = View.VISIBLE
                                tv_prefs_matches_ans_2.visibility = View.VISIBLE

                                tv_prefs_profile_ans_1.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )
                                tv_prefs_profile_ans_2.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_arrow_drop_down, 0
                                )

                                tv_prefs_profile_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.USER_EDUCATION]!!
                                tv_prefs_profile_qstn_1.text =
                                    resources.getString(R.string.education)
                                tv_prefs_profile_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.USER_OCCUPATION]!!
                                tv_prefs_profile_qstn_2.text =
                                    resources.getString(R.string.occupation)

                                tv_prefs_matches_header_1.text =
                                    resources.getString(R.string.education)
                                tv_prefs_matches_hint_1.text =
                                    resources.getString(R.string.which_describes_your_partner_education)
                                tv_prefs_matches_selected_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_education]!!
                                tv_prefs_matches_ans_1.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_education]!!

                                tv_prefs_matches_header_2.text =
                                    resources.getString(R.string.occupation)
                                tv_prefs_matches_hint_2.text =
                                    resources.getString(R.string.what_do_you_expect_your_partner_to_do)
                                tv_prefs_matches_selected_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_occupation]!!
                                tv_prefs_matches_ans_2.text =
                                    sessionManager.userDetails[DllSessionManager.PARTNER_occupation]!!

                                tv_prefs_profile_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.education)
                                    val educationAdapter = EditPrefsEducationAdapter(
                                        this@EditMatchPreferencesActivity, educationList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = educationAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = educationAdapter.getItem(position)
                                                    educationAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected education $data")
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (educationList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val education_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            education_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_profile_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.occupation)
                                    val occupationAdapter = EditPrefsOccupationAdapter(
                                        this@EditMatchPreferencesActivity, occupationList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = occupationAdapter

                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {

                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = occupationAdapter.getItem(position)
                                                    occupationAdapter.setSelectedIndex(position)

                                                    Log.d(logTAG, "selected occupation $data")
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (occupationList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val occupation_update = data
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            occupation_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }
                                }

                                tv_prefs_matches_ans_1.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.education)
                                    val educationAdapter = EditPrefsEducationAdapter(
                                        this@EditMatchPreferencesActivity, educationList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = educationAdapter
                                    selectedEducation.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = educationAdapter.getItem(position)
                                                    educationAdapter.selectedList(selectedEducation)
                                                    if (selectedEducation.contains(data)) {
                                                        selectedEducation.remove(data)
                                                    } else {
                                                        selectedEducation.add(data)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "selected education $data size ${selectedEducation.size}"
                                                    )
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (educationList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_education_update =
                                            commaSeparatedString(selectedEducation)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_education_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                                tv_prefs_matches_ans_2.setOnClickListener {
                                    mDialog.show()
                                    ll_dialog_prefs.removeAllViews()
                                    var data = ""
                                    tv_dialog_prefs_header.text =
                                        resources.getString(R.string.occupation)
                                    val occupationAdapter = EditPrefsOccupationAdapter(
                                        this@EditMatchPreferencesActivity, occupationList
                                    )
                                    val recyclerView = RecyclerView(mDialog.context)
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutParams = recyclerViewLayoutParams
                                    recyclerView.layoutManager =
                                        LinearLayoutManager(
                                            mDialog.context,
                                            LinearLayout.VERTICAL,
                                            false
                                        )
                                    recyclerView.adapter = occupationAdapter
                                    selectedOccupation.clear()
                                    recyclerView.addOnItemTouchListener(
                                        RecyclerItemClickListener(this@EditMatchPreferencesActivity,
                                            recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(
                                                    view: View,
                                                    position: Int
                                                ) {

                                                    data = occupationAdapter.getItem(position)
                                                    occupationAdapter.selectedList(
                                                        selectedOccupation
                                                    )
                                                    if (selectedOccupation.contains(data)) {
                                                        selectedOccupation.remove(data)
                                                    } else {
                                                        selectedOccupation.add(data)
                                                    }
                                                    Log.d(
                                                        logTAG,
                                                        "selected occupation $data size ${selectedOccupation.size}"
                                                    )
                                                }
                                            })
                                    )

                                    val empty_text = CustomTextView(mDialog.context)
                                    empty_text.text =
                                        resources.getString(R.string.no_data_available)
                                    empty_text.gravity = Gravity.CENTER
                                    empty_text.setTextColor(
                                        ContextCompat.getColor(
                                            this@EditMatchPreferencesActivity,
                                            R.color.profile_title_color
                                        )
                                    )
                                    empty_text.visibility = if (occupationList.size > 0) {
                                        View.GONE
                                    } else {
                                        View.VISIBLE
                                    }

                                    ll_dialog_prefs.addView(recyclerView)
                                    ll_dialog_prefs.addView(empty_text)
                                    tv_dialog_prefs_done.setOnClickListener {
                                        val partner_occupation_update =
                                            commaSeparatedString(selectedOccupation)
                                        updatePreferences(
                                            userId_session,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            partner_occupation_update,
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            ""
                                        )
                                        mDialog.dismiss()
                                    }

                                }

                            }
                        }

                        Log.d(logTAG, "position : $position $item")
                    }
                }
            )
        )
    }

    private fun initialize() {

        match_prefs_ll = findViewById(R.id.match_prefs_ll)
        ll_prefs_main = findViewById(R.id.ll_prefs_main)
        ll_prefs_profile_main = findViewById(R.id.ll_prefs_profile_main)
        ll_prefs_profile_header = findViewById(R.id.ll_prefs_profile_header)
        ll_prefs_profile_1 = findViewById(R.id.ll_prefs_profile_1)
        ll_prefs_profile_2 = findViewById(R.id.ll_prefs_profile_2)
        ll_prefs_matches_main = findViewById(R.id.ll_prefs_matches_main)
        ll_prefs_matches_header = findViewById(R.id.ll_prefs_matches_header)
        ll_prefs_matches_1 = findViewById(R.id.ll_prefs_matches_1)
        ll_prefs_matches_2 = findViewById(R.id.ll_prefs_matches_2)
        rl_prefs = findViewById(R.id.rl_prefs)

        tv_prefs_profile_qstn_1 = findViewById(R.id.tv_prefs_profile_qstn_1)
        tv_prefs_profile_ans_1 = findViewById(R.id.tv_prefs_profile_ans_1)
        tv_prefs_profile_qstn_2 = findViewById(R.id.tv_prefs_profile_qstn_2)
        tv_prefs_profile_ans_2 = findViewById(R.id.tv_prefs_profile_ans_2)
        tv_prefs_matches_header_1 = findViewById(R.id.tv_prefs_matches_header_1)
        tv_prefs_matches_hint_1 = findViewById(R.id.tv_prefs_matches_hint_1)
        tv_prefs_matches_selected_1 = findViewById(R.id.tv_prefs_matches_selected_1)
        tv_prefs_matches_ans_1 = findViewById(R.id.tv_prefs_matches_ans_1)
        tv_prefs_matches_header_2 = findViewById(R.id.tv_prefs_matches_header_2)
        tv_prefs_matches_hint_2 = findViewById(R.id.tv_prefs_matches_hint_2)
        tv_prefs_matches_selected_2 = findViewById(R.id.tv_prefs_matches_selected_2)
        tv_prefs_matches_ans_2 = findViewById(R.id.tv_prefs_matches_ans_2)


        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()

        tv_prefs_header = findViewById(R.id.tv_prefs_header)

        iv_prefs_back = findViewById(R.id.iv_prefs_back)

        recyclerView_prefs = findViewById(R.id.recyclerView_prefs)

        // add titles to list
        recycle_name = resources.getStringArray(R.array.match_prefs_edit)
        // add corresponding images to list
        recycle_images = intArrayOf(
            R.drawable.ic_match_distance,
            R.drawable.ic_match_smoke_drink,
            R.drawable.ic_match_ethnicity,
            R.drawable.ic_match_age,
            R.drawable.ic_match_religion,
            R.drawable.ic_match_education_ic
        )
        // add image & title to array list
        recycle_items = ArrayList()
        for (i in 0 until recycle_name!!.size) {
            recycle_items!!.add(EditPrefsPojo(recycle_name!![i], recycle_images!![i]))
        }

        prefsLayoutManager =
            LinearLayoutManager(this@EditMatchPreferencesActivity, LinearLayout.HORIZONTAL, false)
        recyclerView_prefs.layoutManager = prefsLayoutManager
        recyclerView_prefs.setHasFixedSize(true)
        editPrefsAdapter = EditPrefsAdapter(this@EditMatchPreferencesActivity, recycle_items!!)
        recyclerView_prefs.adapter = editPrefsAdapter

        mDialog = Dialog(this@EditMatchPreferencesActivity)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        mDialog.setContentView(R.layout.dialog_match_prefs_edit)
        val dialog_window = mDialog.window
        dialog_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        iv_dialog_prefs_close = mDialog.findViewById(R.id.iv_dialog_prefs_close)
        ll_dialog_prefs = mDialog.findViewById(R.id.ll_dialog_prefs)
        tv_dialog_prefs_header = mDialog.findViewById(R.id.tv_dialog_prefs_header)
        tv_dialog_prefs_done = mDialog.findViewById(R.id.tv_dialog_prefs_done)

        progressBar = Dialog(mDialog.context)
        progressBar.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressBar.setContentView(R.layout.alert_dialog_loading)
        progressBar.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressBar.setCancelable(false)
        progressBar.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressBar.dismiss()
                }
                return true
            }
        })

        selectedSmoking = ArrayList()
        selectedDrinking = ArrayList()
        selectedEthnicity = ArrayList()
        selectedReligion = ArrayList()
        selectedmTongue = ArrayList()
        selectedEducation = ArrayList()
        selectedOccupation = ArrayList()
        religionDataList = ArrayList()
        religionDataListtest = ArrayList()
        religionDataList1 = ArrayList()
        religionsList = ArrayList()
        religionsList1 = ArrayList()
        motherTongueDataList = ArrayList()
        citiesDataList = ArrayList()
        citiesList = ArrayList()
        ethnicityList = ArrayList()
        educationList = ArrayList()
        occupationList = ArrayList()
        incomeList = ArrayList()
        smokeList = ArrayList()
        drinkList = ArrayList()
        questionsList = ArrayList()
        countriesDataList = ArrayList()
        countriesList = ArrayList()
        statesNamesList = ArrayList<String>()

        min_age_array = ArrayList()
        for (i in MIN_AGE..MAX_AGE) {
            val age = i.toString()
            min_age_array.add(age)
        }

        max_age_array = ArrayList()
        for (i in MIN_AGE_mx..MAX_AGE) {
            val age = i.toString()
            max_age_array.add(age)
        }

        min_age_adapter =
            object : ArrayAdapter<String>(
                this@EditMatchPreferencesActivity,
                R.layout.item_spinner,
                min_age_array
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return true
                }

                override fun getDropDownView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    return view
                }
            }
        min_age_adapter.setDropDownViewResource(R.layout.item_spinner)

        max_age_adapter =
            object : ArrayAdapter<String>(
                this@EditMatchPreferencesActivity,
                R.layout.item_spinner,
                max_age_array
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return true
                }

                override fun getDropDownView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    return view
                }
            }
        max_age_adapter.setDropDownViewResource(R.layout.item_spinner)

        city_SearchAdapter = LocationSearchAdapter(this@EditMatchPreferencesActivity, citiesList)

    }

    fun convertToDp(size: Int): Int {
        val scale = resources.displayMetrics.density
        return (size * scale + 0.5f).toInt()
    }

    private fun commaSeparatedString(list: ArrayList<String>): String {
        var result = ""
        if (list.size > 0) {
            val sb = StringBuilder()
            for (s in list) {
                sb.append(s).append(",")
            }
            result = sb.deleteCharAt(sb.length - 1).toString()
        }
        return result
    }

    private fun getCities(state_stg: String) {
        citiesDataList = ArrayList()
        citiesList = ArrayList()

        val cityApi = ApiInterface.create()
        val cityCall = cityApi.getCities(state_stg)
        cityCall.enqueue(object : Callback<CitiesResponse> {
            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                Log.e(logTAG, "CitiesResponse : error : ${t.toString()}")
                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                Log.d(logTAG, "CitiesResponse: success: ${response.body()!!.result}")

                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                if (response.body() != null) {
                    if (response.body()!!.status == "1") {
                        citiesDataList = response.body()!!.data!!

                        for (i in 0 until citiesDataList.size) {
                            citiesList.add(citiesDataList[i].city_name)
                        }
                        city_SearchAdapter =
                            LocationSearchAdapter(this@EditMatchPreferencesActivity, citiesList)
                        city_searchListView.adapter = city_SearchAdapter

                    } else if (response.body()!!.status == "2") {
                        citiesDataList = ArrayList()
                    }
                } else {
                    Toast.makeText(
                        this@EditMatchPreferencesActivity,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun getCountries() {

        val cityApi = ApiInterface.create()
        val cityCall = cityApi.getCountries()
        cityCall.enqueue(object : Callback<CountryListResponse> {
            override fun onFailure(call: Call<CountryListResponse>, t: Throwable) {
                Log.e(logTAG, "countriesResponse : error : ${t.toString()}")
                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<CountryListResponse>,
                response: Response<CountryListResponse>
            ) {
                Log.d(logTAG, "countriesResponse: success: ${response.body()!!.result}")

                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                if (response.body() != null) {
                    if (response.body()!!.status == "1") {
                        countriesDataList = response.body()!!.data!!

                        for (i in 0 until countriesDataList.size) {
                            countriesList.add(countriesDataList[i].country_name)
                        }
                        country_SearchAdapter =
                            LocationSearchAdapter(this@EditMatchPreferencesActivity, countriesList)


                    } else if (response.body()!!.status == "2") {
                        countriesDataList = ArrayList()

                    }
                } else {
                    Toast.makeText(
                        this@EditMatchPreferencesActivity,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        })
    }

    private fun getReligionApi() {
        val religionApi = ApiInterface.create()
        val religionCall = religionApi.getReligionApi()
        religionDataListtest!!.clear()
        religionDataList!!.clear()
        religionDataList1!!.clear()
        religionCall.enqueue(object : Callback<ReligionResponse> {
            override fun onFailure(call: Call<ReligionResponse>, t: Throwable) {
                Log.e(logTAG, "ReligionResponse : error : $t")
                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }

                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<ReligionResponse>, response: Response<ReligionResponse>
            ) {
                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }

                if (response.body() != null) {
                    Log.d(
                        logTAG,
                        "${response.body()!!.data!!.size}ReligionResponse: success: ${response.body()!!.data.toString()}"
                    )
                    if (response.body()!!.status == "1") {
                        religionDataList1 = response.body()!!.data
                        religionDataList = response.body()!!.data

                        if (religionDataList!!.size > 0) {
                            for (i in 1 until religionDataList!!.size) {
                                religionDataListtest!!.add(religionDataList!![i].religion_name)
                            }
                        }
                        Log.d(
                            logTAG,
                            "" + religionDataListtest!!.size + " ReligionResponse: success:" + religionDataList!!.size + "--" + religionDataList1!!.size
                        )
                    }
                } else {
                    Toast.makeText(
                        this@EditMatchPreferencesActivity,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun getRegisterApi() {
        val api = ApiInterface.create()
        val apiCall = api.registerProfileApi()
        apiCall.enqueue(object : Callback<RegisterProfileSetupResponse> {
            override fun onFailure(call: Call<RegisterProfileSetupResponse>, t: Throwable) {
                Log.e(logTAG, "RegisterProfileSetupResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<RegisterProfileSetupResponse>,
                response: Response<RegisterProfileSetupResponse>
            ) {

                Log.d(
                    logTAG,
                    "RegisterProfileSetupResponse: success: ${response.body()!!.data.toString()}"
                )

                if (response.body()!!.status.equals("1")) {
                    questionsList = response.body()!!.data

                    for (i in 0 until questionsList!!.size) {
                        if (questionsList!!.get(i).title == "ethnicity") {
                            ethnicityList = questionsList!![i].answer!!
                        }
                        if (questionsList!!.get(i).title == "education") {
                            educationList = questionsList!![i].answer!!
                        }
                        if (questionsList!!.get(i).title == "occupation") {
                            occupationList = questionsList!![i].answer!!
                        }
                        if (questionsList!!.get(i).title == "income") {
                            incomeList = questionsList!![i].answer!!
                        }
                        if (questionsList!!.get(i).title == "smoke") {
                            smokeList = questionsList!![i].answer!!
                        }
                        if (questionsList!!.get(i).title == "drink") {
                            drinkList = questionsList!![i].answer!!
                        }
                    }
                }
            }
        })
    }

    private fun getMotherTongueApi() {
        val motherTongueApi = ApiInterface.create()
        val motherTongueCall = motherTongueApi.getMotherTongueApi()
        motherTongueCall.enqueue(object : Callback<MotherTongueResponse> {
            override fun onFailure(call: Call<MotherTongueResponse>, t: Throwable) {
                Log.e(logTAG, "motherTongueResponse : error : $t")
                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(
                call: Call<MotherTongueResponse>, response: Response<MotherTongueResponse>
            ) {

                Log.d(logTAG, "motherTongueResponse: success: ${response.body()!!.data.toString()}")
                if (response.body() != null) {
                    if (response.body()!!.status == "1") {

                        motherTongueDataList = response.body()!!.data!!
                        Log.d(logTAG, "motherTongueResponse: size: ${motherTongueDataList.size}")

                    } else {
                        Toast.makeText(
                            this@EditMatchPreferencesActivity,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } else {
                    Toast.makeText(
                        this@EditMatchPreferencesActivity,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun updatePreferences(
        user_id: String,
        zipcode: String,
        state: String,
        city: String,
        ethnicity: String,
        religion: String,
        denomination: String,
        education: String,
        income: String,
        smoke: String,
        drink: String,
        height_ft: String,
        height_inc: String,
        height_cms: String,
        passionate: String,
        leisure_time: String,
        thankful_1: String,
        thankful_2: String,
        thankful_3: String,
        occupation: String,
        partner_age_min: String,
        partner_age_max: String,
        partner_match_distance: String,
        partner_ethnicity: String,
        partner_religion: String,
        partner_denomination: String,
        partner_education: String,
        partner_occupation: String,
        partner_smoke: String,
        partner_drink: String,
        partner_match_search: String,
        country: String,
        mother_tongue: String,
        partner_mother_tongue: String,
        dob: String,
        push_notification_status: String = "",
        email_notification_status: String = ""
    ) {
        var zipcode_stg: String
        var state_stg: String
        var city_stg: String
        var ethnicity_stg: String
        var religion_stg: String
        var denomination_stg: String
        var education_stg: String
        var smoke_stg: String
        var drink_stg: String
        var occupation_stg: String
        var partner_age_min_stg: String
        var partner_age_max_stg: String
        var partner_match_distance_stg: String
        var partner_ethnicity_stg: String
        var partner_religion_stg: String
        var partner_denomination_stg: String
        var partner_education_stg: String
        var partner_occupation_stg: String
        var partner_smoke_stg: String
        var partner_drink_stg: String
        var partner_match_search_stg: String
        var country_stg: String
        var mother_tongue_stg: String
        var partner_mother_tongue_stg: String
        var dob_stg: String

        val api = ApiInterface.create()
        val callApi = api.updateMatchPreferences(
            user_id,
            zipcode,
            state,
            city,
            ethnicity,
            religion,
            denomination,
            education,
            income,
            smoke,
            drink,
            height_ft,
            height_inc,
            height_cms,
            passionate,
            leisure_time,
            thankful_1,
            thankful_2,
            thankful_3,
            occupation,
            partner_age_min,
            partner_age_max,
            partner_match_distance,
            partner_ethnicity,
            partner_religion,
            partner_denomination,
            partner_education,
            partner_occupation,
            partner_smoke,
            partner_drink,
            partner_match_search,
            country,
            mother_tongue,
            partner_mother_tongue,
            dob,
            push_notification_status,
            email_notification_status

        )
        callApi.enqueue(object : Callback<UpdateMatchPreferenceResponse> {
            override fun onFailure(call: Call<UpdateMatchPreferenceResponse>, t: Throwable) {
                Log.e(logTAG, "UpdateMatchPreferenceResponse : error : ${t.toString()}")
                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                Toast.makeText(
                    this@EditMatchPreferencesActivity,
                    "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<UpdateMatchPreferenceResponse>,
                response: Response<UpdateMatchPreferenceResponse>
            ) {
                Log.d(logTAG, "UpdateMatchPreferenceResponse: success: ${response.body()!!.result}")

                if (progressBar.isShowing) {
                    progressBar.dismiss()
                }
                if (response.body() != null) {
                    if (response.body()!!.status.equals("1")) {

                        if (zipcode != "") {
                            zipcode_stg = zipcode
                            tv_prefs_profile_ans_1.text = zipcode
                        } else {
                            zipcode_stg = sessionManager.userDetails[DllSessionManager.USER_ZIP]!!
                        }

                        if (state != "") {
                            state_stg = state
                        } else {
                            state_stg = sessionManager.userDetails[DllSessionManager.USER_STATE]!!
                        }

                        if (city != "") {
                            city_stg = city
                        } else {
                            city_stg = sessionManager.userDetails[DllSessionManager.USER_CITY]!!
                        }

                        if (city != "" || state != "") {
                            tv_prefs_profile_ans_2.text = "$city_stg, $state_stg"
                        }

                        if (ethnicity != "") {
                            ethnicity_stg = ethnicity
                            tv_prefs_profile_ans_1.text = ethnicity

                        } else {
                            ethnicity_stg =
                                sessionManager.userDetails[DllSessionManager.USER_ETHNICITY]!!

                        }

                        if (religion != "") {
                            religion_stg = religion
                            tv_prefs_profile_ans_1.text = religion
                        } else {
                            religion_stg =
                                sessionManager.userDetails[DllSessionManager.USER_RELIGION]!!
                        }

                        if (mother_tongue != "") {
                            mother_tongue_stg = mother_tongue
                            tv_prefs_profile_ans_2.text = mother_tongue
                        } else {
                            mother_tongue_stg =
                                sessionManager.userDetails[DllSessionManager.USER_MOTHER_TONGUE]!!
                        }

                        if (education != "") {
                            education_stg = education
                            tv_prefs_profile_ans_1.text = education
                        } else {
                            education_stg =
                                sessionManager.userDetails[DllSessionManager.USER_EDUCATION]!!
                        }

                        if (smoke != "") {
                            smoke_stg = smoke
                            tv_prefs_profile_ans_1.text = smoke
                        } else {
                            smoke_stg = sessionManager.userDetails[DllSessionManager.USER_SMOKE]!!
                        }

                        if (drink != "") {
                            drink_stg = drink
                            tv_prefs_profile_ans_2.text = drink
                        } else {
                            drink_stg = sessionManager.userDetails[DllSessionManager.USER_DRINK]!!
                        }

                        if (occupation != "") {
                            occupation_stg = occupation
                            tv_prefs_profile_ans_2.text = occupation
                        } else {
                            occupation_stg =
                                sessionManager.userDetails[DllSessionManager.USER_OCCUPATION]!!
                        }

                        partner_age_min_stg = if (partner_age_min != "") {
                            partner_age_min
                        } else {
                            sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MIN]!!
                        }

                        partner_age_max_stg = if (partner_age_max != "") {
                            partner_age_max
                        } else {
                            sessionManager.userDetails[DllSessionManager.PARTNER_AGE_MAX]!!
                        }

                        if (partner_age_max != "" || partner_age_min != "") {
                            tv_prefs_matches_ans_1.text =
                                "$partner_age_min_stg to $partner_age_max_stg"
                            tv_prefs_matches_selected_1.text =
                                "$partner_age_min_stg to $partner_age_max_stg"
                        }

                        if (partner_match_distance != "") {
                            partner_match_distance_stg = partner_match_distance
                            tv_prefs_matches_ans_1.text = partner_match_distance
                            tv_prefs_matches_selected_1.text = partner_match_distance
                        } else {
                            partner_match_distance_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_DISTANCE]!!
                        }

                        if (partner_match_search != "") {
                            partner_match_search_stg = partner_match_search
                            tv_prefs_matches_ans_2.text = partner_match_search
                            tv_prefs_matches_selected_2.text = partner_match_search
                        } else {
                            partner_match_search_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_MATCH_SEARCH]!!
                        }

                        if (partner_ethnicity != "") {
                            partner_ethnicity_stg = partner_ethnicity
                            tv_prefs_matches_ans_1.text = partner_ethnicity
                            tv_prefs_matches_selected_1.text = partner_ethnicity
                        } else {
                            partner_ethnicity_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_ethnicity]!!
                        }

                        if (partner_religion != "") {
                            partner_religion_stg = partner_religion
                            tv_prefs_matches_ans_1.text = partner_religion
                            tv_prefs_matches_selected_1.text = partner_religion
                        } else {
                            partner_religion_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_religion]!!
                        }

                        if (partner_mother_tongue != "") {
                            partner_mother_tongue_stg = partner_mother_tongue
                            tv_prefs_matches_ans_2.text = partner_mother_tongue
                            tv_prefs_matches_selected_2.text = partner_mother_tongue
                        } else {
                            partner_mother_tongue_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_MOTHER_TONGUE]!!
                        }

                        if (partner_education != "") {
                            partner_education_stg = partner_education
                            tv_prefs_matches_ans_1.text = partner_education
                            tv_prefs_matches_selected_1.text = partner_education
                        } else {
                            partner_education_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_education]!!
                        }

                        if (partner_occupation != "") {
                            partner_occupation_stg = partner_occupation
                            tv_prefs_matches_ans_2.text = partner_occupation
                            tv_prefs_matches_selected_2.text = partner_occupation
                        } else {
                            partner_occupation_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_occupation]!!
                        }

                        if (partner_smoke != "") {
                            partner_smoke_stg = partner_smoke
                            tv_prefs_matches_ans_1.text = partner_smoke
                            tv_prefs_matches_selected_1.text = partner_smoke
                        } else {
                            partner_smoke_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_smoke]!!
                        }

                        if (partner_drink != "") {
                            partner_drink_stg = partner_drink
                            tv_prefs_matches_ans_2.text = partner_drink
                            tv_prefs_matches_selected_2.text = partner_drink
                        } else {
                            partner_drink_stg =
                                sessionManager.userDetails[DllSessionManager.PARTNER_drink]!!
                        }

                        if (country != "") {
                            country_stg = country
                        } else {
                            country_stg =
                                sessionManager.userDetails[DllSessionManager.USER_COUNTRY]!!
                        }

                        denomination_stg = ""
                        partner_denomination_stg = ""

                        if (dob != "") {
                            dob_stg = dob
                            tv_prefs_profile_ans_1.text = editDateFormatUi(dob)
                            tv_prefs_profile_ans_2.text = getAge(dob).toString()
                        } else {
                            val y_m_d = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                            y_m_d.isLenient = false
                            val m_d_y = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                            m_d_y.isLenient = false
                            dob_stg = sessionManager.userDetails[DllSessionManager.USER_DOB]!!

                        }

                        sessionManager.updateProfileMatchPreferences(
                            zipcode_stg,
                            state_stg,
                            city_stg,
                            ethnicity_stg,
                            religion_stg,
                            denomination_stg,
                            education_stg,
                            smoke_stg,
                            drink_stg,
                            occupation_stg,
                            partner_age_min_stg,
                            partner_age_max_stg,
                            partner_match_distance_stg,
                            partner_ethnicity_stg,
                            partner_religion_stg,
                            partner_denomination_stg,
                            partner_education_stg,
                            partner_occupation_stg,
                            partner_smoke_stg,
                            partner_drink_stg,
                            partner_match_search_stg,
                            country_stg,
                            mother_tongue_stg,
                            partner_mother_tongue_stg,
                            dob_stg
                        )

                        Log.d(logTAG, sessionManager.userDetails.toString())
                        Toast.makeText(
                            this@EditMatchPreferencesActivity,
                            "Saved Successfully!",
                            Toast.LENGTH_SHORT
                        ).show()

                    } else {
                        Toast.makeText(
                            this@EditMatchPreferencesActivity, "Failed to Save!", Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        this@EditMatchPreferencesActivity,
                        "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                    ).show()
                }

            }
        })

    }

    inner class EditPrefsAdapter(context: Context, list: ArrayList<EditPrefsPojo>) :
        RecyclerView.Adapter<EditPrefsAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<EditPrefsPojo> = ArrayList()
        private var selectedIndex = -1

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            position: Int
        ): EditPrefsAdapter.ViewHolder {

            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycle_match_prefs, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {

            return mList.size

        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        override fun onBindViewHolder(holder: EditPrefsAdapter.ViewHolder, position: Int) {

            val title = mList[position].getName()
            val image = mList[position].getImage()
            holder.bind(title!!, image!!)
            if (selectedIndex != -1 && position == selectedIndex) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ll_match_prefs.background =
                        mContext!!.resources.getDrawable(
                            R.drawable.edit_prefs_selected_bg,
                            mContext!!.theme
                        )

                } else {
                    holder.ll_match_prefs.background =
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_selected_bg)
                }
                holder.iv_prefs_type.setColorFilter(
                    ContextCompat.getColor(mContext!!, R.color.white),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ll_match_prefs.background =
                        mContext!!.resources.getDrawable(
                            R.drawable.edit_prefs_default_bg,
                            mContext!!.theme
                        )
                } else {
                    holder.ll_match_prefs.background =
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_default_bg)
                }
                holder.iv_prefs_type.colorFilter = null
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_prefs_name: TextView = view.findViewById(R.id.tv_prefs_name)
            var iv_prefs_type: ImageView = view.findViewById(R.id.iv_prefs_type)
            var ll_match_prefs: RelativeLayout = view.findViewById(R.id.ll_match_prefs)


            @SuppressLint("NewApi")
            fun bind(name: String, imageId: Int) {

                tv_prefs_name.text = name
                iv_prefs_type.setImageResource(imageId)

            }

        }

        fun getItem(position: Int): String? {
            return mList[position].getName()
        }

        fun setSelectedIndex(index: Int) {
            selectedIndex = index
            notifyDataSetChanged()
        }

        fun getSelectedIndex(): Int {
            return selectedIndex
        }

    }

    override fun onBackPressed() {
        val intent = Intent(this@EditMatchPreferencesActivity, DashboardActivity::class.java)
        intent.putExtra("from_list", "profile_photos")
        startActivity(intent)
    }

    private fun editDateFormatServer(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d(logTAG, "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun editDateFormatUi(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d(logTAG, "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun getAge(dateOfBirth: String): Int {

        var date_only: Date? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        try {
            date_only = sdf.parse(dateOfBirth)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val c = Calendar.getInstance()
        c.time = date_only
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob.set(mYear, mMonth, mDay)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }
        val ageInt = age

        return ageInt
    }

    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,userId_session)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD", PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@EditMatchPreferencesActivity);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@EditMatchPreferencesActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString())
                                sliderLayout.addSlider(textSliderView)
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@EditMatchPreferencesActivity);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }


}