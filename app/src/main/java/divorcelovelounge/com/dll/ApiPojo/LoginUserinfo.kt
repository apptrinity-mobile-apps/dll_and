package divorcelovelounge.com.dll.ApiPojo

class LoginUserinfo {

    val id: String? = null
    val gender: String? = null
    val looking_for: String? = null
    val first_name: String? = null
    val username: String? = null
    val password: String? = null
    val phone_number: String? = null
    val email: String? = null
    val zipcode: String? = null
    val state: String? = null
    val referal_from: String? = null
    val children: String? = null
    val city: String? = null
    val dob: String? = null
    val ethnicity: String? = null
    val religion: String? = null
    val denomination: String? = null
    val education: String? = null
    val occupation: String? = null
    val income: String? = null
    val smoke: String? = null
    val drink: String? = null
    val height_ft: String? = null
    val height_inc: String? = null
    val height_cms: String? = null
    val passionate: String? = null
    val leisure_time: String? = null
    val thankful_1: String? = null
    val thankful_2: String? = null
    val thankful_3: String? = null
    val partner_age_min: String? = null
    val partner_age_max: String? = null
    val partner_match_distance: String? = null
    val partner_match_search: String? = null
    val partner_ethnicity: String? = null
    val partner_religion: String? = null
    val partner_denomination: String? = null
    val partner_education: String? = null
    val partner_occupation: String? = null
    val partner_smoke: String? = null
    val partner_drink: String? = null
    val photo1: String? = null
    val photo1_approve: String? = null
    val photo2: String? = null
    val photo2_approve: String? = null
    val photo3: String? = null
    val photo3_approve: String? = null
    val photo4: String? = null
    val photo4_approve: String? = null
    val photo5: String? = null
    val photo5_approve: String? = null
    val photo6: String? = null
    val photo6_approve: String? = null
    val photo7: String? = null
    val photo7_approve: String? = null
    val photo8: String? = null
    val photo8_approve: String? = null
    val photo9: String? = null
    val photo9_approve: String? = null
    val photo10: String? = null
    val photo10_approve: String? = null
    val status: String? = null
    val signup_step: String? = null
    val provider: String? = null
    val country: String? = null
    val mtongue_name: String? = null
    val partner_mtongue_name: String? = null

    /*     "id": "2",
        "gender": "male",
        "looking_for": "male",
        "first_name": "ravi",
        "email": "ravi@indobytes.com",
        "password": "e10adc3949ba59abbe56e057f20f883e",
        "zipcode": "95110",
        "state": "CA",
        "referal_from": "41",
        "children": "1",
        "city": "hyderabad",
        "dob": "3-1-1985",
        "ethnicity": "Indian",
        "religion": "Christian",
        "denomination": "Telugu",
        "education": "Bachelors",
        "occupation": "Software Engineer",
        "income": "$60,000 - $125,000",
        "smoke": "Never",
        "drink": "Never",
        "height": "163",
        "passionate": "passinate",
        "leisure_time": "leisure time",
        "thankful_1": "thank ful1",
        "thankful_2": "thank ful2",
        "thankful_3": "thank ful3",
       "partner_age_min": "0",
        "partner_age_max": "0",
        "partner_match_distance": null,
        "partner_match_search": null,
        "partner_ethnicity": null,
        "partner_religion": null,
        "partner_education": null,
        "partner_occupation": null,
        "partner_smoke": null,
        "partner_drink": null,
        "photo1": null,
        "photo1_approve": "UNAPPROVED",
        "photo2": null,
        "photo2_approve": "UNAPPROVED",
        "photo3": null,
        "photo3_approve": "UNAPPROVED",
        "photo4": null,
        "photo4_approve": "UNAPPROVED",
        "photo5": null,
        "photo5_approve": "UNAPPROVED",
        "photo6": null,
        "photo6_approve": "UNAPPROVED",
        "status": "1",
        "signup_step": "1"
*/

}