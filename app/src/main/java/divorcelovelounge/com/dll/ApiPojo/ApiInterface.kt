package divorcelovelounge.com.dll.ApiPojo

import divorcelovelounge.com.dll.Pojos.quizHashmap
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import java.util.ArrayList
import java.util.concurrent.TimeUnit

interface ApiInterface {
    @FormUrlEncoded
    @POST("login")
    fun loginApi(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String
    ): Call<CheckLoginResponse>

    @FormUrlEncoded
    @POST("matches")
    fun matchesApi(
        @Field("user_id") user_id: String,
        @Field("mypreference") mypreference: String,
        @Field("height_max") height_max: String,
        @Field("height_min") height_min: String,
        @Field("city") city: String,
        @Field("min_age") min_age: String,
        @Field("max_age") max_age: String,
        @Field("current_country") gps_country: String,
        @Field("current_state") gps_state: String,
        @Field("current_city") gps_city: String/*,
        @Field("page") page: String*/

    ): Call<CheckMatchesResponse>


    @FormUrlEncoded
    @POST("myFavourites")
    fun myFavouritesApi(@Field("user_id") user_id: String): Call<myFavouriteResponse>

    @FormUrlEncoded
    @POST("FavouritedMe")
    fun FavouritedMeApi(@Field("user_id") user_id: String): Call<myFavouriteResponse>

    @FormUrlEncoded
    @POST("interestedMe")
    fun interestedMeApi(@Field("user_id") user_id: String): Call<myFavouriteResponse>

    @FormUrlEncoded
    @POST("myInterest")
    fun myInterestApi(@Field("user_id") user_id: String): Call<myFavouriteResponse>


    @FormUrlEncoded
    @POST("whoViewedMe")
    fun whoViewedMeApi(@Field("user_id") user_id: String): Call<myFavouriteResponse>

    @FormUrlEncoded
    @POST("sendInterest")
    fun sendInterestApi(@Field("user_id") user_id: String, @Field("id") id: String): Call<ContactusResponse>


    @FormUrlEncoded
    @POST("like")
    fun likeApi(@Field("user_id") user_id: String, @Field("id") id: String): Call<ContactusResponse>


    @FormUrlEncoded
    @POST("iterestAccept")
    fun iterestAcceptApi(@Field("user_id") user_id: String, @Field("id") id: String): Call<ContactusResponse>

    @FormUrlEncoded
    @POST("iterestReject")
    fun iterestRejectApi(@Field("user_id") user_id: String, @Field("id") id: String): Call<ContactusResponse>


    @FormUrlEncoded
    @POST("contactUs")
    fun contactUsApi(@Field("name") name: String, @Field("phone") phone: String, @Field("email") email: String, @Field("msg") msg: String): Call<ContactusResponse>

    @FormUrlEncoded
    @POST("getChatList")
    fun getChatListApi(@Field("user_id") user_id: String): Call<ChatListResponse>


    @FormUrlEncoded
    @POST("register")
    fun registerApi(
        @Field("gender") gender: String,
        @Field("looking_for") looking_for: String,
        @Field("first_name") first_name: String,
        @Field("dob") dob: String,
        @Field("zipcode") zipcode: String,
        @Field("country") country: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("referal_from") referal_from: String,
        @Field("phone_number") phone_number: String,
        @Field("username") username: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String,
        @Field("terms_conditions") terms_conditions: String
    ): Call<RegisterprofileResponse>


    @FormUrlEncoded
    @POST("socialregister")
    fun socialregisterApi(
        @Field("gender") gender: String,
        @Field("looking_for") looking_for: String,
        @Field("dob") dob: String,
        @Field("zipcode") zipcode: String,
        @Field("country") country: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("user_id") user_id: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String,
        @Field("terms_conditions") terms_conditions: String,
        @Field("username") username: String
    ): Call<SocialLoginresponse>


    @FormUrlEncoded
    @POST("profileSetup")
    fun profileSetupApi(
        @Field("user_id") user_id: String,
        @Field("children") children: String,
        //@Field("city") city: String,
        // @Field("dob") dob_month: String,
        @Field("ethnicity") ethnicity: String,
        @Field("religion") religion: String,
        @Field("mtongue_name") mtongue_name: String,
        @Field("education") education: String,
        @Field("occupation") occupation: String,
        @Field("income") income: String,
        @Field("smoke") smoke: String,
        @Field("drink") drink: String,
        @Field("height_ft") height_ft: String,
        @Field("height_inc") height_inc: String,
        @Field("height_cms") height_cms: String,
        @Field("passionate") passionate: String,
        @Field("leisure_time") leisure_time: String,
        @Field("thankful_1") thankful_1: String,
        @Field("thankful_2") thankful_2: String,
        @Field("thankful_3") thankful_3: String
    ): Call<ProfileSetupResponse>

    @FormUrlEncoded
    @POST("profileMatch")
    fun profileMatchApi(
        @Field("user_id") user_id: String,
        @Field("partner_ethnicity") partner_ethnicity: String,
        @Field("partner_religion") partner_religion: String,
        @Field("partner_mtongue_name") partner_mtongue_name: String,
        @Field("partner_education") partner_education: String,
        @Field("partner_age_min") partner_age_min: String,
        @Field("partner_age_max") partner_age_max: String,
        @Field("partner_match_distance") partner_match_distance: String,
        @Field("partner_match_search") partner_match_search: String,
        @Field("partner_occupation") partner_occupation: String,
        @Field("partner_smoke") partner_smoke: String,
        @Field("partner_drink") partner_drink: String
    ): Call<ProfileMatchResponse>


    //    @FormUrlEncoded
    @POST("quiz")
    fun quizApi(): Call<QuizResponse>

    @FormUrlEncoded
    @POST("saveQuiz")
    fun saveQuiz(@Field("user_id") user_id: String, @Field("quiz") resultarray: JSONObject): Call<QuizSubmitResponse>

    @POST("registerProfile")
    fun registerProfileApi(): Call<RegisterProfileSetupResponse>

    @POST("getReligion")
    fun getReligionApi(): Call<ReligionResponse>

    @FormUrlEncoded
    @POST("getCaste")
    fun getCasteApi(@Field("religion_id") religion_id: String): Call<CasteResponse>

    @FormUrlEncoded
    @POST("getProfilePhotos")
    fun getProfilePhotosApi(@Field("user_id") user_id: String): Call<ProfilePhotosResponse>

    @FormUrlEncoded
    @POST("changePhoto")
    fun changePhotoApi(
        @Field("user_id") user_id: String, @Field("photo_no") photo_no: String, @Field("photo") photo: String, @Field(
            "photo_name"
        ) photo_name: String
    ): Call<UploadPhotoResponse>

    @FormUrlEncoded
    @POST("setProfilePic")
    fun setProfilePicApi(@Field("user_id") user_id: String, @Field("photo_no") photo_no: String): Call<UploadPhotoResponse>


    @FormUrlEncoded
    @POST("deletePhoto")
    fun deletePhotoApi(@Field("user_id") user_id: String, @Field("photo_no") photo_no: String, @Field("photo_name") photo_name: String): Call<UploadPhotoResponse>


    @GET("getMembershipList")
    fun getMembershipListApi(): Call<MembershipListResponse>

    @FormUrlEncoded
    @POST("makePayment")
    fun makemembershippayment(
        @Field("user_id") user_id: String,
        @Field("payment_type") payment_type: String,
        @Field("plan_id") plan_id: String,
        @Field("duration_id") duration_id: String,
        @Field("stripeToken") stripeToken: String,
        @Field("offercode") offercode: String,
        @Field("type") type: String,
        @Field("value") value: String
    ): Call<UpdateMatchPreferenceResponse>

    @FormUrlEncoded
    @POST("myPlan")
    fun membershipmyplan(@Field("user_id") user_id: String): Call<MemberMyPlanResponse>

    @FormUrlEncoded
    @POST("getCities")
    fun getCities(@Field("state_code") state_code: String): Call<CitiesResponse>

    @GET("getCountries")
    fun getCountries(): Call<CountryListResponse>

    @FormUrlEncoded
    @POST("getMultipleCastes")
    fun getMultipleCaste(@Field("religion_id") religion_id: String): Call<CasteResponse>


    @FormUrlEncoded
    @POST("sociallogin")
    fun socialloginApi(
        @Field("email") email: String,
        @Field("provider") provider: String,
        @Field("name") name: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String
    ): Call<SocialLoginresponse>


    @FormUrlEncoded
    @POST("updatematchPreference")
    fun updateMatchPreferences(
        @Field("user_id") user_id: String,
        @Field("zipcode") zipcode: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("ethnicity") ethnicity: String,
        @Field("religion") religion: String,
        @Field("denomination") denomination: String,
        @Field("education") education: String,
        @Field("income") income: String,
        @Field("smoke") smoke: String,
        @Field("drink") drink: String,
        @Field("height_ft") height_ft: String,
        @Field("height_inc") height_inc: String,
        @Field("height_cms") height_cms: String,
        @Field("passionate") passionate: String,
        @Field("leisure_time") leisure_time: String,
        @Field("thankful_1") thankful_1: String,
        @Field("thankful_2") thankful_2: String,
        @Field("thankful_3") thankful_3: String,
        @Field("occupation") occupation: String,
        @Field("partner_age_min") partner_age_min: String,
        @Field("partner_age_max") partner_age_max: String,
        @Field("partner_match_distance") partner_match_distance: String,
        @Field("partner_ethnicity") partner_ethnicity: String,
        @Field("partner_religion") partner_religion: String,
        @Field("partner_denomination") partner_denomination: String,
        @Field("partner_education") partner_education: String,
        @Field("partner_occupation") partner_occupation: String,
        @Field("partner_smoke") partner_smoke: String,
        @Field("partner_drink") partner_drink: String,
        @Field("partner_match_search") partner_match_search: String,
        @Field("country") country: String,
        @Field("mtongue_name") mtongue_name: String,
        @Field("partner_mtongue_name") partner_mtongue_name: String,
        @Field("dob") dob: String,
        @Field("push_notification_status") push_notification_status: String,
        @Field("email_notification_status") email_notification_status: String
    ): Call<UpdateMatchPreferenceResponse>


    @FormUrlEncoded
    @POST("partnerProfile")
    fun getPartnerProfile(
        @Field("user_id") user_id: String,
        @Field("match_id") match_id: String
    ): Call<PartnerProfileResponse>


    @FormUrlEncoded
    @POST("blockUser")
    fun blockUserApi(
        @Field("user_id") user_id: String,
        @Field("id") id: String
    ): Call<BlockUserResponse>


    @FormUrlEncoded
    @POST("viewContact")
    fun viewContactApi(
        @Field("user_id") user_id: String,
        @Field("id") id: String
    ): Call<BlockUserResponse>

    @FormUrlEncoded
    @POST("blockList")
    fun getblockListApi(@Field("user_id") user_id: String): Call<BlockListResponse>

    @FormUrlEncoded
    @POST("contactList")
    fun contactListApi(@Field("user_id") user_id: String): Call<BlockListResponse>

    @FormUrlEncoded
    @POST("messagesInboxList")
    fun messagesInboxListApi(@Field("user_id") user_id: String): Call<InboxmeResponse>

    @FormUrlEncoded
    @POST("messengersList")
    fun messengersListApi(@Field("user_id") user_id: String): Call<InboxmeResponse>

    @FormUrlEncoded
    @POST("messagesSentList")
    fun messagesSentListApi(@Field("user_id") user_id: String): Call<InboxmeResponse>

    @FormUrlEncoded
    @POST("messagesImportantList")
    fun messagesImportantListApi(@Field("user_id") user_id: String): Call<InboxmeResponse>

    @FormUrlEncoded
    @POST("messagesTrashList")
    fun messagesTrashListApi(@Field("user_id") user_id: String): Call<InboxmeResponse>


    @FormUrlEncoded
    @POST("makeImportant")
    fun makeImportantApi(@Field("msg_id") msg_id: String, @Field("imp_status") imp_status: String, @Field("msg_type") msg_type: String): Call<BlockListResponse>


    @FormUrlEncoded
    @POST("makeTrash")
    fun makeTrashApi(@Field("msg_id") msg_id: String, @Field("trash_status") trash_status: String, @Field("msg_type") msg_type: String): Call<BlockListResponse>

    @FormUrlEncoded
    @POST("composeMessage")
    fun composeMessageApi(@Field("user_id") msg_id: String, @Field("send_to") trash_status: String, @Field("content") msg_type: String): Call<BlockListResponse>


    @FormUrlEncoded
    @POST("composeUsersList")
    fun composeUsersListApi(@Field("user_id") msg_id: String): Call<ComposeUsersListResponse>

    @FormUrlEncoded
    @POST("forgotPassword")
    fun forgotPasswordApi(@Field("email") email: String): Call<BlockListResponse>

    @GET("mothertongue")
    fun getMotherTongueApi(): Call<MotherTongueResponse>

    @FormUrlEncoded
    @POST("checkPromocode")
    fun checkPromocodeApi(@Field("user_id") user_id: String, @Field("promocode") promocode: String): Call<PromoCodeResponse>

    @FormUrlEncoded
    @POST("checkMemberStatus")
    fun checkMemberStatusApi(@Field("user_id") user_id: String): Call<PromoCodeResponse>


    @FormUrlEncoded
    @POST("replyMessage")
    fun replyMessageApi(
        @Field("msg_id") msg_id: String,
        @Field("msg_from") msg_from: String, @Field("msg_to") msg_to: String, @Field("msg_content") msg_content: String, @Field(
            "msg_status"
        ) msg_status: String
    ): Call<ReplyMessageResponse>

    @FormUrlEncoded
    @POST("getMsgDeatilview")
    fun getMsgDeatilviewApi(@Field("msg_id") msg_id: String,@Field("user_id") user_id: String,@Field("match_id") match_id: String): Call<MessageDetailviewResponse>

    @FormUrlEncoded
    @POST("memberMessages")
    fun memberMessagesApi(@Field("date_id") msg_id: String,@Field("user_id") user_id: String,@Field("member_id") match_id: String): Call<MessageDetailviewResponse>


    @GET("supportTypes")
    fun getSupportTypesApi(): Call<SupportTypesResponse>

    @FormUrlEncoded
    @POST("supportInsert")
    fun supportInsertApi(
        @Field("support_id") support_id: String,
        @Field("message") message: String,
        @Field("user_id") user_id: String
    ): Call<SupportTypesResponse>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePasswordApi(
        @Field("user_id") user_id: String,
        @Field("new_password") new_password: String,
        @Field("old_password") old_password: String
    ): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("logout")
    fun logoutApi(
        @Field("user_id") user_id: String
    ): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("quizDisplay")
    fun quizDisplayApi(
        @Field("user_id") user_id: String
    ): Call<QuizDisplayResponse>

    @FormUrlEncoded
    @POST("updateQuiz")
    fun updateQuizApi(
        @Field("user_id") user_id: String,
        @Field("question_id") question_id: String,
        @Field("type") type: String,
        @Field("quiz") result_array: JSONArray
    ): Call<QuizDisplayResponse>

    @FormUrlEncoded
    @POST("getUserCounts")
    fun getUserCountsApi(
        @Field("user_id") user_id: String
    ): Call<UserInterestCountResponse>


    @FormUrlEncoded
    @POST("getUsermessagesCounts")
    fun getUsermessagesCountsApi(
        @Field("user_id") user_id: String
    ): Call<MessagecountResponse>

    @FormUrlEncoded
    @POST("checkEmail")
    fun checkEmailApi(
        @Field("email") email: String
    ): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("checkUsername")
    fun checkUsernameAPI(
        @Field("username") Uname: String
    ): Call<ChangePasswordResponse>


    @FormUrlEncoded
    @POST("updateLoginStatus")
    fun updateLoginStatusAPI(
        @Field("user_id") UiD: String,
        @Field("login_status") login_status: String
    ): Call<ChangePasswordResponse>
@FormUrlEncoded
    @POST("activeUserList")
    fun activeUserListAPI(
        @Field("user_id") UiD: String
    ): Call<ActiveUsersResponse>


    @FormUrlEncoded
    @POST("getXmppchatList")
    fun getXmppchatListAPI(
        @Field("username") Uname: String
    ): Call<XmppListResponse>

/*https://divorcelovelounge.com/api/addslist*/

    @FormUrlEncoded
    @POST("addslist")
    fun adslistAPI(
        @Field("country") country: String,@Field("state") state: String,@Field("city") city: String,@Field("page") page: String,@Field("user_id") user_id: String
    ): Call<AdsListResponse>

    @GET("chatStatus")
    fun chatStatusAPI(): Call<ChatStatusResponse>



    companion object Factory {
//        val BASE_URL = "http://3.17.42.133/staging/api/"
        val BASE_URL = "https://divorcelovelounge.com/api/"
     // val BASE_URL = "http://18.190.90.133/dll/api/"


        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}