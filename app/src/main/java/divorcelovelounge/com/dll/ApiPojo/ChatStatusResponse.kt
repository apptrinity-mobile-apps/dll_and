package divorcelovelounge.com.dll.ApiPojo

/*{
    "status": "1",
    "result": "success",
    "chat_status": "0"
}*/

class ChatStatusResponse{
    val status: String=""
    val result: String=""
    val chat_status: String=""
    val message: String=""
}