package divorcelovelounge.com.dll.ApiPojo

import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem

class ChatListResponse {

    val status: String=""
    val result: String=""
    val data: ArrayList<CardItem>? = null
}