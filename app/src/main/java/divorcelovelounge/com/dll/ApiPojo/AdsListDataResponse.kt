package divorcelovelounge.com.dll.ApiPojo

class AdsListDataResponse {
/*{
    "status": "1",
    "result": "success",
    "data": [
        {
            "client_name": "1",
            "country_name": "India",
            "state_name": "Andhra Pradesh",
            "city_name": "Adoni",
            "page_in": "Membership Payment,Profile",
            "add_image": "837351_pictures-900X350.png",
            "add_link": "fyhdkutgyughk",
            "add_type": "Custom",
            "status": "APPROVED"
        }
    ]
}*/

    val client_name: String? = null
    val country_name: String? = null
    val state_name: String? = null
    val city_name: String? = null
    val page_in: String? = null
    val add_image: String? = null
    val add_link: String? = null
    val add_type: String? = null
    val start_date: String? = null
    val end_date: String? = null
    val status: String? = null
}