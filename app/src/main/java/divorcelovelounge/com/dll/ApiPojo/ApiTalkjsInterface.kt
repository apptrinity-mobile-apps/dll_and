package divorcelovelounge.com.dll.ApiPojo

import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*
import java.util.concurrent.TimeUnit

interface ApiTalkjsInterface {


    @Headers("Authorization: Bearer sk_live_EXkW3yEcNrqQ9nlgg5SnvJ4g")
    @GET("{userId}/conversations?unreadsOnly=true")
    fun getTalkJsApi(@Path("userId") userId: String?): Call<TalkJsDataResponse>


    companion object Factory {
        val BASE_URL = "https://api.talkjs.com/v1/M5iybBK9/users/"


        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        fun create(): ApiTalkjsInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiTalkjsInterface::class.java)
        }
    }
}