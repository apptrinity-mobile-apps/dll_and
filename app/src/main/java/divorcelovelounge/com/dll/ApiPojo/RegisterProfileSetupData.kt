package divorcelovelounge.com.dll.ApiPojo

class RegisterProfileSetupData {
    val question_id:String = ""
    val register_type:String = ""
    val question:String = ""
    val option_type:String = ""
    val title:String = ""
    val answer:ArrayList<RegisterProfileSetupAnswer>? = null
}