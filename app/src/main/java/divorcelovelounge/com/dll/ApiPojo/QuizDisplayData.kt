package divorcelovelounge.com.dll.ApiPojo

class QuizDisplayData {
    var question_id: String? = null
    var question: String? = null
    var option_type: String? = null
    val answer_list: ArrayList<QuizDisplayAnswerData>? = null
}
