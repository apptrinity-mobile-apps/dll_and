package divorcelovelounge.com.dll.ApiPojo

class CitiesDataResponse {
    val city_id: String = ""
    val country_code: String = ""
    val state_code: String = ""
    val district_id: String = ""
    val city_name: String = ""
    val status: String = ""
}
