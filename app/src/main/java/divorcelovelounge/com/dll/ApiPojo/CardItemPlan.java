package divorcelovelounge.com.dll.ApiPojo;


import java.util.ArrayList;

public class CardItemPlan {

    private String id;
    private String plan_type;
    private String plan_amount;
    private String discount_amount;
    private String plan_amount_type;
    private String plan_duration;
    private String plan_contacts;
    private String profile;
    private String favoritedme;
    private String viewedme;
    private String plan_msg;
    private String interest;
    private String chat;
    private String plan_offers;
    private String package_type;
    private String status;
    private String created_date;
    private ArrayList<Durations_list> durations;

    public CardItemPlan(String id, String plan_type, String plan_amount, String discount_amount, String plan_amount_type, String plan_duration, String plan_contacts, String profile, String favoritedme, String viewedme, String plan_msg, String interest, String chat, String plan_offers, String package_type, String status, String created_date, ArrayList<Durations_list> durations) {
        this.id = id;
        this.plan_type = plan_type;
        this.plan_amount = plan_amount;
        this.discount_amount = discount_amount;
        this.plan_amount_type = plan_amount_type;
        this.plan_duration = plan_duration;
        this.plan_contacts = plan_contacts;
        this.profile = profile;
        this.favoritedme = favoritedme;
        this.viewedme = viewedme;
        this.plan_msg = plan_msg;
        this.interest = interest;
        this.chat = chat;
        this.plan_offers = plan_offers;
        this.package_type = package_type;
        this.status = status;
        this.created_date = created_date;
        this.durations = durations;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(String plan_type) {
        this.plan_type = plan_type;
    }

    public String getPlan_amount() {
        return plan_amount;
    }

    public void setPlan_amount(String plan_amount) {
        this.plan_amount = plan_amount;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getPlan_amount_type() {
        return plan_amount_type;
    }

    public void setPlan_amount_type(String plan_amount_type) {
        this.plan_amount_type = plan_amount_type;
    }

    public String getPlan_duration() {
        return plan_duration;
    }

    public void setPlan_duration(String plan_duration) {
        this.plan_duration = plan_duration;
    }

    public String getPlan_contacts() {
        return plan_contacts;
    }

    public void setPlan_contacts(String plan_contacts) {
        this.plan_contacts = plan_contacts;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getFavoritedme() {
        return favoritedme;
    }

    public void setFavoritedme(String favoritedme) {
        this.favoritedme = favoritedme;
    }

    public String getViewedme() {
        return viewedme;
    }

    public void setViewedme(String viewedme) {
        this.viewedme = viewedme;
    }

    public String getPlan_msg() {
        return plan_msg;
    }

    public void setPlan_msg(String plan_msg) {
        this.plan_msg = plan_msg;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public String getPlan_offers() {
        return plan_offers;
    }

    public void setPlan_offers(String plan_offers) {
        this.plan_offers = plan_offers;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public ArrayList<Durations_list> getDurations() {
        return durations;
    }

    public void setDurations(ArrayList<Durations_list> durations) {
        this.durations = durations;
    }
}
