package divorcelovelounge.com.dll.Helper

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.AbsListView
import android.widget.AbsListView.OnScrollListener
import android.widget.LinearLayout
import android.widget.ScrollView


class StretchTopScrollView : ScrollView, OnScrollListener {

    var topView: View? = null
        private set
    var bottomView: View? = null
        private set
    private var mNormalHeight: Int = 0
    private var mMaxHeight: Int = 0
    private var mChangeListener: onOverScrollChanged? = null
    private var mFactor = 1.6f

    private val touchListener = object : OnTouchEventListener {
        override fun onTouchEvent(ev: MotionEvent) {
            if (ev.action == MotionEvent.ACTION_UP) {
                if (topView != null && topView!!.height > mNormalHeight) {
                    val animation = ResetAnimation(topView!!, mNormalHeight)
                    animation.duration = 400
                    topView!!.startAnimation(animation)
                }
            }
        }
    }

    private interface OnTouchEventListener {
        fun onTouchEvent(ev: MotionEvent)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context) : super(context) {}

    fun setFactor(f: Float) {
        mFactor = f

        topView!!.postDelayed({
            mNormalHeight = topView!!.height
            mMaxHeight = (mNormalHeight * mFactor).toInt()
        }, 50)
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()

        require(childCount <= 1) { "Root layout must be a LinearLayout, and only one child on this view!" }

        require(!(childCount == 0 || getChildAt(0) !is LinearLayout)) { "Root layout is not a LinearLayout!" }

        if (childCount == 1 && getChildAt(0) is LinearLayout) {
            val parent = getChildAt(0) as LinearLayout

            require(parent.childCount == 2) { "Root LinearLayout's has not EXACTLY two Views!" }
            topView = parent.getChildAt(0)
            bottomView = parent.getChildAt(1)

            topView!!.postDelayed({
                mNormalHeight = topView!!.height
                mMaxHeight = (mNormalHeight * mFactor).toInt()
            }, 50)
        }

    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
    }

    override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {}

    override fun overScrollBy(
        deltaX: Int, deltaY: Int, scrollX: Int,
        scrollY: Int, scrollRangeX: Int, scrollRangeY: Int,
        maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean
    ): Boolean {

        if (scrollY == 0) {
            //down, zoom in
            if (deltaY < 0 && topView!!.layoutParams.height + Math.abs(deltaY) > mMaxHeight) {
                topView!!.layoutParams.height = mMaxHeight
            } else if (deltaY < 0 && topView!!.layoutParams.height + Math.abs(deltaY) <= mMaxHeight) {
                topView!!.layoutParams.height += Math.abs(deltaY)
            } else if (deltaY > 0 && topView!!.layoutParams.height - Math.abs(deltaY) < mNormalHeight) {
                topView!!.layoutParams.height = mNormalHeight
            } else if (deltaY > 0 && topView!!.layoutParams.height - Math.abs(deltaY) >= mNormalHeight) {
                topView!!.layoutParams.height -= Math.abs(deltaY)
            }//up, zoom out
        }

        if (mChangeListener != null)
            mChangeListener!!.onChanged(
                (mMaxHeight - topView!!.layoutParams.height.toFloat()) / (mMaxHeight - mNormalHeight)
            )

        if (deltaY != 0 && scrollY == 0) {
            topView!!.requestLayout()
            bottomView!!.requestLayout()
        }

        if (topView!!.layoutParams.height == mNormalHeight) {
            super.overScrollBy(
                deltaX, deltaY, scrollX,
                scrollY, scrollRangeX, scrollRangeY,
                maxOverScrollX, maxOverScrollY, isTouchEvent
            )
        }

        return true
    }

    override fun onScroll(
        view: AbsListView, firstVisibleItem: Int,
        visibleItemCount: Int, totalItemCount: Int
    ) {
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        touchListener.onTouchEvent(ev)
        return super.onTouchEvent(ev)
    }

    interface onOverScrollChanged {
        fun onChanged(v: Float)
    }

    fun setChangeListener(changeListener: onOverScrollChanged) {
        mChangeListener = changeListener
    }

    inner class ResetAnimation internal constructor(
        internal var mView: View,
        internal var targetHeight: Int
    ) : Animation() {
        internal var originalHeight: Int = 0
        internal var extraHeight: Int = 0

        init {
            originalHeight = mView.height
            extraHeight = this.targetHeight - originalHeight
        }

        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            val newHeight = (targetHeight - extraHeight * (1 - interpolatedTime)).toInt()
            mView.layoutParams.height = newHeight
            mView.requestLayout()

            if (mChangeListener != null)
                mChangeListener!!.onChanged(
                    (mMaxHeight - topView!!.layoutParams.height.toFloat()) / (mMaxHeight - mNormalHeight)
                )

        }
    }
}
