package divorcelovelounge.com.dll.Helper

import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import divorcelovelounge.com.dll.DashboardActivity
import divorcelovelounge.com.dll.R
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL


class FirebaseMessageService : FirebaseMessagingService() {
    private val TAG = "FirebaseMessageService"
    var bitmap: Bitmap? = null
    var message: String? = null
    lateinit var sessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        sessionManager = DllSessionManager(this)
        user_details = sessionManager.userDetails

        if (user_details.size > 0) {

            if (sessionManager.isLoggedIn) {

                Log.e("dataChat", remoteMessage!!.data.toString())

                if (remoteMessage.data.isNotEmpty()) {
                    try {
                        val params = remoteMessage.data
                        val object1 = JSONObject(params)
                        Log.e("JSON_OBJECT", object1.toString())
                        //  bitmap = getBitmapFromUrl(object1.getString("image"))
                        //val type = object1.getString("type")
                        sendNotification(remoteMessage /*bitmap!!,message*/)

                        try {
                            val map = remoteMessage.data
                            handleDataMessage(map)
                        } catch (e: Exception) {
                            Log.e(TAG, "Exception: " + e.message)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }
        }



    }


    private fun handleDataMessage(map: Map<String, String>) {
        Log.e(TAG, "push json: " + map.toString())

        try {

            val type = map["type"]
            val alert = map["alert"]

            /* if (type == "interest") {

                 val intent = Intent(applicationContext, DashboardActivity::class.java)
                 intent.putExtra("from_list", "notification")
                 startActivity(intent)
                 val stackBuilder = TaskStackBuilder.create(this)
                 stackBuilder.addNextIntent(intent)

                this.startActivity(intent)

             } else if (type == "messages") {

                 val intent = Intent(applicationContext, MessagesActivity::class.java)
                 startActivity(intent)
                 val stackBuilder = TaskStackBuilder.create(this)
                 stackBuilder.addNextIntent(intent)

                 this.startActivity(intent)

             }*/

        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }


    }


    fun sendNotification(remoteMessage: RemoteMessage?) {
        Log.e("remoteMessage", remoteMessage.toString())



        Log.e("remoteMessage_rrrr", remoteMessage.toString())
        var alert = ""
        var type = ""
        if (remoteMessage!!.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                val object1 = JSONObject(params)
                Log.e("JSON_OBJECT", object1.toString())
              //  Log.e("@@1", object1.getString("title").toString())
               // Log.e("@@2", object1.getJSONObject("title").toString())
                /*if(object1.getString("title").toString().equals("infolinx")){
                    Log.e("@@1", object1.getString("title").toString())
                    alert = object1.getString("message")
                    type = "talkJs"

                }else{*/
                    Log.e("JSON_else", object1.toString())
                    alert = object1.getString("alert")
                    type = object1.getString("type")
               // }
                //if(object1.getJSONObject("talkjs"))


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = "channel-01"
        val channelName = "DLL"
        val importance = NotificationManager.IMPORTANCE_HIGH

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.dll_appicon)
            .setContentTitle("DLL")
            .setSound(defaultSoundUri)
            .setContentText(alert)
            .setAutoCancel(true)

        if(type == "interest"){
            val resultIntent = Intent(this, DashboardActivity::class.java)
            resultIntent.putExtra("from_list", "notification")
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())

        }else if(type == "messages"){



            val resultIntent = Intent(this, DashboardActivity::class.java)
            resultIntent.putExtra("from_list", "messages_list")
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())


        }else if(type == "like"){


            val resultIntent = Intent(this, DashboardActivity::class.java)
            resultIntent.putExtra("from_list", "notification")
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())


        }else if(type == "talkJs"){


            val resultIntent = Intent(this, DashboardActivity::class.java)
            resultIntent.putExtra("from_list", "chat_list")
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())


        }

    }



    private fun getBitmapFromUrl(imageUrl: String): Bitmap? {
        return try {
            val url = URL(imageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)

        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    private fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }

}