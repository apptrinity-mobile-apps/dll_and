package divorcelovelounge.com.dll.Helper

import android.widget.Filter
import divorcelovelounge.com.dll.ApiPojo.All_Matches
import divorcelovelounge.com.dll.Fragments.InterestMe
import java.util.ArrayList

class CustomFilter(internal var filterList: ArrayList<All_Matches>, internal var adapter: InterestMe.IMDiscoverListAdapter) :
    Filter() {
    //FILTERING OCURS
    override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
        var constraint = constraint
        val results = Filter.FilterResults()
        //CHECK CONSTRAINT VALIDITY
        if (constraint != null && constraint.length > 0) {
            //CHANGE TO UPPER
            constraint = constraint.toString().toUpperCase()
            //STORE OUR FILTERED PLAYERS
            val filteredPlayers = ArrayList<All_Matches>()
            for (i in filterList.indices) {
                //CHECK
                if (filterList[i].interest_status!!.contains(constraint)) {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPlayers.add(filterList[i])
                }
            }
            results.count = filteredPlayers.size
            results.values = filteredPlayers
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
        adapter.mServiceList = results.values as ArrayList<All_Matches>
        //REFRESH
        adapter.notifyDataSetChanged()
    }
}