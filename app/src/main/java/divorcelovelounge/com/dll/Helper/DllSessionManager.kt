package divorcelovelounge.com.dll.Helper

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import divorcelovelounge.com.dll.RegisterLoginActivity
import java.util.*

//RAVITEJA
class DllSessionManager(internal var _context: Context) {
    internal var pref: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var PRIVATE_MODE = 0

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)


    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[USER_ID] = pref.getString(USER_ID, "")!!
            user[USER_NAME] = pref.getString(USER_NAME, "")!!
            user[USER_NAME_ID] = pref.getString(USER_NAME_ID, "")!!
            user[USER_PASSWORD] = pref.getString(USER_PASSWORD, "")!!
            user[USER_GENDER] = pref.getString(USER_GENDER, "")!!
            user[USER_LOOKING] = pref.getString(USER_LOOKING, "")!!
            user[USER_EMAIL] = pref.getString(USER_EMAIL, "")!!
            user[USER_ZIP] = pref.getString(USER_ZIP, "")!!
            user[USER_STATE] = pref.getString(USER_STATE, "")!!
            user[USER_REFER] = pref.getString(USER_REFER, "")!!
            user[USER_CHILD] = pref.getString(USER_CHILD, "")!!
            user[USER_CITY] = pref.getString(USER_CITY, "")!!
            user[USER_DOB] = pref.getString(USER_DOB, "")!!
            user[USER_PHONE_NUMBER] = pref.getString(USER_PHONE_NUMBER, "")!!
            user[USER_ETHNICITY] = pref.getString(USER_ETHNICITY, "")!!
            user[USER_RELIGION] = pref.getString(USER_RELIGION, "")!!
            user[USER_DENOMINATION] = pref.getString(USER_DENOMINATION, "")!!
            user[USER_EDUCATION] = pref.getString(USER_EDUCATION, "")!!
            user[USER_OCCUPATION] = pref.getString(USER_OCCUPATION, "")!!
            user[USER_INCOME] = pref.getString(USER_INCOME, "")!!
            user[USER_SMOKE] = pref.getString(USER_SMOKE, "")!!
            user[USER_DRINK] = pref.getString(USER_DRINK, "")!!
            user[USER_HEIGHT_FT] = pref.getString(USER_HEIGHT_FT, "")!!
            user[USER_HEIGHT_INC] = pref.getString(USER_HEIGHT_INC, "")!!
            user[USER_HEIGHT_CMS] = pref.getString(USER_HEIGHT_CMS, "")!!
            user[USER_PASSIONATE] = pref.getString(USER_PASSIONATE, "")!!
            user[USER_LEISURE_TIME] = pref.getString(USER_LEISURE_TIME, "")!!
            user[USER_THANK_1] = pref.getString(USER_THANK_1, "")!!
            user[USER_THANK_2] = pref.getString(USER_THANK_2, "")!!
            user[USER_THANK_3] = pref.getString(USER_THANK_3, "")!!
            user[PARTNER_AGE_MIN] = pref.getString(PARTNER_AGE_MIN, "")!!
            user[PARTNER_AGE_MAX] = pref.getString(PARTNER_AGE_MAX, "")!!
            user[PARTNER_MATCH_DISTANCE] = pref.getString(PARTNER_MATCH_DISTANCE, "")!!
            user[PARTNER_MATCH_SEARCH] = pref.getString(PARTNER_MATCH_SEARCH, "")!!
            user[PARTNER_ethnicity] = pref.getString(PARTNER_ethnicity, "")!!
            user[PARTNER_religion] = pref.getString(PARTNER_religion, "")!!
            user[PARTNER_denomination] = pref.getString(PARTNER_denomination, "")!!
            user[PARTNER_education] = pref.getString(PARTNER_education, "")!!
            user[PARTNER_occupation] = pref.getString(PARTNER_occupation, "")!!
            user[PARTNER_smoke] = pref.getString(PARTNER_smoke, "")!!
            user[PARTNER_drink] = pref.getString(PARTNER_drink, "")!!
            user[PHOTO1] = pref.getString(PHOTO1, "")!!
            user[PHOTO1_APPROVE] = pref.getString(PHOTO1_APPROVE, "")!!
            user[PHOTO2] = pref.getString(PHOTO2, "")!!
            user[PHOTO2_APPROVE] = pref.getString(PHOTO2_APPROVE, "")!!
            user[PHOTO3] = pref.getString(PHOTO3, "")!!
            user[PHOTO3_APPROVE] = pref.getString(PHOTO3_APPROVE, "")!!
            user[PHOTO4] = pref.getString(PHOTO4, "")!!
            user[PHOTO4_APPROVE] = pref.getString(PHOTO4_APPROVE, "")!!
            user[PHOTO5] = pref.getString(PHOTO5, "")!!
            user[PHOTO5_APPROVE] = pref.getString(PHOTO5_APPROVE, "")!!
            user[PHOTO6] = pref.getString(PHOTO6, "")!!
            user[PHOTO6_APPROVE] = pref.getString(PHOTO6_APPROVE, "")!!
            user[PHOTO7] = pref.getString(PHOTO7, "")!!
            user[PHOTO7_APPROVE] = pref.getString(PHOTO7_APPROVE, "")!!
            user[PHOTO8] = pref.getString(PHOTO8, "")!!
            user[PHOTO8_APPROVE] = pref.getString(PHOTO8_APPROVE, "")!!
            user[PHOTO9] = pref.getString(PHOTO9, "")!!
            user[PHOTO9_APPROVE] = pref.getString(PHOTO9_APPROVE, "")!!
            user[PHOTO10] = pref.getString(PHOTO10, "")!!
            user[PHOTO10_APPROVE] = pref.getString(PHOTO10_APPROVE, "")!!

            user[USER_STATUS] = pref.getString(USER_STATUS, "")!!
            user[USER_SIGNUP_STATUS] = pref.getString(USER_SIGNUP_STATUS, "")!!
            user[USER_PROVIDER] = pref.getString(USER_PROVIDER, "")!!
            user[USER_COUNTRY] = pref.getString(USER_COUNTRY, "")!!
            user[USER_MOTHER_TONGUE] = pref.getString(USER_MOTHER_TONGUE, "")!!
            user[PARTNER_MOTHER_TONGUE] = pref.getString(PARTNER_MOTHER_TONGUE, "")!!

            user[MEMBERSHIP_STATUS] = pref.getString(MEMBERSHIP_STATUS, "")!!
            user[MEMBERSHIP_PLAN_START] = pref.getString(MEMBERSHIP_PLAN_START, "")!!
            user[MEMBERSHIP_PLAN_EXPIRY] = pref.getString(MEMBERSHIP_PLAN_EXPIRY, "")!!
            user[MEMBERSHIP_SERVER_DATE] = pref.getString(MEMBERSHIP_SERVER_DATE, "")!!
            user[MEMBERSHIP_CHAT_STATUS] = pref.getString(MEMBERSHIP_CHAT_STATUS, "")!!
            user[MEMBERSHIP_FAVORITE_ME_STATUS] = pref.getString(MEMBERSHIP_FAVORITE_ME_STATUS, "")!!
            user[MEMBERSHIP_VIEWED_ME_STATUS] = pref.getString(MEMBERSHIP_VIEWED_ME_STATUS, "")!!
            user[PUSH_NOTIFY_STATUS] = pref.getString(PUSH_NOTIFY_STATUS, "")!!
            user[EMAIL_NOTIFY_STATUS] = pref.getString(EMAIL_NOTIFY_STATUS, "")!!


            return user

        }

    val locationDetails: HashMap<String, String>
        get() {
            val gps = HashMap<String, String>()
            gps[USER_GPS_COUNTRY] = pref.getString(USER_GPS_COUNTRY, "")!!
            gps[USER_GPS_STATE] = pref.getString(USER_GPS_STATE, "")!!
            gps[USER_GPS_CITY] = pref.getString(USER_GPS_CITY, "")!!

            return gps

        }


    val chatStatus: HashMap<String, String>
        get() {
            val chatStatus = HashMap<String, String>()
            chatStatus[CHAT_STATUS] = pref.getString(CHAT_STATUS, "")!!
            chatStatus[CHAT_MSG] = pref.getString(CHAT_MSG, "")!!
            return chatStatus

        }

    fun getSid(): String? {

        return pref.getString(XMPP_CHAT_COUNT, "")
    }




    fun userProfileSetup(
            children_stg: String,
            city_stg: String,
            // dob_stg: String,
            ethnicity_stg: String,
            religion_stg: String,
            mother_tongue_stg: String,
            education_stg: String,
            occupation_stg: String,
            income_stg: String,
            smoke_stg: String,
            drink_stg: String,
            height_ft_stg: String,
            height_in_stg: String,
            height_cm_stg: String,
            passionate_stg: String,
            leisure_time_stg: String,
            thankful_1_stg: String,
            thankful_2_stg: String,
            thankful_3_stg: String,
            user_signup_status_stg: String
    ) {
        editor.putString(USER_CHILD, children_stg)
        editor.putString(USER_CITY, city_stg)
        //
        editor.putString(USER_ETHNICITY, ethnicity_stg)
        editor.putString(USER_RELIGION, religion_stg)
        editor.putString(USER_MOTHER_TONGUE, mother_tongue_stg)
        editor.putString(USER_EDUCATION, education_stg)
        editor.putString(USER_OCCUPATION, occupation_stg)
        editor.putString(USER_INCOME, income_stg)
        editor.putString(USER_SMOKE, smoke_stg)
        editor.putString(USER_DRINK, drink_stg)
        editor.putString(USER_HEIGHT_FT, height_ft_stg)
        editor.putString(USER_HEIGHT_INC, height_in_stg)
        editor.putString(USER_HEIGHT_CMS, height_cm_stg)
        editor.putString(USER_PASSIONATE, passionate_stg)
        editor.putString(USER_LEISURE_TIME, leisure_time_stg)
        editor.putString(USER_THANK_1, thankful_1_stg)
        editor.putString(USER_THANK_2, thankful_2_stg)
        editor.putString(USER_THANK_3, thankful_3_stg)
        editor.putString(USER_SIGNUP_STATUS, user_signup_status_stg)

        // commit changes
        editor.commit()
    }

    fun userMatchPreferences(
            partner_age_min: String,
            partner_age_max: String,
            partner_match_distance: String,
            partner_match_search: String,
            partner_ethnicity: String,
            partner_religion: String,
            partner_mtongue_name: String,
            partner_education: String,
            partner_occupation: String,
            partner_smoke: String,
            partner_drink: String,
            user_signup_status_stg: String
    ) {

        editor.putString(PARTNER_AGE_MIN, partner_age_min)
        editor.putString(PARTNER_AGE_MAX, partner_age_max)
        editor.putString(PARTNER_MATCH_DISTANCE, partner_match_distance)
        editor.putString(PARTNER_MATCH_SEARCH, partner_match_search)
        editor.putString(PARTNER_ethnicity, partner_ethnicity)
        editor.putString(PARTNER_religion, partner_religion)
        editor.putString(PARTNER_MOTHER_TONGUE, partner_mtongue_name)
        editor.putString(PARTNER_education, partner_education)
        editor.putString(PARTNER_occupation, partner_occupation)
        editor.putString(PARTNER_smoke, partner_smoke)
        editor.putString(PARTNER_drink, partner_drink)
        editor.putString(USER_SIGNUP_STATUS, user_signup_status_stg)

        // commit changes
        editor.commit()
    }

    fun XmppChatCount(count:String){
        editor.putString(XMPP_CHAT_COUNT,count)
        editor.commit()
    }



    fun Imagesonly(images:String){
        editor.putString(CHAT_IMAGES_ONLY,images)
        editor.commit()
    }

    fun Nameonly(name:String){
        editor.putString(CHAT_NAME_ONLY,name)
        editor.commit()
    }
    fun getImagesData():String?{
        return pref.getString(CHAT_IMAGES_ONLY, "")
    }

    fun getNamesData():String?{
        return pref.getString(CHAT_NAME_ONLY, "")
    }
    fun userSignUpStatus(user_signup_status: String) {
        editor.putString(USER_SIGNUP_STATUS, user_signup_status)

        editor.commit()
    }

    fun profile_pic_updated(pic_data: String) {
        editor.putString(PHOTO1, pic_data)
        editor.commit()
    }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(
            user_id_stg: String,
            user_gender_stg: String,
            user_looking_stg: String,
            user_name_stg: String,
            user_email_stg: String,
            user_zip_stg: String,
            user_state_stg: String,
            user_refer_stg: String,
            children_stg: String,
            city_stg: String,
            dob_stg: String,
            user_phone_number: String,
            ethnicity_stg: String,
            religion_stg: String,
            denomination_stg: String,
            education_stg: String,
            occupation_stg: String,
            income_stg: String,
            smoke_stg: String,
            drink_stg: String,
            height_ft_stg: String,
            height_in_stg: String,
            height_cm_stg: String,
            passionate_stg: String,
            leisure_time_stg: String,
            thankful_1_stg: String,
            thankful_2_stg: String,
            thankful_3_stg: String,
            partner_age_min: String,
            partner_age_max: String,
            partner_match_distance: String,
            partner_match_search: String,
            partner_ethnicity: String,
            partner_religion: String,
            partner_denomination: String,
            partner_education: String,
            partner_occupation: String,
            partner_smoke: String,
            partner_drink: String,
            photo1: String,
            photo1_approve: String,
            photo2: String,
            photo2_approve: String,
            photo3: String,
            photo3_approve: String,
            photo4: String,
            photo4_approve: String,
            photo5: String,
            photo5_approve: String,
            photo6: String,
            photo6_aprove: String,
            photo7: String,
            photo7_aprove: String,
            photo8: String,
            photo8_aprove: String,
            photo9: String,
            photo9_aprove: String,
            photo10: String,
            photo10_aprove: String,

            user_status_stg: String,
            user_signup_status_stg: String,
            provider: String,
            country: String,
            mother_tongue: String,
            partner_mother_tongue: String,
            user_name_id_stg: String,
            password_stg: String,
            is_login:Boolean


    ) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, is_login)
        // Storing name in pref
        editor.putString(USER_ID, user_id_stg)
        editor.putString(USER_GENDER, user_gender_stg)
        editor.putString(USER_LOOKING, user_looking_stg)
        editor.putString(USER_NAME, user_name_stg)
        editor.putString(USER_NAME_ID, user_name_id_stg)
        editor.putString(USER_PASSWORD, password_stg)
        editor.putString(USER_EMAIL, user_email_stg)
        editor.putString(USER_ZIP, user_zip_stg)
        editor.putString(USER_STATE, user_state_stg)
        editor.putString(USER_REFER, user_refer_stg)
        editor.putString(USER_CHILD, children_stg)
        editor.putString(USER_CITY, city_stg)
        editor.putString(USER_DOB, dob_stg)
        editor.putString(USER_PHONE_NUMBER, user_phone_number)
        editor.putString(USER_ETHNICITY, ethnicity_stg)
        editor.putString(USER_RELIGION, religion_stg)
        editor.putString(USER_DENOMINATION, denomination_stg)
        editor.putString(USER_EDUCATION, education_stg)
        editor.putString(USER_OCCUPATION, occupation_stg)
        editor.putString(USER_INCOME, income_stg)
        editor.putString(USER_SMOKE, smoke_stg)
        editor.putString(USER_DRINK, drink_stg)
        editor.putString(USER_HEIGHT_FT, height_ft_stg)
        editor.putString(USER_HEIGHT_INC, height_in_stg)
        editor.putString(USER_HEIGHT_CMS, height_cm_stg)
        editor.putString(USER_PASSIONATE, passionate_stg)
        editor.putString(USER_LEISURE_TIME, leisure_time_stg)
        editor.putString(USER_THANK_1, thankful_1_stg)
        editor.putString(USER_THANK_2, thankful_2_stg)
        editor.putString(USER_THANK_3, thankful_3_stg)
        editor.putString(PARTNER_AGE_MIN, partner_age_min)
        editor.putString(PARTNER_AGE_MAX, partner_age_max)
        editor.putString(PARTNER_MATCH_DISTANCE, partner_match_distance)
        editor.putString(PARTNER_MATCH_SEARCH, partner_match_search)
        editor.putString(PARTNER_ethnicity, partner_ethnicity)
        editor.putString(PARTNER_religion, partner_religion)
        editor.putString(PARTNER_denomination, partner_denomination)
        editor.putString(PARTNER_education, partner_education)
        editor.putString(PARTNER_occupation, partner_occupation)
        editor.putString(PARTNER_smoke, partner_smoke)
        editor.putString(PARTNER_drink, partner_drink)
        editor.putString(PHOTO1, photo1)
        editor.putString(PHOTO1_APPROVE, photo1_approve)
        editor.putString(PHOTO2, photo2)
        editor.putString(PHOTO2_APPROVE, photo2_approve)
        editor.putString(PHOTO3, photo3)
        editor.putString(PHOTO3_APPROVE, photo3_approve)
        editor.putString(PHOTO4, photo4)
        editor.putString(PHOTO4_APPROVE, photo4_approve)
        editor.putString(PHOTO5, photo5)
        editor.putString(PHOTO5_APPROVE, photo5_approve)
        editor.putString(PHOTO6, photo6)
        editor.putString(PHOTO6_APPROVE, photo6_aprove)
        editor.putString(PHOTO7, photo7)
        editor.putString(PHOTO7_APPROVE, photo7_aprove)
        editor.putString(PHOTO8, photo8)
        editor.putString(PHOTO8_APPROVE, photo8_aprove)
        editor.putString(PHOTO9, photo9)
        editor.putString(PHOTO9_APPROVE, photo9_aprove)
        editor.putString(PHOTO10, photo10)
        editor.putString(PHOTO10_APPROVE, photo10_aprove)

        editor.putString(USER_STATUS, user_status_stg)
        editor.putString(USER_SIGNUP_STATUS, user_signup_status_stg)
        editor.putString(USER_PROVIDER, provider)
        editor.putString(USER_COUNTRY, country)
        editor.putString(USER_MOTHER_TONGUE, mother_tongue)
        editor.putString(PARTNER_MOTHER_TONGUE, partner_mother_tongue)

        // commit changes
        editor.commit()
    }

    fun PhotoApprove(p1Aprrove:String){
        editor.putString(PHOTO1_APPROVE, p1Aprrove)
        editor.commit()
    }

    fun updateProfileMatchPreferences(
            zipcode: String,
            state: String,
            city: String,
            ethnicity: String,
            religion: String,
            denomination: String,
            education: String,
            smoke: String,
            drink: String,
            occupation: String,
            partner_age_min: String,
            partner_age_max: String,
            partner_match_distance: String,
            partner_ethnicity: String,
            partner_religion: String,
            partner_denomination: String,
            partner_education: String,
            partner_occupation: String,
            partner_smoke: String,
            partner_drink: String,
            partner_match_search: String,
            country: String,
            mother_tongue: String,
            partner_mother_tongue: String,
            dob: String
    ) {
        editor.putString(USER_ZIP, zipcode)
        editor.putString(USER_CITY, city)
        editor.putString(USER_STATE, state)
        editor.putString(USER_ETHNICITY, ethnicity)
        editor.putString(USER_RELIGION, religion)
        editor.putString(USER_DENOMINATION, denomination)
        editor.putString(USER_EDUCATION, education)
        editor.putString(USER_OCCUPATION, occupation)
        editor.putString(USER_SMOKE, smoke)
        editor.putString(USER_DRINK, drink)
        editor.putString(PARTNER_AGE_MIN, partner_age_min)
        editor.putString(PARTNER_AGE_MAX, partner_age_max)
        editor.putString(PARTNER_MATCH_DISTANCE, partner_match_distance)
        editor.putString(PARTNER_MATCH_SEARCH, partner_match_search)
        editor.putString(PARTNER_ethnicity, partner_ethnicity)
        editor.putString(PARTNER_religion, partner_religion)
        editor.putString(PARTNER_denomination, partner_denomination)
        editor.putString(PARTNER_education, partner_education)
        editor.putString(PARTNER_occupation, partner_occupation)
        editor.putString(PARTNER_smoke, partner_smoke)
        editor.putString(PARTNER_drink, partner_drink)
        editor.putString(USER_COUNTRY, country)
        editor.putString(USER_MOTHER_TONGUE, mother_tongue)
        editor.putString(PARTNER_MOTHER_TONGUE, partner_mother_tongue)
        editor.putString(USER_DOB, dob)

        editor.commit()
    }

    fun updateMemberShipDetails(
            membership_status: String,
            plan_start_date: String,
            plan_expiry: String,
            server_date: String,
            chat: String,
            favoritedme: String,
            viewedme: String
    ) {
        editor.putString(MEMBERSHIP_STATUS, membership_status)
        editor.putString(MEMBERSHIP_PLAN_START, plan_start_date)
        editor.putString(MEMBERSHIP_PLAN_EXPIRY, plan_expiry)
        editor.putString(MEMBERSHIP_SERVER_DATE, server_date)
        editor.putString(MEMBERSHIP_CHAT_STATUS, chat)
        editor.putString(MEMBERSHIP_FAVORITE_ME_STATUS, favoritedme)
        editor.putString(MEMBERSHIP_VIEWED_ME_STATUS, viewedme)

        editor.commit()
    }


    fun gpsLocationDetails(country:String,state:String,city:String){

        editor.putString(USER_GPS_COUNTRY, country)
        editor.putString(USER_GPS_STATE, state)
        editor.putString(USER_GPS_CITY, city)


        editor.commit()
    }
    fun getChatStaus(status:String,msg:String){
        editor.putString(CHAT_STATUS, status)
        editor.putString(CHAT_MSG, msg)
        editor.commit()
    }
    fun CreateNotificationSession(push_notification_stats_stg:String,email_notification_status:String){

        editor.putString(PUSH_NOTIFY_STATUS, push_notification_stats_stg)
        editor.putString(EMAIL_NOTIFY_STATUS, email_notification_status)


        editor.commit()
    }


    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, RegisterLoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            // Staring Login Activity
            _context.startActivity(i)
        }
    }

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()
        // After logout redirect user to Loing Activity
        val i = Intent(_context, RegisterLoginActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        // Staring Login Activity
        _context.startActivity(i)
    }


    companion object {

        var USER_ID = "userid"
        var USER_GENDER = "gender"
        var USER_LOOKING = "looking_for"
        var USER_NAME = "first_name"
        var USER_NAME_ID = "name_id"
        var USER_PASSWORD = "password"
        var USER_EMAIL = "email"
        var USER_ZIP = "zipcode"
        var USER_STATE = "state"
        var USER_REFER = "referal_from"
        var USER_CHILD = "children"
        var USER_CITY = "city"
        var USER_DOB = "dob"
        var USER_PHONE_NUMBER = "user_phone_number"
        var USER_ETHNICITY = "ethnicity"
        var USER_RELIGION = "religion"
        var USER_DENOMINATION = "denomination"
        var USER_EDUCATION = "education"
        var USER_OCCUPATION = "occupation"
        var USER_INCOME = "income"
        var USER_SMOKE = "smoke"
        var USER_DRINK = "drink"
        var USER_HEIGHT_FT = "height_ft"
        var USER_HEIGHT_INC = "height_inc"
        var USER_HEIGHT_CMS = "height_cms"
        var USER_PASSIONATE = "passionate"
        var USER_LEISURE_TIME = "leisure_time"
        var USER_THANK_1 = "thankful_1"
        var USER_THANK_2 = "thankful_2"
        var USER_THANK_3 = "thankful_3"
        var PARTNER_AGE_MIN = "part_age_min"
        var PARTNER_AGE_MAX = "part_age_max"
        var PARTNER_MATCH_DISTANCE = "match_distance"
        var PARTNER_MATCH_SEARCH = "match_search_imp"
        var PARTNER_ethnicity = "PARTNER_ethnicity"
        var PARTNER_religion = "PARTNER_religion"
        var PARTNER_denomination = "PARTNER_denomination"
        var PARTNER_education = "PARTNER_education"
        var PARTNER_occupation = "PARTNER_occupation"
        var PARTNER_smoke = "PARTNER_smoke"
        var PARTNER_drink = "PARTNER_drink"
        var PHOTO1 = "PHOTO1"
        var PHOTO1_APPROVE = "PHOTO1_APPROVE"
        var PHOTO2 = "PHOTO2"
        var PHOTO2_APPROVE = "PHOTO2_APPROVE"
        var PHOTO3 = "PHOTO3"
        var PHOTO3_APPROVE = "PHOTO3_APPROVE"
        var PHOTO4 = "PHOTO4"
        var PHOTO4_APPROVE = "PHOTO4_APPROVE"
        var PHOTO5 = "PHOTO5"
        var PHOTO5_APPROVE = "PHOTO5_APPROVE"
        var PHOTO6 = "PHOTO6"
        var PHOTO6_APPROVE = "PHOTO6_APPROVE"
        var PHOTO7 = "PHOTO7"
        var PHOTO7_APPROVE = "PHOTO7_APPROVE"
        var PHOTO8 = "PHOTO8"
        var PHOTO8_APPROVE = "PHOTO8_APPROVE"
        var PHOTO9 = "PHOTO9"
        var PHOTO9_APPROVE = "PHOTO9_APPROVE"
        var PHOTO10 = "PHOTO10"
        var PHOTO10_APPROVE = "PHOTO10_APPROVE"


        var USER_STATUS = "status"
        var USER_SIGNUP_STATUS = "signup_status"
        var USER_PROVIDER = "provider_status"
        var USER_COUNTRY = "country"
        var USER_MOTHER_TONGUE = "mother_tongue"
        var PARTNER_MOTHER_TONGUE = "partner_mtongue_name"

        // membership plan
        var MEMBERSHIP_STATUS = "membership_status"
        var MEMBERSHIP_PLAN_START = "plan_start_date"
        var MEMBERSHIP_PLAN_EXPIRY = "plan_expiry"
        var MEMBERSHIP_SERVER_DATE = "server_date"
        var MEMBERSHIP_CHAT_STATUS = "chat"
        var MEMBERSHIP_FAVORITE_ME_STATUS = "favoritedme"
        var MEMBERSHIP_VIEWED_ME_STATUS = "viewedme"

        var IS_LOGIN = "IsLoggedIn"
        var PREF_NAME = "dllapp"

        var PUSH_NOTIFY_STATUS = "push_status"
        var EMAIL_NOTIFY_STATUS = "email_status"

        var XMPP_CHAT_COUNT="xmpp_count"

        var CHAT_IMAGES_ONLY="images_count"
        var CHAT_NAME_ONLY="names_count"

        val USER_GPS_COUNTRY="country_get_gps"
        val USER_GPS_STATE="state_get_gps"
        val USER_GPS_CITY="city_get_gps"

        val CHAT_STATUS="main_chat_status"
        val CHAT_MSG="main_chat_essage"

    }
}