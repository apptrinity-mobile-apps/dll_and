package divorcelovelounge.com.dll.Helper

import android.content.Context
import android.content.SharedPreferences


class DllSlideManager(internal var fcontext: Context) {

    internal var sharedPreferences: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var PRIVATE_MODE = 0

    val isFirstTimeLaunchOnly: Boolean
        get() = sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH_ONLY, true)

    val isFirstTimeShowed: Boolean
        get() = sharedPreferences.getBoolean(IS_FIRST_TIME_SHOW, true)

    init {
        sharedPreferences = fcontext.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = sharedPreferences.edit()
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH_ONLY, isFirstTime)
        editor.commit()
    }

    fun setFirstTimeShowed(isFirstTimeShow: Boolean) {
        editor.putBoolean(IS_FIRST_TIME_SHOW, isFirstTimeShow)
        editor.commit()
    }

    companion object {
        private val PREF_NAME = "slides"
        private val IS_FIRST_TIME_LAUNCH_ONLY = "IsFirstTimeLaunchOnly"
        private val IS_FIRST_TIME_SHOW = "IsFirstTimeShow"
    }

}
