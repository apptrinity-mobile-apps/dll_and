package divorcelovelounge.com.dll

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.*
import android.widget.*
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SupportActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    private lateinit var back_chat_toolbar: ImageView
    private lateinit var iv_alert_close: ImageView
    private lateinit var success_iv_alert_close: ImageView

    private lateinit var tv_send_btn_id: CustomTextViewBold
    private lateinit var tv_alert_done: CustomTextViewBold
    private lateinit var success_tv_alert_done: CustomTextViewBold

    private lateinit var tv_alert_header: CustomTextView
    private lateinit var success_tv_alert_header: CustomTextView
    private lateinit var searchEmpty: CustomTextView
    private lateinit var success_searchEmpty: CustomTextView

    private lateinit var et_message_id: EditText
    private lateinit var et_type_id: EditText
    private lateinit var searchEditText: EditText
    private lateinit var success_searchEditText: EditText

    private lateinit var progressDialog: Dialog
    private lateinit var support_type_dialog: Dialog
    private lateinit var success_dialog: Dialog

    private lateinit var searchListView: ListView
    private lateinit var success_searchListView: ListView

    private lateinit var supportTypesAdapter: SupportTypesAdapter

    private lateinit var sessionManager: DllSessionManager

    private lateinit var userDetails: HashMap<String, String>

    private val logTAG = "SupportActivity"
    var user_id = ""
    var message_stg = ""
    var type_stg = ""
    var type_id = ""
    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>

    lateinit var location_details_list: java.util.HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)

        sessionManager = DllSessionManager(this@SupportActivity)
        userDetails = sessionManager.userDetails
        user_id = userDetails[DllSessionManager.USER_ID]!!

        initialize()

        et_type_id.isFocusable = false
        searchEditText.visibility = View.GONE
        searchListView.visibility = View.VISIBLE
        searchEmpty.visibility = View.GONE
        tv_alert_header.text = "Support"
        getSupportTypes()

        searchListView.setOnItemClickListener { parent, view, position, id ->
            view.isSelected = true
            try {
                type_stg = supportTypesAdapter.getItem(position).toString()
                type_id = supportTypesAdapter.getId(position).toString()
                supportTypesAdapter.setSelectedIndex(position)
                Log.d(logTAG, "selected type: $type_stg")

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }



        et_type_id.setOnClickListener {
            support_type_dialog.show()
        }

        iv_alert_close.setOnClickListener {
            support_type_dialog.dismiss()
        }

        tv_alert_done.setOnClickListener {
            et_type_id.setText(type_stg)
            support_type_dialog.dismiss()
        }

        back_chat_toolbar.setOnClickListener {
            onBackPressed()
        }

        tv_send_btn_id.setOnClickListener {
            message_stg = et_message_id.text.toString()
            type_stg = et_type_id.text.toString()
            if (message_stg != "" && type_stg != "") {
                supportInsertApi(type_id, message_stg, user_id)
            } else {
                Toast.makeText(this@SupportActivity, "Fields are mandatory!", Toast.LENGTH_SHORT).show()
            }

        }

    }

    private fun initialize() {
        back_chat_toolbar = findViewById(R.id.back_chat_toolbar)
        tv_send_btn_id = findViewById(R.id.tv_send_btn_id)
        et_message_id = findViewById(R.id.et_message_id)
        et_type_id = findViewById(R.id.et_type_id)

        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()

        location_details_list = sessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

        getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Support")

        /* progress loader */
        progressDialog = Dialog(this@SupportActivity)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

        /* support types alert dialog */
        support_type_dialog = Dialog(this@SupportActivity)
        support_type_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        support_type_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        support_type_dialog.setContentView(R.layout.alert_dialog_location_search)
        val layout_window = support_type_dialog.window
        layout_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        iv_alert_close = support_type_dialog.findViewById(R.id.iv_alert_close)
        searchListView = support_type_dialog.findViewById(R.id.searchListView)
        searchEditText = support_type_dialog.findViewById(R.id.searchEditText)
        tv_alert_header = support_type_dialog.findViewById(R.id.tv_alert_header)
        tv_alert_done = support_type_dialog.findViewById(R.id.tv_alert_done)
        searchEmpty = support_type_dialog.findViewById(R.id.searchEmpty)

    }

    private fun getSupportTypes() {

        progressDialog.show()
        var supportTypes: ArrayList<SupportTypesDataResponse> = ArrayList()

        val supportApi = ApiInterface.create()
        val call = supportApi.getSupportTypesApi()
        call.enqueue(object : Callback<SupportTypesResponse> {
            override fun onFailure(call: Call<SupportTypesResponse>, t: Throwable) {
                Log.e(logTAG, "supportApi Response : error : ${t.toString()}")
                Toast.makeText(this@SupportActivity, "Something went wrong. Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<SupportTypesResponse>,
                response: Response<SupportTypesResponse>
            ) {

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                Log.d(logTAG, "supportApi Response: success: ${response.body()!!.result}")

                if (response.body()!!.status == "1") {

                    val data = response.body()!!.data
                    for (i in 0 until data!!.size) {
                        supportTypes = data
                    }

                    supportTypesAdapter = SupportTypesAdapter(this@SupportActivity, supportTypes)
                    searchListView.adapter = supportTypesAdapter
                    supportTypesAdapter.notifyDataSetChanged()

                } else {

                }


            }
        })

    }

    private fun supportInsertApi(support_id: String, message: String, user_id: String) {

        progressDialog.show()

        val supportInsert = ApiInterface.create()
        val call = supportInsert.supportInsertApi(support_id, message, user_id)
        call.enqueue(object : Callback<SupportTypesResponse> {
            override fun onFailure(call: Call<SupportTypesResponse>, t: Throwable) {
                Log.e(logTAG, "supportInsertApi Response : error : ${t.toString()}")
                Toast.makeText(this@SupportActivity, "Something went wrong. Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<SupportTypesResponse>,
                response: Response<SupportTypesResponse>
            ) {

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                Log.d(logTAG, "supportInsertApi Response: success: ${response.body()!!.result}")

                if (response.body()!!.status == "1") {

                    successDialog(response.body()!!.result)

                } else {

                }


            }
        })

    }

    fun successDialog(status:String){
        success_dialog = Dialog(this@SupportActivity)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.alert_dialog_location_search)
        val layout_window = success_dialog.window
        layout_window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        success_iv_alert_close = success_dialog.findViewById(R.id.iv_alert_close)
        success_searchListView = success_dialog.findViewById(R.id.searchListView)
        success_searchEditText = success_dialog.findViewById(R.id.searchEditText)
        success_tv_alert_header = success_dialog.findViewById(R.id.tv_alert_header)
        success_tv_alert_done = success_dialog.findViewById(R.id.tv_alert_done)
        success_searchEmpty = success_dialog.findViewById(R.id.searchEmpty)

        success_searchListView.visibility = View.GONE
        success_searchEditText.visibility = View.GONE
        success_searchEmpty.visibility = View.VISIBLE
        success_tv_alert_header.text = "Support"
        success_searchEmpty.setPadding(0,30,0,30)
        success_searchEmpty.text = status
        success_dialog.show()
        success_tv_alert_done.setOnClickListener {
            et_type_id.setText("")
            et_message_id.setText("")
            success_dialog.dismiss()
        }

        success_iv_alert_close.setOnClickListener {
            et_type_id.setText("")
            et_message_id.setText("")
            success_dialog.dismiss()
        }
    }

    inner class SupportTypesAdapter(
        private val context: Activity,
        private val data: ArrayList<SupportTypesDataResponse>
    ) :
        BaseAdapter() {

        private val mInflater: LayoutInflater = LayoutInflater.from(context)
        private var selectedIndex = -1

        override fun getItem(position: Int): Any {
            return data[position].name
        }

        fun getId(position: Int): Any {
            return data[position].id
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

        inner class ViewHolder {
            internal var tv_text: TextView? = null
            internal var ll_item_list: LinearLayout? = null
        }

        fun setSelectedIndex(ind: Int) {
            selectedIndex = ind
            notifyDataSetChanged()
        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            val view: View
            val holder: ViewHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.item_list_profile_setup, parent, false)
                holder = ViewHolder()
                holder.tv_text = view.findViewById(R.id.item_name)
                holder.ll_item_list = view.findViewById(R.id.ll_item_list)

                view.tag = holder
            } else {
                view = convertView
                holder = view.tag as ViewHolder
            }

            if (selectedIndex != -1 && position == selectedIndex) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ll_item_list!!.background =
                            context.resources.getDrawable(R.drawable.selected_bg, context.theme)
                    holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color, context.theme))
                } else {
                    holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg)
                    holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ll_item_list!!.background =
                            context.resources.getDrawable(R.drawable.edittext_bg, context.theme)
                    holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray, context.theme))
                } else {
                    holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg)
                    holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray))
                }
            }

            holder.ll_item_list!!.setPadding(0, 15, 0, 15)
            holder.tv_text!!.text = data[position].name
            return view
        }
    }


    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,user_id)

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD", PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@SupportActivity);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@SupportActivity);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString());
                                sliderLayout.addSlider(textSliderView);
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@SupportActivity);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }


}
