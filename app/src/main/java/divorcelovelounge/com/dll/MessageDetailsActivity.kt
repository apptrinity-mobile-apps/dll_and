package divorcelovelounge.com.dll

import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.Pair
import android.view.*
import android.widget.*
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.PicassoTransformations.BlurTransformation
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import divorcelovelounge.com.dll.SparkButtonHelper.SparkEventListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MessageDetailsActivity : AppCompatActivity() {
    private lateinit var rl_prefs: RelativeLayout
    private lateinit var iv_prefs_back: ImageView
    lateinit var tv_title_id:CustomTextViewBold
    lateinit var tv_from_msg_id: CustomTextView
    lateinit var iv_msg_imp_status_id: SparkButton
    lateinit var iv_delete_id: ImageView
    lateinit var iv_profile_pic_id: ImageView
    lateinit var btn_replypostcomment: ImageView
    lateinit var et_reply_comment: EditText
    lateinit var iv_undo_id: ImageView
    lateinit var rv_messagedetailview: RecyclerView

    var msg_id_stg=""
    var msg_to_stg=""
    var msg_from_stg=""
    var msg_content_stg=""
    var msg_date_id_stg=""
   // var msg_important_sender_stg=""
  //  var msg_important_receiver_stg=""
    var msg_date_stg=""
   // var trash_sender_stg=""
   // var trash_receiver_stg=""
    var first_name_stg=""
    var photo1_stg=""
    var from_stg=""
    var from_msg_status=""
    var imp_sent_reciever_stg=""
    lateinit var progress_1: ProgressBar
    lateinit var iv_from_id: ImageView
    lateinit var iv_option_menu: ImageView
    lateinit var ll_full_prof_id: LinearLayout

    lateinit var swipe_refresh_layout:SwipeRefreshLayout

    lateinit var dllSessionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    var messagesdetailistArray :ArrayList<getMessageDetaildataResponse>?=null

    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_details)

        dllSessionManager = DllSessionManager(this)
        user_details = dllSessionManager.userDetails

        msg_id_stg=intent.getStringExtra("msg_id_stg")
        msg_to_stg=intent.getStringExtra("msg_to_stg")
        msg_from_stg=intent.getStringExtra("msg_from_stg")
        msg_content_stg=intent.getStringExtra("msg_content_stg")



        msg_date_id_stg=intent.getStringExtra("date_id_stg")
       // msg_important_sender_stg=intent.getStringExtra("msg_important_sender_stg")
       // msg_important_receiver_stg=intent.getStringExtra("msg_important_receiver_stg")
        msg_date_stg=intent.getStringExtra("msg_date_stg")
       // trash_sender_stg=intent.getStringExtra("trash_sender_stg")
       // trash_receiver_stg=intent.getStringExtra("trash_receiver_stg")
        first_name_stg=intent.getStringExtra("first_name_stg")
        photo1_stg=intent.getStringExtra("photo1")
        from_stg=intent.getStringExtra("from_stg")

        Log.e("photo_lll",photo1_stg)
       getMessageDetailViewApi()


        ll_full_prof_id = findViewById(R.id.ll_full_prof_id)
        iv_option_menu = findViewById(R.id.iv_option_menu)
        iv_from_id = findViewById(R.id.iv_from_id)
        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout)
        rl_prefs = findViewById(R.id.rl_prefs)
        iv_prefs_back = findViewById(R.id.iv_prefs_back)
        iv_prefs_back.setOnClickListener {
            onBackPressed()
        }

        Picasso.with(this)
            .load(photo1_stg)
            .error(R.drawable.ic_man_user)
            .transform(FullProfile.CircleTransform())
            .into(iv_from_id)

        tv_title_id=findViewById(R.id.tv_title_id)
        tv_from_msg_id=findViewById(R.id.tv_from_msg_id)
        iv_msg_imp_status_id=findViewById(R.id.iv_msg_imp_status_id)
        iv_msg_imp_status_id=findViewById(R.id.iv_msg_imp_status_id)
        iv_delete_id=findViewById(R.id.iv_delete_id)
        //iv_profile_pic_id=findViewById(R.id.iv_profile_pic_id)
        iv_undo_id=findViewById(R.id.iv_undo_id)
        et_reply_comment=findViewById(R.id.et_reply_comment)
        btn_replypostcomment=findViewById(R.id.btn_replypostcomment)
        rv_messagedetailview=findViewById(R.id.rv_messagedetailview)
        progress_1=findViewById(R.id.progress_1)

        tv_title_id.text=first_name_stg
        tv_from_msg_id.text=from_stg


        if(from_stg.equals("Inbox")){
            from_msg_status = "inbox"
        }else if(from_stg.equals("Sent")){
            from_msg_status = "sent"
        }else if(from_stg.equals("Important")){
            from_msg_status = "important"
        }


        swipe_refresh_layout.setOnRefreshListener {
            // Initialize a new Runnable
                // Update the text view text with a random number
                getMessageDetailViewApi()
            swipe_refresh_layout.isRefreshing = false


        }



        /*if(from_stg.equals("Trash")){
            iv_msg_imp_status_id.visibility= View.GONE
            iv_undo_id.visibility= View.VISIBLE
        }else{
            iv_msg_imp_status_id.visibility= View.VISIBLE
            iv_undo_id.visibility= View.GONE
        }*/


       /* if (msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {
            if (msg_important_sender_stg.equals("Yes")) {
                iv_msg_imp_status_id.isChecked = true
            } else {
                iv_msg_imp_status_id.isChecked = false
            }
        }
        if (msg_from_stg != (user_details[DllSessionManager.USER_ID]!!.toString())) {
            if (msg_important_receiver_stg.equals("Yes")) {
                iv_msg_imp_status_id.isChecked = true
            } else {
                iv_msg_imp_status_id.isChecked = false
            }
        }*/


        iv_undo_id.setOnClickListener {
            if(msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){

                imp_sent_reciever_stg="sender"

            }else{
                imp_sent_reciever_stg="reciever"
            }


            val apiService = ApiInterface.create()
            val call = apiService.makeTrashApi(msg_id_stg,"No",imp_sent_reciever_stg)
            Log.d("REQUEST", call.toString() + "")
            call.enqueue(object : Callback<BlockListResponse> {
                override fun onResponse(
                    call: Call<BlockListResponse>,
                    response: retrofit2.Response<BlockListResponse>?
                ) {

                    //  if (response != null) {
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {
                        onBackPressed()
                    }

                    //  }
                }

                override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                    Log.w("Result_Order_details", t.toString())
                }
            })
        }


        iv_delete_id.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Alert!")
                .setMessage("Are You Sure you want to proceed with Delete?")
                .setCancelable(false)
                .setPositiveButton("DELETE", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()

                        if(msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){

                            imp_sent_reciever_stg="sender"

                        }else{
                            imp_sent_reciever_stg="reciever"
                        }


                        val apiService = ApiInterface.create()
                        val call = apiService.makeTrashApi(msg_id_stg,"Yes",imp_sent_reciever_stg)
                        Log.d("REQUEST", call.toString() + "")
                        call.enqueue(object : Callback<BlockListResponse> {
                            override fun onResponse(
                                call: Call<BlockListResponse>,
                                response: retrofit2.Response<BlockListResponse>?
                            ) {

                                //  if (response != null) {
                                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {
                                    onBackPressed()
                                }

                                //  }
                            }

                            override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                                Log.w("Result_Order_details", t.toString())
                            }
                        })


                    }
                })
                .setNegativeButton("CANCEL", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }
                })

            val alertDialog = builder.create()
            alertDialog.show()
        }


        iv_msg_imp_status_id.setEventListener(object : SparkEventListener {
            override fun onEvent(button: ImageView, buttonState: Boolean) {
                if (buttonState) {
                    //Toast.makeText(context, buttonState.toString(), Toast.LENGTH_SHORT).show()


                    if (msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {

                        imp_sent_reciever_stg = "sender"

                    } else {
                        imp_sent_reciever_stg = "reciever"
                    }
                    /*if(inbox_main_array.get(position).msg_from!=(user_details[DllSessionManager.USER_ID]!!.toString())){


                    }*/


                    val apiService = ApiInterface.create()
                    val call = apiService.makeImportantApi(
                        msg_id_stg,
                        "Yes",
                        imp_sent_reciever_stg
                    )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<BlockListResponse> {
                        override fun onResponse(
                            call: Call<BlockListResponse>,
                            response: retrofit2.Response<BlockListResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                // fav_from_stg = main_user_id
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })

                } else {
                    if (msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {

                        imp_sent_reciever_stg = "sender"

                    } else {
                        imp_sent_reciever_stg = "reciever"
                    }
                    /*if(inbox_main_array.get(position).msg_from!=(user_details[DllSessionManager.USER_ID]!!.toString())){


                    }*/

                    val apiService = ApiInterface.create()
                    val call = apiService.makeImportantApi(
                        msg_id_stg,
                        "No",
                        imp_sent_reciever_stg
                    )
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<BlockListResponse> {
                        override fun onResponse(
                            call: Call<BlockListResponse>,
                            response: retrofit2.Response<BlockListResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                // fav_from_stg = main_user_id
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }
            }

            override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
            }

            override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
            }
        })

        btn_replypostcomment.setOnClickListener {


            if(et_reply_comment.text.toString().equals("")){
                Toast.makeText(this, "Please Enter Comment", Toast.LENGTH_SHORT).show()
            }else{

                if(msg_to_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){

                    ReplyMessageApi(msg_id_stg,msg_to_stg,msg_from_stg,et_reply_comment.text.toString(),from_msg_status)

                }else{
                    ReplyMessageApi(msg_id_stg,msg_from_stg,msg_to_stg,et_reply_comment.text.toString(),from_msg_status)
                }


                /*if(from_msg_status=="inbox"){



                    ReplyMessageApi(msg_id_stg,msg_to_stg,msg_from_stg,et_reply_comment.text.toString(),from_msg_status)
                }else if (from_msg_status =="sent"){
                    ReplyMessageApi(msg_id_stg,msg_from_stg,msg_to_stg,et_reply_comment.text.toString(),from_msg_status)
                }else if(from_msg_status == "important"){
                   if( !msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {
                       ReplyMessageApi(msg_id_stg,msg_to_stg,msg_from_stg,et_reply_comment.text.toString(),"inbox")
                    }else{
                       ReplyMessageApi(msg_id_stg,msg_from_stg,msg_to_stg,et_reply_comment.text.toString(),"sent")
                   }

                }*/
            }



          /*  val intent= Intent(this@MessageDetailsActivity,ComposeMessageActivity::class.java)

            if(msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){
                intent.putExtra("msg_from_id",msg_to_stg)
            }else{
                intent.putExtra("msg_from_id",msg_from_stg)
            }
            intent.putExtra("first_name_stg",first_name_stg)
            intent.putExtra("from_stg","reply")
            startActivity(intent)*/





        }



        iv_option_menu.setOnClickListener {
            val wrapper = ContextThemeWrapper(this, R.style.PopupMenu)
            val popup = PopupMenu(wrapper, iv_option_menu, Gravity.CENTER)

            popup.menuInflater.inflate(R.menu.msg_option_menu, popup.menu)

            //popup.gravity = 20
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item_menu: MenuItem): Boolean {
                    val i = item_menu.itemId
                    if (i == R.id.delete_id) {

                        val builder = AlertDialog.Builder(this@MessageDetailsActivity)
                        builder.setTitle("Alert!")
                            .setMessage("Are You Sure you want to proceed with Delete?")
                            .setCancelable(false)
                            .setPositiveButton("DELETE", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, id: Int) {
                                    dialog!!.dismiss()

                                    if(msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){

                                        imp_sent_reciever_stg="sender"

                                    }else{
                                        imp_sent_reciever_stg="reciever"
                                    }


                                    val apiService = ApiInterface.create()
                                    val call = apiService.makeTrashApi(msg_id_stg,"Yes",imp_sent_reciever_stg)
                                    Log.d("REQUEST", call.toString() + "")
                                    call.enqueue(object : Callback<BlockListResponse> {
                                        override fun onResponse(
                                            call: Call<BlockListResponse>,
                                            response: retrofit2.Response<BlockListResponse>?
                                        ) {

                                            //  if (response != null) {
                                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {
                                                onBackPressed()
                                            }

                                            //  }
                                        }

                                        override fun onFailure(call: Call<BlockListResponse>, t: Throwable) {
                                            Log.w("Result_Order_details", t.toString())
                                        }
                                    })


                                }
                            })
                            .setNegativeButton("CANCEL", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, id: Int) {
                                    dialog!!.dismiss()
                                }
                            })

                        val alertDialog = builder.create()
                        alertDialog.show()
                        return true
                    } else return if (i == R.id.clear_msg_id) {


                        return true
                    } else {
                        onMenuItemClick(item_menu)
                    }
                }

            })

            popup.show()

        }


        ll_full_prof_id.setOnClickListener {

            if(msg_from_stg.equals("admin")){

            }else {

                if(msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())){
                    val internt = Intent(this, FullProfile::class.java)
                    val pairs = arrayOfNulls<Pair<View, String>>(2)
                    pairs[0] = Pair<View, String>(iv_from_id, "mainimageTreansition")
                    pairs[1] = Pair<View, String>(tv_title_id, "nametransition")
                    val aOptions = ActivityOptions.makeSceneTransitionAnimation(this, *pairs)

                    internt.putExtra("from_screen", "dashboard")
                    internt.putExtra("id", msg_to_stg)
                    internt.putExtra("photo1", photo1_stg)
                    internt.putExtra("name_stg", first_name_stg)
                    internt.putExtra("gender_stg", "0")
                    internt.putExtra("age_stg", "0")
                    internt.putExtra("location_stg", "0")
                    startActivity(internt, aOptions.toBundle())
                }else{
                    val internt = Intent(this, FullProfile::class.java)
                    val pairs = arrayOfNulls<Pair<View, String>>(2)
                    pairs[0] = Pair<View, String>(iv_from_id, "mainimageTreansition")
                    pairs[1] = Pair<View, String>(tv_title_id, "nametransition")
                    val aOptions = ActivityOptions.makeSceneTransitionAnimation(this, *pairs)

                    internt.putExtra("from_screen", "dashboard")
                    internt.putExtra("id", msg_from_stg)
                    internt.putExtra("photo1", photo1_stg)
                    internt.putExtra("name_stg", first_name_stg)
                    internt.putExtra("gender_stg", "0")
                    internt.putExtra("age_stg", "0")
                    internt.putExtra("location_stg", "0")
                    startActivity(internt, aOptions.toBundle())
                }



            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val home_intent = Intent(this@MessageDetailsActivity, DashboardActivity::class.java)
        home_intent.putExtra("from_list", "messages_list")
        startActivity(home_intent)

    }


    fun ReplyMessageApi(msg_id_stg:String,from_msg_id:String,to_msg_id:String,message_content:String,from_msg_status:String){
        val apiService = ApiInterface.create()
        progress_1.setVisibility(View.VISIBLE)
        val call = apiService.replyMessageApi(msg_id_stg,
            from_msg_id,  to_msg_id,
                    message_content,from_msg_status
        )
        Log.d("ReplyMessageApi", msg_id_stg+"------"+from_msg_id+"------"+to_msg_id+"-----"+message_content+"---"+from_msg_status)
        call.enqueue(object : Callback<ReplyMessageResponse> {
            override fun onResponse(
                call: Call<ReplyMessageResponse>,
                response: retrofit2.Response<ReplyMessageResponse>?
            ) {
                if (response!!.body()!!.status.equals("1")) {
                    getMessageDetailViewApi()
                    et_reply_comment.setText("")
                }
            }

            override fun onFailure(call: Call<ReplyMessageResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    fun getMessageDetailViewApi(){
        messagesdetailistArray= ArrayList()


        val apiService = ApiInterface.create()


        if (msg_from_stg.equals(user_details[DllSessionManager.USER_ID]!!.toString())) {

            val call = apiService.memberMessagesApi(
                msg_date_id_stg,user_id = user_details[DllSessionManager.USER_ID]!!.toString(),match_id = msg_to_stg)
            messagesdetailistArray!!.clear()
            call.enqueue(object : Callback<MessageDetailviewResponse> {
                override fun onResponse(
                    call: Call<MessageDetailviewResponse>,
                    response: retrofit2.Response<MessageDetailviewResponse>?
                ) {
                    swipe_refresh_layout.isRefreshing = false
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                        progress_1.setVisibility(View.GONE)
                        if (response.body()!!.userdata != null) {
                            Log.w("MessageList_RESPONSE_RESULT", "Result : " + response.body()!!.userdata)
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: ArrayList<getMessageDetaildataResponse>? = response.body()!!.userdata!!.msg_data!!
                            for (item: getMessageDetaildataResponse in list!!.iterator()) {
                                messagesdetailistArray!!.add(item)

                                //Log.w("messages", "Result : "+messagesdetailistArray.get())
                            }
                            Log.w("NEWSLISTARRAYSIZE", "Result : " + messagesdetailistArray!!.size)


                            val news_list_adapter = NewsCommentsDetailsRecyclerAdapter(messagesdetailistArray!!, this@MessageDetailsActivity!!)
                            rv_messagedetailview.setAdapter(news_list_adapter)
                            news_list_adapter.notifyDataSetChanged()
                            rv_messagedetailview.setHasFixedSize(true)

                            rv_messagedetailview.layoutManager = LinearLayoutManager(this@MessageDetailsActivity, LinearLayoutManager.VERTICAL, false)
                            //rv_messagedetailview.setLayoutManager(LinearLayoutManager(this@MessageDetailsActivity))
                            rv_messagedetailview.setItemAnimator(DefaultItemAnimator())
                            Log.w("SCROLLPOSITION", "SENT")



                            Log.d("NEWSARRAYSIZE", messagesdetailistArray!!.size.toString())

                            if (messagesdetailistArray!!.size == 0) {
                                //noservice!!.visibility = View.VISIBLE
                                rv_messagedetailview!!.visibility = View.GONE
                            }else{
                                rv_messagedetailview.smoothScrollToPosition(messagesdetailistArray!!.size - 1)
                            }
                        }



                    }

                    //  }
                }

                override fun onFailure(call: Call<MessageDetailviewResponse>, t: Throwable) {
                    Log.w("Result_Order_details", t.toString())
                }
            })
        } else {
            val call = apiService.memberMessagesApi(
                msg_date_id_stg,user_id = user_details[DllSessionManager.USER_ID]!!.toString(),match_id = msg_from_stg)
            messagesdetailistArray!!.clear()
            call.enqueue(object : Callback<MessageDetailviewResponse> {
                override fun onResponse(
                    call: Call<MessageDetailviewResponse>,
                    response: retrofit2.Response<MessageDetailviewResponse>?
                ) {
                    //  if (response != null) {
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                        progress_1.setVisibility(View.GONE)
                        if (response.body()!!.userdata != null) {
                            Log.w("MessageList_RESPONSE_RESULT", "Result : " + response.body()!!.userdata)
                            // setProfileDatatoView(response.body()!!.user_info)
                            val list: ArrayList<getMessageDetaildataResponse>? = response.body()!!.userdata!!.msg_data!!
                            for (item: getMessageDetaildataResponse in list!!.iterator()) {
                                messagesdetailistArray!!.add(item)
                            }
                            Log.w("NEWSLISTARRAYSIZE", "Result : " + messagesdetailistArray!!.size)
                            val news_list_adapter = NewsCommentsDetailsRecyclerAdapter(messagesdetailistArray!!, this@MessageDetailsActivity!!)
                            rv_messagedetailview.setAdapter(news_list_adapter)
                            news_list_adapter.notifyDataSetChanged()
                            rv_messagedetailview.setHasFixedSize(true)
                            rv_messagedetailview.setLayoutManager(LinearLayoutManager(this@MessageDetailsActivity))

                            rv_messagedetailview.setItemAnimator(DefaultItemAnimator())

                            Log.d("NEWSARRAYSIZE", messagesdetailistArray!!.size.toString())

                            if (messagesdetailistArray!!.size == 0) {
                                //noservice!!.visibility = View.VISIBLE
                                rv_messagedetailview!!.visibility = View.GONE
                            }else{
                                rv_messagedetailview.smoothScrollToPosition(messagesdetailistArray!!.size - 1)
                            }
                        }

                    }

                    //  }
                }

                override fun onFailure(call: Call<MessageDetailviewResponse>, t: Throwable) {
                    Log.w("Result_Order_details", t.toString())
                }
            })
        }



        Log.d("MessageDetailViewApi", msg_id_stg)

    }



    inner class NewsCommentsDetailsRecyclerAdapter(val mServiceList: ArrayList<getMessageDetaildataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder2>() {


        internal lateinit var my_loader: Dialog
        internal lateinit var sessionManager: DllSessionManager
        var comment_id_string = ""

        var arrowdownup = false
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.message_list_item, parent, false))

        }

        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {


            sessionManager = DllSessionManager(context)

            if(mServiceList.get(position).msg_from.equals(user_details.get(DllSessionManager.USER_ID))){

                holder?.tv_message_from_detail.setText(mServiceList.get(position).msg_content.trim())
                holder?.tv_msg_detail_from_date.setText(editDateFormat(mServiceList.get(position).msg_date))

                holder?.tv_message_from_detail.visibility = View.VISIBLE
                holder?.tv_msg_detail_from_date.visibility = View.VISIBLE
               // holder?.iv_user_img.visibility = View.VISIBLE

                holder?.tv_message_by_me.visibility = View.GONE
                holder?.tv_msg_detail_date.visibility = View.GONE

            }else{

                holder?.tv_message_by_me.setText(mServiceList.get(position).msg_content.trim())
                holder?.tv_msg_detail_date.setText(editDateFormat(mServiceList.get(position).msg_date))

                holder?.tv_message_by_me.visibility = View.VISIBLE
                holder?.tv_msg_detail_date.visibility = View.VISIBLE

                holder?.tv_message_from_detail.visibility = View.GONE
                holder?.tv_msg_detail_from_date.visibility = View.GONE
               // holder?.iv_user_img.visibility = View.GONE

            }


            /*Picasso.with(context)
                .load(photobaseurl+user_details.get(DllSessionManager.PHOTO1).toString())
                .error(R.drawable.ic_man_user)
                .into(holder?.iv_user_img)*/



          //  Log.e("phot_lll",mServiceList.get(position).photo1)
        }




        // Gets the number of animals in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }

    }


    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val tv_message_by_me = view.findViewById(R.id.tv_message_by_me) as CustomTextView
        val tv_msg_detail_date = view.findViewById(R.id.tv_msg_detail_date) as CustomTextView
        val tv_message_from_detail = view.findViewById(R.id.tv_message_from_detail) as CustomTextView
        val tv_msg_detail_from_date = view.findViewById(R.id.tv_msg_detail_from_date) as CustomTextView
        val iv_user_img = view.findViewById(R.id.iv_user_img) as ImageView
       // val ll_messages_from = view.findViewById(R.id.ll_messages_from) as LinearLayout




    }

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

}
