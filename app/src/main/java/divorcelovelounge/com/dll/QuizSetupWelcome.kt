package divorcelovelounge.com.dll

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.widget.ImageView
import android.widget.LinearLayout
import divorcelovelounge.com.dll.Helper.DllSessionManager

class QuizSetupWelcome : AppCompatActivity() {

    lateinit var ll_proceed: LinearLayout
    lateinit var ll_skip: LinearLayout
    lateinit var iv_quiz_img: ImageView
    lateinit var session: DllSessionManager
    lateinit var userDetails: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_setup_welcome)

        ll_proceed = findViewById(R.id.ll_proceed)
        ll_skip = findViewById(R.id.ll_skip)
        iv_quiz_img = findViewById(R.id.iv_quiz_img)
        session = DllSessionManager(this@QuizSetupWelcome)
        userDetails = session.userDetails

        iv_quiz_img.setImageResource(R.drawable.quiz_img)

        ll_proceed.setOnClickListener {
            val intent = Intent(this@QuizSetupWelcome, QuizSetupOne::class.java)
            startActivity(intent)
        }
        ll_skip.setOnClickListener {
            val builder = AlertDialog.Builder(this@QuizSetupWelcome)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to SKIP?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {

                        val photo1 = userDetails.get(DllSessionManager.PHOTO1)
                        if(photo1.equals("0")){
                            val home_intent = Intent(this@QuizSetupWelcome, ProfilePhotos::class.java)
                            //home_intent.putExtra("from_list","match_setup")
                            startActivity(home_intent)
                            finish()
                        }else {

                            val home_intent = Intent(this@QuizSetupWelcome, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "match_setup")
                            startActivity(home_intent)
                            finish()
                        }


                        /*val home_intent = Intent(this@QuizSetupWelcome, DashboardActivity::class.java)
                        home_intent.putExtra("from_list","match_setup")
                        startActivity(home_intent)
                        finish()*/
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }
    }

    /*override fun onBackPressed() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity()
        } else {
            ActivityCompat.finishAffinity(this)
        }
    }*/
}
