package divorcelovelounge.com.dll

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import divorcelovelounge.com.dll.Adapters.LocationSearchAdapter
import divorcelovelounge.com.dll.Adapters.ProfileLocationAdapter
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.*
import divorcelovelounge.com.dll.Utils.InputFilterMinMax
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class RegisterAccount : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    lateinit var sp_iam_id: Spinner
    lateinit var sp_looking_id: Spinner
    lateinit var ll_profile_1: LinearLayout
    lateinit var ll_profile_2: LinearLayout
    lateinit var bt_lets_go_id: Button
    lateinit var bt_next_id: Button
    lateinit var et_first_name_id: EditText
    lateinit var et_zipcode_id: EditText
    lateinit var et_mobile_id: EditText
    lateinit var et_born_id: EditText
    // lateinit var sp_state_id: Spinner
    lateinit var et_user_name_id: EditText
    lateinit var et_social_name_id: EditText
    lateinit var et_email_id: EditText
    lateinit var et_password_id: EditText
    lateinit var et_state_id: EditText
    lateinit var sp_how_know_id: Spinner
    lateinit var iv_register_back: ImageView
    private lateinit var progressDialog: Dialog
    private lateinit var state_searchListView: ListView
    private lateinit var state_searchEditText: EditText
    private lateinit var state_searchEmpty: CustomTextView
    private lateinit var country_searchListView: ListView
    private lateinit var country_searchEditText: EditText
    private lateinit var country_searchEmpty: CustomTextView
    private lateinit var city_searchListView: ListView
    private lateinit var city_searchEditText: EditText
    private lateinit var city_searchEmpty: CustomTextView

    var profile_stage = "one"
    var iam_a_stg = ""
    var lookinig_a_stg = ""
    var state_stg = ""
    var how_know_stg = ""

    /*var gender_stg = ""
    var lookinig_for_a_stg = ""
    var get_dob_social_stg = ""
    var get_zipcode_social_stg = ""
    var social_country = ""
    var state_social_stg = ""*/
    var user_id_social_stg = ""

    var get_name_stg = ""
    var get_zipcode_stg = ""
    var get_email_stg = ""
    var get_uName_stg = ""
    var get_password_stg = ""
    var get_mobile_stg = ""
    var get_dob_stg = ""
    var location = ""
    var oclickboolean = ""
    var country = "USA"
    var city = ""
    lateinit var dllSessionManager: DllSessionManager
    private lateinit var stateSearchAdapter: LocationSearchAdapter
    private lateinit var countrySearchAdapter: LocationSearchAdapter

    private lateinit var state_search_dialog: Dialog
    private lateinit var country_search_dialog: Dialog
    private lateinit var city_search_dialog: Dialog
    private lateinit var tv_state_alert_done: CustomTextViewBold
    private lateinit var tv_state_alert_header: CustomTextView
    private lateinit var iv_state_alert_close: ImageView
    private lateinit var tv_country_alert_done: CustomTextViewBold
    private lateinit var tv_country_alert_header: CustomTextView
    private lateinit var iv_country_alert_close: ImageView
    private lateinit var tv_city_alert_done: CustomTextViewBold
    private lateinit var tv_city_alert_header: CustomTextView
    private lateinit var iv_city_alert_close: ImageView
    private lateinit var et_country_id: EditText

    lateinit var countriesList: ArrayList<String>
    lateinit var countriesDataList: ArrayList<CountriesDataResponse>

    var device_token_stg = ""
    var device_type_stg = ""
    var dataStorage: DataStorage = DataStorage()

    lateinit var tv_terms_id: TextView
    lateinit var cb_terms_id: CheckBox

    lateinit var terms_dialog: Dialog
    lateinit var tv_ok_btn_id: CustomTextView
    lateinit var terms_web_id: WebView

    lateinit var paylink: String
    lateinit var uri: Uri

    var citiesDataList: ArrayList<CitiesDataResponse> = ArrayList()
    var citiesList: ArrayList<String> = ArrayList()
    private lateinit var cityAdapter: ProfileLocationAdapter
    private lateinit var et_city_id: EditText

    private var emailAlreadyExists = false
    private var uNameAlreadyExists = false
    lateinit var sliderLayout: SliderLayout
    private lateinit var addsarrayList: ArrayList<AdsListDataResponse>

    private val ALL_PERMISSION_CODE = 23

    lateinit var location_details_list: HashMap<String, String>
    var gps_county_stg=""
    var gps_state_stg=""
    var gps_city_stg=""

    @SuppressLint("SetJavaScriptEnabled", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_account)
        dllSessionManager = DllSessionManager(this@RegisterAccount)

        val from = intent.getStringExtra("from")

        device_token_stg = dataStorage.getFcmToken(this)
        device_type_stg = resources.getString(R.string.device_type)


        val state_codes = resources.getStringArray(R.array.state_codes)
        val states_names = resources.getStringArray(R.array.state_names)

        getCountries()


        sp_iam_id = findViewById(R.id.sp_iam_id)
        sp_looking_id = findViewById(R.id.sp_looking_id)
        ll_profile_1 = findViewById(R.id.ll_profile_1)
        ll_profile_2 = findViewById(R.id.ll_profile_2)
        et_first_name_id = findViewById(R.id.et_first_name_id)
        et_zipcode_id = findViewById(R.id.et_zipcode_id)
        // sp_state_id = findViewById(R.id.sp_state_id)
        et_user_name_id = findViewById(R.id.et_user_name_id)
        et_social_name_id = findViewById(R.id.et_social_name_id)
        et_email_id = findViewById(R.id.et_email_id)
        et_mobile_id = findViewById(R.id.et_mobile_id)
        et_password_id = findViewById(R.id.et_password_id)
        sp_how_know_id = findViewById(R.id.sp_how_know_id)
        bt_lets_go_id = findViewById(R.id.bt_lets_go_id)
        bt_next_id = findViewById(R.id.bt_next_id)
        iv_register_back = findViewById(R.id.iv_register_back)
        et_state_id = findViewById(R.id.et_state_id)
        et_born_id = findViewById(R.id.et_born_id)
        et_country_id = findViewById(R.id.et_country_id)
        cb_terms_id = findViewById(R.id.cb_terms_id)
        tv_terms_id = findViewById(R.id.tv_terms_id)
        et_city_id = findViewById(R.id.et_city_id)

        sliderLayout = findViewById(R.id.slider)
        addsarrayList = ArrayList()

        location_details_list = dllSessionManager.locationDetails
        gps_county_stg= location_details_list[DllSessionManager.USER_GPS_COUNTRY].toString()
        gps_state_stg= location_details_list[DllSessionManager.USER_GPS_STATE].toString()
        gps_city_stg= location_details_list[DllSessionManager.USER_GPS_CITY].toString()

      //  getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Signup")

        tv_terms_id.paintFlags = tv_terms_id.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        et_state_id.isFocusable = false
        et_city_id.isFocusable = false

        if (from.equals("social_register")) {
            et_first_name_id.setText(dllSessionManager.userDetails.get(DllSessionManager.USER_NAME))
            user_id_social_stg =
                (dllSessionManager.userDetails[DllSessionManager.USER_ID]).toString()
            et_first_name_id.isFocusable = false
            iv_register_back.visibility = View.GONE
            et_social_name_id.visibility=View.VISIBLE

        }

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

        val statesNamesList = ArrayList<String>()
        for (i in 0 until states_names.size) {
            statesNamesList.add(states_names[i])
        }

        et_mobile_id.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(10))

/* Alert dialog for location search - Start */
        state_search_dialog = Dialog(this@RegisterAccount)
        state_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        state_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        state_search_dialog.setContentView(R.layout.alert_dialog_location_search)
        val filter_window = state_search_dialog.window
        filter_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        tv_state_alert_done = state_search_dialog.findViewById(R.id.tv_alert_done)
        iv_state_alert_close = state_search_dialog.findViewById(R.id.iv_alert_close)
        tv_state_alert_header = state_search_dialog.findViewById(R.id.tv_alert_header)

        state_searchEditText = state_search_dialog.findViewById(R.id.searchEditText)
        state_searchListView = state_search_dialog.findViewById(R.id.searchListView)
        state_searchEmpty = state_search_dialog.findViewById(R.id.searchEmpty)

        tv_state_alert_header.text = resources.getString(R.string.search_your_state)
        state_searchEditText.hint = resources.getString(R.string.search_your_state)

        stateSearchAdapter =
            LocationSearchAdapter(this@RegisterAccount, statesNamesList)
        state_searchListView.adapter = stateSearchAdapter

        state_searchEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {
                if (char.isNotEmpty()) {
                    stateSearchAdapter.filter.filter(char.toString())
                } else {
                    stateSearchAdapter.filter.filter("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}
        })

        state_searchListView.emptyView = state_searchEmpty
        state_searchListView.setOnItemClickListener { adapterView, view, position, l ->

            try {
                location = stateSearchAdapter.getItem(position).toString()
                state_stg = state_codes[position].toString()
                if (country == "USA") {
                    getCityApi(state_stg)
                }
                stateSearchAdapter.setSelectedIndex(position)
                Log.d("RegisterAccount", "location : $location $state_stg")
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

        }

        tv_state_alert_done.setOnClickListener {
            et_state_id.setText(location)
            state_search_dialog.dismiss()
        }

        iv_state_alert_close.setOnClickListener {
            state_search_dialog.dismiss()
        }
        /* Alert dialog for location search - End */

        /* Alert dialog for country search - Start */
        country_search_dialog = Dialog(this@RegisterAccount)
        country_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        country_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        country_search_dialog.setContentView(R.layout.alert_dialog_location_search)
        val filter_window_country = country_search_dialog.window
        filter_window_country!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        tv_country_alert_done = country_search_dialog.findViewById(R.id.tv_alert_done)
        iv_country_alert_close = country_search_dialog.findViewById(R.id.iv_alert_close)
        tv_country_alert_header = country_search_dialog.findViewById(R.id.tv_alert_header)

        country_searchEditText = country_search_dialog.findViewById(R.id.searchEditText)
        country_searchListView = country_search_dialog.findViewById(R.id.searchListView)
        country_searchEmpty = country_search_dialog.findViewById(R.id.searchEmpty)

        tv_country_alert_header.text = "Select your Country"
        country_searchEditText.hint = "Select your Country"

        country_searchEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {
                if (char.isNotEmpty()) {
                    countrySearchAdapter.filter.filter(char.toString())
                } else {
                    countrySearchAdapter.filter.filter("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}

        })

        country_searchListView.emptyView = country_searchEmpty
        country_searchListView.setOnItemClickListener { adapterView, view, position, l ->

            try {
                country = countrySearchAdapter.getItem(position).toString()
                countrySearchAdapter.setSelectedIndex(position)
                Log.d("RegisterAccount", "country : $country ")
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

        }

        tv_country_alert_done.setOnClickListener {
            et_country_id.setText(country)
            et_state_id.setText("")
            et_city_id.setText("")
            et_zipcode_id.setText("")
            Log.d("INSIDEDONE", "country : $country $state_stg")
            if (country == "USA") {
                et_zipcode_id.clearFocus()
                et_state_id.isFocusable = false
                et_city_id.isFocusable = false
                et_state_id.hint = "Select your State"
                et_city_id.hint = "Select your City"
                et_zipcode_id.filters =
                    arrayOf<InputFilter>(InputFilterMinMax(0, 99999), InputFilter.LengthFilter(5))
                this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            } else {
                et_state_id.hint = "Enter your State"
                et_zipcode_id.clearFocus()
                et_state_id.isFocusable = true
                et_state_id.isClickable = false
                et_state_id.isFocusableInTouchMode = true
                et_city_id.hint = "Enter your City"
                et_city_id.isFocusable = true
                et_city_id.isClickable = false
                et_city_id.isFocusableInTouchMode = true
                et_zipcode_id.filters =
                    arrayOf<InputFilter>(InputFilterMinMax(0, 999999), InputFilter.LengthFilter(6))
                this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            }

            country_search_dialog.dismiss()
            state_search_dialog.dismiss()

        }

        iv_country_alert_close.setOnClickListener {
            country_search_dialog.dismiss()
        }
        /* Alert dialog for country search - End */

        /* Alert dialog for city search - Start */
        city_search_dialog = Dialog(this@RegisterAccount)
        city_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        city_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        city_search_dialog.setContentView(R.layout.alert_dialog_location_search)
        val filter_window_city = city_search_dialog.window
        filter_window_city!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        tv_city_alert_done = city_search_dialog.findViewById(R.id.tv_alert_done)
        iv_city_alert_close = city_search_dialog.findViewById(R.id.iv_alert_close)
        tv_city_alert_header = city_search_dialog.findViewById(R.id.tv_alert_header)

        city_searchEditText = city_search_dialog.findViewById(R.id.searchEditText)
        city_searchListView = city_search_dialog.findViewById(R.id.searchListView)
        city_searchEmpty = city_search_dialog.findViewById(R.id.searchEmpty)

        tv_city_alert_header.text = "Select your City"
        city_searchEditText.hint = "Select your City"

        city_searchEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(char: CharSequence, start: Int, before: Int, count: Int) {
                if (char.isNotEmpty()) {
                    cityAdapter.filter.filter(char.toString())
                } else {
                    cityAdapter.filter.filter("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}

        })

        city_searchListView.emptyView = city_searchEmpty
        city_searchListView.setOnItemClickListener { adapterView, view, position, l ->

            try {
                city = cityAdapter.getItem(position).toString()
                cityAdapter.setSelectedIndex(position)

                Log.d("RegisterAccount", "city : $city ")
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

        }

        tv_city_alert_done.setOnClickListener {
            et_city_id.setText(city)
            Log.d("INSIDEDONE", "country : $country $state_stg $city")
            if (country == "USA") {
                et_city_id.isFocusable = false
                et_city_id.hint = "Select your City"
            } else {
                et_city_id.hint = "Enter your City"
                et_city_id.isFocusable = true
                et_city_id.isClickable = false
                et_city_id.isFocusableInTouchMode = true
            }

            city_search_dialog.dismiss()

        }

        iv_city_alert_close.setOnClickListener {
            city_search_dialog.dismiss()
        }

        /* Alert dialog for city search - End */

        et_state_id.setOnClickListener {
            Log.d("click", "country : $country")
            if (country == "USA") {
                state_search_dialog.show()
            } else {
                Log.d("STATEELSE", "country : $country")
                state_search_dialog.dismiss()
                et_state_id.isFocusable = true
                et_state_id.isClickable = true

            }

        }

        et_city_id.setOnClickListener {
            Log.d("click", "country : $country")
            if (country == "USA") {
                city_search_dialog.show()
            } else {
                Log.d("STATEELSE", "country : $country")
                city_search_dialog.dismiss()
                et_city_id.isFocusable = true
                et_city_id.isClickable = true

            }
        }

        et_born_id.setOnClickListener {
            showDateTimePicker()
        }

        bt_lets_go_id.setOnClickListener {

            if (iam_a_stg == lookinig_a_stg) {
                Toast.makeText(
                    this@RegisterAccount,
                    "Please choose different gender!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (et_first_name_id.text.toString() == "" || et_zipcode_id.text.toString() == "" ||
                et_state_id.text.toString() == "" || et_born_id.text.toString() == "" || et_country_id.text.toString() == "" || et_city_id.text.toString() == ""
            ) {
                Toast.makeText(this@RegisterAccount, "All fields are required!", Toast.LENGTH_SHORT)
                    .show()
            } else if (!cb_terms_id.isChecked()) {
                Toast.makeText(
                    this@RegisterAccount,
                    "Accept Terms and Conditions",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if (from == "social_register") {

                    if (country != "USA") {
                        state_stg = et_state_id.text.toString()
                        city = et_city_id.text.toString()
                    }

                    /*et_social_name_id.text.toString().trim().equals("")*/
                    if (uNameAlreadyExists==true){
                        socialregisterApi(
                            iam_a_stg,
                            lookinig_a_stg,
                            editDateFormat(et_born_id.text.toString()),
                            et_zipcode_id.text.toString(),
                            country,
                            state_stg,
                            city,
                            user_id_social_stg,
                            device_token_stg,
                            device_type_stg,
                            et_social_name_id.text.toString().trim(),
                            "1"
                        )
                    }



                } else {
                    ll_profile_1.visibility = View.GONE
                    ll_profile_2.visibility = View.VISIBLE
                    //sliderLayout.visibility = View.VISIBLE

                    getAdsLisrAPI(gps_county_stg, gps_state_stg, gps_city_stg, "Signup")

                    profile_stage = "two"
                }
            }

        }

        iv_register_back.setOnClickListener {
            onBackPressed()
        }


        /* et_state_id.setOnClickListener {
             state_search_dialog.show()
         }*/

        et_country_id.setOnClickListener {
            country_search_dialog.show()
        }

        // phone number text watcher
        /* et_mobile_id.addTextChangedListener(PhoneNumberFormattingTextWatcher())*/



        et_password_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val password_stg = et_password_id.text.toString()
                Log.e("data count : ", password_stg + "---" + password_stg.length)
                if (password_stg.length > 5) {
                    et_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_correct,
                        0
                    )
                } else if (password_stg.length == 0) {
                    et_password_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    et_password_id.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_error,
                        0
                    )
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        et_email_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (Patterns.EMAIL_ADDRESS.matcher(et_email_id.text.toString()).matches()) {
                    checkIfEmailExists(et_email_id.text.toString())
                } else {
                    et_email_id.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_error, 0
                    )
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        })

        et_user_name_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //Service Call
                    checkUsernameAPI(et_user_name_id.text.toString(),"register")

                Log.e("username_lll",et_user_name_id.text.toString())

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        })

        et_social_name_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //Service Call
                    checkUsernameAPI(et_social_name_id.text.toString(),"social")

                Log.e("username_lll",et_social_name_id.text.toString())

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        })


        bt_next_id.setOnClickListener {

            get_password_stg = et_password_id.text.toString()
            Log.d("RegisterAccount", "emailAlreadyExists : $emailAlreadyExists")

            if (et_email_id.text.toString() == "" || et_password_id.text.toString() == "") {
                Toast.makeText(
                    this@RegisterAccount,
                    "All fields are required!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email_id.text.toString()).matches()) {
                Toast.makeText(
                    this@RegisterAccount,
                    "Invalid Email address!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (get_password_stg.length < 6) {
                Toast.makeText(
                    this@RegisterAccount,
                    "Password should atleast 6 characters",
                    Toast.LENGTH_SHORT
                ).show()
            }else if(uNameAlreadyExists==false){
                Toast.makeText(
                    this@RegisterAccount,
                    "Invalid Username",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                progressDialog.show()

                get_name_stg = et_first_name_id.text.toString()
                get_zipcode_stg = et_zipcode_id.text.toString()
                get_mobile_stg = et_mobile_id.text.toString()
                get_dob_stg = editDateFormat(et_born_id.text.toString())
                get_email_stg = et_email_id.text.toString()
                get_uName_stg = et_user_name_id.text.toString()
                //get_password_stg = et_password_id.text.toString()
                if (country != "USA") {
                    state_stg = et_state_id.text.toString()
                    city = et_city_id.text.toString()
                }
                Log.e("lookinig_a_stg",lookinig_a_stg+"----"+iam_a_stg)

                val apiService = ApiInterface.create()

                val call = apiService.registerApi(
                    iam_a_stg,
                    lookinig_a_stg,
                    get_name_stg,
                    get_dob_stg,
                    get_zipcode_stg,
                    country,
                    state_stg,
                    city,
                    how_know_stg,
                    get_mobile_stg,
                    get_uName_stg,
                    get_email_stg,
                    get_password_stg,
                    device_token_stg,
                    device_type_stg,
                    "1"
                )
                call.enqueue(object : Callback<RegisterprofileResponse> {
                    override fun onResponse(
                        call: Call<RegisterprofileResponse>,
                        response: retrofit2.Response<RegisterprofileResponse>?
                    ) {

                        if (response!!.body() != null) {

                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                            }


                            if (response.body()!!.status == "1") {

                                val user_data = response.body()!!.user_info
                                Log.e(
                                    "user_data_lll",
                                    user_data.toString() + "---" + user_data!!.email
                                )

                                LoginApi_Register(
                                    user_data.email,
                                    user_data.password,
                                    device_token_stg,
                                    device_type_stg
                                )

                            } else {
                                Log.e("user_data_lll", response.body()!!.status)
                                Toast.makeText(
                                    this@RegisterAccount,
                                    response.body()!!.result, Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        } else {
                            Toast.makeText(
                                this@RegisterAccount,
                                "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                            ).show()
                        }

                    }

                    override fun onFailure(call: Call<RegisterprofileResponse>, t: Throwable) {
                        Log.w("Result_Order_details", t.toString())

                        if (progressDialog.isShowing) {
                            progressDialog.dismiss()
                        }

                        Toast.makeText(
                            this@RegisterAccount,
                            "Something went wrong! Please try again", Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            }


        }


        sp_iam_id.onItemSelectedListener =
            object : OnItemSelectedListener {
                override fun onItemSelected(
                    parentView: AdapterView<*>,
                    selectedItemView: View,
                    position: Int,
                    id: Long
                ) {
                    // your code here
                    iam_a_stg = parentView.getItemAtPosition(position).toString()
                    sp_looking_id.setSelection(position)

                    Log.e("gender_selected", iam_a_stg)
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    // your code here
                }

            }

        sp_looking_id.onItemSelectedListener =
            object : OnItemSelectedListener {
                override fun onItemSelected(
                    parentView: AdapterView<*>,
                    selectedItemView: View,
                    position: Int,
                    id: Long
                ) {
                    // your code here
                    lookinig_a_stg = parentView.getItemAtPosition(position).toString()
                    //sp_iam_id.setSelection(position)
                    Log.e("gender_a_selected", lookinig_a_stg)
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    // your code here
                }

            }

        /*sp_state_id.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                // your code here
                //  state_stg = parentView.getItemAtPosition(position).toString()
                state_stg = state_codes[position].toString()
                Log.e("gender_a_selected", state_stg)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        }*/

        sp_how_know_id.onItemSelectedListener =
            object : OnItemSelectedListener {
                override fun onItemSelected(
                    parentView: AdapterView<*>,
                    selectedItemView: View,
                    position: Int,
                    id: Long
                ) {
                    // your code here
                    how_know_stg = parentView.getItemAtPosition(position).toString()
                    Log.e("gender_a_selected", how_know_stg)
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    // your code here
                }

            }


        paylink = "https://divorcelovelounge.com/appTermsOfUse"
        if (paylink.contains("http") || paylink.contains("https")) {
            uri = Uri.parse(paylink)
        } else {
            uri = Uri.parse("http://$paylink")
        }

        terms_dialog = Dialog(this)
        terms_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        terms_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        terms_dialog.setContentView(R.layout.terms_layout_dialog)
        val success_window = terms_dialog.window
        success_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )


        terms_web_id = terms_dialog.findViewById(R.id.terms_web_id)

        tv_ok_btn_id = terms_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            terms_dialog.dismiss()

        }

        terms_web_id.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        terms_web_id.settings.javaScriptEnabled = true;
        terms_web_id.isHorizontalScrollBarEnabled = false;
        terms_web_id.loadUrl(uri.toString())



        tv_terms_id.setOnClickListener {
            terms_dialog.show()
        }

    }


    override fun onBackPressed() {
        if (profile_stage.equals("two")) {
            ll_profile_1.visibility = View.VISIBLE
            ll_profile_2.visibility = View.GONE
            sliderLayout.visibility = View.GONE
            profile_stage = "one"
        } else {
            super.onBackPressed()
        }
        //super.onBackPressed()
    }

    fun LoginApi_Register(
        email: String,
        password: String,
        device_token: String,
        device_type: String
    ) {
        var user_city = ""
        var user_dob = ""
        var user_phone_number = ""
        var user_ethnicity = ""
        var user_religion = ""
        var user_denomination = ""
        var user_education = ""
        var user_occupation = ""
        var user_income = ""
        var user_smoke = ""
        var user_drink = ""
        var user_height_ft = ""
        var user_height_inc = ""
        var user_height_cms = ""
        var user_passionate = ""
        var user_leisure_time = ""
        var user_thankful_1 = ""
        var user_thankful_2 = ""
        var user_thankful_3 = ""
        var part_age_min_data = ""
        var part_age_max_data = ""
        var match_distance_data = ""
        var match_search_imp_data = ""
        var partner_ethnicity = ""
        var partner_religion = ""
        var partner_denomination = ""
        var partner_education = ""
        var partner_occupation = ""
        var partner_smoke = ""
        var partner_drink = ""
        var photo1 = ""
        var photo2 = ""
        var photo3 = ""
        var photo4 = ""
        var photo5 = ""
        var photo6 = ""
        var photo7 = ""
        var photo8 = ""
        var photo9 = ""
        var photo10 = ""
        var provider = ""
        var country_stg = ""
        var mother_tongue_stg = ""
        var partner_mother_tongue_stg = ""
        var gender = ""
        var looking_for = ""
        var user_first_name = ""
        var user_email = ""
        var user_zipcode = ""
        var user_state = ""
        var user_referal_from = ""
        var username_stg = ""
        var new_password_stg = ""


        // progress_bar_login.visibility = View.VISIBLE
        val apiService = ApiInterface.create()
        val call = apiService.loginApi(email, password, device_token, device_type)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<CheckLoginResponse> {
            override fun onResponse(
                call: Call<CheckLoginResponse>,
                response: retrofit2.Response<CheckLoginResponse>?
            ) {
                // progress_bar_login.visibility = View.GONE
                // login_dialog.dismiss()
                if (response!!.body() != null) {
                    if (response.body()!!.status == "1" || response.body()!!.result.toString() == "success") {

                        val user_data = response.body()!!.user_info


                        if (user_data!!.city.equals(null)) {
                            user_city = "0"
                        } else {
                            user_city = user_data.city!!
                        }
                        if (user_data.dob.equals(null)) {
                            user_dob = "0"
                        } else {
                            user_dob = user_data.dob!!
                        }
                        if (user_data.provider.equals(null)) {
                            provider = "0"
                        } else {
                            provider = user_data.provider!!
                        }
                        if (user_data.phone_number.equals(null)) {
                            user_phone_number = "0"
                        } else {
                            user_phone_number = user_data.phone_number!!
                        }
                        if (user_data.ethnicity.equals(null)) {
                            user_ethnicity = "0"
                        } else {
                            user_ethnicity = user_data.ethnicity!!
                        }
                        if (user_data.religion.equals(null)) {
                            user_religion = "0"
                        } else {
                            user_religion = user_data.religion!!
                        }

                        if (user_data.education.equals(null)) {
                            user_education = "0"
                        } else {
                            user_education = user_data.education!!
                        }
                        if (user_data.occupation.equals(null)) {
                            user_occupation = "0"
                        } else {
                            user_occupation = user_data.occupation!!
                        }
                        if (user_data.income.equals(null)) {
                            user_income = "0"
                        } else {
                            user_income = user_data.income!!
                        }
                        if (user_data.smoke.equals(null)) {
                            user_smoke = "0"
                        } else {
                            user_smoke = user_data.smoke!!
                        }
                        if (user_data.drink.equals(null)) {
                            user_drink = "0"
                        } else {
                            user_drink = user_data.drink!!
                        }
                        if (user_data.height_ft.equals(null)) {
                            user_height_ft = "0"
                        } else {
                            user_height_ft = user_data.height_ft!!
                        }
                        if (user_data.height_inc.equals(null)) {
                            user_height_inc = "0"
                        } else {
                            user_height_inc = user_data.height_inc!!
                        }
                        if (user_data.height_cms.equals(null)) {
                            user_height_cms = "0"
                        } else {
                            user_height_cms = user_data.height_cms!!
                        }
                        if (user_data.passionate.equals(null)) {
                            user_passionate = "0"
                        } else {
                            user_passionate = user_data.passionate!!
                        }
                        if (user_data.leisure_time.equals(null)) {
                            user_leisure_time = "0"
                        } else {
                            user_leisure_time = user_data.leisure_time!!
                        }
                        if (user_data.thankful_1.equals(null)) {
                            user_thankful_1 = "0"
                        } else {
                            user_thankful_1 = user_data.thankful_1!!
                        }
                        if (user_data.thankful_2.equals(null)) {
                            user_thankful_2 = "0"
                        } else {
                            user_thankful_2 = user_data.thankful_2!!
                        }
                        if (user_data.thankful_3.equals(null)) {
                            user_thankful_3 = "0"
                        } else {
                            user_thankful_3 = user_data.thankful_3!!
                        }
                        if (user_data.partner_age_min.equals(null)) {
                            part_age_min_data = "0"
                        } else {
                            part_age_min_data = user_data.partner_age_min!!
                        }
                        if (user_data.partner_age_max.equals(null)) {
                            part_age_max_data = "0"
                        } else {
                            part_age_max_data = user_data.partner_age_max!!
                        }

                        if (user_data.partner_match_distance.equals(null)) {
                            match_distance_data = "0"
                        } else {
                            match_distance_data = user_data.partner_match_distance!!
                        }
                        if (user_data.partner_match_search.equals(null)) {
                            match_search_imp_data = "0"
                        } else {
                            match_search_imp_data = user_data.partner_match_search!!
                        }
                        if (user_data.partner_ethnicity.equals(null)) {
                            partner_ethnicity = "0"
                        } else {
                            partner_ethnicity = user_data.partner_ethnicity!!
                        }
                        if (user_data.partner_religion.equals(null)) {
                            partner_religion = "0"
                        } else {
                            partner_religion = user_data.partner_religion!!
                        }

                        if (user_data.partner_education.equals(null)) {
                            partner_education = "0"
                        } else {
                            partner_education = user_data.partner_education!!
                        }
                        if (user_data.partner_occupation.equals(null)) {
                            partner_occupation = "0"
                        } else {
                            partner_occupation = user_data.partner_occupation!!
                        }
                        if (user_data.partner_smoke.equals(null)) {
                            partner_smoke = "0"
                        } else {
                            partner_smoke = user_data.partner_smoke!!
                        }
                        if (user_data.partner_drink.equals(null)) {
                            partner_drink = "0"
                        } else {
                            partner_drink = user_data.partner_drink!!
                        }
                        if (user_data.photo1.equals(null)) {
                            photo1 = "0"
                        } else {
                            photo1 = user_data.photo1!!
                        }
                        if (user_data.photo2.equals(null)) {
                            photo2 = "0"
                        } else {
                            photo2 = user_data.photo2!!
                        }
                        if (user_data.photo3.equals(null)) {
                            photo3 = "0"
                        } else {
                            photo3 = user_data.photo3!!
                        }
                        if (user_data.photo4.equals(null)) {
                            photo4 = "0"
                        } else {
                            photo4 = user_data.photo4!!
                        }
                        if (user_data.photo5.equals(null)) {
                            photo5 = "0"
                        } else {
                            photo5 = user_data.photo5!!
                        }
                        if (user_data.photo6.equals(null)) {
                            photo6 = "0"
                        } else {
                            photo6 = user_data.photo6!!
                        }
                        if (user_data.photo7.equals(null)) {
                            photo7 = "0"
                        } else {
                            photo7 = user_data.photo7!!
                        }
                        if (user_data.photo8.equals(null)) {
                            photo8 = "0"
                        } else {
                            photo8 = user_data.photo8!!
                        }
                        if (user_data.photo9.equals(null)) {
                            photo9 = "0"
                        } else {
                            photo9 = user_data.photo9!!
                        }
                        if (user_data.photo10.equals(null)) {
                            photo10 = "0"
                        } else {
                            photo10 = user_data.photo10!!
                        }

                        if (user_data.gender.equals(null)) {
                            gender = "0"
                        } else {
                            gender = user_data.gender!!
                        }
                        if (user_data.looking_for.equals(null)) {
                            looking_for = "0"
                        } else {
                            looking_for = user_data.looking_for!!
                        }
                        if (user_data.first_name.equals(null)) {
                            user_first_name = "0"
                        } else {
                            user_first_name = user_data.first_name!!
                        }
                        if (user_data.email.equals(null)) {
                            user_email = "0"
                        } else {
                            user_email = user_data.email!!
                        }
                        if (user_data.zipcode.equals(null)) {
                            user_zipcode = "0"
                        } else {
                            user_zipcode = user_data.zipcode!!
                        }
                        if (user_data.state.equals(null)) {
                            user_state = "0"
                        } else {
                            user_state = user_data.state!!
                        }
                        if (user_data.referal_from.equals(null)) {
                            user_referal_from = "0"
                        } else {
                            user_referal_from = user_data.referal_from!!
                        }
                        if (user_data.country.equals(null)) {
                            country_stg = "0"
                        } else {
                            country_stg = user_data.country!!
                        }
                        if (user_data.mtongue_name.equals(null)) {
                            mother_tongue_stg = "0"
                        } else {
                            mother_tongue_stg = user_data.mtongue_name!!
                        }
                        if (user_data.partner_mtongue_name.equals(null)) {
                            partner_mother_tongue_stg = "0"
                        } else {
                            partner_mother_tongue_stg = user_data.partner_mtongue_name!!
                        }

                        if (user_data.username.equals(null)) {
                            username_stg = "0"
                        } else {
                            username_stg = user_data.username!!
                        }
                        if (user_data.password.equals(null)) {
                            new_password_stg = "0"
                        } else {
                            new_password_stg = user_data.password!!
                        }
                        Log.e("part_age_min_data", part_age_min_data)
                        dllSessionManager.createLoginSession(
                            user_data.id!!,
                            gender,
                            looking_for,
                            user_first_name,
                            user_email,
                            user_zipcode,
                            user_state,
                            user_referal_from,
                            user_data.children!!,
                            user_city,
                            user_dob,
                            user_phone_number,
                            user_ethnicity,
                            user_religion,
                            user_denomination,
                            user_education,
                            user_occupation,
                            user_income,
                            user_smoke,
                            user_drink,
                            user_height_ft,
                            user_height_inc,
                            user_height_cms,
                            user_passionate,
                            user_leisure_time,
                            user_thankful_1,
                            user_thankful_2,
                            user_thankful_3,
                            part_age_min_data,
                            part_age_max_data,
                            match_distance_data,
                            match_search_imp_data,
                            partner_ethnicity,
                            partner_religion,
                            partner_denomination,
                            partner_education,
                            partner_occupation,
                            partner_smoke,
                            partner_drink,
                            photo1,
                            user_data.photo1_approve!!,
                            photo2,
                            user_data.photo2_approve!!,
                            photo3,
                            user_data.photo3_approve!!,
                            photo4,
                            user_data.photo4_approve!!,
                            photo5,
                            user_data.photo5_approve!!,
                            photo6,
                            user_data.photo6_approve!!,
                            photo7,
                            user_data.photo7_approve!!,
                            photo8,
                            user_data.photo8_approve!!,
                            photo9,
                            user_data.photo9_approve!!,
                            photo10,
                            user_data.photo10_approve!!,
                            user_data.status!!,
                            user_data.signup_step!!,
                            provider,
                            country_stg,
                            mother_tongue_stg,
                            partner_mother_tongue_stg,
                            username_stg,
                            new_password_stg,
                            true

                        )



                        val home_intent =
                            Intent(this@RegisterAccount, ProfileSetupWelcome::class.java)
                        startActivity(home_intent)
                        finish()

                        if (user_data.signup_step.equals("1")) {
                            /*val home_intent = Intent(this@RegisterAccount, ProfileSetupWelcome::class.java)
                            startActivity(home_intent)
                            finish()*/
                        } else if (user_data.signup_step.equals("2")) {

                        } else if (user_data.signup_step.equals("3")) {

                        } else {
                            /* val home_intent = Intent(this@RegisterAccount, DashboardActivity::class.java)
                             startActivity(home_intent)
                             finish()*/
                        }
                    }
                    if (response.body()!!.status.equals("status")) {
                        Toast.makeText(
                            this@RegisterAccount,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                } else {
                    Toast.makeText(
                        this@RegisterAccount,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

            override fun onFailure(call: Call<CheckLoginResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong! Please try again",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        })
    }

    private fun showDateTimePicker() {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        currentDate.add(Calendar.YEAR, -18)
        currentDate.add(Calendar.DATE, -1)
        val datePickerDialog =
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthofyear, dayofmonth ->
                    date.set(year, monthofyear, dayofmonth)

                    val myFormat = "MM-dd-yyyy"
                    val sdf = SimpleDateFormat(myFormat, Locale.US)
                    val date2 = sdf.format(date.time)
                    Log.d(
                        "RegisterAccount",
                        "calendar date : ${date.time} ===== formatted date : $date2"
                    )

                    /* if (getAge(date.time) > 18) {
                         et_born_id.setText(date2)
                     } else {
                         et_born_id.setText("")
                         Toast.makeText(this@RegisterAccount, "Your age must be above 18!", Toast.LENGTH_SHORT).show()
                     }*/

                    et_born_id.setText(date2)

                },
                currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DATE)
            )

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 568025136000L);
        // datePickerDialog.getDatePicker().setMaxDate(DateTime().minusYears(18))
        datePickerDialog.show()
    }

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun getAge(dateOfBirth: String): Int {

        var date_only: Date? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        try {
            date_only = sdf.parse(dateOfBirth)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val c = Calendar.getInstance()
        c.time = date_only
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob.set(mYear, mMonth, mDay)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }

        val ageInt = age

        return ageInt
    }

    private fun getCountries() {
        countriesDataList = java.util.ArrayList()
        countriesList = java.util.ArrayList()

        val cityApi = ApiInterface.create()
        val cityCall = cityApi.getCountries()
        cityCall.enqueue(object : Callback<CountryListResponse> {
            override fun onFailure(call: Call<CountryListResponse>, t: Throwable) {
                Log.e("RegisterAcitivty", "countriesResponse : error : ${t.toString()}")
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }
                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<CountryListResponse>,
                response: Response<CountryListResponse>
            ) {
                Log.d("RegisterAcitivty", "countriesResponse: success: ${response.body()!!.result}")

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }
                if (response.body() != null) {
                    if (response.body()!!.status.equals("1")) {
                        countriesDataList = response.body()!!.data!!

                        for (i in 0 until countriesDataList.size) {
                            countriesList.add(countriesDataList[i].country_name)
                        }
                        countrySearchAdapter =
                            LocationSearchAdapter(this@RegisterAccount, countriesList)
                        country_searchListView.adapter = countrySearchAdapter

                        Log.e("position", countriesList.indexOf("USA").toString())
                        val pos = countriesList.indexOf("USA")
                        countrySearchAdapter.setSelectedIndex(countriesList.indexOf("USA"))

                        et_country_id.setText(countriesList.get(pos).toString())

                    } else if (response.body()!!.status.equals("2")) {
                        countriesDataList = java.util.ArrayList()

                    }
                } else {
                    Toast.makeText(
                        this@RegisterAccount,
                        "Something went wrong. Please try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        })
    }


    fun socialregisterApi(
        gender: String,
        looking_for: String,
        dob: String,
        zipcode: String,
        country: String,
        state: String,
        city: String,
        user_id: String,
        device_token: String,
        device_type: String,
        username: String,
        terms_conditions: String
    ) {


        var user_city = ""
        var user_dob = ""
        var user_phone_number = ""
        var user_ethnicity = ""
        var user_religion = ""
        var user_denomination = ""
        var user_education = ""
        var user_occupation = ""
        var user_income = ""
        var user_smoke = ""
        var user_drink = ""
        var user_height_ft = ""
        var user_height_inc = ""
        var user_height_cms = ""
        var user_passionate = ""
        var user_leisure_time = ""
        var user_thankful_1 = ""
        var user_thankful_2 = ""
        var user_thankful_3 = ""
        var part_age_min_data = ""
        var part_age_max_data = ""
        var match_distance_data = ""
        var match_search_imp_data = ""
        var partner_ethnicity = ""
        var partner_religion = ""
        var partner_denomination = ""
        var partner_education = ""
        var partner_occupation = ""
        var partner_smoke = ""
        var partner_drink = ""
        var photo1 = ""
        var photo2 = ""
        var photo3 = ""
        var photo4 = ""
        var photo5 = ""
        var photo6 = ""
        var photo7 = ""
        var photo8 = ""
        var photo9 = ""
        var photo10 = ""
        var provider_stg = ""
        var country_stg = ""
        var mother_tongue_stg = ""
        var partner_mother_tongue_stg = ""
        var referal_from = ""
        var gender_stg = ""
        var looking_for_stg = ""
        var user_first_name = ""
        var user_email = ""
        var user_zipcode = ""
        var user_state = ""
        var username_stg = ""
        var new_password_stg = ""

        val apiService = ApiInterface.create()
        val call = apiService.socialregisterApi(
            gender = gender,
            looking_for = looking_for,
            dob = dob,
            zipcode = zipcode,
            country = country,
            state = state,
            city = city,
            user_id = user_id,
            device_token = device_token,
            device_type = device_type,
            terms_conditions = terms_conditions,
            username = username
        )
        call.enqueue(object : Callback<SocialLoginresponse> {
            override fun onResponse(
                call: Call<SocialLoginresponse>,
                response: retrofit2.Response<SocialLoginresponse>?
            ) {

                if (response!!.body() != null) {

                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }


                    if (response.body()!!.status == "1") {

                        val user_data = response.body()!!.data

                        if (user_data!!.city.equals(null)) {
                            user_city = "0"
                        } else {
                            user_city = user_data.city!!
                        }
                        if (user_data!!.provider.equals(null)) {
                            provider_stg = "0"
                        } else {
                            provider_stg = user_data.provider!!
                        }

                        if (user_data!!.referal_from.equals(null)) {
                            referal_from = "0"
                        } else {
                            referal_from = user_data.referal_from!!
                        }
                        if (user_data.dob.equals(null)) {
                            user_dob = "0"
                        } else {
                            user_dob = user_data.dob!!
                        }
                        if (user_data.phone_number.equals(null)) {
                            user_phone_number = "0"
                        } else {
                            user_phone_number = user_data.phone_number!!
                        }
                        if (user_data.ethnicity.equals(null)) {
                            user_ethnicity = "0"
                        } else {
                            user_ethnicity = user_data.ethnicity!!
                        }
                        if (user_data.religion.equals(null)) {
                            user_religion = "0"
                        } else {
                            user_religion = user_data.religion!!
                        }

                        if (user_data.education.equals(null)) {
                            user_education = "0"
                        } else {
                            user_education = user_data.education!!
                        }
                        if (user_data.occupation.equals(null)) {
                            user_occupation = "0"
                        } else {
                            user_occupation = user_data.occupation!!
                        }
                        if (user_data.income.equals(null)) {
                            user_income = "0"
                        } else {
                            user_income = user_data.income!!
                        }
                        if (user_data.smoke.equals(null)) {
                            user_smoke = "0"
                        } else {
                            user_smoke = user_data.smoke!!
                        }
                        if (user_data.drink.equals(null)) {
                            user_drink = "0"
                        } else {
                            user_drink = user_data.drink!!
                        }
                        if (user_data.height_ft.equals(null)) {
                            user_height_ft = "0"
                        } else {
                            user_height_ft = user_data.height_ft!!
                        }
                        if (user_data.height_inc.equals(null)) {
                            user_height_inc = "0"
                        } else {
                            user_height_inc = user_data.height_inc!!
                        }
                        if (user_data.height_cms.equals(null)) {
                            user_height_cms = "0"
                        } else {
                            user_height_cms = user_data.height_cms!!
                        }
                        if (user_data.passionate.equals(null)) {
                            user_passionate = "0"
                        } else {
                            user_passionate = user_data.passionate!!
                        }
                        if (user_data.leisure_time.equals(null)) {
                            user_leisure_time = "0"
                        } else {
                            user_leisure_time = user_data.leisure_time!!
                        }
                        if (user_data.thankful_1.equals(null)) {
                            user_thankful_1 = "0"
                        } else {
                            user_thankful_1 = user_data.thankful_1!!
                        }
                        if (user_data.thankful_2.equals(null)) {
                            user_thankful_2 = "0"
                        } else {
                            user_thankful_2 = user_data.thankful_2!!
                        }
                        if (user_data.thankful_3.equals(null)) {
                            user_thankful_3 = "0"
                        } else {
                            user_thankful_3 = user_data.thankful_3!!
                        }
                        if (user_data.partner_age_min.equals(null)) {
                            part_age_min_data = "0"
                        } else {
                            part_age_min_data = user_data.partner_age_min!!
                        }
                        if (user_data.partner_age_max.equals(null)) {
                            part_age_max_data = "0"
                        } else {
                            part_age_max_data = user_data.partner_age_max!!
                        }

                        if (user_data.partner_match_distance.equals(null)) {
                            match_distance_data = "0"
                        } else {
                            match_distance_data = user_data.partner_match_distance!!
                        }
                        if (user_data.partner_match_search.equals(null)) {
                            match_search_imp_data = "0"
                        } else {
                            match_search_imp_data = user_data.partner_match_search!!
                        }
                        if (user_data.partner_ethnicity.equals(null)) {
                            partner_ethnicity = "0"
                        } else {
                            partner_ethnicity = user_data.partner_ethnicity!!
                        }
                        if (user_data.partner_religion.equals(null)) {
                            partner_religion = "0"
                        } else {
                            partner_religion = user_data.partner_religion!!
                        }

                        if (user_data.partner_education.equals(null)) {
                            partner_education = "0"
                        } else {
                            partner_education = user_data.partner_education!!
                        }
                        if (user_data.partner_occupation.equals(null)) {
                            partner_occupation = "0"
                        } else {
                            partner_occupation = user_data.partner_occupation!!
                        }
                        if (user_data.partner_smoke.equals(null)) {
                            partner_smoke = "0"
                        } else {
                            partner_smoke = user_data.partner_smoke!!
                        }
                        if (user_data.partner_drink.equals(null)) {
                            partner_drink = "0"
                        } else {
                            partner_drink = user_data.partner_drink!!
                        }
                        if (user_data.photo1.equals(null)) {
                            photo1 = "0"
                        } else {
                            photo1 = user_data.photo1!!
                        }
                        if (user_data.photo2.equals(null)) {
                            photo2 = "0"
                        } else {
                            photo2 = user_data.photo2!!
                        }
                        if (user_data.photo3.equals(null)) {
                            photo3 = "0"
                        } else {
                            photo3 = user_data.photo3!!
                        }
                        if (user_data.photo4.equals(null)) {
                            photo4 = "0"
                        } else {
                            photo4 = user_data.photo4!!
                        }
                        if (user_data.photo5.equals(null)) {
                            photo5 = "0"
                        } else {
                            photo5 = user_data.photo5!!
                        }
                        if (user_data.photo6.equals(null)) {
                            photo6 = "0"
                        } else {
                            photo6 = user_data.photo6!!
                        }
                        if (user_data.photo7.equals(null)) {
                            photo7 = "0"
                        } else {
                            photo7 = user_data.photo7!!
                        }
                        if (user_data.photo8.equals(null)) {
                            photo8 = "0"
                        } else {
                            photo8 = user_data.photo8!!
                        }
                        if (user_data.photo9.equals(null)) {
                            photo9 = "0"
                        } else {
                            photo9 = user_data.photo9!!
                        }
                        if (user_data.photo10.equals(null)) {
                            photo10 = "0"
                        } else {
                            photo10 = user_data.photo10!!
                        }

                        if (user_data.gender.equals(null)) {
                            gender_stg = "0"
                        } else {
                            gender_stg = user_data.gender!!
                        }
                        if (user_data.looking_for.equals(null)) {
                            looking_for_stg = "0"
                        } else {
                            looking_for_stg = user_data.looking_for!!
                        }
                        if (user_data.first_name.equals(null)) {
                            user_first_name = "0"
                        } else {
                            user_first_name = user_data.first_name!!
                        }
                        if (user_data.email.equals(null)) {
                            user_email = "0"
                        } else {
                            user_email = user_data.email!!
                        }
                        if (user_data.zipcode.equals(null)) {
                            user_zipcode = "0"
                        } else {
                            user_zipcode = user_data.zipcode!!
                        }
                        if (user_data.state.equals(null)) {
                            user_state = "0"
                        } else {
                            user_state = user_data.state!!
                        }
                        if (user_data.country.equals(null)) {
                            country_stg = "0"
                        } else {
                            country_stg = user_data.country!!
                        }
                        if (user_data.mtongue_name.equals(null)) {
                            mother_tongue_stg = "0"
                        } else {
                            mother_tongue_stg = user_data.mtongue_name!!
                        }
                        if (user_data.partner_mtongue_name.equals(null)) {
                            partner_mother_tongue_stg = "0"
                        } else {
                            partner_mother_tongue_stg = user_data.partner_mtongue_name!!
                        }
                        if (user_data.username.equals(null)) {
                            username_stg = "0"
                        } else {
                            username_stg = user_data.username!!
                        }

                        if (user_data.password.equals(null)) {
                            new_password_stg = "0"
                        } else {
                            new_password_stg = user_data.password!!
                        }
                        Log.e("part_age_min_data", part_age_min_data)
                        dllSessionManager.createLoginSession(
                            user_data.id!!,
                            gender_stg,
                            looking_for_stg,
                            user_first_name,
                            user_email,
                            user_zipcode,
                            user_state,
                            referal_from,
                            user_data.children!!,
                            user_city,
                            user_dob,
                            user_phone_number,
                            user_ethnicity,
                            user_religion,
                            user_denomination,
                            user_education,
                            user_occupation,
                            user_income,
                            user_smoke,
                            user_drink,
                            user_height_ft,
                            user_height_inc,
                            user_height_cms,
                            user_passionate,
                            user_leisure_time,
                            user_thankful_1,
                            user_thankful_2,
                            user_thankful_3,
                            part_age_min_data,
                            part_age_max_data,
                            match_distance_data,
                            match_search_imp_data,
                            partner_ethnicity,
                            partner_religion,
                            partner_denomination,
                            partner_education,
                            partner_occupation,
                            partner_smoke,
                            partner_drink,
                            photo1,
                            user_data.photo1_approve!!,
                            photo2,
                            user_data.photo2_approve!!,
                            photo3,
                            user_data.photo3_approve!!,
                            photo4,
                            user_data.photo4_approve!!,
                            photo5,
                            user_data.photo5_approve!!,
                            photo6,
                            user_data.photo6_approve!!,
                            photo7,
                            user_data.photo7_approve!!,
                            photo8,
                            user_data.photo8_approve!!,
                            photo9,
                            user_data.photo9_approve!!,
                            photo10,
                            user_data.photo10_approve!!,
                            user_data.status!!,
                            user_data.signup_step!!,
                            provider_stg,
                            country_stg,
                            mother_tongue_stg,
                            partner_mother_tongue_stg,
                            username_stg,
                            new_password_stg,
                            true
                        )

                        Log.e(
                            "user_social_data_lll",
                            user_data.toString() + "---" + user_data!!.email
                        )
                        val intent = Intent(this@RegisterAccount, ProfileSetupWelcome::class.java)
                        startActivity(intent)

                    } else {
                        Log.e("user_data_lll", response.body()!!.status)
                        Toast.makeText(
                            this@RegisterAccount,
                            response.body()!!.result, Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                } else {
                    Toast.makeText(
                        this@RegisterAccount,
                        "Something went wrong. Please try again!", Toast.LENGTH_SHORT
                    ).show()
                }

            }

            override fun onFailure(call: Call<SocialLoginresponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong! Please try again", Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    private fun getCityApi(state_code: String) {
        val cityApi = ApiInterface.create()
        val cityCall = cityApi.getCities(state_code)
        cityCall.enqueue(object : Callback<CitiesResponse> {


            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                Log.d("REGISTERaCCOUNT", "CitiesResponse: success: ${response.body()!!.status}")

                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }

                if (response.body()!!.status.equals("1")) {
                    citiesDataList = response.body()!!.data!!

                    for (i in 0 until citiesDataList.size) {
                        citiesList.add(citiesDataList[i].city_name)
                    }
                    cityAdapter = ProfileLocationAdapter(this@RegisterAccount, citiesList)
                    city_searchListView.adapter = cityAdapter

                } else if (response.body()!!.status.equals("2")) {
                    citiesDataList = ArrayList()
                    cityAdapter = ProfileLocationAdapter(this@RegisterAccount, citiesList)
                    city_searchListView.adapter = cityAdapter
                }

            }

            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                Log.e("REGISTERaCCOUNT", "CitiesResponse : error : ${t.toString()}")
                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong. Please try again!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        })
    }

    private fun checkIfEmailExists(email: String) {

        val checkEmail = ApiInterface.create()
        val call = checkEmail.checkEmailApi(email)
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }
                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong! Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                emailAlreadyExists = true
            }

            override fun onResponse(
                call: Call<ChangePasswordResponse>,
                response: Response<ChangePasswordResponse>
            ) {
                try {
                    Log.d("RegisterAccount", "checkEmailApi : ${response.body()!!.status}")
                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }

                    when {
                        response.body()!!.status == "1" -> {
                            et_email_id.setCompoundDrawablesWithIntrinsicBounds(
                                0, 0, R.drawable.ic_correct, 0
                            )
                            emailAlreadyExists = false
                            et_email_id.error = null
                        }
                        response.body()!!.status == "2" -> {
                            et_email_id.error = response.body()!!.result
                            emailAlreadyExists = true
                        }
                        else -> {
                            et_email_id.setCompoundDrawablesWithIntrinsicBounds(
                                0, 0, R.drawable.ic_error, 0
                            )
                            emailAlreadyExists = true
                            et_email_id.error = null
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun checkUsernameAPI(uName: String,type: String) {

        val checkEmail = ApiInterface.create()
        val call = checkEmail.checkUsernameAPI(uName)
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                if (progressDialog.isShowing) {
                    progressDialog.dismiss()
                }
                Toast.makeText(
                    this@RegisterAccount,
                    "Something went wrong! Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                uNameAlreadyExists = false
            }

            override fun onResponse(
                call: Call<ChangePasswordResponse>,
                response: Response<ChangePasswordResponse>
            ) {
                try {
                    Log.d("RegisterAccount_llll", "checkUsernameApi : ${response.body()!!.status}")
                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }

                    when {
                        response.body()!!.status == "1" -> {
                            if (type=="register"){
                                et_user_name_id.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_correct, 0
                                )
                                uNameAlreadyExists = true
                                et_user_name_id.error = null
                            }else if(type=="social"){
                                et_social_name_id.setCompoundDrawablesWithIntrinsicBounds(
                                    0, 0, R.drawable.ic_correct, 0
                                )
                                uNameAlreadyExists = true
                                et_social_name_id.error = null
                            }


                        }
                        response.body()!!.status == "2" -> {

                            if (type=="register"){
                                et_user_name_id.error = response.body()!!.result
                                uNameAlreadyExists = false
                            }else if(type=="social"){
                                et_social_name_id .error = response.body()!!.result
                                uNameAlreadyExists = false
                            }


                        }
                        else -> {
                            et_user_name_id.setCompoundDrawablesWithIntrinsicBounds(
                                0, 0, R.drawable.ic_error, 0
                            )
                            if (type=="register"){
                                et_user_name_id.error = response.body()!!.result
                                uNameAlreadyExists = false
                            }else if(type=="social"){
                                et_social_name_id .error = response.body()!!.result
                                uNameAlreadyExists = false
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun getAdsLisrAPI(
        country_stg: String,
        state_stg: String,
        city_stg: String,
        from_stg: String
    ) {
        val getCountsApi = ApiInterface.create()

        val religionCall = getCountsApi.adslistAPI(country_stg, state_stg, city_stg, from_stg,"0")

        religionCall.enqueue(object : Callback<AdsListResponse> {
            override fun onFailure(call: Call<AdsListResponse>, t: Throwable) {
                Log.e("FavouritesFragment", "ChangePasswordResponse : error : ${t.toString()}")

            }

            override fun onResponse(
                call: Call<AdsListResponse>, response: Response<AdsListResponse>
            ) {
                try {
                    if (response.body()!!.status.equals("1") || response.body()!!.result.equals("success")) {
                        // Log.e("date_only", response.body()!!.data!!.get(0).add_image.toString())

                        if (response.body()!!.data!!.size != 0) {
                            sliderLayout.visibility=View.VISIBLE
                            for (i in 0 until response.body()!!.data!!.size) {
                                addsarrayList.add(response.body()!!.data!![i]!!)

                                Log.e("IMAGEADD_lll",PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)

                                val textSliderView = TextSliderView(this@RegisterAccount);
                                textSliderView
                                    .description(response.body()!!.data!!.get(i).add_type)
                                    .image(PhotoBaseUrl().BASE_ADS_URL+response.body()!!.data!!.get(i).add_image)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(this@RegisterAccount);
                                textSliderView.bundle(Bundle());
                                textSliderView.getBundle()
                                    .putString("extra",response.body()!!.data!!.get(i).add_link.toString());
                                sliderLayout.addSlider(textSliderView);
                            }
                            //sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            //sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(DescriptionAnimation());
                            sliderLayout.setDuration(10000);
                            sliderLayout.addOnPageChangeListener(this@RegisterAccount);


                        } else {
                            sliderLayout.visibility=View.GONE
                            Log.e("date_only", "No Data")
                        }
                    }else {
                        Log.e("date_only", "Fail")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onSliderClick(slider: BaseSliderView?) {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(slider!!.bundle.get("extra").toString())));
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }





    private fun toast(message: String) {
        try {
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            // log("Window has been closed")
        }

    }


    override fun onConnected(@Nullable bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {
        toast("Suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        toast("Failed")
    }

}
