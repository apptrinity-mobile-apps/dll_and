package divorcelovelounge.com.dll.SwitchButtonLib
/**
 * Created by lorenzorigato on 24/06/2017.
 */

interface IRightToLeftProvider {
    fun isRightToLeft(): Boolean
}