package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.*
import divorcelovelounge.com.dll.ApiPojo.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.graphics.drawable.ColorDrawable
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.*
import divorcelovelounge.com.dll.Helper.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import kotlin.collections.ArrayList

//RAVITEJA
class QuizEdit : AppCompatActivity() {

    /* UI components */
    // Layouts
    private lateinit var ll_single: LinearLayout
    private lateinit var ll_multiple: LinearLayout
    private lateinit var ll_rating: LinearLayout
    private lateinit var ll_quiz_main: LinearLayout
    private lateinit var tv_show_more: LinearLayout

    // TextViews
    lateinit var tv_back: TextView
    lateinit var tv_continue: TextView

    // ListView
    lateinit var lv_single: ListView

    // Toolbar
    private lateinit var customToolBar: Toolbar

    // Dialog
    private lateinit var progressDialog: Dialog

    // TextViews
    private lateinit var tv_question_rating: CustomTextViewBold
    private lateinit var tv_question_radio: CustomTextViewBold
    private lateinit var tv_question_checkbox: CustomTextViewBold
    private lateinit var ll_no_data: CustomTextViewBold
    private lateinit var tv_submit: TextView
    private lateinit var tv_edit: TextView

    // SeekBar
    private lateinit var horizontal_progress_bar: SignSeekBar

    // ToggleButton
    private lateinit var toggle_button: ToggleButton

    /* Constants */
    private lateinit var sessionManager: DllSessionManager
    private lateinit var selectedAnswers: ArrayList<QuizDisplayAnswerData>
    private lateinit var quizData: ArrayList<QuizDisplayData>

    val logTAG = "QuizEdit"
    var user_id: String? = null
    var data_size: Int? = null
    var question_id = ""
    var current_data_counter = 0
    var rating = 0
    var rating_position = 0
    var first_time = 0
    var getSize: Float = 0.0f
    var edit_status = false

    var drawable_edit: Drawable? = null
    var drawable_default: Drawable? = null

    var final_json_array = JSONArray()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_setup_one)
        title = ""

        initialize()

        if (!sessionManager.userDetails[DllSessionManager.USER_ID].equals("")) {
            user_id = sessionManager.userDetails[DllSessionManager.USER_ID]
        }
        Log.d(logTAG, "USER ID : $user_id")

        setSupportActionBar(customToolBar)

        tv_continue.visibility = View.GONE
        tv_back.visibility = View.VISIBLE
        tv_edit.visibility = View.VISIBLE
        horizontal_progress_bar.visibility = View.GONE
        lv_single.visibility = View.GONE
        tv_submit.visibility = View.GONE
        tv_submit.text = "Submit"

        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color))

        if (InternetConnection.checkConnection(this@QuizEdit)) {
            progressDialog.show()
            ll_no_data.text = resources.getString(R.string.loading)
            quizApiCall()
        } else {
            if (progressDialog.isShowing) {
                progressDialog.dismiss()
            }
            ll_no_data.text = resources.getString(R.string.no_network_connection)
            val snackbar: Snackbar =
                Snackbar.make(ll_quiz_main, "Please Check Your Connection", Snackbar.LENGTH_INDEFINITE)
            snackbar.show()
            snackbar.setAction("Reload") { _ ->
                if (InternetConnection.checkConnection(this@QuizEdit)) {
                    snackbar.dismiss()
                    progressDialog.show()
                    ll_no_data.text = resources.getString(R.string.loading)
                    quizApiCall()
                } else {
                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }
                    ll_no_data.text = resources.getString(R.string.no_network_connection)
                    val snackbar1: Snackbar =
                        Snackbar.make(ll_quiz_main, "Please Check Your Connection", Snackbar.LENGTH_INDEFINITE)
                    snackbar1.show()
                    snackbar1.setAction("Reload") {
                        if (InternetConnection.checkConnection(this@QuizEdit)) {
                            snackbar.dismiss()
                            progressDialog.show()
                            ll_no_data.text = resources.getString(R.string.loading)
                            quizApiCall()
                        } else {
                            snackbar.show()
                        }
                    }
                }
            }
        }

        tv_edit.setOnClickListener {
            toggle_button.toggle()
        }

        tv_continue.setOnClickListener {
            first_time = 0
            Log.d(logTAG, "data_array_count: $data_size  current_data_counter $current_data_counter")
            if (current_data_counter == data_size) {
                Log.d(logTAG, "size equal")
                val homeIntent = Intent(this@QuizEdit, DashboardActivity::class.java)
                homeIntent.putExtra("from_list", "profile_photos")
                startActivity(homeIntent)
                this@QuizEdit.finish()

            } else {
                if (current_data_counter < data_size!!) {
                    current_data_counter += 1
                }
                if (current_data_counter == (data_size!! - 1)) {
                    Log.e("data_array_skip: ", "SKIP")
                    tv_continue.text = "Done"
                    tv_continue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                } else {
                    if (current_data_counter == data_size!!) {
                        // do nothing
                    } else {
                        tv_continue.text = "Next"
                        tv_continue.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_next, 0)
                    }
                }

                if (current_data_counter < data_size!!) {
                    horizontal_progress_bar.setProgress(getSize * current_data_counter)
                    horizontal_progress_bar.animate()
                    Log.d(logTAG, "data_array: $data_size  current_data_counter $current_data_counter")
                    when (quizData[current_data_counter].option_type!!) {
                        ("radio") -> {

                            ll_single.visibility = View.VISIBLE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.GONE

                            ll_single.removeAllViews()

                            tv_question_radio.text = quizData[current_data_counter].question.toString()
                            question_id = quizData[current_data_counter].question_id.toString()
                            selectedAnswers.clear()
                            var list: ArrayList<QuizDisplayAnswerData>? = null
                            list = quizData[current_data_counter].answer_list

                            val singleSelectorAdapter: SingleSelectorQuizDisplayAdapter
                            val recyclerView = RecyclerView(this)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                            ll_single.addView(tv_question_radio)
                            ll_single.addView(recyclerView)

                            singleSelectorAdapter = SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                            recyclerView.adapter = singleSelectorAdapter

                            val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                                object : RecyclerItemClickListener.OnItemClickListener {
                                    override fun onItemClick(view: View, position: Int) {
                                        val dataS = singleSelectorAdapter.getItem(position)
                                        selectedAnswers.clear()
                                        selectedAnswers.add(dataS)
                                        singleSelectorAdapter.setSelectedAnswers(selectedAnswers)

                                        Log.d(logTAG, "clicked : selected answer:  ${dataS.answer}")
                                    }
                                })

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list!!.size > 0) {
                                        for (i in 0 until list!!.size) {
                                            val answer_obj = JSONObject()
                                            try {
                                                answer_obj.put("answer_id", list!![i].ans_id)
                                                answer_obj.put("answer", "")
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    Log.d(
                                        logTAG,
                                        "radio edit_status $edit_status final_json_array ${final_json_array.toString()}"
                                    )

                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val singleSelectorAdapter1 =
                                            SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "yes")

                                        val touchListener1 = RecyclerItemClickListener(
                                            this@QuizEdit, recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(view: View, position: Int) {
                                                    val dataS = singleSelectorAdapter1.getItem(position)
                                                    selectedAnswers.clear()
                                                    selectedAnswers.add(dataS)
                                                    singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                    Log.d(
                                                        logTAG,
                                                        "touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                    )

                                                    final_json_array.getJSONObject(position)
                                                        .put("answer_id", dataS.ans_id)
                                                    if (final_json_array.getJSONObject(position).getString("answer").equals(
                                                            dataS.answer
                                                        )
                                                    ) {
                                                        final_json_array.getJSONObject(position).put("answer", "")
                                                    } else {
                                                        final_json_array.getJSONObject(position)
                                                            .put("answer", dataS.answer)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "radio OnItemClickListener final_json_array ${final_json_array.toString()}"
                                                    )
                                                }
                                            })

                                        recyclerView.adapter = singleSelectorAdapter1
                                        singleSelectorAdapter1.notifyDataSetChanged()
                                        var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                        already_selected_list = ArrayList()
                                        for (i in 0 until list!!.size) {
                                            if (list!![i].user_answer != "") {
                                                already_selected_list.add(list!![i])
                                            } else {
                                                already_selected_list = ArrayList()
                                            }
                                        }
                                        selectedAnswers.addAll(already_selected_list)
                                        singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                        recyclerView.addOnItemTouchListener(touchListener1)

                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color)
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default, null, null, null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val singleSelectorAdapter2 =
                                            SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                                        recyclerView.adapter = singleSelectorAdapter2
                                        recyclerView.removeOnItemTouchListener(touchListener)
                                    }
                                }
                            })
                        }
                        ("checkbox") -> {

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.VISIBLE
                            tv_show_more.visibility = View.GONE

                            selectedAnswers.clear()
                            ll_multiple.removeAllViews()
                            tv_question_checkbox.text = quizData[current_data_counter].question.toString()
                            question_id = quizData[current_data_counter].question_id.toString()
                            ll_multiple.addView(tv_question_checkbox)

                            var list: ArrayList<QuizDisplayAnswerData>? = null
                            val multiSelectorAdapter: MultiSelectorQuizDisplayAdapter
                            val recyclerView = RecyclerView(this)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                            ll_multiple.addView(recyclerView)

                            list = quizData[current_data_counter].answer_list
                            multiSelectorAdapter = MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                            recyclerView.adapter = multiSelectorAdapter

                            val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                                object : RecyclerItemClickListener.OnItemClickListener {
                                    override fun onItemClick(view: View, position: Int) {
                                        val dataS = multiSelectorAdapter.getItem(position)
                                        if (selectedAnswers.contains(dataS)) {
                                            selectedAnswers.remove(dataS)
                                        } else {
                                            selectedAnswers.add(dataS)
                                        }
                                        multiSelectorAdapter.setSelectedAnswers(selectedAnswers)
                                        Log.d(logTAG, "clicked : selected answer:  ${dataS.answer}")
                                    }
                                })

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list!!.size > 0) {
                                        for (i in 0 until list!!.size) {
                                            val answer_obj = JSONObject()
                                            try {
                                                answer_obj.put("answer_id", list!![i].ans_id)
                                                answer_obj.put("answer", "")
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    Log.d(
                                        logTAG,
                                        "edit_status $edit_status final_json_array ${final_json_array.toString()}"
                                    )
                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val multiSelectorAdapter1 =
                                            MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "yes")

                                        val touchListener1 = RecyclerItemClickListener(
                                            this@QuizEdit, recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(view: View, position: Int) {
                                                    val dataS = multiSelectorAdapter1.getItem(position)
                                                    if (selectedAnswers.contains(dataS)) {
                                                        selectedAnswers.remove(dataS)
                                                    } else {
                                                        selectedAnswers.add(dataS)
                                                    }
                                                    multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                    Log.d(
                                                        logTAG,
                                                        "touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                    )

                                                    final_json_array.getJSONObject(position)
                                                        .put("answer_id", dataS.ans_id)
                                                    if (final_json_array.getJSONObject(position).getString("answer").equals(
                                                            dataS.answer
                                                        )
                                                    ) {
                                                        final_json_array.getJSONObject(position).put("answer", "")
                                                    } else {
                                                        final_json_array.getJSONObject(position)
                                                            .put("answer", dataS.answer)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "OnItemClickListener final_json_array ${final_json_array.toString()}"
                                                    )
                                                }
                                            })

                                        recyclerView.adapter = multiSelectorAdapter1
                                        multiSelectorAdapter1.notifyDataSetChanged()
                                        var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                        already_selected_list = ArrayList()
                                        for (i in 0 until list!!.size) {
                                            if (list!![i].user_answer != "") {
                                                already_selected_list.add(list!![i])
                                            } else {
                                                already_selected_list = ArrayList()
                                            }
                                        }
                                        selectedAnswers.addAll(already_selected_list)
                                        multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                        recyclerView.addOnItemTouchListener(touchListener1)

                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color)
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default, null, null, null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val multiSelectorAdapter2 =
                                            MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                                        recyclerView.adapter = multiSelectorAdapter2
                                        recyclerView.removeOnItemTouchListener(touchListener)
                                    }
                                }
                            })
                        }
                        ("rating") -> {

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.VISIBLE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.VISIBLE

                            ll_rating.removeAllViews()
                            tv_question_rating.text = quizData[current_data_counter].question.toString()
                            ll_rating.addView(tv_question_rating)
                            question_id = quizData[current_data_counter].question_id.toString()

                            val list: ArrayList<QuizDisplayAnswerData>? =
                                quizData[current_data_counter].answer_list

                            val recyclerView_rating = RecyclerView(this)
                            val linearLayoutManager =
                                LinearLayoutManager(this@QuizEdit, LinearLayout.VERTICAL, false)
                            var lastVisibleItemIndex: Int?

                            recyclerView_rating.setHasFixedSize(true)
                            recyclerView_rating.layoutManager = linearLayoutManager
                            ll_rating.addView(recyclerView_rating)
                            var ratingAdapter: RatingAdapter
                            ratingAdapter = RatingAdapter(this@QuizEdit, list!!, "no")
                            recyclerView_rating.adapter = ratingAdapter
                            ratingAdapter.notifyDataSetChanged()

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list.size > 0) {
                                        for (i in 0 until list.size) {
                                            val answer = list[i].user_answer
                                            val answer_obj = JSONObject()
                                            try {
                                                if (answer == "") {
                                                    answer_obj.put("answer_id", list[i].ans_id)
                                                    answer_obj.put("answer", "0")
                                                } else {
                                                    answer_obj.put("answer_id", list[i].ans_id)
                                                    answer_obj.put("answer", answer)
                                                }
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    Log.d(logTAG, "rating final_json_array ${final_json_array.toString()}")
                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE
                                        recyclerView_rating.adapter = null
                                        ratingAdapter = RatingAdapter(this@QuizEdit, list, "yes")
                                        recyclerView_rating.adapter = ratingAdapter
                                        ratingAdapter.notifyDataSetChanged()
                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(
                                                this@QuizEdit,
                                                R.color.register_btn_color
                                            )
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default,
                                            null,
                                            null,
                                            null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE
                                        recyclerView_rating.adapter = null
                                        ratingAdapter = RatingAdapter(this@QuizEdit, list, "no")
                                        recyclerView_rating.adapter = ratingAdapter
                                        ratingAdapter.notifyDataSetChanged()
                                    }
                                }
                            })

                            if (list.size <= 0) {
                                tv_show_more.visibility = View.GONE
                            } else {
                                tv_show_more.visibility = View.VISIBLE
                            }

                            recyclerView_rating.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    val currentFirstVisible =
                                        linearLayoutManager.findLastCompletelyVisibleItemPosition()
                                    lastVisibleItemIndex = currentFirstVisible
                                    Log.d(
                                        logTAG,
                                        "lastVisibleItemIndex listener $lastVisibleItemIndex"
                                    )
                                    if ((lastVisibleItemIndex) == (list.size - 1)) {
                                        tv_show_more.visibility = View.GONE
                                    } else {
                                        tv_show_more.visibility = View.VISIBLE
                                    }
                                }
                            })

                            tv_show_more.setOnClickListener {
                                val items_count = recyclerView_rating.adapter!!.itemCount
                                lastVisibleItemIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                                if ((lastVisibleItemIndex) == (items_count - 1)) {
                                    tv_show_more.visibility = View.GONE
                                } else {
                                    tv_show_more.visibility = View.VISIBLE
                                }

                                linearLayoutManager.smoothScrollToPosition(recyclerView_rating, null, items_count - 1)
                            }
                        }
                    }
                } else if (current_data_counter == data_size!!) {
                    horizontal_progress_bar.setProgress(100f)
                    horizontal_progress_bar.animate()
                    val homeIntent = Intent(this@QuizEdit, DashboardActivity::class.java)
                    homeIntent.putExtra("from_list", "profile_photos")
                    startActivity(homeIntent)
                    this@QuizEdit.finish()
                }
            }
        }

        tv_submit.setOnClickListener {
            progressDialog.show()

            val apiInterface = ApiInterface.create()
            val callApi = apiInterface.updateQuizApi(user_id!!, question_id, "android", final_json_array)
            callApi.enqueue(object : Callback<QuizDisplayResponse> {
                override fun onFailure(call: Call<QuizDisplayResponse>, t: Throwable) {
                    Log.e(logTAG, "updateQuiz Response error : " + t.toString())
                }

                override fun onResponse(
                    call: Call<QuizDisplayResponse>, response: Response<QuizDisplayResponse>
                ) {
                    Log.d(logTAG, "updateQuiz Response success : ${response.isSuccessful}")
                    try {
                        if (response != null) {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                            }
                            Toast.makeText(this@QuizEdit, response.body()!!.result, Toast.LENGTH_SHORT).show()
                            val refreshIntent = intent
                            finish()
                            startActivity(refreshIntent)
                        } else {

                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            })
        }

        tv_back.setOnClickListener {

            Log.d(logTAG, "current_data_counter $current_data_counter")

            if (current_data_counter == 0) {
                val homeIntent = Intent(this@QuizEdit, DashboardActivity::class.java)
                homeIntent.putExtra("from_list", "profile_photos")
                startActivity(homeIntent)

            } else if (current_data_counter < data_size!!) {
                current_data_counter -= 1
            }

            if (current_data_counter == (data_size!! - 1)) {
                tv_continue.text = "Done"
                tv_continue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            } else {
                if (current_data_counter == data_size!!) {
                    // do nothing
                } else {
                    tv_continue.text = "Next"
                    tv_continue.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_next, 0)
                }
            }

            if (current_data_counter < data_size!!) {
                horizontal_progress_bar.setProgress(getSize * current_data_counter)
                horizontal_progress_bar.animate()
                Log.d(logTAG, "data_array: $data_size  current_data_counter $current_data_counter")
                when (quizData[current_data_counter].option_type!!) {
                    ("radio") -> {

                        ll_single.visibility = View.VISIBLE
                        ll_rating.visibility = View.GONE
                        ll_multiple.visibility = View.GONE
                        tv_show_more.visibility = View.GONE

                        ll_single.removeAllViews()

                        tv_question_radio.text = quizData[current_data_counter].question.toString()
                        question_id = quizData[current_data_counter].question_id.toString()
                        selectedAnswers.clear()
                        var list: ArrayList<QuizDisplayAnswerData>? = null
                        list = quizData[current_data_counter].answer_list

                        val singleSelectorAdapter: SingleSelectorQuizDisplayAdapter
                        val recyclerView = RecyclerView(this)
                        recyclerView.setHasFixedSize(true)
                        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                        ll_single.addView(tv_question_radio)
                        ll_single.addView(recyclerView)

                        singleSelectorAdapter = SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                        recyclerView.adapter = singleSelectorAdapter

                        val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                            object : RecyclerItemClickListener.OnItemClickListener {
                                override fun onItemClick(view: View, position: Int) {
                                    val dataS = singleSelectorAdapter.getItem(position)
                                    selectedAnswers.clear()
                                    selectedAnswers.add(dataS)
                                    singleSelectorAdapter.setSelectedAnswers(selectedAnswers)
                                    Log.d(logTAG, "clicked : selected answer:  ${dataS.answer}")
                                }
                            })

                        toggle_button.setOnCheckedChangeListener(object :
                            CompoundButton.OnCheckedChangeListener {
                            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                edit_status = isChecked
                                final_json_array = JSONArray()
                                Log.d(logTAG, "edit_status $edit_status")
                                if (edit_status) {
                                    tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                    tv_submit.visibility = View.VISIBLE
                                    tv_continue.visibility = View.GONE
                                    tv_back.visibility = View.GONE

                                    selectedAnswers.clear()
                                    list = quizData[current_data_counter].answer_list
                                    recyclerView.adapter = null
                                    val singleSelectorAdapter1 =
                                        SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "yes")

                                    val touchListener1 = RecyclerItemClickListener(
                                        this@QuizEdit, recyclerView,
                                        object : RecyclerItemClickListener.OnItemClickListener {
                                            override fun onItemClick(view: View, position: Int) {
                                                val dataS = singleSelectorAdapter1.getItem(position)
                                                selectedAnswers.clear()
                                                selectedAnswers.add(dataS)
                                                singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                Log.d(
                                                    logTAG,
                                                    "touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                )
                                            }
                                        })

                                    recyclerView.adapter = singleSelectorAdapter1
                                    singleSelectorAdapter1.notifyDataSetChanged()
                                    var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                    already_selected_list = ArrayList()
                                    for (i in 0 until list!!.size) {
                                        if (list!![i].user_answer != "") {
                                            already_selected_list.add(list!![i])
                                        } else {
                                            already_selected_list = ArrayList()
                                        }
                                    }
                                    selectedAnswers.addAll(already_selected_list)
                                    singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                    recyclerView.addOnItemTouchListener(touchListener1)

                                } else {
                                    tv_edit.setTextColor(
                                        ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color)
                                    )
                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                        drawable_default, null, null, null
                                    )
                                    tv_submit.visibility = View.GONE
                                    tv_continue.visibility = View.VISIBLE
                                    tv_back.visibility = View.VISIBLE

                                    selectedAnswers.clear()
                                    list = quizData[current_data_counter].answer_list
                                    recyclerView.adapter = null
                                    val singleSelectorAdapter2 =
                                        SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                                    recyclerView.adapter = singleSelectorAdapter2
                                    recyclerView.removeOnItemTouchListener(touchListener)
                                }
                            }
                        })
                    }
                    ("checkbox") -> {

                        ll_single.visibility = View.GONE
                        ll_rating.visibility = View.GONE
                        ll_multiple.visibility = View.VISIBLE
                        tv_show_more.visibility = View.GONE

                        selectedAnswers.clear()
                        ll_multiple.removeAllViews()
                        tv_question_checkbox.text = quizData[current_data_counter].question.toString()
                        question_id = quizData[current_data_counter].question_id.toString()
                        ll_multiple.addView(tv_question_checkbox)

                        var list: ArrayList<QuizDisplayAnswerData>? = null
                        val multiSelectorAdapter: MultiSelectorQuizDisplayAdapter
                        val recyclerView = RecyclerView(this)
                        recyclerView.setHasFixedSize(true)
                        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                        ll_multiple.addView(recyclerView)

                        list = quizData[current_data_counter].answer_list
                        multiSelectorAdapter = MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                        recyclerView.adapter = multiSelectorAdapter

                        val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                            object : RecyclerItemClickListener.OnItemClickListener {
                                override fun onItemClick(view: View, position: Int) {
                                    val dataS = multiSelectorAdapter.getItem(position)
                                    if (selectedAnswers.contains(dataS)) {
                                        selectedAnswers.remove(dataS)
                                    } else {
                                        selectedAnswers.add(dataS)
                                    }
                                    multiSelectorAdapter.setSelectedAnswers(selectedAnswers)
                                    Log.d(logTAG, "clicked : selected answer:  ${dataS.answer}")
                                }
                            })

                        toggle_button.setOnCheckedChangeListener(object :
                            CompoundButton.OnCheckedChangeListener {
                            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                edit_status = isChecked
                                final_json_array = JSONArray()
                                Log.d(logTAG, "edit_status $edit_status")
                                if (edit_status) {
                                    tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                    tv_submit.visibility = View.VISIBLE
                                    tv_continue.visibility = View.GONE
                                    tv_back.visibility = View.GONE

                                    selectedAnswers.clear()
                                    list = quizData[current_data_counter].answer_list
                                    recyclerView.adapter = null
                                    val multiSelectorAdapter1 =
                                        MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "yes")

                                    val touchListener1 = RecyclerItemClickListener(
                                        this@QuizEdit, recyclerView,
                                        object : RecyclerItemClickListener.OnItemClickListener {
                                            override fun onItemClick(view: View, position: Int) {
                                                val dataS = multiSelectorAdapter1.getItem(position)
                                                if (selectedAnswers.contains(dataS)) {
                                                    selectedAnswers.remove(dataS)
                                                } else {
                                                    selectedAnswers.add(dataS)
                                                }
                                                multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                Log.d(
                                                    logTAG,
                                                    "touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                )
                                            }
                                        })

                                    recyclerView.adapter = multiSelectorAdapter1
                                    multiSelectorAdapter1.notifyDataSetChanged()
                                    var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                    already_selected_list = ArrayList()
                                    for (i in 0 until list!!.size) {
                                        if (list!![i].user_answer != "") {
                                            already_selected_list.add(list!![i])
                                        } else {
                                            already_selected_list = ArrayList()
                                        }
                                    }
                                    selectedAnswers.addAll(already_selected_list)
                                    multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                    recyclerView.addOnItemTouchListener(touchListener1)

                                } else {
                                    tv_edit.setTextColor(
                                        ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color)
                                    )
                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                        drawable_default, null, null, null
                                    )
                                    tv_submit.visibility = View.GONE
                                    tv_continue.visibility = View.VISIBLE
                                    tv_back.visibility = View.VISIBLE

                                    selectedAnswers.clear()
                                    list = quizData[current_data_counter].answer_list
                                    recyclerView.adapter = null
                                    val multiSelectorAdapter2 =
                                        MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                                    recyclerView.adapter = multiSelectorAdapter2
                                    recyclerView.removeOnItemTouchListener(touchListener)
                                }
                            }
                        })
                    }
                    ("rating") -> {

                        ll_single.visibility = View.GONE
                        ll_rating.visibility = View.VISIBLE
                        ll_multiple.visibility = View.GONE
                        tv_show_more.visibility = View.VISIBLE


                        ll_rating.removeAllViews()
                        tv_question_rating.text = quizData[current_data_counter].question.toString()
                        ll_rating.addView(tv_question_rating)
                        question_id = quizData[current_data_counter].question_id.toString()

                        val list: ArrayList<QuizDisplayAnswerData>? =
                            quizData[current_data_counter].answer_list
                        if (list!!.size > 0) {
                            for (i in 0 until list.size) {
                                val answer = list[i].user_answer
                                val answer_obj = JSONObject()
                                try {
                                    if (answer == "") {
                                        answer_obj.put("answer_id", list[i].ans_id)
                                        answer_obj.put("answer", "0")

                                    } else {
                                        answer_obj.put("answer_id", list[i].ans_id)
                                        answer_obj.put("answer", answer)

                                    }
                                    final_json_array.put(answer_obj)

                                } catch (e: JSONException) {

                                }
                            }
                        }

                        Log.d(logTAG, "rating answers size: ${list.size} ")

                        val recyclerView_rating = RecyclerView(this)
                        val linearLayoutManager =
                            LinearLayoutManager(this@QuizEdit, LinearLayout.VERTICAL, false)
                        var lastVisibleItemIndex: Int?

                        recyclerView_rating.setHasFixedSize(true)
                        recyclerView_rating.layoutManager = linearLayoutManager
                        ll_rating.addView(recyclerView_rating)
                        var ratingAdapter: RatingAdapter
                        ratingAdapter = RatingAdapter(this@QuizEdit, list, "no")

                        recyclerView_rating.adapter = ratingAdapter
                        ratingAdapter.notifyDataSetChanged()

                        toggle_button.setOnCheckedChangeListener(object :
                            CompoundButton.OnCheckedChangeListener {
                            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                edit_status = isChecked
                                final_json_array = JSONArray()
                                Log.d(logTAG, "edit_status $edit_status")
                                if (edit_status) {
                                    tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))

                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                    tv_submit.visibility = View.VISIBLE
                                    tv_continue.visibility = View.GONE
                                    tv_back.visibility = View.GONE
                                    recyclerView_rating.adapter = null
                                    ratingAdapter = RatingAdapter(this@QuizEdit, list, "yes")
                                    recyclerView_rating.adapter = ratingAdapter
                                    ratingAdapter.notifyDataSetChanged()
                                } else {
                                    tv_edit.setTextColor(
                                        ContextCompat.getColor(
                                            this@QuizEdit,
                                            R.color.register_btn_color
                                        )
                                    )
                                    tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                        drawable_default,
                                        null,
                                        null,
                                        null
                                    )
                                    tv_submit.visibility = View.GONE
                                    tv_continue.visibility = View.VISIBLE
                                    tv_back.visibility = View.VISIBLE
                                    recyclerView_rating.adapter = null
                                    ratingAdapter = RatingAdapter(this@QuizEdit, list, "no")
                                    recyclerView_rating.adapter = ratingAdapter
                                    ratingAdapter.notifyDataSetChanged()
                                }
                            }
                        })

                        if (list.size <= 0) {
                            tv_show_more.visibility = View.GONE
                        } else {
                            tv_show_more.visibility = View.VISIBLE
                        }

                        tv_show_more.setOnClickListener {
                            val items_count = recyclerView_rating.adapter!!.itemCount
                            lastVisibleItemIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                            if ((lastVisibleItemIndex) == (items_count - 1)) {
                                tv_show_more.visibility = View.GONE
                            } else {
                                tv_show_more.visibility = View.VISIBLE
                            }

                            linearLayoutManager.smoothScrollToPosition(recyclerView_rating, null, items_count - 1)
                        }
                    }
                }
            } else if (current_data_counter == data_size!!) {
                horizontal_progress_bar.setProgress(100f)
                horizontal_progress_bar.animate()
                val homeIntent = Intent(this@QuizEdit, DashboardActivity::class.java)
                homeIntent.putExtra("from_list", "profile_photos")
                startActivity(homeIntent)
                this@QuizEdit.finish()
            }
        }

    }

    private fun initialize() {

        /* setting custom tool bar */
        customToolBar = findViewById(R.id.quizToolBar)
        tv_continue = customToolBar.findViewById(R.id.tv_quiz_next)
        tv_back = customToolBar.findViewById(R.id.tv_quiz_back)

        sessionManager = DllSessionManager(this@QuizEdit)

        ll_rating = findViewById(R.id.ll_rating)
        ll_multiple = findViewById(R.id.ll_multiple)
        ll_single = findViewById(R.id.ll_single)

        lv_single = findViewById(R.id.lv_single)
        toggle_button = findViewById(R.id.toggle_button)

        tv_question_checkbox = findViewById(R.id.tv_question_checkbox)
        tv_question_radio = findViewById(R.id.tv_question_radio)
        tv_question_rating = findViewById(R.id.tv_question_rating)

        tv_submit = findViewById(R.id.tv_skip)
        tv_edit = findViewById(R.id.tv_edit)
        tv_show_more = findViewById(R.id.tv_show_more)
        ll_no_data = findViewById(R.id.ll_no_data)
        ll_quiz_main = findViewById(R.id.ll_quiz_main)
        horizontal_progress_bar = findViewById(R.id.horizontal_progress_bar)
        horizontal_progress_bar.isClickable = false
        horizontal_progress_bar.isFocusable = false
        horizontal_progress_bar.isEnabled = false

        quizData = ArrayList()
        selectedAnswers = ArrayList()

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

        drawable_default = ContextCompat.getDrawable(this, R.drawable.ic_pencil_edit_button)
        drawable_edit = ContextCompat.getDrawable(this, R.drawable.ic_pencil_edit_button)

        drawable_edit = DrawableCompat.wrap(drawable_edit!!)
        DrawableCompat.setTint(drawable_edit!!, Color.BLUE)
        DrawableCompat.setTintMode(drawable_edit!!, PorterDuff.Mode.SRC_IN)

    }

    private fun quizApiCall() {

        val apiService = ApiInterface.create()
        val call = apiService.quizDisplayApi(user_id!!)
        Log.d(logTAG, "Api Call Request : " + call.toString())
        call.enqueue(object : Callback<QuizDisplayResponse> {
            override fun onFailure(call: Call<QuizDisplayResponse>, t: Throwable) {
                Log.e(logTAG, "Response error : " + t.printStackTrace())
                ll_no_data.visibility = View.VISIBLE
            }

            override fun onResponse(
                call: Call<QuizDisplayResponse>, response: Response<QuizDisplayResponse>
            ) {
                if (response.body() != null) {
                    Log.d(logTAG, "Response success : ${response.body()}")
                    quizData = ArrayList()
                    data_size = 0
                    val data = response.body()!!.quizlist
                    data_size = data!!.size
                    tv_continue.visibility = View.VISIBLE
                    tv_back.visibility = View.VISIBLE
                    for (i in 0 until data.size) {
                        quizData = data
                    }
                    when (data[current_data_counter].option_type!!) {
                        ("radio") -> {
                            ll_single.visibility = View.VISIBLE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.GONE
                            ll_single.removeAllViews()

                            tv_question_radio.text = data[current_data_counter].question.toString()
                            question_id = data[current_data_counter].question_id.toString()
                            selectedAnswers.clear()

                            var list: ArrayList<QuizDisplayAnswerData>? = null
                            list = response.body()!!.quizlist!![current_data_counter].answer_list

                            val singleSelectorAdapter: SingleSelectorQuizDisplayAdapter
                            val recyclerView = RecyclerView(applicationContext)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager =
                                    LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
                            ll_single.addView(tv_question_radio)
                            ll_single.addView(recyclerView)

                            singleSelectorAdapter = SingleSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                            recyclerView.adapter = singleSelectorAdapter

                            val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                                object : RecyclerItemClickListener.OnItemClickListener {
                                    override fun onItemClick(view: View, position: Int) {
                                        val dataS = singleSelectorAdapter.getItem(position)
                                        selectedAnswers.clear()
                                        selectedAnswers.add(dataS)
                                        singleSelectorAdapter.setSelectedAnswers(selectedAnswers)
                                        Log.d(logTAG, "clicked : selected answer:  ${dataS.answer}")
                                    }
                                })

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list.size > 0) {
                                        for (i in 0 until list.size) {
                                            val answer_obj = JSONObject()
                                            try {
                                                answer_obj.put("answer_id", list[i].ans_id)
                                                answer_obj.put("answer", "")
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val singleSelectorAdapter1 =
                                            SingleSelectorQuizDisplayAdapter(this@QuizEdit, list, "yes")

                                        val touchListener1 = RecyclerItemClickListener(
                                            this@QuizEdit, recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(view: View, position: Int) {
                                                    val dataS = singleSelectorAdapter1.getItem(position)
                                                    selectedAnswers.clear()
                                                    selectedAnswers.add(dataS)
                                                    singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                    Log.d(
                                                        logTAG,
                                                        "touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                    )

                                                    final_json_array.getJSONObject(position)
                                                        .put("answer_id", dataS.ans_id)
                                                    if (final_json_array.getJSONObject(position).getString("answer").equals(
                                                            dataS.answer
                                                        )
                                                    ) {
                                                        final_json_array.getJSONObject(position).put("answer", "")
                                                    } else {
                                                        final_json_array.getJSONObject(position)
                                                            .put("answer", dataS.answer)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "radio OnItemClickListener final_json_array ${final_json_array.toString()}"
                                                    )
                                                }
                                            })

                                        recyclerView.adapter = singleSelectorAdapter1
                                        singleSelectorAdapter1.notifyDataSetChanged()
                                        var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                        already_selected_list = ArrayList()
                                        for (i in 0 until list.size) {
                                            if (list[i].user_answer != "") {
                                                already_selected_list.add(list[i])
                                            } else {
                                                already_selected_list = ArrayList()
                                            }
                                        }
                                        selectedAnswers.addAll(already_selected_list)
                                        singleSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                        recyclerView.addOnItemTouchListener(touchListener1)

                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(this@QuizEdit, R.color.register_btn_color)
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default, null, null, null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE

                                        selectedAnswers.clear()
                                        recyclerView.adapter = null
                                        val singleSelectorAdapter2 =
                                            SingleSelectorQuizDisplayAdapter(this@QuizEdit, list, "no")
                                        recyclerView.adapter = singleSelectorAdapter2
                                        recyclerView.removeOnItemTouchListener(touchListener)
                                    }
                                }
                            })
                        }
                        ("checkbox") -> {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                                ll_no_data.visibility = View.GONE
                            }

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.VISIBLE
                            tv_show_more.visibility = View.GONE

                            selectedAnswers.clear()
                            ll_multiple.removeAllViews()
                            tv_question_checkbox.text = data[current_data_counter].question.toString()
                            ll_multiple.addView(tv_question_checkbox)

                            val list: ArrayList<QuizDisplayAnswerData>? =
                                response.body()!!.quizlist!![current_data_counter].answer_list
                            question_id = response.body()!!.quizlist!![current_data_counter].question_id.toString()
                            val multiSelectorAdapter: MultiSelectorQuizDisplayAdapter
                            multiSelectorAdapter = MultiSelectorQuizDisplayAdapter(this@QuizEdit, list!!, "no")
                            val recyclerView = RecyclerView(applicationContext)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager =
                                    LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
                            ll_multiple.addView(recyclerView)

                            recyclerView.adapter = multiSelectorAdapter
                            val touchListener = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                                object : RecyclerItemClickListener.OnItemClickListener {
                                    override fun onItemClick(view: View, position: Int) {
                                        val dataS = multiSelectorAdapter.getItem(position)
                                        if (selectedAnswers.contains(dataS)) {
                                            selectedAnswers.remove(dataS)
                                        } else {
                                            selectedAnswers.add(dataS)
                                        }
                                        multiSelectorAdapter.setSelectedAnswers(selectedAnswers)
                                    }
                                })

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list.size > 0) {
                                        for (i in 0 until list.size) {
                                            val answer_obj = JSONObject()
                                            try {
                                                answer_obj.put("answer_id", list[i].ans_id)
                                                answer_obj.put("answer", "")
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    Log.d(
                                        logTAG,
                                        "edit_status $edit_status final_json_array ${final_json_array.toString()}"
                                    )

                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE

                                        selectedAnswers.clear()
                                        val multiSelectorAdapter1 =
                                            MultiSelectorQuizDisplayAdapter(this@QuizEdit, list, "yes")

                                        val touchListener1 = RecyclerItemClickListener(this@QuizEdit, recyclerView,
                                            object : RecyclerItemClickListener.OnItemClickListener {
                                                override fun onItemClick(view: View, position: Int) {
                                                    val dataS = multiSelectorAdapter1.getItem(position)
                                                    if (selectedAnswers.contains(dataS)) {
                                                        selectedAnswers.remove(dataS)
                                                    } else {
                                                        selectedAnswers.add(dataS)
                                                    }
                                                    multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                                    Log.d(
                                                        logTAG,
                                                        "service touchListener1 clicked : selected answer:  ${dataS.answer}"
                                                    )
                                                    final_json_array.getJSONObject(position)
                                                        .put("answer_id", dataS.ans_id)
                                                    if (final_json_array.getJSONObject(position).getString("answer").equals(
                                                            dataS.answer
                                                        )
                                                    ) {
                                                        final_json_array.getJSONObject(position).put("answer", "")
                                                    } else {
                                                        final_json_array.getJSONObject(position)
                                                            .put("answer", dataS.answer)
                                                    }

                                                    Log.d(
                                                        logTAG,
                                                        "OnItemClickListener final_json_array ${final_json_array.toString()}"
                                                    )
                                                }
                                            })

                                        recyclerView.adapter = multiSelectorAdapter1
                                        multiSelectorAdapter1.notifyDataSetChanged()
                                        var already_selected_list: ArrayList<QuizDisplayAnswerData>
                                        already_selected_list = ArrayList()
                                        for (i in 0 until list.size) {
                                            if (list[i].user_answer != "") {
                                                already_selected_list.add(list[i])
                                            } else {
                                                already_selected_list = ArrayList()
                                            }
                                        }
                                        selectedAnswers.addAll(already_selected_list)
                                        multiSelectorAdapter1.setSelectedAnswers(selectedAnswers)
                                        recyclerView.addOnItemTouchListener(touchListener1)

                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(
                                                this@QuizEdit,
                                                R.color.register_btn_color
                                            )
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default,
                                            null,
                                            null,
                                            null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE

                                        selectedAnswers.clear()
                                        val multiSelectorAdapter2 =
                                            MultiSelectorQuizDisplayAdapter(this@QuizEdit, list, "no")
                                        recyclerView.adapter = multiSelectorAdapter2
                                        multiSelectorAdapter2.notifyDataSetChanged()
                                        recyclerView.removeOnItemTouchListener(touchListener)

                                    }
                                }
                            })
                            Log.d(logTAG, "selected array ${selectedAnswers.size}")
                            Log.d(logTAG, "$question_id  ===== $current_data_counter====== $data_size")

                        }
                        ("rating") -> {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                                ll_no_data.visibility = View.GONE
                            }

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.VISIBLE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.VISIBLE

                            question_id = data[current_data_counter].question_id.toString()
                            tv_question_rating.text = data[current_data_counter].question.toString()

                            val list: ArrayList<QuizDisplayAnswerData>? =
                                response.body()!!.quizlist!![current_data_counter].answer_list
                            val recyclerView_rating = RecyclerView(this@QuizEdit)
                            val linearLayoutManager =
                                LinearLayoutManager(this@QuizEdit, LinearLayout.VERTICAL, false)
                            var lastVisibleItemIndex: Int?

                            recyclerView_rating.setHasFixedSize(true)
                            recyclerView_rating.layoutManager = linearLayoutManager
                            ll_rating.addView(recyclerView_rating)
                            var ratingAdapter: RatingAdapter
                            ratingAdapter = RatingAdapter(this@QuizEdit, list!!, "no")
                            recyclerView_rating.adapter = ratingAdapter
                            ratingAdapter.notifyDataSetChanged()

                            toggle_button.setOnCheckedChangeListener(object :
                                CompoundButton.OnCheckedChangeListener {
                                override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                                    edit_status = isChecked
                                    final_json_array = JSONArray()
                                    if (list.size > 0) {
                                        for (i in 0 until list.size) {
                                            val answer = list[i].user_answer
                                            val answer_obj = JSONObject()
                                            try {
                                                if (answer == "") {
                                                    answer_obj.put("answer_id", list[i].ans_id)
                                                    answer_obj.put("answer", "0")
                                                } else {
                                                    answer_obj.put("answer_id", list[i].ans_id)
                                                    answer_obj.put("answer", answer)
                                                }
                                                final_json_array.put(answer_obj)
                                            } catch (e: JSONException) {

                                            }
                                        }
                                    }
                                    Log.d(
                                        logTAG,
                                        "apirating answers size: ${list.size} final_json_array ${final_json_array.toString()}"
                                    )

                                    if (edit_status) {
                                        tv_edit.setTextColor(ContextCompat.getColor(this@QuizEdit, R.color.blue))
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(drawable_edit, null, null, null)
                                        tv_submit.visibility = View.VISIBLE
                                        tv_continue.visibility = View.GONE
                                        tv_back.visibility = View.GONE
                                        recyclerView_rating.adapter = null
                                        ratingAdapter = RatingAdapter(this@QuizEdit, list, "yes")
                                        recyclerView_rating.adapter = ratingAdapter
                                        ratingAdapter.notifyDataSetChanged()
                                    } else {
                                        tv_edit.setTextColor(
                                            ContextCompat.getColor(
                                                this@QuizEdit,
                                                R.color.register_btn_color
                                            )
                                        )
                                        tv_edit.setCompoundDrawablesWithIntrinsicBounds(
                                            drawable_default,
                                            null,
                                            null,
                                            null
                                        )
                                        tv_submit.visibility = View.GONE
                                        tv_continue.visibility = View.VISIBLE
                                        tv_back.visibility = View.VISIBLE
                                        recyclerView_rating.adapter = null
                                        ratingAdapter = RatingAdapter(this@QuizEdit, list, "no")
                                        recyclerView_rating.adapter = ratingAdapter
                                        ratingAdapter.notifyDataSetChanged()
                                    }
                                }
                            })

                            if (list.size <= 0) {
                                tv_show_more.visibility = View.GONE
                            } else {
                                tv_show_more.visibility = View.VISIBLE
                            }

                            recyclerView_rating.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)

                                    val currentFirstVisible =
                                        linearLayoutManager.findLastCompletelyVisibleItemPosition()
                                    lastVisibleItemIndex = currentFirstVisible
                                    Log.d(
                                        logTAG,
                                        "lastVisibleItemIndex listener $lastVisibleItemIndex"
                                    )
                                    if ((lastVisibleItemIndex) == (list.size - 1)) {
                                        tv_show_more.visibility = View.GONE
                                    } else {
                                        tv_show_more.visibility = View.VISIBLE
                                    }
                                }
                            })

                            tv_show_more.setOnClickListener {
                                val items_count = recyclerView_rating.adapter!!.itemCount
                                lastVisibleItemIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                                if ((lastVisibleItemIndex) == (items_count - 1)) {
                                    tv_show_more.visibility = View.GONE
                                } else {
                                    tv_show_more.visibility = View.VISIBLE
                                }
                                linearLayoutManager.smoothScrollToPosition(recyclerView_rating, null, items_count - 1)
                            }
                        }
                    }
                    first_time = 1
                    getSize = (100 / data_size!!).toFloat()
                } else {
                    ll_no_data.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onBackPressed() {
        val intent = Intent(this@QuizEdit, DashboardActivity::class.java)
        intent.putExtra("from_list", "profile_photos")
        startActivity(intent)
    }

    inner class RatingAdapter(context: Context, list: List<QuizDisplayAnswerData>, edit: String) :
        RecyclerView.Adapter<RatingAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: List<QuizDisplayAnswerData>? = null
        var edit = ""
        var isDraggable = false

        init {
            this.mContext = context
            this.mList = list
            this.edit = edit
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): RatingAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_rating, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }


        @SuppressLint("ClickableViewAccessibility")
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: RatingAdapter.ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            val answer = mList!![position]
            if (edit == "no" || edit == "") {
                isDraggable = true
            } else if (edit == "yes") {
                isDraggable = false
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                holder.slider_rating.min = 0
                rating = holder.slider_rating.min
            }
            holder.slider_rating.max = 10

            val user_rating = answer.user_answer
            holder.tv_rating.text = answer.answer
            if (user_rating == "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.slider_rating.setProgress(0, true)
                } else {
                    holder.slider_rating.progress = 0
                    holder.slider_rating.animate()
                }
                holder.tv_value.text = "0"

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.slider_rating.setProgress(user_rating.toInt(), true)
                } else {
                    holder.slider_rating.animate()
                    holder.slider_rating.progress = user_rating.toInt()
                }
                holder.tv_value.text = user_rating

            }

            holder.slider_rating.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(view: View?, motionEvent: MotionEvent?): Boolean {
                    return isDraggable
                }
            })

            holder.slider_rating.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    rating = progress
                    rating_position = position
                    holder.tv_value.text = rating.toString()
                    Log.d(logTAG, "position $position rating $rating")
                    final_json_array.getJSONObject(position).put("answer_id", mList!![position].ans_id)
                    final_json_array.getJSONObject(position).put("answer", rating.toString())
                    Log.d(logTAG, "array" + final_json_array.toString())

                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                }
            })


        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_rating: TextView = view.findViewById(R.id.tv_rating)
            var tv_value: TextView = view.findViewById(R.id.tv_value)
            var slider_rating: SeekBar = view.findViewById(R.id.slider_rating)

        }

    }

    inner class MultiSelectorQuizDisplayAdapter(
        context: Context, list: ArrayList<QuizDisplayAnswerData>, edit: String
    ) :
        RecyclerView.Adapter<MultiSelectorQuizDisplayAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<QuizDisplayAnswerData> = ArrayList()
        private var mAnswersList: ArrayList<QuizDisplayAnswerData> = ArrayList()
        private var edit = ""

        init {
            this.mContext = context
            this.mList = list
            this.edit = edit
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): MultiSelectorQuizDisplayAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_quiz, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: MultiSelectorQuizDisplayAdapter.ViewHolder, position: Int) {

            val data = mList[position]

            if (edit == "no" || edit == "") {
                if (mAnswersList.isNotEmpty()) {
                    if (mAnswersList.contains(data)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                                mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                                , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                            , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                            mContext!!.resources.getColor(R.color.gray)
                        )
                    }
                }

                if (data.answer == data.user_answer) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                            mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.selected_bg),
                            mContext!!.resources.getColor(R.color.white_color)
                        )
                    }
                }
            } else if (edit == "yes") {
                if (mAnswersList.isNotEmpty()) {
                    if (mAnswersList.contains(data)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                                mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                                , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                            , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                            mContext!!.resources.getColor(R.color.gray)
                        )
                    }
                }

            }
        }


        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            private var tv_answer: TextView = view.findViewById(R.id.tv_name)
            private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_answer_item)

            @SuppressLint("NewApi")
            fun bind(answer: String, bg_drawable: Drawable, text_color: Int) {
                tv_answer.text = answer
                tv_answer.setTextColor(text_color)
                ll_answer_item.background = bg_drawable
            }

        }

        override fun getItemCount(): Int {
            return mList.size
        }

        fun setSelectedAnswers(answersList: ArrayList<QuizDisplayAnswerData>) {
            this.mAnswersList = answersList
            notifyDataSetChanged()
        }

        fun getItem(position: Int): QuizDisplayAnswerData {
            return mList[position]
        }

    }

    inner class SingleSelectorQuizDisplayAdapter(
        context: Context, list: ArrayList<QuizDisplayAnswerData>, edit: String
    ) :
        RecyclerView.Adapter<SingleSelectorQuizDisplayAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<QuizDisplayAnswerData> = ArrayList()
        private var mAnswersList: ArrayList<QuizDisplayAnswerData> = ArrayList()
        private var edit = ""

        init {
            this.mContext = context
            this.mList = list
            this.edit = edit
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): SingleSelectorQuizDisplayAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_quiz, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: SingleSelectorQuizDisplayAdapter.ViewHolder, position: Int) {

            val data = mList[position]

            if (edit == "no" || edit == "") {
                if (mAnswersList.isNotEmpty()) {
                    if (mAnswersList.contains(data)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                                mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                                , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                            , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                            mContext!!.resources.getColor(R.color.gray)
                        )
                    }
                }

                if (data.answer == data.user_answer) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                            mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.selected_bg),
                            mContext!!.resources.getColor(R.color.white_color)
                        )
                    }
                }
            } else if (edit == "yes") {
                if (mAnswersList.isNotEmpty()) {
                    if (mAnswersList.contains(data)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                                mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.selected_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                                , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                            )
                        } else {
                            holder.bind(
                                data.answer,
                                mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                                mContext!!.resources.getColor(R.color.gray)
                            )
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                            , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                        )
                    } else {
                        holder.bind(
                            data.answer,
                            mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                            mContext!!.resources.getColor(R.color.gray)
                        )
                    }
                }

            }
        }


        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            private var tv_answer: TextView = view.findViewById(R.id.tv_name)
            private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_answer_item)

            @SuppressLint("NewApi")
            fun bind(answer: String, bg_drawable: Drawable, text_color: Int) {
                tv_answer.text = answer
                tv_answer.setTextColor(text_color)
                ll_answer_item.background = bg_drawable
            }

        }

        override fun getItemCount(): Int {
            return mList.size
        }

        fun setSelectedAnswers(answersList: ArrayList<QuizDisplayAnswerData>) {
            this.mAnswersList = answersList
            notifyDataSetChanged()
        }

        fun getItem(position: Int): QuizDisplayAnswerData {
            return mList[position]
        }

    }

}
