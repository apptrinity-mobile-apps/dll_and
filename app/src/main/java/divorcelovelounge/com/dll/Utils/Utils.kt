package divorcelovelounge.com.dll.Utils

import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics

import java.io.InputStream
import java.io.OutputStream

object Utils {

    /*Rounded Imageview Utils*/
    fun CopyStream(`is`: InputStream, os: OutputStream) {
        val buffer_size = 1024
        try {

            val bytes = ByteArray(buffer_size)
            while (true) {
                //Read byte from input stream

                val count = `is`.read(bytes, 0, buffer_size)
                if (count == -1)
                    break

                //Write byte from output stream
                os.write(bytes, 0, count)
            }
        } catch (ex: Exception) {
        }

    }


    /*Spark Button Utils*/
    fun mapValueFromRangeToRange(
        value: Double,
        fromLow: Double,
        fromHigh: Double,
        toLow: Double,
        toHigh: Double
    ): Double {
        return toLow + (value - fromLow) / (fromHigh - fromLow) * (toHigh - toLow)
    }

    fun clamp(value: Double, low: Double, high: Double): Double {
        return Math.min(Math.max(value, low), high)
    }

    fun darkenColor(color: Int, multiplier: Float): Int {
        val hsv = FloatArray(3)

        Color.colorToHSV(color, hsv)
        hsv[2] *= multiplier // value component
        return Color.HSVToColor(hsv)
    }

    fun dpToPx(context: Context, dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }


    /*Auto fit GridView*/

    fun calculateNoOfColumns(context: Context, columnWidthDp: Float): Int { // For example columnWidthdp=180
        val displayMetrics = context.resources.displayMetrics
        val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
        return (screenWidthDp / columnWidthDp + 0.5).toInt()
    }


}