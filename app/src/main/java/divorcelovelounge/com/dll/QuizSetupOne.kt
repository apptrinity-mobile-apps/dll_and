package divorcelovelounge.com.dll

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.*
import java.util.*
import divorcelovelounge.com.dll.Adapters.MultiSelectorAdapter
import divorcelovelounge.com.dll.Adapters.SingleSelectorAdapter
import divorcelovelounge.com.dll.ApiPojo.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.graphics.drawable.ColorDrawable
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.view.*
import divorcelovelounge.com.dll.Helper.*


//RAVITEJA
class QuizSetupOne : AppCompatActivity() {

    /* UI components */
    // Layouts
    private lateinit var ll_single: LinearLayout
    private lateinit var ll_multiple: LinearLayout
    private lateinit var ll_rating: LinearLayout
    private lateinit var ll_quiz_main: LinearLayout

    // TextViews
    lateinit var tv_back: TextView
    lateinit var tv_continue: TextView

    // ListView
    lateinit var lv_single: ListView

    // Toolbar
    private lateinit var customToolBar: Toolbar

    // Dialog
    private lateinit var progressDialog: Dialog

    // TextViews
    private lateinit var tv_question_rating: CustomTextViewBold
    private lateinit var tv_question_radio: CustomTextViewBold
    private lateinit var tv_question_checkbox: CustomTextViewBold
    private lateinit var tv_note_id: CustomTextViewBold
    private lateinit var ll_no_data: CustomTextViewBold

    /* Constants */
    private lateinit var sessionManager: DllSessionManager
    private lateinit var selectedAnswers: ArrayList<QuizAnswerResponse>
    private lateinit var quizData: ArrayList<QuizData>
    private lateinit var QuizAnswersList: ArrayList<HashMap<String, String>>
    private lateinit var QuizQuesAnswersList: ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>
    var ratingAnswerHashMap = HashMap<String, String>()
    val quizQuesAnswerHashMap = HashMap<String, ArrayList<HashMap<String, String>>>()
    lateinit var checkboxanswerHashMap: HashMap<String, String>

    val logTAG = "QuizSetupOne"
    var user_id: String? = null
    var answer: String? = null
    var data_size: Int? = null
    var question_id = ""
    var current_data_counter = 0
    var rating = 0
    var rating_position = 0
    var list: ArrayList<QuizAnswerResponse> = ArrayList()
    var first_time = 0

    var getSize: Float = 0.0f

    lateinit var tv_skip: TextView
    lateinit var horizontal_progress_bar: SignSeekBar
    lateinit var tv_show_more: LinearLayout

    lateinit var dllsesstionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_setup_one)
        title = ""

        sessionManager = DllSessionManager(this@QuizSetupOne)
        if (!sessionManager.userDetails["userid"].equals("")) {
            user_id = sessionManager.userDetails["userid"]
        }

        dllsesstionManager = DllSessionManager(this)
        user_details = dllsesstionManager.userDetails

        Log.d(logTAG, "USER ID : $user_id")

        /* setting custom tool bar */
        customToolBar = findViewById(R.id.quizToolBar)
        tv_continue = customToolBar.findViewById(R.id.tv_quiz_next)
        tv_back = customToolBar.findViewById(R.id.tv_quiz_back)

        setSupportActionBar(customToolBar)

        initialize()

        tv_continue.visibility = View.GONE
        tv_back.visibility = View.GONE


        if (InternetConnection.checkConnection(this@QuizSetupOne)) {
            progressDialog.show()
            ll_no_data.text = resources.getString(R.string.loading)
            quizApiCall()
        } else {
            if (progressDialog.isShowing) {
                progressDialog.dismiss()
            }
            ll_no_data.text = resources.getString(R.string.no_network_connection)
            val snackbar: Snackbar =
                Snackbar.make(ll_quiz_main, "Please Check Your Connection", Snackbar.LENGTH_INDEFINITE)
            snackbar.show()
            snackbar.setAction("Reload", View.OnClickListener {
                if (InternetConnection.checkConnection(this@QuizSetupOne)) {
                    snackbar.dismiss()
                    progressDialog.show()
                    ll_no_data.text = resources.getString(R.string.loading)
                    quizApiCall()
                } else {
                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }
                    ll_no_data.text = resources.getString(R.string.no_network_connection)
                    val snackbar1: Snackbar =
                        Snackbar.make(ll_quiz_main, "Please Check Your Connection", Snackbar.LENGTH_INDEFINITE)
                    snackbar1.show()
                    snackbar1.setAction("Reload", View.OnClickListener {
                        if (InternetConnection.checkConnection(this@QuizSetupOne)) {
                            snackbar.dismiss()
                            progressDialog.show()
                            ll_no_data.text = resources.getString(R.string.loading)
                            quizApiCall()
                        } else {
                            snackbar.show()
                        }
                    })
                }
            })

        }


        tv_continue.setOnClickListener {
            if (current_data_counter == data_size) {
                Log.d(logTAG, "size equal")
                current_data_counter -= 1

            } else {

                /* if (checkValidations()) {
                     Log.d(logTAG, "is valid")*/

                if (first_time == 1 && quizData[current_data_counter].option_type!!.equals("rating")) {
                    QuizAnswersList.add(ratingAnswerHashMap)
                    quizQuesAnswerHashMap.put(question_id, QuizAnswersList)
                    first_time = 0
                } else if (first_time == 0 && quizData[current_data_counter].option_type!!.equals("rating")) {
                    QuizAnswersList = ArrayList()
                    QuizAnswersList.add(ratingAnswerHashMap)
                    quizQuesAnswerHashMap.put(question_id, QuizAnswersList)
                } else {
                    QuizAnswersList = ArrayList()
                    QuizAnswersList.add(checkboxanswerHashMap)
                    quizQuesAnswerHashMap.put(question_id, QuizAnswersList)
                }

                if (current_data_counter < data_size!!) {
                    current_data_counter += 1
                }
                if (current_data_counter.equals(data_size!! - 1)) {
                    Log.e("data_array_skip: ", "SKIP")
                    tv_continue.text = "Continue"
                    tv_skip.visibility = View.GONE
                }


                if (current_data_counter < data_size!!) {

                    horizontal_progress_bar.setProgress(getSize * current_data_counter)
                    horizontal_progress_bar.animate()

                    Log.d(logTAG, "data_array: $data_size  current_data_counter $current_data_counter")

                    when (quizData[current_data_counter].option_type!!) {
                        ("radio") -> {

                            ll_single.visibility = View.VISIBLE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.GONE

                            tv_question_radio.text = quizData[current_data_counter].question.toString()
                            question_id = quizData[current_data_counter].question_id.toString()
                            val list: ArrayList<QuizAnswerResponse>? = quizData[current_data_counter].answer
                            Log.d(
                                logTAG,
                                "data_array count: $current_data_counter radio answers size: ${list!!.size} "
                            )

                            val singleSelectorAdapter = SingleSelectorAdapter(this@QuizSetupOne, list)
                            lv_single.adapter = singleSelectorAdapter
                            checkboxanswerHashMap = HashMap()
                            lv_single.setOnItemClickListener { parent, view, position, id ->
                                view.isSelected = true
                                try {
                                    answer = lv_single.getItemAtPosition(position).toString()
                                    singleSelectorAdapter.setSelectedIndex(position)
                                    val answerId = singleSelectorAdapter.getId(position).toString()

                                    checkboxanswerHashMap[answerId] = answer!!

                                    Log.d(
                                        logTAG,
                                        "data_array count: $current_data_counter radio button $position --- answer $answer "
                                    )
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }

                        }
                        ("checkbox") -> {

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.VISIBLE
                            tv_show_more.visibility = View.GONE

                            //ratingAnswerHashMap.clear()
                            selectedAnswers.clear()
                            ll_multiple.removeAllViews()
                            tv_question_checkbox.text = quizData[current_data_counter].question.toString()
                            ll_multiple.addView(tv_question_checkbox)


                            Log.e("question_lll",quizData[current_data_counter].question.toString()+"----")

                            question_id = quizData[current_data_counter].question_id.toString()
                            if(quizData[current_data_counter].question.toString().equals("How your friends describe you in 4 words?")){
                                ll_multiple.addView(tv_note_id)
                                tv_note_id.text="NOTE: You can choose multiple options also"
                                tv_note_id.visibility=View.VISIBLE
                                Log.e("question_1",quizData[current_data_counter].question.toString()+"----")

                            }else{
                                ll_multiple.removeAllViews()
                                tv_question_checkbox.text = quizData[current_data_counter].question.toString()
                                ll_multiple.addView(tv_question_checkbox)
                                tv_note_id.visibility=View.GONE
                                Log.e("question_2",quizData[current_data_counter].question.toString())

                            }

                            list.clear()
                            list = quizData[current_data_counter].answer!!
                            Log.d(
                                logTAG,
                                "data_array count: $current_data_counter checkbox answers size: ${list.size}  "
                            )

                            val recyclerView = RecyclerView(this)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                            ll_multiple.addView(recyclerView)

                            val multiSelectorAdapter = MultiSelectorAdapter(this@QuizSetupOne, list)
                            checkboxanswerHashMap = HashMap()
                            recyclerView.adapter = multiSelectorAdapter
                            recyclerView.addOnItemTouchListener(
                                RecyclerItemClickListener(this@QuizSetupOne, recyclerView,
                                    object : RecyclerItemClickListener.OnItemClickListener {

                                        override fun onItemClick(view: View, position: Int) {

                                            val dataS = list[position]
                                            val answerId = dataS.answer_id

                                            Log.d(logTAG, "1==selectedAnswers===$position")

                                            if (selectedAnswers.contains(dataS)) {
                                                selectedAnswers.remove(dataS)
                                                Log.d(
                                                    logTAG,
                                                    "1 remove if  ${selectedAnswers.size} ==== ${dataS.answer}"
                                                )
                                                checkboxanswerHashMap.remove(answerId)
                                            } else {
                                                selectedAnswers.add(dataS)
                                                Log.d(
                                                    logTAG,
                                                    "1 add else  ${selectedAnswers.size}    ==== ${dataS.answer}"
                                                )
                                                checkboxanswerHashMap[answerId!!] = dataS.answer.toString()
                                            }

                                            multiSelectorAdapter.selectedAnswers(selectedAnswers)

                                            Log.d(
                                                logTAG,
                                                "1 data_array count: $current_data_counter selected ${dataS.answer} selected array ${selectedAnswers.size}"
                                            )
                                        }
                                    })
                            )

                        }
                        ("rating") -> {

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.VISIBLE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.VISIBLE

                            ll_rating.removeAllViews()
                            tv_question_rating.text = quizData[current_data_counter].question.toString()
                            ll_rating.addView(tv_question_rating)
                            ratingAnswerHashMap = HashMap()
                            question_id = quizData[current_data_counter].question_id.toString()

                            val list: ArrayList<QuizAnswerResponse>? = quizData[current_data_counter].answer
                            val recyclerView_rating = RecyclerView(this)
                            val linearLayoutManager =
                                LinearLayoutManager(this@QuizSetupOne, LinearLayout.VERTICAL, false)
                            var lastVisibleItemIndex: Int?

                            recyclerView_rating.setHasFixedSize(true)
                            recyclerView_rating.layoutManager = linearLayoutManager
                            ll_rating.addView(recyclerView_rating)
                            val ratingAdapter = RatingAdapter(this@QuizSetupOne, list!!)
                            recyclerView_rating.adapter = ratingAdapter
                            ratingAdapter.notifyDataSetChanged()
                            Log.d(
                                logTAG,
                                "data_count: $current_data_counter rating_size: ${list.size} rating $rating"
                            )

                            if (list.size <= 0) {
                                tv_show_more.visibility = View.GONE
                            } else {
                                tv_show_more.visibility = View.VISIBLE
                            }

                            recyclerView_rating.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)

                                    val currentFirstVisible =
                                        linearLayoutManager.findLastCompletelyVisibleItemPosition()
                                    lastVisibleItemIndex = currentFirstVisible
                                    Log.d(
                                        logTAG,
                                        "lastVisibleItemIndex listener $lastVisibleItemIndex"
                                    )
                                    if ((lastVisibleItemIndex) == (list.size - 1)) {
                                        tv_show_more.visibility = View.GONE
                                    } else {
                                        tv_show_more.visibility = View.VISIBLE
                                    }
                                }
                            })

                            tv_show_more.setOnClickListener {
                                val items_count = recyclerView_rating.adapter!!.itemCount
                                lastVisibleItemIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                                if ((lastVisibleItemIndex) == (items_count - 1)) {
                                    tv_show_more.visibility = View.GONE
                                } else {
                                    tv_show_more.visibility = View.VISIBLE
                                }

                                linearLayoutManager.smoothScrollToPosition(
                                    recyclerView_rating,
                                    null, /*lastVisibleItemIndex!! + (items_count / 2)*/
                                    items_count - 1
                                )
                            }

                        }
                    }

                } else {

                    horizontal_progress_bar.setProgress(100f)
                    horizontal_progress_bar.animate()

                    val obj = JSONObject(quizQuesAnswerHashMap)

                    Log.e("discount_exist", obj.toString())

                    progressDialog.show()

                    val apiInterface = ApiInterface.create()
                    val callApi = apiInterface.saveQuiz(user_id!!, obj)
                    callApi.enqueue(object : Callback<QuizSubmitResponse> {
                        override fun onFailure(call: Call<QuizSubmitResponse>, t: Throwable) {
                            Log.e(logTAG, "saveQuiz Response error : " + t.toString())
                        }

                        override fun onResponse(
                            call: Call<QuizSubmitResponse>, response: Response<QuizSubmitResponse>
                        ) {
                            Log.d(logTAG, "saveQuiz Response success : ${response.isSuccessful}")

                            if (response.isSuccessful) {
                                if (progressDialog.isShowing) {
                                    progressDialog.dismiss()
                                }
                                sessionManager.userSignUpStatus("4")

                                Toast.makeText(
                                    this@QuizSetupOne,
                                    "Thank you for submitting your responses !", Toast.LENGTH_SHORT
                                ).show()

                                val photo1 = user_details.get(DllSessionManager.PHOTO1)
                                if(photo1.equals("0")){
                                    val home_intent = Intent(this@QuizSetupOne, ProfilePhotos::class.java)
                                    //home_intent.putExtra("from_list","match_setup")
                                    startActivity(home_intent)
                                    finish()
                                }else {

                                    val home_intent = Intent(this@QuizSetupOne, DashboardActivity::class.java)
                                    home_intent.putExtra("from_list", "quiz_setup")
                                    startActivity(home_intent)
                                    finish()
                                }



                                /*val homeIntent = Intent(this@QuizSetupOne, DashboardActivity::class.java)
                                homeIntent.putExtra("from_list", "quiz_setup")
                                startActivity(homeIntent)
                                this@QuizSetupOne.finish()*/
                            }
                        }
                    })
                }

                /*} else {
                    Log.d(logTAG, "is not valid")
                    // do nothing
                }*/

            }
        }


        tv_skip.setOnClickListener {

            val builder = AlertDialog.Builder(this@QuizSetupOne)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to SKIP?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {

                        val photo1 = user_details.get(DllSessionManager.PHOTO1)
                        if(photo1.equals("0")){
                            val home_intent = Intent(this@QuizSetupOne, ProfilePhotos::class.java)
                            //home_intent.putExtra("from_list","match_setup")
                            startActivity(home_intent)
                            finish()
                        }else {

                            val home_intent = Intent(this@QuizSetupOne, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "quiz_setup")
                            startActivity(home_intent)
                            finish()
                        }


                       /* val home_intent = Intent(this@QuizSetupOne, DashboardActivity::class.java)
                        home_intent.putExtra("from_list", "quiz_setup")
                        startActivity(home_intent)
                        finish()*/
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }

    }

    private fun initialize() {

        ll_rating = findViewById(R.id.ll_rating)
        ll_multiple = findViewById(R.id.ll_multiple)
        ll_single = findViewById(R.id.ll_single)

        lv_single = findViewById(R.id.lv_single)

        tv_question_checkbox = findViewById(R.id.tv_question_checkbox)
        tv_note_id = findViewById(R.id.tv_note_id)
        tv_question_radio = findViewById(R.id.tv_question_radio)
        tv_question_rating = findViewById(R.id.tv_question_rating)

        tv_skip = findViewById(R.id.tv_skip)
        tv_show_more = findViewById(R.id.tv_show_more)
        ll_no_data = findViewById(R.id.ll_no_data)
        ll_quiz_main = findViewById(R.id.ll_quiz_main)
        horizontal_progress_bar = findViewById(R.id.horizontal_progress_bar)
        horizontal_progress_bar.setClickable(false)
        horizontal_progress_bar.setFocusable(false)
        horizontal_progress_bar.setEnabled(false)

        quizData = ArrayList()
        selectedAnswers = ArrayList()
        QuizAnswersList = ArrayList()
        QuizQuesAnswersList = ArrayList()

        progressDialog = Dialog(this)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.setContentView(R.layout.alert_dialog_loading)
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog.setCancelable(false)
        progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    progressDialog.dismiss()
                }
                return true
            }
        })

    }

    private fun checkValidations(): Boolean {
        when (quizData[current_data_counter].option_type!!) {
            ("radio") -> {
                if (!checkboxanswerHashMap.isEmpty()) {
                    Log.d(logTAG, " valid ---- radio ")
                    return true
                } else {
                    Toast.makeText(this@QuizSetupOne, "Please select any one!", Toast.LENGTH_SHORT).show()
                    Log.d(logTAG, "not valid ---- radio ")
                    return false
                }
            }

            ("checkbox") -> {
                if (!checkboxanswerHashMap.isEmpty()) {
                    Log.d(logTAG, " valid ---- check box ")
                    return true
                } else {
                    Toast.makeText(this@QuizSetupOne, "Please select at least one!", Toast.LENGTH_SHORT).show()
                    Log.d(logTAG, "not valid ---- check box ")
                    return false
                }
            }

            ("rating") -> {
                if (!ratingAnswerHashMap.isEmpty()) {
                    if (ratingAnswerHashMap.size != quizData[current_data_counter].answer!!.size) {
                        Toast.makeText(this@QuizSetupOne, "All fields are mandatory!", Toast.LENGTH_SHORT).show()
                        Log.d(logTAG, "not valid --- size not equal rating")
                        return false
                    } else {
                        Log.d(logTAG, " valid ---- first_time $first_time rating")
                        return true
                    }
                } else {
                    Toast.makeText(this@QuizSetupOne, "All fields are mandatory!", Toast.LENGTH_SHORT).show()
                    Log.d(logTAG, "not valid ---- first_time $first_time rating")
                    return false
                }

            }

            else -> {
                return false
            }

        }
    }

    private fun quizApiCall() {

        val apiService = ApiInterface.create()
        val call = apiService.quizApi()
        Log.d(logTAG, "Api Call Request : " + call.toString())
        call.enqueue(object : Callback<QuizResponse> {
            override fun onFailure(call: Call<QuizResponse>, t: Throwable) {
                Log.e(logTAG, "Response error : " + t.printStackTrace())
                ll_no_data.visibility = View.VISIBLE
            }

            override fun onResponse(
                call: Call<QuizResponse>, response: Response<QuizResponse>
            ) {
                if (response.body() != null) {

                    Log.d(logTAG, "Response success : ${response.body()}")
                    val data = response.body()!!.data
                    data_size = data!!.size

                    tv_continue.visibility = View.VISIBLE

                    for (i in 0 until data.size) {
                        quizData = data
                    }

                    when (data[current_data_counter].option_type!!) {
                        ("radio") -> {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                                ll_no_data.visibility = View.GONE
                            }

                            ll_single.visibility = View.VISIBLE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.GONE

                            tv_question_radio.text = data[current_data_counter].question.toString()
                            question_id = data[current_data_counter].question_id.toString()

                            val list: ArrayList<QuizAnswerResponse>? =
                                response.body()!!.data!![current_data_counter].answer
                            Log.d(logTAG, "radio answers size: ${list!!.size} ")
                            checkboxanswerHashMap = HashMap()
                            val singleSelectorAdapter = SingleSelectorAdapter(this@QuizSetupOne, list)
                            lv_single.adapter = singleSelectorAdapter

                            lv_single.setOnItemClickListener { parent, view, position, id ->
                                view.isSelected = true
                                try {
                                    answer = lv_single.getItemAtPosition(position).toString()
                                    singleSelectorAdapter.setSelectedIndex(position)
                                    val answerId = singleSelectorAdapter.getId(position).toString()
                                    checkboxanswerHashMap[answerId] = answer!!

                                    Log.d(logTAG, "radio button $position --- answer $answer ")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }

                        }
                        ("checkbox") -> {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                                ll_no_data.visibility = View.GONE
                            }

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.GONE
                            ll_multiple.visibility = View.VISIBLE
                            tv_show_more.visibility = View.GONE

                            selectedAnswers.clear()
                            ll_multiple.removeAllViews()
                            tv_question_checkbox.text = data[current_data_counter].question.toString()
                            ll_multiple.addView(tv_question_checkbox)

                            checkboxanswerHashMap = HashMap()
                            val list: ArrayList<QuizAnswerResponse>? =
                                response.body()!!.data!![current_data_counter].answer

                            question_id = response.body()!!.data!![current_data_counter].question_id.toString()
                            val multiSelectorAdapter1 = MultiSelectorAdapter(this@QuizSetupOne, list!!)

                            val recyclerView = RecyclerView(applicationContext)
                            recyclerView.setHasFixedSize(true)
                            recyclerView.layoutManager =
                                    LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)
                            ll_multiple.addView(recyclerView)

                            recyclerView.adapter = multiSelectorAdapter1
                            recyclerView.addOnItemTouchListener(
                                RecyclerItemClickListener(this@QuizSetupOne, recyclerView,
                                    object : RecyclerItemClickListener.OnItemClickListener {

                                        override fun onItemClick(view: View, position: Int) {

                                            val dataS = multiSelectorAdapter1.getItem(position)
                                            val answerId = dataS.answer_id

                                            multiSelectorAdapter1.selectedAnswers(selectedAnswers)
                                            if (selectedAnswers.contains(dataS)) {
                                                selectedAnswers.remove(dataS)
                                                checkboxanswerHashMap.remove(answerId)
                                            } else {
                                                selectedAnswers.add(dataS)
                                                checkboxanswerHashMap[answerId!!] = dataS.answer.toString()
                                            }

                                            Log.d(logTAG, "selected $dataS")
                                        }
                                    })

                            )
                            Log.d(logTAG, "selected array ${selectedAnswers.size}")
                            Log.d(logTAG, "$question_id  ===== $current_data_counter====== $data_size")

                        }
                        ("rating") -> {
                            if (progressDialog.isShowing) {
                                progressDialog.dismiss()
                                ll_no_data.visibility = View.GONE
                            }

                            ll_single.visibility = View.GONE
                            ll_rating.visibility = View.VISIBLE
                            ll_multiple.visibility = View.GONE
                            tv_show_more.visibility = View.VISIBLE
                            ratingAnswerHashMap = HashMap()
                            question_id = data[current_data_counter].question_id.toString()
                            tv_question_rating.text = data[current_data_counter].question.toString()
                            Log.d(logTAG, "rating answers size: ${list.size} ")

                            val list: ArrayList<QuizAnswerResponse>? =
                                response.body()!!.data!![current_data_counter].answer
                            val recyclerView_rating = RecyclerView(this@QuizSetupOne)
                            val linearLayoutManager =
                                LinearLayoutManager(this@QuizSetupOne, LinearLayout.VERTICAL, false)
                            var lastVisibleItemIndex: Int?

                            recyclerView_rating.setHasFixedSize(true)
                            recyclerView_rating.layoutManager = linearLayoutManager
                            ll_rating.addView(recyclerView_rating)
                            val ratingAdapter = RatingAdapter(this@QuizSetupOne, list!!)
                            recyclerView_rating.adapter = ratingAdapter
                            ratingAdapter.notifyDataSetChanged()

                            if (list.size <= 0) {
                                tv_show_more.visibility = View.GONE
                            } else {
                                tv_show_more.visibility = View.VISIBLE
                            }

                            recyclerView_rating.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)

                                    val currentFirstVisible =
                                        linearLayoutManager.findLastCompletelyVisibleItemPosition()
                                    lastVisibleItemIndex = currentFirstVisible
                                    Log.d(
                                        logTAG,
                                        "lastVisibleItemIndex listener $lastVisibleItemIndex"
                                    )
                                    if ((lastVisibleItemIndex) == (list.size - 1)) {
                                        tv_show_more.visibility = View.GONE
                                    } else {
                                        tv_show_more.visibility = View.VISIBLE
                                    }
                                }
                            })

                            tv_show_more.setOnClickListener {
                                val items_count = recyclerView_rating.adapter!!.itemCount

                                lastVisibleItemIndex = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                                if ((lastVisibleItemIndex) == (items_count - 1)) {
                                    tv_show_more.visibility = View.GONE
                                } else {
                                    tv_show_more.visibility = View.VISIBLE
                                }
                                linearLayoutManager.smoothScrollToPosition(
                                    recyclerView_rating,
                                    null, /*lastVisibleItemIndex!! + (items_count / 2)*/
                                    items_count - 1
                                )
                            }

                        }

                    }
                    first_time = 1

                    getSize = (100 / data_size!!).toFloat()

                } else {

                    ll_no_data.visibility = View.VISIBLE

                }

            }

        })
    }

    inner class RatingAdapter(context: Context, list: List<QuizAnswerResponse>) :
        RecyclerView.Adapter<RatingAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: List<QuizAnswerResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): RatingAdapter.ViewHolder {

            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_rating, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }


        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: RatingAdapter.ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            val answer = mList!![position]
            holder.tv_rating.text = answer.answer

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                holder.slider_rating.min = 0
                rating = holder.slider_rating.min
            }
            holder.slider_rating.max = 10

            holder.slider_rating.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    rating = progress
                    rating_position = position
                    holder.tv_value.text = rating.toString()
                    Log.d(logTAG, "position $position rating $rating")
                    ratingAnswerHashMap[mList!![position].answer_id.toString()] = rating.toString()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                }
            })

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_rating: TextView = view.findViewById(R.id.tv_rating)
            var tv_value: TextView = view.findViewById(R.id.tv_value)
            var slider_rating: SeekBar = view.findViewById(R.id.slider_rating)

        }

    }

    override fun onBackPressed() {

        if (current_data_counter == 0) {
            progressDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
                override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        progressDialog.dismiss()
                    }
                    return true
                }
            })
            if (!progressDialog.isShowing) {
                this@QuizSetupOne.finish()
            }
        } else {
            alert()
        }

    }

    fun alert() {
        val builder = AlertDialog.Builder(this@QuizSetupOne)
        builder.setTitle("Warning!")
            .setMessage("Do you wish to head start again?")
            .setCancelable(false)
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                }

            })
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    val intent = Intent(this@QuizSetupOne, QuizSetupWelcome::class.java)
                    startActivity(intent)
                    this@QuizSetupOne.finish()
                }

            })

        val alertDialog = builder.create()
        alertDialog.show()
    }

}
