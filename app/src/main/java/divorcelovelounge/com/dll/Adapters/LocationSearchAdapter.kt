package divorcelovelounge.com.dll.Adapters

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import divorcelovelounge.com.dll.R

class LocationSearchAdapter(private val context: Activity, val data: ArrayList<String>) :
    BaseAdapter(), Filterable {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var selectedIndex = -1
    var locationList: ArrayList<String>? = null
    var locationFilteredList: ArrayList<String>? = null
    private var mItemFilter: ItemFilter? = null

    init {
        this.locationList = data
        this.locationFilteredList = data
    }

    override fun getItem(position: Int): Any {
        return locationFilteredList!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return locationFilteredList!!.size
    }

    class ViewHolder {
        internal var tv_text: TextView? = null
        internal var ll_item_list: LinearLayout? = null
    }

    fun setSelectedIndex(ind: Int) {
        selectedIndex = ind
        notifyDataSetChanged()
    }

    fun getSelectedIndex(): Int {
        return selectedIndex
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_listview_single, parent, false)
            holder = ViewHolder()
            holder.tv_text = view.findViewById(R.id.alertTextView)
            holder.ll_item_list = view.findViewById(R.id.ll_item)

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        if (selectedIndex != -1 && position == selectedIndex) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.setBackgroundColor(context.resources.getColor(R.color.register_btn_color,context.theme))
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color,context.theme))
            } else {
                holder.ll_item_list!!.setBackgroundColor(context.resources.getColor(R.color.register_btn_color))
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.setBackgroundColor(context.resources.getColor(R.color.white_color,context.theme))
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray,context.theme))
            } else {
                holder.ll_item_list!!.setBackgroundColor(context.resources.getColor(R.color.white_color))
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray))
            }
        }

        holder.tv_text!!.text = locationFilteredList!![position]
        return view
    }


    override fun getFilter(): Filter {
        if (mItemFilter == null) {
            mItemFilter = ItemFilter()
        }
        return mItemFilter!!
    }

    // Filter class
    private inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            var filterText = constraint
            selectedIndex = -1
            val results = Filter.FilterResults()
            if (filterText != null && filterText.isNotEmpty()) {
                filterText = filterText.toString().toLowerCase()
                val filteredCities = ArrayList<String>()
                for (i in 0 until locationList!!.size) {
                    if (locationList!![i].toLowerCase().contains(filterText)) {
                        filteredCities.add(locationList!![i])
                    }
                }
                results.count = filteredCities.size
                results.values = filteredCities
            } else {
                results.count = locationList!!.size
                results.values = locationList!!
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {

            locationFilteredList = results.values as ArrayList<String>
            notifyDataSetChanged()

        }
    }

}