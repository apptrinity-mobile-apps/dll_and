package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import divorcelovelounge.com.dll.ApiPojo.TalkJsData
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.Utils.ImageLoader
import divorcelovelounge.com.dll.ViewPagerAdapters.CardItem

class ConversationsAdapter(
    private val context: Activity,
    private val data: ArrayList<CardItem>,
    private val talkjsId: HashMap<String, String>
) :
    BaseAdapter() {
    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    private val imageLoader: ImageLoader
    private val mInflater: LayoutInflater
    var photo = ""
    //  private var Boolean isSelected = false

    init {
        mInflater = LayoutInflater.from(context)
        imageLoader = ImageLoader(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var iv_convo_status_icon: ImageView? = null
        internal var iv_convo_user_image: ImageView? = null
        internal var tv_convo_name: CustomTextViewBold? = null
        internal var tv_convo_message: CustomTextView? = null
        internal var linear_layout_item: LinearLayout? = null


    }

    @SuppressLint("SetTextI18n", "NewApi")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.conversations_item_list, parent, false)
            holder = ViewHolder()
            holder.tv_convo_name = view.findViewById<View>(R.id.tv_convo_name) as CustomTextViewBold
            holder.tv_convo_message =
                view.findViewById<View>(R.id.tv_convo_message) as CustomTextView
            holder.iv_convo_user_image =
                view.findViewById<View>(R.id.iv_convo_user_image) as ImageView
            holder.iv_convo_status_icon =
                view.findViewById<View>(R.id.iv_convo_status_icon) as ImageView
            holder.linear_layout_item =
                view.findViewById<View>(R.id.linear_layout_item) as LinearLayout


            Log.e("CHATLIST", data[position].first_name)
            // Log.e("Chat_talkJs",talkjsId[position].lastMessage!!.senderId)


            holder!!.tv_convo_name!!.setText(data[position].first_name)

            if (data[position].photo1_approve == "UNAPPROVED") {
                Picasso.with(context)
                    .load(photobaseurl + "0")
                    .error(R.drawable.ic_man_user)
                    .resize(50, 50)
                    .centerCrop()
                    .into(holder.iv_convo_user_image)
            } else {
                Picasso.with(context)
                    .load(photobaseurl + data[position].photo1)
                    .error(R.drawable.ic_man_user)
                    .into(holder.iv_convo_user_image)
            }


            view.tag = holder

        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        Log.e("@@0", talkjsId.size.toString())
        if (talkjsId.size.toString().equals("0")) {
            Log.e("@@22", talkjsId.size.toString())

        } else {
            Log.e(
                "@@33",
                talkjsId.size.toString() + "---" + talkjsId.keys.toString() + "---" + talkjsId.values
            )


            if (talkjsId.get(data.get(position).id).equals(data.get(position).id)) {
                Log.e(
                    "@@talk",
                    talkjsId.size.toString() + "---" + talkjsId.keys.toString() + "---" + talkjsId.values
                )
                holder.linear_layout_item!!.setBackgroundColor(Color.parseColor("#DEE7DD"));
            } else {
                holder.linear_layout_item!!.setBackgroundColor(Color.parseColor("#F3F1F1"));
            }

            /*if (talkjsId.get(position).equals(data.get(position).id.toString())) {
                holder.linear_layout_item!!.setBackgroundColor(Color.parseColor("#DEE7DD"));
            } else {
                holder.linear_layout_item!!.setBackgroundColor(Color.parseColor("#F3F1F1"));
            }*/
        }


        holder.linear_layout_item!!.setOnClickListener {

            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.linear_layout_item!!.setBackgroundColor(
                    context.resources.getColor(
                        R.color.notification_list_item_selected,
                        context.theme
                    )
                )*/
            holder.linear_layout_item!!.setBackgroundColor(Color.parseColor("#F3F1F1"));
            if (data[position].photo1 == null) {
                photo = "0"

            } else {
                photo = data[position].photo1!!
            }
            Log.e(
                "CHATLIST",
                data[position].id + "" + data[position].first_name + "" + data[position].email + "" + photo
            )
           /* val intent = Intent(context, ChatwebActivity::class.java)
            intent.putExtra("tuser_id", data[position].id)
            intent.putExtra("tusername", data[position].first_name)
            intent.putExtra("tuseremail", data[position].email)
            intent.putExtra("tphoto1", photo)
            context.startActivity(intent)*/


            /* } else {
                 holder.linear_layout_item!!.setBackgroundColor(context.resources.getColor(R.color.notification_list_item_selected))
             }*/

            /* val intent = Intent(context,)
             context.startActivity(intent)*/

        }


        /* if (position % 2 == 1) {
             // Log.d("ConversationsAdapter", "position if")
             holder.iv_convo_status_icon!!.background = context.getDrawable(R.drawable.dot_green)
         } else {
             //Log.d("ConversationsAdapter", "position else")
             holder.iv_convo_status_icon!!.background = context.getDrawable(R.drawable.dot_grey)
         }*/

        // holder.iv_convo_user_image!!.background = context.getDrawable(R.drawable.del_fb_ic)

        return view
    }
}

