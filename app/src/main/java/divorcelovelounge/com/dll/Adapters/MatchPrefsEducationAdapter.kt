package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.RegisterProfileSetupAnswer
import divorcelovelounge.com.dll.R

//RAVITEJA
class MatchPrefsEducationAdapter(context: Context, list: ArrayList<RegisterProfileSetupAnswer>) :
    RecyclerView.Adapter<MatchPrefsEducationAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<RegisterProfileSetupAnswer> = ArrayList()
    private var mSelectedList: ArrayList<String> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MatchPrefsEducationAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_quiz, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: MatchPrefsEducationAdapter.ViewHolder, position: Int) {

        val answer = mList[position]
        val name = answer.answer
        if (mSelectedList.isNotEmpty()) {
            if (mSelectedList.contains(name)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name, mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                        mContext!!.resources.getColor(R.color.white_color,mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.selected_bg) , mContext!!.resources.getColor(R.color.gray))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                        , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.bind(
                    name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                    , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
            } else {
                holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tv_answer: TextView = view.findViewById(R.id.tv_name)
        private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_answer_item)


        @SuppressLint("NewApi")
        fun bind(answer: String, bg_drawable: Drawable, text_color:Int) {

            tv_answer.text = answer
            tv_answer.setTextColor(text_color)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ll_answer_item.background = bg_drawable
            } else {
                ll_answer_item.background = bg_drawable
            }

        }

    }


    fun selectedList(selectedList: ArrayList<String>) {
        this.mSelectedList = selectedList
        notifyDataSetChanged()
    }

    fun getSelectedSize(): Int? {
        return mSelectedList.size
    }

    fun getItem(position: Int): String {
        return mList[position].answer
    }
}