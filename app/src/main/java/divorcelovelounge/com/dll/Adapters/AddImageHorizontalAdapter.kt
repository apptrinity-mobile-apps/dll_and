package divorcelovelounge.com.dll.Adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.ApiPojo.AdsListDataResponse
import divorcelovelounge.com.dll.ApiPojo.PhotoBaseUrl
import divorcelovelounge.com.dll.R

class AddImageHorizontalAdapter(private val mContext: Context, private val listItems: ArrayList<AdsListDataResponse>) :
    RecyclerView.Adapter<AddImageHorizontalAdapter.MyCustomViewHolder>() {
    private val filterList: MutableList<AdsListDataResponse>?
    val adsbaseurl = PhotoBaseUrl().BASE_ADS_URL
    private var uri : Uri?=null

    init {
        this.filterList = java.util.ArrayList()
        // we copy the original list to the filter list and use it for setting row values
        this.filterList!!.addAll(this.listItems)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyCustomViewHolder {

        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.addview_item_list, null)
        return MyCustomViewHolder(view)

    }

    override fun getItemCount(): Int {
        return filterList?.size ?: 0
    }


    override fun onBindViewHolder(holder: MyCustomViewHolder, position: Int) {


        if(filterList?.get(position)!!.add_type.equals("Custom")){
            holder.webview_add.visibility = View.GONE
            Picasso.with(mContext)
                .load(adsbaseurl + filterList?.get(position)!!.add_image.toString())
                .error(R.drawable.ic_man_user)
                .into(holder.item_name)
        } else{
            holder.item_name.visibility = View.GONE
               val paylink = filterList?.get(position)!!.add_link.toString()
                if (paylink!!.contains("http") || paylink!!.contains("https")) {
                    uri = Uri.parse(paylink)
                } else {
                    uri = Uri.parse("http://" + paylink)
                }

            holder.webview_add!!.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    view?.loadUrl(url)

                    return true
                }
            }
            holder.webview_add!!.getSettings().setJavaScriptEnabled(true);
            holder.webview_add!!.setHorizontalScrollBarEnabled(false);
            holder.webview_add!!.loadUrl(uri.toString())

        }

        holder.item_name.setOnClickListener {
            mContext.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(filterList?.get(position)!!.add_link.toString())));
        }

    }




     class MyCustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var item_name: ImageView
        var webview_add: WebView
        init {
            this.item_name = view.findViewById<View>(R.id.img_adv_pic_id) as ImageView
            this.webview_add = view.findViewById<View>(R.id.webview_add) as WebView
            /*  this.tvPlace = (TextView) view.findViewById(R.id.tvPlace);
            this.imgThumb = (ImageView) view.findViewById(R.id.imgThumb);*/
        }

    }
}