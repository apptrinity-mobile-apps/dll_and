package divorcelovelounge.com.dll.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import divorcelovelounge.com.dll.Fragments.*

class MessagesAdapter(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return Inbox.newInstance(1)
            }
            1 -> {
                return Sent.newInstance(2)
            }
            2 -> {
                return Importent.newInstance(3)
            }
            3 -> {
                return Trash.newInstance(4)
            }

            else -> return null
        }
    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}