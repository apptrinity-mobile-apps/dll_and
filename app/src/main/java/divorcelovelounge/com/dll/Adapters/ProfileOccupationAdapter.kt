package divorcelovelounge.com.dll.Adapters

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import divorcelovelounge.com.dll.ApiPojo.RegisterProfileSetupAnswer
import divorcelovelounge.com.dll.R

class ProfileOccupationAdapter (private val context: Activity, val data: ArrayList<RegisterProfileSetupAnswer>) :
    BaseAdapter(), Filterable {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var selectedIndex = -1
    var occupationList: ArrayList<RegisterProfileSetupAnswer>? = null
    var occupationFiltered: ArrayList<RegisterProfileSetupAnswer>? = null
    private var mItemFilter : ItemFilter? = null
    
    init {
        this.occupationList = data
        this.occupationFiltered = data
    }
    
    override fun getItem(position: Int): Any {
        return occupationFiltered!![position].answer
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return occupationFiltered!!.size
    }

    fun getSelectedIndex(): Int {
        return selectedIndex
    }

    class ViewHolder {
        internal var tv_text: TextView? = null
        internal var ll_item_list: LinearLayout? = null
    }

    fun setSelectedIndex(ind: Int) {
        selectedIndex = ind
        notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_list_profile_setup, parent, false)
            holder = ViewHolder()
            holder.tv_text = view.findViewById(R.id.item_name)
            holder.ll_item_list = view.findViewById(R.id.ll_item_list)

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }



        if (selectedIndex != -1 && position == selectedIndex) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray))
            }
        }

        holder.tv_text!!.text = occupationFiltered!!.get(position).answer
        return view
    }

    override fun getFilter(): Filter {
        if (mItemFilter == null) {
            mItemFilter = ItemFilter()
        }
        return mItemFilter!!
    }

    // Filter class
    private inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            var filterText = constraint
            //selectedIndex = -1
            val results = Filter.FilterResults()
            if (constraint != null && filterText!!.isNotEmpty()) {
                filterText = filterText.toString().toUpperCase()
                val filteredOccupation = ArrayList<RegisterProfileSetupAnswer>()
                for (i in 0 until occupationList!!.size) {
                    if (occupationList!![i].answer.toUpperCase().contains(filterText)) {
                        filteredOccupation.add(occupationList!![i])
                    }
                }
                results.count = filteredOccupation.size
                results.values = filteredOccupation
            } else {
                results.count = occupationList!!.size
                results.values = occupationList!!
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            occupationFiltered = results.values as ArrayList<RegisterProfileSetupAnswer>
            notifyDataSetChanged()
        }
    }
    
}
