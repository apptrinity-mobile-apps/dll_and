package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.ReligionData
import divorcelovelounge.com.dll.R

//RAVITEJA
class MatchPrefsReligionAdapter(context: Context, list: ArrayList<ReligionData>) :
    RecyclerView.Adapter<MatchPrefsReligionAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<ReligionData> = ArrayList()
    private var mSelectedList: ArrayList<String> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MatchPrefsReligionAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_two_line, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: MatchPrefsReligionAdapter.ViewHolder, position: Int) {

        val religion_name = mList[position].religion_name

        if (mSelectedList.isNotEmpty()) {
            if (mSelectedList.contains(religion_name)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        religion_name, mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                        mContext!!.resources.getColor(R.color.white_color,mContext!!.theme)
                    )
                } else {
                    holder.bind(religion_name, mContext!!.resources.getDrawable(R.drawable.selected_bg) , mContext!!.resources.getColor(R.color.gray))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        religion_name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                        , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
                } else {
                    holder.bind(religion_name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.bind(
                    religion_name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                    , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
            } else {
                holder.bind(religion_name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tv_answer: TextView = view.findViewById(R.id.item_name)
        private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_item_list)


        @SuppressLint("NewApi")
        fun bind(answer: String, bg_drawable: Drawable, text_color:Int) {
            tv_answer.gravity = Gravity.CENTER
            tv_answer.text = answer
            tv_answer.setTextColor(text_color)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ll_answer_item.background = bg_drawable
            } else {
                ll_answer_item.background = bg_drawable
            }

        }

    }


    fun selectedList(selectedList: ArrayList<String>) {
        this.mSelectedList = selectedList
        notifyDataSetChanged()
    }

    fun getSelectedSize(): Int? {
        return mSelectedList.size
    }

    fun getItem(position: Int): String {
        return mList[position].religion_name
    }
}