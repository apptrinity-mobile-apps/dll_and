package divorcelovelounge.com.dll.Adapters

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.QuizAnswerResponse
import divorcelovelounge.com.dll.R


class RatingAdapter(context: Context, list: List<QuizAnswerResponse>) :
    RecyclerView.Adapter<RatingAdapter.ViewHolder>() {

    var mContext: Context? = null
    private var mList: List<QuizAnswerResponse>? = null
var rating =0

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RatingAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_rating, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList!!.size

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onBindViewHolder(holder: RatingAdapter.ViewHolder, position: Int) {

        val answer = mList!![position]
//        holder.setIsRecyclable(false)


        holder.tv_rating.text = answer.answer
        /* holder.slider_rating.min = 0
         holder.slider_rating.max = 10

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             holder.slider_rating.getThumb(0).thumb =
                     mContext!!.resources.getDrawable(R.drawable.seekbar_dot, mContext!!.theme)
         } else {
             holder.slider_rating.getThumb(0).thumb = mContext!!.resources.getDrawable(R.drawable.seekbar_dot)
         }

         holder.slider_rating.setOnThumbValueChangeListener(object : MultiSlider.SimpleChangeListener() {
             override fun onValueChanged(
                 multiSlider: MultiSlider, thumb: MultiSlider.Thumb, thumbIndex: Int, value: Int
             ) {
                 Log.d("RatingAdapter", "value&answer $value $answer")
                 holder.tv_value.text = value.toString()

             }
         })*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.slider_rating.min = 0

            rating = holder.slider_rating.min
        }
        holder.slider_rating.max = 10



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.slider_rating.thumb = mContext!!.resources.getDrawable(R.drawable.seekbar_dot, mContext!!.theme)
        } else {
            holder.slider_rating.thumb = mContext!!.resources.getDrawable(R.drawable.seekbar_dot)
        }

        holder.slider_rating.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                rating = progress
                holder.tv_value.text = rating.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_rating: TextView = view.findViewById(R.id.tv_rating)
        var tv_value: TextView = view.findViewById(R.id.tv_value)
        var slider_rating: SeekBar = view.findViewById(R.id.slider_rating)

    }

    fun getItem(position: Int): Int {
        return rating
    }
}