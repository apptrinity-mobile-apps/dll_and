package divorcelovelounge.com.dll.ViewPagerAdapters


import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.CardView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import divorcelovelounge.com.dll.ApiPojo.CardItemPlan
import divorcelovelounge.com.dll.ApiPojo.Durations_list
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.CustomTextViewLogin
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.MembershipPlanPaymentActivity
import divorcelovelounge.com.dll.R
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

class MembershipPlanAdapterKotlin(private val context: Context) : PagerAdapter(), CardAdapter {

    private val mViews: MutableList<CardView?>
    private val mData: MutableList<CardItemPlan>
    private var mBaseElevation: Float = 0.toFloat()

    lateinit var filter_dialog: Dialog

    lateinit var duration_array: ArrayList<Durations_list>

    lateinit var rv_duration_list_id: RecyclerView
    lateinit var tv_memberdiscount_price: CustomTextViewBold
    lateinit var tv_plan_duration: CustomTextViewBold
    lateinit var tv_membertotal_price: CustomTextViewBold
    lateinit var tv_discount_percent_id: CustomTextViewBold
    lateinit var ll_main_list: LinearLayout
    lateinit var ll_free_id: LinearLayout

    lateinit var img_check_box_free_id: ImageView
    lateinit var img_check_box_id: ImageView


    var duration_id = "Free"

    var details_adapter: DurationListAdapter? = null
    lateinit var  twoDForm: DecimalFormat
    var adapter_amount_final = 0f

    lateinit var session: DllSessionManager
    lateinit var user_details: HashMap<String,String>

    init {
        mData = ArrayList()
        mViews = ArrayList()
        duration_array = ArrayList()
    }

    fun addCardItem(item: CardItemPlan) {
        mViews.add(null)
        mData.add(item)
    }

    override fun getBaseElevation(): Float {
        return mBaseElevation
    }

    override fun getCardViewAt(position: Int): CardView {
        return mViews[position]!!
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.membership_list_item, container, false)
        container.addView(view)
        bind(mData[position], view)
        val cardView = view.findViewById<CardView>(R.id.cardView)

        if (mBaseElevation == 0f) {
            mBaseElevation = cardView.cardElevation
        }

        cardView.maxCardElevation = mBaseElevation * CardAdapter.MAX_ELEVATION_FACTOR
        mViews[position] = cardView


        /*  rv_duration_list_id.addOnItemClickListener(object: OnItemClickListener {
              override fun onItemClicked(position: Int, view: View) {
  // Your logic
              }
          })*/



        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
        mViews.set(position, null)
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun bind(item: CardItemPlan, view: View) {
        session = DllSessionManager(context)
        user_details = session.userDetails

        twoDForm = DecimalFormat("#.##")
        Log.e("DATASIZE", item.package_type.toString())

        val tv_memberplan = view.findViewById<CustomTextViewBold>(R.id.tv_memberplan)
        val tv_member_plus = view.findViewById<CustomTextViewLogin>(R.id.tv_member_plus)
        tv_plan_duration = view.findViewById<CustomTextViewBold>(R.id.tv_plan_duration)
        tv_discount_percent_id = view.findViewById<CustomTextViewBold>(R.id.tv_discount_percent_id)


        val tv_viewed_you_id = view.findViewById<CustomTextView>(R.id.tv_viewed_you_id)
        val tv_favorited_you_id = view.findViewById<CustomTextView>(R.id.tv_favorited_you_id)
        val tv_msg_id = view.findViewById<CustomTextView>(R.id.tv_msg_id)
        val tv_profile_id = view.findViewById<CustomTextView>(R.id.tv_profile_id)
        val tv_intrest_id = view.findViewById<CustomTextView>(R.id.tv_intrest_id)
        val tv_contacts_id = view.findViewById<CustomTextView>(R.id.tv_contacts_id)
        val tv_live_chat_id = view.findViewById<CustomTextView>(R.id.tv_live_chat_id)

        ll_free_id = view.findViewById(R.id.ll_free_id)
        ll_main_list = view.findViewById(R.id.ll_main_list)




        val btn_memnext = view.findViewById<Button>(R.id.btn_memnext)
        /* val face = Typeface.createFromAsset(context.getAssets(), "fonts/Restaurants_Script.ttf")
         tv_member_plus.setTypeface(face)*/


        tv_memberdiscount_price = view.findViewById<CustomTextViewBold>(R.id.tv_memberdiscount_price)
        tv_membertotal_price = view.findViewById<CustomTextViewBold>(R.id.tv_membertotal_price)

        img_check_box_free_id = view.findViewById<ImageView>(R.id.img_check_box_free_id)
        img_check_box_id = view.findViewById<ImageView>(R.id.img_check_box_id)


        val discount_cal =
            (item.plan_amount.toFloat()) - ((item.plan_amount.toFloat()) * (item.discount_amount.toFloat()) / 100)

        Log.e("discount_cal", discount_cal.toString())


        tv_memberplan.setText(item.package_type)
        tv_plan_duration.setText("1 Month")
        tv_membertotal_price.setText("$" + item.plan_amount + " /month")
        tv_memberdiscount_price.setText("$" + java.lang.Double.valueOf(twoDForm.format(discount_cal)).toString() + " /month")
        tv_discount_percent_id.setText("SAVE" + "\n" + item.discount_amount + " %")
        tv_membertotal_price.setPaintFlags(tv_memberplan.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)

        if (item.viewedme.equals("Yes")) {
            tv_viewed_you_id.setText("See Who's viewed you")
        } else {
            tv_viewed_you_id.setText("Can`t see who’s viewed you")
        }

        if (item.favoritedme.equals("Yes")) {
            tv_favorited_you_id.setText("See Who's favorited you")
        } else {
            tv_favorited_you_id.setText("Can`t see who’s favorited you")
        }
        /*tv_msg_id.setText(item.plan_msg + " messaging")
        tv_profile_id.setText("View " + item.profile + " profiles")
        tv_intrest_id.setText("Sent " + item.interest + " interests")
        tv_contacts_id.setText("View " + item.plan_contacts + " contacts")*/

        tv_msg_id.setText("unlimited" + " messaging")
        tv_profile_id.setText("View " + "unlimited" + " profiles")
        tv_intrest_id.setText("Sent " + "unlimited" + " interests")
        tv_contacts_id.setText("View " + "unlimited" + " contacts")

        if (item.chat.equals("Yes")) {
            tv_live_chat_id.setText("Live Chat is available")
        } else {
            tv_live_chat_id.setText("Live Chat not available")
        }

        if (duration_id.equals("Free")) {
            tv_memberdiscount_price.setText("$" + "0" + " /month")
            tv_discount_percent_id.setText("SAVE" + "\n" + "100" + " %")
            tv_membertotal_price.setText("$" + "100" + " /month")

        }


        if (user_details[DllSessionManager.MEMBERSHIP_STATUS]  == "1") {
            ll_free_id.visibility=View.GONE

            duration_id="one"
            img_check_box_id.setImageDrawable(
                context.getResources().getDrawable(
                    R.drawable.ic_verification_mark,
                    context.getTheme()
                )
            )
            img_check_box_free_id.setImageResource(0)


            if (duration_id.equals("one")) {

                tv_memberdiscount_price.setText("$" + java.lang.Double.valueOf(twoDForm.format(discount_cal)).toString() + " /month")
                tv_discount_percent_id.setText("SAVE" + "\n" + item.discount_amount + " %")
                tv_membertotal_price.setText("$" + item.plan_amount + " /month")

            }

        }else{
            ll_free_id.visibility=View.VISIBLE
        }

        ll_free_id.setOnClickListener {
            duration_id = "Free"
            img_check_box_free_id.setImageDrawable(
                context.getResources().getDrawable(
                    R.drawable.ic_verification_mark,
                    context.getTheme()
                )
            )
            img_check_box_id.setImageResource(0)

            tv_memberdiscount_price.setText("$" + "0" + " /month")
            tv_discount_percent_id.setText("SAVE" + "\n" + "100" + " %")
            tv_membertotal_price.setText("$" + "100" + " /month")
            tv_membertotal_price.visibility = View.VISIBLE


            details_adapter!!.notifyDataSetChanged()
        }

        ll_main_list.setOnClickListener {

            duration_id = "one"
            img_check_box_id.setImageDrawable(
                context.getResources().getDrawable(
                    R.drawable.ic_verification_mark,
                    context.getTheme()
                )
            )
            img_check_box_free_id.setImageResource(0)


            // Double.valueOf(twoDForm.format(d));


            tv_memberdiscount_price.setText("$" + java.lang.Double.valueOf(twoDForm.format(discount_cal)).toString() + " /month")
            tv_discount_percent_id.setText("SAVE" + "\n" + item.discount_amount + " %")
            tv_membertotal_price.setText("$" + item.plan_amount + " /month")

            if(item.discount_amount.equals("0")){
                tv_membertotal_price.visibility = View.GONE
            }else{
                tv_membertotal_price.visibility = View.VISIBLE
            }



            details_adapter!!.notifyDataSetChanged()
        }


        filter_dialog = Dialog(context)
        filter_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        filter_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        filter_dialog.setContentView(R.layout.plan_duration_dialog)
        val window_login = filter_dialog.window
        window_login!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val close_login_id = filter_dialog.findViewById(R.id.close_login_id) as ImageView
        close_login_id.setOnClickListener {
            filter_dialog.dismiss()
        }
        // rv_duration_list_id = filter_dialog.findViewById(R.id.rv_duration_list_id) as RecyclerView //dialog recyclerview working
        rv_duration_list_id = view.findViewById(R.id.rv_duration_list_id) as RecyclerView


        btn_memnext.setOnClickListener {


            //  Log.e("duration_array_kkk",duration_array.size.toString()+"---"+duration_array.get(0).duration_id)

            val discount_cal_1 =
                (item.plan_amount.toFloat()) - ((item.plan_amount.toFloat()) * (item.discount_amount.toFloat()) / 100)
            val intent = Intent(context, MembershipPlanPaymentActivity::class.java)
            intent.putExtra("planid", item.id)
            intent.putExtra("plan_name", item.package_type)
            intent.putExtra("monthly_with_out_discount", item.plan_amount)
            intent.putExtra("monthly_with_discount", discount_cal_1.toString())
            intent.putExtra("plan_discount", item.discount_amount)
            intent.putExtra("duration_id", duration_id)
            //intent.putCharSequenceArrayListExtra("duration_array", duration_array)
            /* val bundle = Bundle()
             bundle.putParcelableArrayList("duration_array", duration_array)*/
            intent.putExtra("duration_array", duration_array);


            /* val args = Bundle()
             args.putSerializable("duration_array", duration_array)
             intent.putExtra("BUNDLE",args)*/

            context.startActivity(intent)
        }



        tv_plan_duration.setOnClickListener {


            filter_dialog.show()


        }

        // duration_array.clear()
        Log.e("size_array", view.id.toString() + "==" + item.durations.size)

        for (k in 0 until item.durations!!.size) {

            duration_array.add(item.durations.get(k))

            Log.e(
                "duration_array",
                duration_array.toString() + "---" + item.durations.get(k).month + "---" + item.durations.get(k).duration_id
            )

        }


        Log.e("duration_array_ll", duration_array.toString() + "---" + item.durations.size + " Size")
        rv_duration_list_id.layoutManager = LinearLayoutManager(context)
        rv_duration_list_id.itemAnimator = DefaultItemAnimator()
        rv_duration_list_id.hasFixedSize()
        details_adapter = DurationListAdapter(duration_array, context)
        details_adapter!!.setHasStableIds(true)
        rv_duration_list_id.adapter = details_adapter
        details_adapter!!.notifyDataSetChanged()


        /*rv_duration_list_id.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                rv_duration_list_id,
                object : RecyclerTouchListener.ClickListener {
                     fun onLongClick(view: View?, position: Int) {
                    }

                    override fun onClick(view: View, position: Int) {
                        val movie = duration_array.get(position)
                        Toast.makeText(context, movie.month + " is selected!", Toast.LENGTH_SHORT)
                            .show()
                    }


                })
        )*/

    }


    /*inner class PlansListAdapter(val mServiceList: ArrayList<Durations_list>, val context: Context) :
        RecyclerView.Adapter<ViewHolder1>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.duration_list_item, parent, false))

        }

        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10

            holder.tv_list_id.text = mServiceList.get(position).month + " Months"
            holder.tv_list_id.setOnClickListener {
                Log.e("position", "" + position)


                duration_id_stg = mServiceList.get(position).duration_id
                tv_plan_duration.text = mServiceList.get(position).month + " Months"



                if (mServiceList.get(position).discount.equals("0")) {
                    tv_membertotal_price.text ="$" + mServiceList.get(position).amount + " /month"
                    tv_memberdiscount_price.text ="$" + mServiceList.get(position).amount + " /month"
                } else {
                    val final_amount =
                        mServiceList.get(position).amount.toFloat() * mServiceList.get(position).discount.toFloat()

                    tv_membertotal_price.text = "$" +mServiceList.get(position).amount + " /month"
                    tv_memberdiscount_price.text =
                        "$" +(Math.round((mServiceList.get(position).amount.toFloat() - final_amount) * 100) / 100).toString() + " /month"

                }

                tv_discount_percent_id.text = "SAVE" + "\n" + mServiceList.get(position).discount + "%"

                notifyDataSetChanged()
                filter_dialog.dismiss()
            }


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        //val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
        val tv_list_id = view.findViewById<CustomTextView>(R.id.tv_list_id)

    }*/


    inner class DurationListAdapter(val mServiceList: ArrayList<Durations_list>, val context: Context) :
        RecyclerView.Adapter<ViewHolder1>() {
        var index = -1

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.duration_check_box_item, parent, false))

        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            //  val imagePos = position %10
            holder.ll_duration_list_id.setOnClickListener {
                // index = position
                notifyDataSetChanged()
                duration_id = mServiceList.get(position).duration_id
                // tv_main_amount_id.visibility=View.INVISIBLE
            }
            /*if (index == position) {
                holder.img_main_id.setImageDrawable(
                    getResources().getDrawable(
                        R.drawable.ic_verification_mark,
                        getApplicationContext().getTheme()
                    )
                )


            } else {
                holder.img_main_id.setImageResource(0)
            }*/

            if (duration_id.equals(mServiceList.get(position).duration_id)) {

                adapter_amount_final =
                    mServiceList.get(position).amount.toFloat() - (mServiceList.get(position).amount.toFloat() * mServiceList.get(
                        position
                    ).discount.toFloat() / 100)

                holder.img_main_id.setImageDrawable(
                    context.getResources().getDrawable(
                        R.drawable.ic_verification_mark,
                        context.getTheme()
                    )
                )

                img_check_box_id.setImageResource(0)
                img_check_box_free_id.setImageResource(0)

                tv_membertotal_price.setText("$ " + mServiceList.get(position).amount + " /month")
                // tv_memberdiscount_price.setText(mServiceList.get(position).discount)


                if(mServiceList.get(position).discount.equals("0")){
                    tv_membertotal_price.visibility = View.GONE
                }else{
                    tv_membertotal_price.visibility = View.VISIBLE
                }

                tv_discount_percent_id.text = "SAVE" + "\n" + mServiceList.get(position).discount + "%"
                tv_memberdiscount_price.setText("$ " + java.lang.Double.valueOf(twoDForm.format(adapter_amount_final)).toString().toString() + " /month")
                // holder.tv_main_after_discount_id.visibility=View.GONE
                //tv_main_amount_id.visibility=View.INVISIBLE

            } else {
                holder.img_main_id.setImageResource(0)

                //  holder.tv_main_after_discount_id.visibility=View.INVISIBLE
            }

            holder.duration_txt_id.setText(mServiceList.get(position).month + " Months")


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }


    }

    class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        //val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
        val ll_duration_list_id = view.findViewById<LinearLayout>(R.id.ll_duration_list_id)
        val duration_txt_id = view.findViewById<CustomTextView>(R.id.duration_txt_id)
        val img_main_id = view.findViewById<ImageView>(R.id.img_main_id)
        val tv_main_after_discount_id = view.findViewById<CustomTextView>(R.id.tv_main_after_discount_id)

    }
}


