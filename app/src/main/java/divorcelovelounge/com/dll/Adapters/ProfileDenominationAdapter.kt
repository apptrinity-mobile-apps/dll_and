package divorcelovelounge.com.dll.Adapters

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import divorcelovelounge.com.dll.ApiPojo.CasteData
import divorcelovelounge.com.dll.R

class ProfileDenominationAdapter (private val context: Activity, private val data: ArrayList<String>) :
    BaseAdapter(), Filterable {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var selectedIndex = -1
    var casteData: ArrayList<String>? = null
    var casteFilterDataList: ArrayList<String>? = null
    private var mItemFilter: ItemFilter? = null

    init {
        this.casteData = data
        this.casteFilterDataList = data
    }

    override fun getItem(position: Int): Any {
        return casteFilterDataList!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return casteFilterDataList!!.size
    }

    class ViewHolder {
        internal var tv_text: TextView? = null
        internal var ll_item_list: LinearLayout? = null
    }

    fun setSelectedIndex(ind: Int) {
        selectedIndex = ind
        notifyDataSetChanged()
    }

    fun getSelectedIndex(): Int {
        return selectedIndex
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_list_two_line, parent, false)
            holder = ViewHolder()
            holder.tv_text = view.findViewById(R.id.item_name)
            holder.ll_item_list = view.findViewById(R.id.ll_item_list)

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        if (selectedIndex != -1 && position == selectedIndex) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray))
            }
        }
        holder.tv_text!!.gravity = Gravity.CENTER
        holder.tv_text!!.text = casteFilterDataList!![position]
        return view
    }

    override fun getFilter(): Filter {
        if (mItemFilter == null) {
            mItemFilter = ItemFilter()
        }
        return mItemFilter!!
    }

    // Filter class
    private inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            var filterText = constraint
            selectedIndex = -1
            val results = Filter.FilterResults()
            if (filterText != null && filterText.isNotEmpty()) {
                filterText = filterText.toString().toLowerCase()
                val filteredCities = ArrayList<String>()
                for (i in 0 until casteData!!.size) {
                    if (casteData!![i].toLowerCase().contains(filterText)) {
                        filteredCities.add(casteData!![i])
                    }
                }
                results.count = filteredCities.size
                results.values = filteredCities
            } else {
                results.count = casteData!!.size
                results.values = casteData!!
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {

            casteFilterDataList = results.values as ArrayList<String>
            notifyDataSetChanged()

        }
    }

}