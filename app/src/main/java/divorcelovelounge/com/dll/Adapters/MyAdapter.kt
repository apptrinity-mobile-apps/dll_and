package divorcelovelounge.com.dll.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import divorcelovelounge.com.dll.Fragments.*

class MyAdapter(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return MyFavourite.newInstance(1)
            }
            1 -> {
                return FavouriteMe.newInstance(2)
            }
            2 -> {
                return MyInterest.newInstance(3)
            }
            3 -> {
                return InterestMe()
            }
            4 -> {
                return ViewedMeFragment()
            }
            else -> return null
        }
    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}