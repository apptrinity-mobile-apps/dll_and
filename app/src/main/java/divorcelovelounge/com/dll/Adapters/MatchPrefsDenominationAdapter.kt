package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.R

class MatchPrefsDenominationAdapter (context: Context, list: ArrayList<String>) :
    RecyclerView.Adapter<MatchPrefsDenominationAdapter.ViewHolder>(),Filterable {

    private var mContext: Context? = null
    private var mList: ArrayList<String> = ArrayList()
    private var mFilteredList: ArrayList<String> = ArrayList()
    private var mSelectedList: ArrayList<String> = ArrayList()
    private var mItemFilter : ItemFilter? = null

    init {
        this.mContext = context
        this.mList = list
        this.mFilteredList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MatchPrefsDenominationAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_two_line, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mFilteredList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: MatchPrefsDenominationAdapter.ViewHolder, position: Int) {

        val answer = mFilteredList[position]
        val name = answer
        if (mSelectedList.isNotEmpty()) {
            if (mSelectedList.contains(name)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name, mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                        mContext!!.resources.getColor(R.color.white_color,mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.selected_bg) , mContext!!.resources.getColor(R.color.gray))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                        , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.bind(
                    name, mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                    , mContext!!.resources.getColor(R.color.gray,mContext!!.theme))
            } else {
                holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edittext_bg), mContext!!.resources.getColor(R.color.gray))
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tv_answer: TextView = view.findViewById(R.id.item_name)
        private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_item_list)


        @SuppressLint("NewApi")
        fun bind(answer: String, bg_drawable: Drawable, text_color:Int) {
            tv_answer.gravity = Gravity.CENTER
            tv_answer.text = answer
            tv_answer.setTextColor(text_color)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ll_answer_item.background = bg_drawable
            } else {
                ll_answer_item.background = bg_drawable
            }

        }

    }

    fun selectedList(selectedList: ArrayList<String>) {
        this.mSelectedList = selectedList
        notifyDataSetChanged()
    }

    fun getSelectedSize(): Int? {
        return mSelectedList.size
    }

    fun selectAll(){
        mSelectedList.clear()
        mSelectedList.addAll(mList)
        notifyDataSetChanged()
    }

    fun clearAll(){
        mSelectedList.clear()
        notifyDataSetChanged()
    }

    fun getItem(position: Int): String {
        return mFilteredList[position]
    }

    fun getSize():Int?{
        return mFilteredList.size
    }

    // Filter
    override fun getFilter(): Filter {
        if (mItemFilter == null) {
            mItemFilter = ItemFilter()
        }
        return mItemFilter!!
    }

    // Filter class
     inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            var filterText = constraint
          //  selectedIndex = -1
            val results = Filter.FilterResults()
            if (constraint != null && filterText!!.isNotEmpty()) {
                filterText = filterText.toString().toUpperCase()
                val filteredOccupation = ArrayList<String>()
                for (i in 0 until mList.size) {
                    if (mList[i].toUpperCase().contains(filterText)) {
                        filteredOccupation.add(mList[i])
                    }
                }
                results.count = filteredOccupation.size
                results.values = filteredOccupation
            } else {
                results.count = mList.size
                results.values = mList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            mFilteredList = results.values as ArrayList<String>
            notifyDataSetChanged()
        }

    }
}