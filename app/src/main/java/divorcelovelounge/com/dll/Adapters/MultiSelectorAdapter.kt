package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.QuizAnswerResponse
import divorcelovelounge.com.dll.R


class MultiSelectorAdapter(context: Context, list: ArrayList<QuizAnswerResponse>) :
    RecyclerView.Adapter<MultiSelectorAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<QuizAnswerResponse> = ArrayList()
    private var mAnswersList: ArrayList<QuizAnswerResponse> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MultiSelectorAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_quiz, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    override fun onBindViewHolder(holder: MultiSelectorAdapter.ViewHolder, position: Int) {

        val answer = mList[position]
        if (mAnswersList.isNotEmpty()) {
            if (mAnswersList.contains(answer)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        answer.answer.toString(),
                        mContext!!.resources.getDrawable(R.drawable.selected_bg, mContext!!.theme),
                        mContext!!.resources.getColor(R.color.white_color, mContext!!.theme)
                    )
                } else {
                    holder.bind(
                        answer.answer.toString(),
                        mContext!!.resources.getDrawable(R.drawable.selected_bg),
                        mContext!!.resources.getColor(R.color.gray)
                    )
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        answer.answer.toString(),
                        mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                        , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                    )
                } else {
                    holder.bind(
                        answer.answer.toString(),
                        mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                        mContext!!.resources.getColor(R.color.gray)
                    )

                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.bind(
                    answer.answer.toString(),
                    mContext!!.resources.getDrawable(R.drawable.edittext_bg, mContext!!.theme)
                    , mContext!!.resources.getColor(R.color.gray, mContext!!.theme)
                )
            } else {
                holder.bind(
                    answer.answer.toString(),
                    mContext!!.resources.getDrawable(R.drawable.edittext_bg),
                    mContext!!.resources.getColor(R.color.gray)
                )

            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tv_answer: TextView = view.findViewById(R.id.tv_name)
        private var ll_answer_item: LinearLayout = view.findViewById(R.id.ll_answer_item)

        @SuppressLint("NewApi")
        fun bind(answer: String, bg_drawable: Drawable, text_color: Int) {

            tv_answer.text = answer
            tv_answer.setTextColor(text_color)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ll_answer_item.background = bg_drawable
            } else {
                ll_answer_item.background = bg_drawable
            }

        }

    }

    fun selectedAnswers(answersList: ArrayList<QuizAnswerResponse>) {
        this.mAnswersList = answersList
        notifyDataSetChanged()
    }

    fun clear() {
        this.mAnswersList = ArrayList()
    }

    fun getItem(position: Int): QuizAnswerResponse {
        return mList[position]
    }
}
