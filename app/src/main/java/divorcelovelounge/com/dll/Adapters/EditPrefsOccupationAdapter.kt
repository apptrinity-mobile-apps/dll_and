package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.CasteData
import divorcelovelounge.com.dll.ApiPojo.RegisterProfileSetupAnswer
import divorcelovelounge.com.dll.R

//RAVITEJA
class EditPrefsOccupationAdapter(context: Context, list: ArrayList<RegisterProfileSetupAnswer>) :
    RecyclerView.Adapter<EditPrefsOccupationAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<RegisterProfileSetupAnswer> = ArrayList()
    private var mSelectedList: ArrayList<String> = ArrayList()
    private var selectedIndex = -1

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): EditPrefsOccupationAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycle_edit_prefs, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: EditPrefsOccupationAdapter.ViewHolder, position: Int) {

        val answer = mList[position]
        val name = answer.answer
        if (mSelectedList.isNotEmpty()) {
            if (mSelectedList.contains(name)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name,
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_selected_bg, mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_selected_bg))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name,
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_default_bg, mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_default_bg))
                }
            }
        } else {
            if (selectedIndex != -1 && position == selectedIndex) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name,
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_selected_bg, mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_selected_bg))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.bind(
                        name,
                        mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_default_bg, mContext!!.theme)
                    )
                } else {
                    holder.bind(name, mContext!!.resources.getDrawable(R.drawable.edit_prefs_list_default_bg))
                }
            }

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tv_edit_prefs_name: TextView = view.findViewById(R.id.tv_edit_prefs_name)
        private var ll_edit_prefs_item: LinearLayout = view.findViewById(R.id.ll_edit_prefs_item)


        @SuppressLint("NewApi")
        fun bind(answer: String, bg_color: Drawable) {

            tv_edit_prefs_name.text = answer
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ll_edit_prefs_item.background = bg_color
            } else {
                ll_edit_prefs_item.background = bg_color
            }

        }

    }

    fun selectedList(selectedList: ArrayList<String>) {
        this.mSelectedList = selectedList
        notifyDataSetChanged()
    }

    fun getSelectedSize(): Int? {
        return mSelectedList.size
    }

    fun getItem(position: Int): String {
        return mList[position].answer
    }

    fun setSelectedIndex(ind: Int) {
        selectedIndex = ind
        notifyDataSetChanged()
    }

    fun getSelectedIndex(): Int {
        return selectedIndex
    }
}

