package divorcelovelounge.com.dll.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.R
import divorcelovelounge.com.dll.Utils.ImageLoader

class NotificationAdapter(private val context: Activity/*, private val data: ArrayList<MyNotificationsPojo>*/) :
    BaseAdapter() {

    private val imageLoader: ImageLoader
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
        imageLoader = ImageLoader(context)
    }

    override fun getCount(): Int {
        return /*data.size*/ 10
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var iv_notify_user_image: ImageView? = null
        internal var iv_notify_fav_icon: ImageView? = null
        internal var tv_notify_name: CustomTextViewBold? = null
        internal var tv_notify_data: CustomTextView? = null
        internal var tv_notify_location: CustomTextView? = null

    }

    @SuppressLint("SetTextI18n", "NewApi")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.notifications_item_list, parent, false)
            holder = ViewHolder()
            holder.tv_notify_name = view.findViewById<View>(R.id.tv_notify_name) as CustomTextViewBold
            holder.tv_notify_data = view.findViewById<View>(R.id.tv_notify_data) as CustomTextView
            holder.tv_notify_location = view.findViewById<View>(R.id.tv_notify_location) as CustomTextView
            holder.iv_notify_fav_icon = view.findViewById<View>(R.id.iv_notify_fav_icon) as ImageView
            holder.iv_notify_user_image = view.findViewById<View>(R.id.iv_notify_user_image) as ImageView

            view.tag = holder

        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

       /* holder.tv_notify_name!!.text = data[position].getName()
        holder.tv_notify_data!!.text = data[position].getData()
        holder.tv_notify_location!!.text = "from " + data[position].getLocation()

        imageLoader.DisplayImage(data[position].getPicture().toString(), holder.iv_notify_user_image!!)

        if (data[position].getLiked_status().toString().equals("interested") || data[position].getLiked_status().toString().equals("super liked")
        ) {
            holder.iv_notify_fav_icon!!.setImageResource(R.drawable.star)
        } else {
            holder.iv_notify_fav_icon!!.setImageResource(0)
        }*/
      //  holder.iv_notify_pic!!.setImageResource(R.drawable.del_fb_ic)

        // TODO delete after testing
        if (position % 2 == 1) {
           // Log.d("NotificationAdapter","position if")
            holder.iv_notify_fav_icon!!.background = context.getDrawable(android.R.drawable.star_big_on)
        } else {
            //Log.d("NotificationAdapter","position else")
            holder.iv_notify_fav_icon!!.alpha = 0f
        }

        holder.iv_notify_user_image!!.background = context.getDrawable(R.drawable.del_fb_ic)

        return view
    }
}