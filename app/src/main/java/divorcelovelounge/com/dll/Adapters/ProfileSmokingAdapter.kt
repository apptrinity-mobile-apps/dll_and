package divorcelovelounge.com.dll.Adapters

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import divorcelovelounge.com.dll.ApiPojo.RegisterProfileSetupAnswer
import divorcelovelounge.com.dll.R

class ProfileSmokingAdapter (private val context: Activity, private val data: ArrayList<RegisterProfileSetupAnswer>) :
    BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var selectedIndex = -1

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }

    class ViewHolder {
        internal var tv_text: TextView? = null
        internal var ll_item_list: LinearLayout? = null
    }

    fun setSelectedIndex(ind: Int) {
        selectedIndex = ind
        notifyDataSetChanged()
    }

    fun getSelectedIndex(): Int {
        return selectedIndex
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_list_profile_setup, parent, false)
            holder = ViewHolder()
            holder.tv_text = view.findViewById(R.id.item_name)
            holder.ll_item_list = view.findViewById(R.id.ll_item_list)

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }


        if (selectedIndex != -1 && position == selectedIndex) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.selected_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.white_color))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg, context.theme)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray,context.theme))
            } else {
                holder.ll_item_list!!.background = context.resources.getDrawable(R.drawable.edittext_bg)
                holder.tv_text!!.setTextColor(context.resources.getColor(R.color.gray))
            }
        }

        holder.tv_text!!.text = data.get(position).answer
        return view
    }


}