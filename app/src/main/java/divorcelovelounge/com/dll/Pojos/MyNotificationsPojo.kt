package divorcelovelounge.com.dll.Pojos

class MyNotificationsPojo {

    private var name: String? = null
    private var location: String? = null
    private var data: String? = null
    private var picture: String? = null
    private var liked_status: String? = null

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getLocation(): String? {
        return location
    }

    fun setLocation(location: String) {
        this.location = location
    }

    fun getData(): String? {
        return data
    }

    fun setData(data: String) {
        this.data = data
    }

    fun getPicture(): String? {
        return picture
    }

    fun setPicture(picture: String) {
        this.picture = picture
    }

    fun getLiked_status(): String? {
        return liked_status
    }

    fun setLiked_status(liked_status: String) {
        this.liked_status = liked_status
    }

}