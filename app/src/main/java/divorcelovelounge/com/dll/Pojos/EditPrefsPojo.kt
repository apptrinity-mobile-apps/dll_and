package divorcelovelounge.com.dll.Pojos

class EditPrefsPojo(private var name: String?, private var image: Int?) {

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getImage(): Int? {
        return image
    }

    fun setImage(image: Int) {
        this.image = image
    }
}