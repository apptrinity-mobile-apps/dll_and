package divorcelovelounge.com.dll.MultiSelectSpinnerLib;

import java.util.List;


public interface SpinnerListener {
    void onItemsSelected(List<KeyPairBoolData> items);
}
