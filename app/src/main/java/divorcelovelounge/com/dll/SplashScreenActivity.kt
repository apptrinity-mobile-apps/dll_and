package divorcelovelounge.com.dll

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ProgressBar
import divorcelovelounge.com.dll.ApiPojo.ApiInterface
import divorcelovelounge.com.dll.ApiPojo.ProfilePhotosResponse
import divorcelovelounge.com.dll.Helper.DllSessionManager
import retrofit2.Call
import retrofit2.Callback
import java.util.HashMap


class SplashScreenActivity : AppCompatActivity() {
    private var splashLoaded = false

    lateinit var dllSessionManager: DllSessionManager
    lateinit var progress_bar_login: ProgressBar
    lateinit var user_details: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        scheduleSplashScreen()
    }

    private fun scheduleSplashScreen() {
        if (!splashLoaded) {



            dllSessionManager = DllSessionManager(this@SplashScreenActivity)
            user_details = dllSessionManager.userDetails
            if (dllSessionManager.isLoggedIn) {

                if(user_details.get(DllSessionManager.PHOTO1).equals("0")){
                    val home_intent = Intent(this@SplashScreenActivity, ProfilePhotos::class.java)
                    //home_intent.putExtra("from_list", "register_list")
                    startActivity(home_intent)
                    finish()
                }else {
                    ProfilePhotosAPI()

                }

            }else{
                val secondsDelayed = 2
                Handler().postDelayed({
                    startActivity(Intent(this, RegisterLoginActivity::class.java))
                    finish()
                }, (secondsDelayed * 1000).toLong())

                splashLoaded = true
            }


        } else {
            val goToMainActivity = Intent(this, IntroductionActivity::class.java)
            goToMainActivity.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(goToMainActivity)
            finish()
        }
    }
    fun ProfilePhotosAPI() {
        val apiService = ApiInterface.create()
        val call = apiService.getProfilePhotosApi(user_details.get(DllSessionManager.USER_ID).toString())
        call.enqueue(object : Callback<ProfilePhotosResponse> {
            override fun onResponse(
                call: Call<ProfilePhotosResponse>,
                response: retrofit2.Response<ProfilePhotosResponse>?
            ) {

                //  if (response != null) {
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                    Log.e("data_2",response!!.body()!!.photos!!.photo1+"---"+response!!.body()!!.photos!!.photo1_approve)

                    if(response!!.body()!!.photos!!.photo1_approve.equals("APPROVED")){

                        //user_details.get(DllSessionManager.PHOTO1).set

                        dllSessionManager.PhotoApprove(response!!.body()!!.photos!!.photo1_approve)


                    }

                    val home_intent = Intent(this@SplashScreenActivity, DashboardActivity::class.java)
                    home_intent.putExtra("from_list", "register_list")
                    startActivity(home_intent)
                    finish()




                }

            }

            override fun onFailure(call: Call<ProfilePhotosResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

}
