package divorcelovelounge.com.dll

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.CustomTextViewLogin
import divorcelovelounge.com.dll.Helper.DllSessionManager

class ProfileSetupWelcome : AppCompatActivity() {

    lateinit var ll_next_id: LinearLayout
    lateinit var img_profile_id: ImageView
    lateinit var tv_user_name: CustomTextViewLogin
    var user_name = ""

    lateinit var dllsesstionManager: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    lateinit var ll_skip:LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_setup_welcome)

        img_profile_id = findViewById(R.id.img_profile_id)
        ll_skip = findViewById(R.id.ll_skip)
        tv_user_name = findViewById(R.id.tv_user_name)

        dllsesstionManager = DllSessionManager(this)
        user_details = dllsesstionManager.userDetails

        Log.e(
            "gender_id",
            user_details.get(DllSessionManager.USER_GENDER) + "------" + user_details[DllSessionManager.USER_NAME]
        )

        if (user_details.get(DllSessionManager.USER_GENDER).equals("Man") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "man"
            ) || user_details.get(DllSessionManager.USER_GENDER).equals("Men") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "men"
            ) || user_details.get(DllSessionManager.USER_GENDER).equals("MAN") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "MEN"
            )
        ) {
            img_profile_id.setImageResource(R.drawable.profile_men)
        } else if (user_details.get(DllSessionManager.USER_GENDER).equals("woman") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "Woman"
            ) || user_details.get(DllSessionManager.USER_GENDER).equals("women") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "Women"
            ) || user_details.get(DllSessionManager.USER_GENDER).equals("WOMAN") || user_details.get(DllSessionManager.USER_GENDER).equals(
                "WOMEN"
            )
        ) {
            img_profile_id.setImageResource(R.drawable.profile_women)
        }else{
            img_profile_id.setImageResource(R.drawable.matches_img)
        }

        if (user_details[DllSessionManager.USER_NAME].equals(null)) {
            user_name = ""
        } else {
            user_name = user_details[DllSessionManager.USER_NAME].toString()
        }

        tv_user_name.text = "Hi $user_name , Nice to meet you"

        ll_next_id = findViewById(R.id.ll_next_id)
        ll_next_id.setOnClickListener {
            val intent = Intent(this@ProfileSetupWelcome, ProfileSetupOne::class.java)

            startActivity(intent)
        }

        ll_skip.setOnClickListener {
            val builder = AlertDialog.Builder(this@ProfileSetupWelcome)
            builder.setTitle("Warning!")
                .setMessage("Do you wish to SKIP?")
                .setCancelable(false)
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {
                        dialog!!.dismiss()
                    }

                })
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, id: Int) {

                        var photo1 = user_details.get(DllSessionManager.PHOTO1)
                        if(photo1.equals("0")){
                            val home_intent = Intent(this@ProfileSetupWelcome, ProfilePhotos::class.java)
                            //home_intent.putExtra("from_list","match_setup")
                            startActivity(home_intent)
                            finish()
                        }else {

                            val home_intent = Intent(this@ProfileSetupWelcome, DashboardActivity::class.java)
                            home_intent.putExtra("from_list", "match_setup")
                            startActivity(home_intent)
                            finish()
                        }
                    }

                })

            val alertDialog = builder.create()
            alertDialog.show()

        }

    }


}
