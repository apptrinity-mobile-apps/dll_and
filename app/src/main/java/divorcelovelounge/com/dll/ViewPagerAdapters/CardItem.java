package divorcelovelounge.com.dll.ViewPagerAdapters;


public class CardItem {

    private String id;
    private String date_id;
    private String gender;
    private String looking_for;
    private String first_name;
    private String username;
    private String email;
    private String zipcode;
    private String state;
    private String referal_from;
    private String children;
    private String city;
    private String dob;
    private String ethnicity;
    private String religion;
    private String denomination;
    private String education;
    private String occupation;
    private String income;
    private String smoke;
    private String drink;
    private String height_ft;
    private String height_inc;
    private String height_cms;
    private String passionate;
    private String leisure_time;
    private String thankful_1;
    private String thankful_2;
    private String thankful_3;
    private String partner_age_min;
    private String partner_age_max;
    private String partner_match_distance;
    private String partner_match_search;
    private String partner_ethnicity;
    private String partner_religion;
    private String partner_denomination;
    private String partner_education;
    private String partner_occupation;
    private String partner_smoke;
    private String partner_drink;
    private String photo1;
    private String photo1_approve;
    private String photo2;
    private String photo2_approve;
    private String photo3;
    private String photo3_approve;
    private String photo4;
    private String photo4_approve;
    private String photo5;
    private String photo5_approve;
    private String photo6;
    private String photo6_approve;
    private String photo7;
    private String photo7_approve;
    private String photo8;
    private String photo8_approve;
    private String photo9;
    private String photo9_approve;
    private String photo10;
    private String photo10_approve;
    private String status;
    private String signup_step;
    private String fav_id;
    private String fav_from;
    private String fav_to;
    private String interest_from;
    private String interest_to;
    private String interest_status;
    private String plan_status;
    private String view_type;
/*  "add_id": "2",
            "client_name": "4",
            "country_name": "India",
            "state_name": "Telangana",
            "page_in": "Home",
            "add_image": "757891_Home_850X1430.jpg",
            "add_link": "http://www.apptrinity.com",
            "view_type": "add",
            "add_type": "Custom",
            "status": "APPROVED"*/
    private  String add_id;
    private  String client_name;
    private  String country_name;
    private  String page_in;
    private  String add_image;
    private  String add_link;


    public CardItem(String id, String date_id, String gender, String looking_for, String first_name, String username, String email, String zipcode, String state, String referal_from, String children, String city, String dob, String ethnicity, String religion, String denomination, String education, String occupation, String income, String smoke, String drink, String height_ft, String height_inc, String height_cms, String passionate, String leisure_time, String thankful_1, String thankful_2, String thankful_3, String partner_age_min, String partner_age_max, String partner_match_distance, String partner_match_search, String partner_ethnicity, String partner_religion, String partner_denomination, String partner_education, String partner_occupation, String partner_smoke, String partner_drink, String photo1, String photo1_approve, String photo2, String photo2_approve, String photo3, String photo3_approve, String photo4, String photo4_approve, String photo5, String photo5_approve, String photo6, String photo6_approve,String photo7, String photo7_approve,String photo8, String photo8_approve,String photo9, String photo9_approve,String photo10, String photo10_approve, String status, String signup_step, String fav_id, String fav_from, String fav_to, String interest_from, String interest_to, String interest_status, String plan_status, String view_type, String add_image, String add_link) {
        this.id = id;
        this.date_id = date_id;
        this.gender = gender;
        this.looking_for = looking_for;
        this.first_name = first_name;
        this.username = username;
        this.email = email;
        this.zipcode = zipcode;
        this.state = state;
        this.referal_from = referal_from;
        this.children = children;
        this.city = city;
        this.dob = dob;
        this.ethnicity = ethnicity;
        this.religion = religion;
        this.denomination = denomination;
        this.education = education;
        this.occupation = occupation;
        this.income = income;
        this.smoke = smoke;
        this.drink = drink;
        this.height_ft = height_ft;
        this.height_inc = height_inc;
        this.height_cms = height_cms;
        this.passionate = passionate;
        this.leisure_time = leisure_time;
        this.thankful_1 = thankful_1;
        this.thankful_2 = thankful_2;
        this.thankful_3 = thankful_3;
        this.partner_age_min = partner_age_min;
        this.partner_age_max = partner_age_max;
        this.partner_match_distance = partner_match_distance;
        this.partner_match_search = partner_match_search;
        this.partner_ethnicity = partner_ethnicity;
        this.partner_religion = partner_religion;
        this.partner_denomination = partner_denomination;
        this.partner_education = partner_education;
        this.partner_occupation = partner_occupation;
        this.partner_smoke = partner_smoke;
        this.partner_drink = partner_drink;
        this.photo1 = photo1;
        this.photo1_approve = photo1_approve;
        this.photo2 = photo2;
        this.photo2_approve = photo2_approve;
        this.photo3 = photo3;
        this.photo3_approve = photo3_approve;
        this.photo4 = photo4;
        this.photo4_approve = photo4_approve;
        this.photo5 = photo5;
        this.photo5_approve = photo5_approve;
        this.photo6 = photo6;
        this.photo6_approve = photo6_approve;
        this.photo7 = photo7;
        this.photo7_approve = photo7_approve;
        this.photo8 = photo8;
        this.photo8_approve = photo8_approve;
        this.photo9 = photo9;
        this.photo9_approve = photo9_approve;
        this.photo10 = photo10;
        this.photo10_approve = photo10_approve;
        this.status = status;
        this.signup_step = signup_step;
        this.fav_id = fav_id;
        this.fav_from = fav_from;
        this.fav_to = fav_to;
        this.interest_from = interest_from;
        this.interest_to = interest_to;
        this.interest_status = interest_status;
        this.plan_status = plan_status;
        this.view_type = view_type;
        this.add_image = add_image;
        this.add_link = add_link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate_id() {
        return date_id;
    }

    public void setDate_id(String date_id) {
        this.date_id = date_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLooking_for() {
        return looking_for;
    }

    public void setLooking_for(String looking_for) {
        this.looking_for = looking_for;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReferal_from() {
        return referal_from;
    }

    public void setReferal_from(String referal_from) {
        this.referal_from = referal_from;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getHeight_ft() {
        return height_ft;
    }

    public void setHeight_ft(String height_ft) {
        this.height_ft = height_ft;
    }


    public String getHeight_inc() {
        return height_inc;
    }

    public void setHeight_inc(String height_inc) {
        this.height_inc = height_inc;
    }

    public String getHeight_cms() {
        return height_cms;
    }

    public void setHeight_cms(String height_cms) {
        this.height_cms = height_cms;
    }

    public String getPassionate() {
        return passionate;
    }

    public void setPassionate(String passionate) {
        this.passionate = passionate;
    }

    public String getLeisure_time() {
        return leisure_time;
    }

    public void setLeisure_time(String leisure_time) {
        this.leisure_time = leisure_time;
    }

    public String getThankful_1() {
        return thankful_1;
    }

    public void setThankful_1(String thankful_1) {
        this.thankful_1 = thankful_1;
    }

    public String getThankful_2() {
        return thankful_2;
    }

    public void setThankful_2(String thankful_2) {
        this.thankful_2 = thankful_2;
    }

    public String getThankful_3() {
        return thankful_3;
    }

    public void setThankful_3(String thankful_3) {
        this.thankful_3 = thankful_3;
    }

    public String getPartner_age_min() {
        return partner_age_min;
    }

    public void setPartner_age_min(String partner_age_min) {
        this.partner_age_min = partner_age_min;
    }

    public String getPartner_age_max() {
        return partner_age_max;
    }

    public void setPartner_age_max(String partner_age_max) {
        this.partner_age_max = partner_age_max;
    }

    public String getPartner_match_distance() {
        return partner_match_distance;
    }

    public void setPartner_match_distance(String partner_match_distance) {
        this.partner_match_distance = partner_match_distance;
    }

    public String getPartner_match_search() {
        return partner_match_search;
    }

    public void setPartner_match_search(String partner_match_search) {
        this.partner_match_search = partner_match_search;
    }

    public String getPartner_ethnicity() {
        return partner_ethnicity;
    }

    public void setPartner_ethnicity(String partner_ethnicity) {
        this.partner_ethnicity = partner_ethnicity;
    }

    public String getPartner_religion() {
        return partner_religion;
    }

    public void setPartner_religion(String partner_religion) {
        this.partner_religion = partner_religion;
    }

    public String getPartner_denomination() {
        return partner_denomination;
    }

    public void setPartner_denomination(String partner_denomination) {
        this.partner_denomination = partner_denomination;
    }

    public String getPartner_education() {
        return partner_education;
    }

    public void setPartner_education(String partner_education) {
        this.partner_education = partner_education;
    }

    public String getPartner_occupation() {
        return partner_occupation;
    }

    public void setPartner_occupation(String partner_occupation) {
        this.partner_occupation = partner_occupation;
    }

    public String getPartner_smoke() {
        return partner_smoke;
    }

    public void setPartner_smoke(String partner_smoke) {
        this.partner_smoke = partner_smoke;
    }

    public String getPartner_drink() {
        return partner_drink;
    }

    public void setPartner_drink(String partner_drink) {
        this.partner_drink = partner_drink;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public String getPhoto1_approve() {
        return photo1_approve;
    }

    public void setPhoto1_approve(String photo1_approve) {
        this.photo1_approve = photo1_approve;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto2_approve() {
        return photo2_approve;
    }

    public void setPhoto2_approve(String photo2_approve) {
        this.photo2_approve = photo2_approve;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto3_approve() {
        return photo3_approve;
    }

    public void setPhoto3_approve(String photo3_approve) {
        this.photo3_approve = photo3_approve;
    }

    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public String getPhoto4_approve() {
        return photo4_approve;
    }

    public void setPhoto4_approve(String photo4_approve) {
        this.photo4_approve = photo4_approve;
    }

    public String getPhoto5() {
        return photo5;
    }

    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public String getPhoto5_approve() {
        return photo5_approve;
    }

    public void setPhoto5_approve(String photo5_approve) {
        this.photo5_approve = photo5_approve;
    }

    public String getPhoto6() {
        return photo6;
    }

    public void setPhoto6(String photo6) {
        this.photo6 = photo6;
    }

    public String getPhoto6_approve() {
        return photo6_approve;
    }

    public void setPhoto6_approve(String photo6_approve) {
        this.photo6_approve = photo6_approve;
    }

    public String getPhoto7() {
        return photo7;
    }

    public void setPhoto7(String photo7) {
        this.photo7 = photo7;
    }

    public String getPhoto7_approve() {
        return photo7_approve;
    }

    public void setPhoto7_approve(String photo7_approve) {
        this.photo7_approve = photo7_approve;
    }

    public String getPhoto8() {
        return photo8;
    }

    public void setPhoto8(String photo8) {
        this.photo8 = photo8;
    }

    public String getPhoto8_approve() {
        return photo8_approve;
    }

    public void setPhoto8_approve(String photo8_approve) {
        this.photo8_approve = photo8_approve;
    }

    public String getPhoto9() {
        return photo9;
    }

    public void setPhoto9(String photo9) {
        this.photo9 = photo9;
    }

    public String getPhoto9_approve() {
        return photo9_approve;
    }

    public void setPhoto9_approve(String photo9_approve) {
        this.photo9_approve = photo9_approve;
    }

    public String getPhoto10() {
        return photo10;
    }

    public void setPhoto10(String photo10) {
        this.photo10 = photo10;
    }

    public String getPhoto10_approve() {
        return photo10_approve;
    }

    public void setPhoto10_approve(String photo10_approve) {
        this.photo10_approve = photo10_approve;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSignup_step() {
        return signup_step;
    }

    public void setSignup_step(String signup_step) {
        this.signup_step = signup_step;
    }

    public String getFav_id() {
        return fav_id;
    }

    public void setFav_id(String fav_id) {
        this.fav_id = fav_id;
    }

    public String getFav_from() {
        return fav_from;
    }

    public void setFav_from(String fav_from) {
        this.fav_from = fav_from;
    }

    public String getFav_to() {
        return fav_to;
    }

    public void setFav_to(String fav_to) {
        this.fav_to = fav_to;
    }

    public String getInterest_from() {
        return interest_from;
    }

    public void setInterest_from(String interest_from) {
        this.interest_from = interest_from;
    }

    public String getInterest_to() {
        return interest_to;
    }

    public void setInterest_to(String interest_to) {
        this.interest_to = interest_to;
    }

    public String getInterest_status() {
        return interest_status;
    }

    public void setInterest_status(String interest_status) {
        this.interest_status = interest_status;
    }

    public String getPlan_status() {
        return plan_status;
    }

    public void setPlan_status(String plan_status) {
        this.plan_status = plan_status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getView_type() {
        return view_type;
    }

    public void setView_type(String view_type) {
        this.view_type = view_type;
    }

    public String getAdd_image() {
        return add_image;
    }

    public void setAdd_image(String add_image) {
        this.add_image = add_image;
    }

    public String getAdd_link() {
        return add_link;
    }

    public void setAdd_link(String add_link) {
        this.add_link = add_link;
    }
}
