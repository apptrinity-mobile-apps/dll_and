package divorcelovelounge.com.dll.ViewPagerAdapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.CardView
import android.util.Log
import android.util.Pair
import android.view.*
import android.widget.*
import com.squareup.picasso.Picasso
import divorcelovelounge.com.dll.*
import divorcelovelounge.com.dll.ApiPojo.*
import divorcelovelounge.com.dll.Helper.CustomTextView
import divorcelovelounge.com.dll.Helper.CustomTextViewBold
import divorcelovelounge.com.dll.Helper.DllSessionManager
import divorcelovelounge.com.dll.PicassoTransformations.BlurTransformation
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton
import divorcelovelounge.com.dll.SparkButtonHelper.SparkEventListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class CardPagerAdapterKotlin(private val context: Context, user_id: String) : PagerAdapter(),
    CardAdapter, View.OnTouchListener {

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        info_id.setImageResource(R.drawable.info_ic)
        return false
    }

    override fun getCardViewAt(position: Int): CardView {
        return mViews[position]!!

    }

    lateinit var info_id: ImageView
    lateinit var iv_favorite_user_image: ImageView
    lateinit var iv_convo_user_image: ImageView
    lateinit var tv_convo_name: CustomTextViewBold
    lateinit var iv_gender_id: ImageView
    lateinit var tv_age_id: CustomTextView
    lateinit var tv_distance_id: CustomTextView
    lateinit var iv_location_id: ImageView
    lateinit var btn_interest_id: SparkButton
    lateinit var btn_like_id: SparkButton
    lateinit var ll_bottom_like_id: LinearLayout
    lateinit var ll_user_title_id: LinearLayout

    private val mViews: MutableList<CardView?>
    private val mData: MutableList<CardItem>
    private var mBaseElevation: Float = 0.toFloat()


    private var partner_id = ""
    private var religion_stg = ""
    private var partner_match_distance_stg = ""
    private var partner_match_search_stg = ""
    private var partner_ethnicity_stg = ""
    private var partner_religion_stg = ""
    private var partner_denomination_stg = ""
    private var partner_education_stg = ""
    private var partner_occupation_stg = ""
    private var partner_smoke_stg = ""
    private var partner_drink_stg = ""
    private var photo1_stg = ""
    private var photo2_stg = ""
    private var photo3_stg = ""
    private var photo4_stg = ""
    private var photo5_stg = ""
    private var photo6_stg = ""
    private var photo7_stg = ""
    private var photo8_stg = ""
    private var photo9_stg = ""
    private var photo10_stg = ""
    private var fav_id_stg = ""
    private var fav_from_stg = ""
    private var fav_to_stg = ""
    private var interest_from_stg = ""
    private var interest_to_stg = ""
    private var interest_status_stg = ""
    private var phone_number_stg = ""

    private var user_smoke_stg = ""
    private var user_drink_stg = ""
    private var user_denomination_stg = ""
    var height_ft_stg: String? = null
    var height_inc_stg: String? = null
    var height_cm_stg: String? = null
    var city_stg: String? = null
    var dob_stg: String? = null
    var ethnicity_stg: String? = null
    var education_stg: String? = null
    var occupation_stg: String? = null
    var income_stg: String? = null
    var passionate_stg: String? = null
    var leisure_time_stg: String? = null
    var thankful_1_stg: String? = null
    var thankful_2_stg: String? = null
    var thankful_3_stg: String? = null
    var partner_age_min_stg: String? = null
    var partner_age_max_stg: String? = null

    var photo1_approve_stg = ""
    var plan_status = ""
    var photo1_status = ""


    lateinit var success_dialog: Dialog

    lateinit var tv_ok_btn_id: CustomTextViewBold
    lateinit var tv_msg_title_id: CustomTextViewBold
    lateinit var tv_full_msg_id: CustomTextView
    lateinit var img_profile_pic_id: ImageView
    lateinit var iv_close: ImageView
    val photbaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    private var main_user_id = user_id
    lateinit var session: DllSessionManager
    lateinit var user_details: HashMap<String, String>

    var plan_expire_status: Long = 0
    val photobaseurl = PhotoBaseUrl().BASE_PHOTO_URL
    lateinit var cv_star_id: CardView

    init {
        mData = ArrayList()
        mViews = ArrayList()
    }

    fun addCardItem(item: CardItem) {
        mViews.add(null)
        mData.add(item)
    }

    override fun getBaseElevation(): Float {
        return mBaseElevation
    }

    /*override fun getCardViewAt(position: Int): CardView {
        return mViews[position]!!
    }*/

    override fun getCount(): Int {
        return mData.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.dashboard_list_item, container, false)
        container.addView(view)
        bind(mData[position], view)
        val cardView = view.findViewById<CardView>(R.id.cardView)

        if (mBaseElevation == 0f) {
            mBaseElevation = cardView.cardElevation
        }

        cardView.maxCardElevation = mBaseElevation * CardAdapter.MAX_ELEVATION_FACTOR
        mViews[position] = cardView
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
        mViews.set(position, null)
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun bind(item: CardItem, view: View) {
        info_id = view.findViewById<ImageView>(R.id.info_id)
        iv_favorite_user_image = view.findViewById<ImageView>(R.id.iv_favorite_user_image)
        iv_convo_user_image = view.findViewById<ImageView>(R.id.iv_convo_user_image)
        tv_convo_name = view.findViewById<CustomTextViewBold>(R.id.tv_convo_name)
        iv_gender_id = view.findViewById<ImageView>(R.id.iv_gender_id)
        tv_age_id = view.findViewById<CustomTextView>(R.id.tv_age_id)
        tv_distance_id = view.findViewById<CustomTextView>(R.id.tv_distance_id)
        iv_location_id = view.findViewById<ImageView>(R.id.iv_location_id)
        btn_interest_id = view.findViewById<SparkButton>(R.id.btn_interest_id)
        btn_like_id = view.findViewById<SparkButton>(R.id.btn_like_id)
        cv_star_id = view.findViewById<CardView>(R.id.cv_star_id)
        ll_bottom_like_id = view.findViewById<LinearLayout>(R.id.ll_bottom_like_id)
        ll_user_title_id = view.findViewById<LinearLayout>(R.id.ll_user_title_id)

        session = DllSessionManager(context)
        user_details = session.userDetails


        tv_convo_name.text = item.first_name
        tv_distance_id.text = item.city

if(item.view_type.equals("user")){

    ll_bottom_like_id.visibility=View.VISIBLE
    ll_user_title_id.visibility=View.VISIBLE
    if (item.id == null || item.id == "") {
        partner_id = ""
    } else {
        partner_id = item.id
    }
    if (item.religion == null || item.religion == "") {
        religion_stg = "0"
    } else {
        religion_stg = item.religion
    }
    if (item.partner_match_distance == null || item.partner_match_distance == "") {
        partner_match_distance_stg = "0"
    } else {
        partner_match_distance_stg = item.partner_match_distance
    }

    if (item.partner_match_search == null || item.partner_match_search == "") {
        partner_match_search_stg = "0"
    } else {
        partner_match_search_stg = item.partner_match_search
    }

    if (item.partner_ethnicity == null || item.partner_ethnicity == "") {
        partner_ethnicity_stg = "0"
    } else {
        partner_ethnicity_stg = item.partner_ethnicity
    }
    if (item.partner_religion == null || item.partner_religion == "") {
        partner_religion_stg = "0"
    } else {
        partner_religion_stg = item.partner_religion
    }

    if (item.partner_denomination == null || item.partner_denomination == "") {
        partner_denomination_stg = "0"
    } else {
        partner_denomination_stg = item.partner_denomination
    }

    if (item.partner_education == null || item.partner_education == "") {
        partner_education_stg = "0"
    } else {
        partner_education_stg = item.partner_education
    }
    if (item.partner_occupation == null || item.partner_occupation == "") {
        partner_occupation_stg = "0"
    } else {
        partner_occupation_stg = item.partner_occupation
    }
    if (item.partner_smoke == null || item.partner_smoke == "") {
        partner_smoke_stg = "0"
    } else {
        partner_smoke_stg = item.partner_smoke
    }
    if (item.partner_drink == null || item.partner_drink == "") {
        partner_drink_stg = "0"
    } else {
        partner_drink_stg = item.partner_drink
    }

    if (item.photo1 == null || item.photo1 == "") {
        photo1_stg = "0"
        // imgview.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        Log.e("photo1_stg_0", photo1_stg)

    } else {
        photo1_stg = item.photo1
        Log.e("photo1_stg", photo1_stg)
    }

    if (item.photo1_approve == null || item.photo1_approve == "") {
        photo1_approve_stg = "0"
    } else {
        photo1_approve_stg = item.photo1_approve
    }

    if (item.photo2 == null || item.photo2 == "") {
        photo2_stg = "0"
    } else {
        photo2_stg = item.photo2
    }
    if (item.photo3 == null || item.photo3 == "") {
        photo3_stg = "0"
    } else {
        photo3_stg = item.photo3
    }
    if (item.photo4 == null || item.photo4 == "") {
        photo4_stg = "0"
    } else {
        photo4_stg = item.photo4
    }
    if (item.photo5 == null || item.photo5 == "") {
        photo5_stg = "0"
    } else {
        photo5_stg = item.photo5
    }
    if (item.photo6 == null || item.photo6 == "") {
        photo6_stg = "0"
    } else {
        photo6_stg = item.photo6
    }
    if (item.photo7 == null || item.photo7 == "") {
        photo7_stg = "0"
    } else {
        photo7_stg = item.photo7
    }
    if (item.photo8 == null || item.photo8 == "") {
        photo8_stg = "0"
    } else {
        photo8_stg = item.photo8
    }
    if (item.photo9 == null || item.photo9 == "") {
        photo9_stg = "0"
    } else {
        photo9_stg = item.photo9
    }
    if (item.photo10 == null || item.photo10 == "") {
        photo10_stg = "0"
    } else {
        photo10_stg = item.photo10
    }

    if (item.fav_id == null || item.fav_id == "") {
        fav_id_stg = "0"
    } else {
        fav_id_stg = item.fav_id
    }
    if (item.fav_from == null || item.fav_from == "") {
        fav_from_stg = "0"
    } else {
        fav_from_stg = item.fav_from
    }
    if (item.fav_to == null || item.fav_to == "") {
        fav_to_stg = "0"
    } else {
        fav_to_stg = item.fav_to
    }
    if (item.interest_from == null || item.interest_from == "") {
        interest_from_stg = "0"
    } else {
        interest_from_stg = item.interest_from
    }
    if (item.interest_to == null || item.interest_to == "") {
        interest_to_stg = "0"
    } else {
        interest_to_stg = item.interest_to
    }
    if (item.interest_status == null || item.interest_status == "") {
        interest_status_stg = "0"
    } else {
        interest_status_stg = item.interest_status
    }

    if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1") {
        btn_interest_id.isEnabled = true
    } else if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "2") {
        btn_interest_id.isEnabled = false

    } else {
        btn_interest_id.isEnabled = false


    }

    if (interest_from_stg == user_details.get(DllSessionManager.USER_ID).toString()) {
        btn_interest_id.isChecked = true
        btn_interest_id.isEnabled = false
    } else {
        btn_interest_id.isChecked = false

    }



    if (item.plan_status == "" || item.plan_status == null) {
        plan_status = "0"
    } else {
        plan_status = item.plan_status
    }

    if (fav_from_stg == main_user_id) {
        btn_like_id.isChecked = true
    } else {
        btn_like_id.isChecked = false
    }

    if (item.gender.equals("Woman")) {
        iv_gender_id.setImageResource(R.drawable.new_female_ic)
    } else {
        iv_gender_id.setImageResource(R.drawable.new_male_ic)
    }


    if (item.dob != null) {
        var date_only: Date? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        try {
            date_only = sdf.parse(item.dob)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        val c = Calendar.getInstance()
        //Set time in milliseconds
        // c.setTimeInMillis(user_date_id.toLong())
        c.time = date_only
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)


        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()

        dob.set(mYear, mMonth, mDay)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }

        val ageInt = age

        tv_age_id.setText(ageInt.toString())
    } else {
        tv_age_id.setText("0")
    }

    if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1") {
        if (photo1_stg.equals("0")) {
            Picasso.with(context)
                .load(photobaseurl + photo1_stg)
                .error(R.drawable.ic_man_png)
                .into(iv_favorite_user_image)
            iv_favorite_user_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE)
        } else {
            if (photo1_approve_stg == "UNAPPROVED") {
                Picasso.with(context)
                    .load(R.drawable.ic_man_png)
                    .into(iv_favorite_user_image)
                iv_favorite_user_image.scaleType = ImageView.ScaleType.CENTER_INSIDE
            } else {
                Picasso.with(context)
                    .load(photobaseurl + photo1_stg)
                    .error(R.drawable.ic_man_png)
                    .into(iv_favorite_user_image)
                iv_favorite_user_image.setScaleType(ImageView.ScaleType.CENTER_CROP)
            }

        }
    } else {
        if (photo1_stg.equals("0")) {
            Picasso.with(context)
                .load(photobaseurl + photo1_stg)
                .error(R.drawable.ic_man_png)
                // .transform(BlurTransformation(context))
                .into(iv_favorite_user_image)
            iv_favorite_user_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE)
        } else {
            if (photo1_approve_stg == "UNAPPROVED") {
                Picasso.with(context)
                    .load(R.drawable.ic_man_png)
                    //.transform(BlurTransformation(context, 25))
                    .into(iv_favorite_user_image)
                iv_favorite_user_image.scaleType = ImageView.ScaleType.CENTER_INSIDE
            } else {
                Picasso.with(context)
                    .load(photobaseurl + photo1_stg)
                    .error(R.drawable.ic_man_png)
                    .transform(BlurTransformation(context))
                    .into(iv_favorite_user_image)
                iv_favorite_user_image.setScaleType(ImageView.ScaleType.CENTER_CROP)
            }

        }
    }

    if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1") {
        if (photo1_stg.equals("0")) {
            Picasso.with(context)
                .load(photobaseurl + photo1_stg)
                .error(R.drawable.ic_man_png)
                .transform(FullProfile.CircleTransform())
                .into(iv_convo_user_image)
        } else {
            if (photo1_approve_stg == "UNAPPROVED") {
                Picasso.with(context)
                    .load(R.drawable.ic_man_png)
                    .transform(FullProfile.CircleTransform())
                    .into(iv_convo_user_image)
            } else {
                Picasso.with(context)
                    .load(photobaseurl + photo1_stg)
                    .error(R.drawable.ic_man_png)
                    .transform(FullProfile.CircleTransform())
                    .into(iv_convo_user_image)
            }
        }

    } else {
        if (photo1_stg.equals("0")) {
            Picasso.with(context)
                .load(photobaseurl + photo1_stg)
                .error(R.drawable.ic_man_png)
                .transform(
                    arrayListOf(
                        BlurTransformation(context),
                        FullProfile.CircleTransform()
                    )
                )
                .into(iv_convo_user_image)
        } else {
            if (photo1_approve_stg == "UNAPPROVED") {
                Picasso.with(context)
                    .load(R.drawable.ic_man_png)
                    //.transform(arrayListOf(BlurTransformation(context, 15), FullProfileActivity.CircleTransform()))
                    .into(iv_convo_user_image)
            } else {
                Picasso.with(context)
                    .load(photobaseurl + photo1_stg)
                    .error(R.drawable.ic_man_png)
                    .transform(
                        arrayListOf(
                            BlurTransformation(context),
                            FullProfile.CircleTransform()
                        )
                    )
                    .into(iv_convo_user_image)
            }
        }
    }



    if (user_details.get(DllSessionManager.MEMBERSHIP_STATUS).equals("1")) {
        val Server_date = user_details.get(DllSessionManager.MEMBERSHIP_SERVER_DATE)
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val date_server = sdf.parse(Server_date)


        val expir_date = user_details.get(DllSessionManager.MEMBERSHIP_PLAN_EXPIRY)
        val date_expir = sdf.parse(expir_date)


        val diff = date_expir.getTime() - date_server.getTime()

        System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

        plan_expire_status = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)

        iv_favorite_user_image.setMarginExtensionFunction(0,0,0,65)


    }
}else{
    ll_bottom_like_id.visibility=View.GONE
    ll_user_title_id.visibility=View.GONE

    Picasso.with(context)
        .load(PhotoBaseUrl().BASE_ADS_URL + item.add_image)
        .error(R.drawable.ic_man_png)
        .into(iv_favorite_user_image)
    iv_favorite_user_image.setScaleType(ImageView.ScaleType.FIT_XY)

    iv_favorite_user_image.setMarginExtensionFunction(0,0,0,0)
}


        iv_favorite_user_image.setOnClickListener {

            if(item.view_type.equals("user")) {
                // getPartnerProfile(main_user_id, item.id)
                var location = "0"
                if (item.city == null || item.city == "") {
                    location = "0"
                } else {
                    location = item.city!!
                }


                val internt = Intent(context, FullProfile::class.java)
                val pairs = arrayOfNulls<Pair<View, String>>(7)
                pairs[0] = Pair<View, String>(iv_favorite_user_image, "mainimageTreansition")
                pairs[1] = Pair<View, String>(iv_convo_user_image, "profiletransition")
                pairs[2] = Pair<View, String>(tv_convo_name, "nametransition")
                pairs[3] = Pair<View, String>(iv_gender_id, "gendertransition")
                pairs[4] = Pair<View, String>(tv_age_id, "agetransition")
                pairs[5] = Pair<View, String>(iv_location_id, "locationtransition")
                pairs[6] = Pair<View, String>(tv_distance_id, "distancetransition")
                val aOptions =
                    ActivityOptions.makeSceneTransitionAnimation(context as Activity, *pairs)


                internt.putExtra("from_screen", "dashboard")
                internt.putExtra("id", item.id)
                internt.putExtra("photo1", item.photo1)
                internt.putExtra("name_stg", item.first_name)
                internt.putExtra("gender_stg", item.gender)
                internt.putExtra("age_stg", item.dob)
                internt.putExtra("location_stg", location)
                context.startActivity(internt, aOptions.toBundle())
            }else{
                context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(item.add_link.toString())));
            }

        }

        info_id.setOnClickListener {


            if (item.equals(item)) {
                info_id.setImageResource(R.drawable.info_active_ic)
            }


            val wrapper = ContextThemeWrapper(context, R.style.PopupMenu)
            val popup = PopupMenu(wrapper, info_id, Gravity.CENTER)

            popup.menuInflater.inflate(R.menu.popup_menu, popup.menu)

            popup.gravity = 20
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item_menu: MenuItem): Boolean {
                    val i = item_menu.itemId
                    if (i == R.id.action_profile_id) {
                        //getPartnerProfile(main_user_id, item.id)
                        var location = "0"
                        if (item.city == null || item.city == "") {
                            location = "0"
                        } else {
                            location = item.city!!
                        }
                        val internt = Intent(context, FullProfile::class.java)

                        val pairs = arrayOfNulls<Pair<View, String>>(7)
                        pairs[0] =
                            Pair<View, String>(iv_favorite_user_image, "mainimageTreansition")
                        pairs[1] = Pair<View, String>(iv_convo_user_image, "profiletransition")
                        pairs[2] = Pair<View, String>(tv_convo_name, "nametransition")
                        pairs[3] = Pair<View, String>(iv_gender_id, "gendertransition")
                        pairs[4] = Pair<View, String>(tv_age_id, "agetransition")
                        pairs[5] = Pair<View, String>(iv_location_id, "locationtransition")
                        pairs[6] = Pair<View, String>(tv_distance_id, "distancetransition")
                        val aOptions = ActivityOptions.makeSceneTransitionAnimation(
                            context as Activity,
                            *pairs
                        )


                        internt.putExtra("from_screen", "dashboard")
                        internt.putExtra("id", item.id)
                        internt.putExtra("photo1", item.photo1)
                        internt.putExtra("name_stg", item.first_name)
                        internt.putExtra("gender_stg", item.gender)
                        internt.putExtra("age_stg", item.dob)
                        internt.putExtra("location_stg", location)
                        context.startActivity(internt, aOptions.toBundle())


                        return true
                    } else return if (i == R.id.action_msg_id) {

                        if (user_details.get(DllSessionManager.PHOTO1_APPROVE).equals("UNAPPROVED")) {
                            //btn_interest_id.isEnabled = false
                            //btn_like_id.isEnabled = false
                            Toast.makeText(
                                context,
                                "Can't use this feature till admin approve your image",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {

                            if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1" && plan_expire_status >= 0) {
                                val intent = Intent(context, ComposeMessageActivity::class.java)
                                intent.putExtra("msg_from_id", item.id)
                                intent.putExtra("first_name_stg", item.first_name)
                                intent.putExtra("from_stg", "dashboard")
                                context.startActivity(intent)
                            } else if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "2") {

                                memberShipAlertDialog()

                            } else {
                                memberShipAlertDialog()
                            }
                        }


                        //do something
                        true
                    } else if (i == R.id.action_block_user_id) {


                        success_dialog = Dialog(context)
                        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

                        success_dialog.setContentView(R.layout.success_dialog_layout)
                        val success_window = success_dialog.window
                        success_window!!.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        img_profile_pic_id = success_dialog.findViewById(R.id.img_profile_pic_id)
                        tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
                        tv_msg_title_id.text = "Alert!"
                        tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
                        tv_full_msg_id.text = "Are you sure you want to Block ${item.first_name}?"
                        iv_close = success_dialog.findViewById(R.id.iv_close)
                        iv_close.visibility = View.VISIBLE
                        tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
                        tv_ok_btn_id.setOnClickListener {
                            success_dialog.dismiss()
                            val api = ApiInterface.create()
                            val blockUserApi = api.blockUserApi(main_user_id, item.id)
                            blockUserApi.enqueue(object : Callback<BlockUserResponse> {
                                override fun onFailure(
                                    call: Call<BlockUserResponse>,
                                    t: Throwable
                                ) {
                                    Toast.makeText(
                                        context, "Something went wrong!", Toast.LENGTH_SHORT
                                    ).show()
                                }

                                override fun onResponse(
                                    call: Call<BlockUserResponse>,
                                    response: Response<BlockUserResponse>
                                ) {
                                    Log.d("FullProfileActivity", response.body()!!.result)
                                    if (response != null) {

                                        if (response.body() != null) {

                                            if (response.body()!!.status == "1") {

                                                val home_intent =
                                                    Intent(context, DashboardActivity::class.java)
                                                home_intent.putExtra("from_list", "quiz_setup")
                                                context.startActivity(home_intent)

                                            } else {
                                                Toast.makeText(
                                                    context,
                                                    "Something went wrong",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                        } else {
                                            Toast.makeText(
                                                context, "Please try again!", Toast.LENGTH_SHORT
                                            ).show()
                                        }

                                    } else {
                                        Toast.makeText(
                                            context, "Something went wrong!", Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            })
                        }


                        if (item.photo1_approve == null || item.photo1_approve == "") {
                            photo1_approve_stg = "0"
                        } else {
                            photo1_approve_stg = item.photo1_approve
                        }

                        if (item.photo1 == null || item.photo1 == "") {
                            photo1_stg = "0"
                        } else {
                            photo1_stg = item.photo1
                        }


                        if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1") {
                            if (photo1_stg.equals("0")) {
                                Picasso.with(context)
                                    .load(photobaseurl + photo1_stg)
                                    .error(R.drawable.ic_man_png)
                                    .into(img_profile_pic_id)
                            } else {
                                if (photo1_approve_stg == "UNAPPROVED") {
                                    Picasso.with(context)
                                        .load(R.drawable.ic_man_png)
                                        .into(img_profile_pic_id)
                                } else {
                                    Picasso.with(context)
                                        .load(photobaseurl + photo1_stg)
                                        .error(R.drawable.ic_man_png)
                                        .into(img_profile_pic_id)
                                }

                            }
                        } else {
                            if (photo1_stg.equals("0")) {
                                Picasso.with(context)
                                    .load(photobaseurl + photo1_stg)
                                    .error(R.drawable.ic_man_png)
                                    // .transform(BlurTransformation(context))
                                    .into(img_profile_pic_id)
                            } else {
                                if (photo1_approve_stg == "UNAPPROVED") {
                                    Picasso.with(context)
                                        .load(R.drawable.ic_man_png)
                                        //.transform(BlurTransformation(context, 25))
                                        .into(img_profile_pic_id)
                                } else {
                                    Picasso.with(context)
                                        .load(photobaseurl + photo1_stg)
                                        .error(R.drawable.ic_man_png)
                                        .transform(BlurTransformation(context))
                                        .into(img_profile_pic_id)
                                }

                            }
                        }


                        iv_close.setOnClickListener {
                            success_dialog.dismiss()
                        }

                        success_dialog.show()


                        /* Block user */
                        /*val builder = AlertDialog.Builder(context)
                        builder.setTitle("Alert!")
                            .setMessage("Are you sure you want to Block ${item.first_name}?")
                            .setCancelable(false)
                            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, id: Int) {
                                    dialog!!.dismiss()
                                }

                            })
                            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface?, id: Int) {


                                }
                            })

                        val alertDialog = builder.create()
                        alertDialog.show()*/
                        true
                    } else if (i == R.id.action_report_user_id) {
                        //do something
                        true
                    } else {
                        onMenuItemClick(item_menu)
                    }
                }
            })

            popup.show()
        }


        if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "1" && plan_expire_status >= 0) {


            btn_interest_id.setEventListener(object : SparkEventListener {
                override fun onEvent(button: ImageView, buttonState: Boolean) {
                    /*if (plan_status == "1") {*/
                    if (interest_from_stg == main_user_id) {
                        btn_interest_id.setEventListener(null)
                        btn_interest_id.isEnabled = false
                        btn_interest_id.isClickable = false
                        button.setImageDrawable(context.getDrawable(R.drawable.ic_yellow_interest))
                    } else {

                        if (buttonState) {
                            //Toast.makeText(context, buttonState.toString(), Toast.LENGTH_SHORT).show()
                            val apiService = ApiInterface.create()
                            val call = apiService.sendInterestApi(main_user_id, item.id)
                            Log.d("REQUEST", call.toString() + "")
                            call.enqueue(object : Callback<ContactusResponse> {
                                override fun onResponse(
                                    call: Call<ContactusResponse>,
                                    response: retrofit2.Response<ContactusResponse>?
                                ) {
                                    //  if (response != null) {
                                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                            "success"
                                        )
                                    ) {
                                        val user_data = response.body()!!.data
                                        Toast.makeText(
                                            context,
                                            "Interest Sent Successfully.",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        interest_from_stg = main_user_id
                                        btn_interest_id.isEnabled = false
                                        btn_interest_id.isClickable = false
                                        btn_interest_id.setEventListener(null)

                                    } else if (response!!.body()!!.status.equals("2")) {
                                        btn_interest_id.setEventListener(null)
                                        btn_interest_id.isEnabled = false
                                        btn_interest_id.isClickable = false
                                        btn_interest_id.setEventListener(null)
                                    }
                                    //  }
                                }

                                override fun onFailure(
                                    call: Call<ContactusResponse>,
                                    t: Throwable
                                ) {
                                    Log.w("Result_Order_details", t.toString())
                                }
                            })


                        } else {
                            btn_interest_id.isEnabled = false
                            btn_interest_id.isClickable = false

                            /* val apiService = ApiInterface.create()
                         val call = apiService.sendInterestApi(main_user_id, item.id)
                         Log.d("REQUEST", call.toString() + "")
                         call.enqueue(object : Callback<ContactusResponse> {
                             override fun onResponse(
                                 call: Call<ContactusResponse>,
                                 response: retrofit2.Response<ContactusResponse>?
                             ) {
                                 //  if (response != null) {
                                 if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                         "success"
                                     )
                                 ) {
                                     val user_data = response.body()!!.data
                                     // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                     interest_from_stg = main_user_id
                                     btn_interest_id.isEnabled = false

                                 }
                                 //  }
                             }

                             override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                                 Log.w("Result_Order_details", t.toString())
                             }
                         })*/


                        }


                    }

                }


                override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
                }

                override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
                }
            })


        } else if (user_details[DllSessionManager.MEMBERSHIP_STATUS] == "2") {
            btn_interest_id.setOnClickListener {
                memberShipAlertDialog()
            }
        } else {
            btn_interest_id.setOnClickListener {
                memberShipAlertDialog()
            }
        }

        if (user_details.get(DllSessionManager.PHOTO1_APPROVE).equals("UNAPPROVED")) {
            btn_interest_id.isEnabled = false
            btn_like_id.isEnabled = false


        }
        cv_star_id.setOnClickListener {
            if (user_details.get(DllSessionManager.PHOTO1_APPROVE).equals("UNAPPROVED")) {
                Toast.makeText(context, "you account is under progress", Toast.LENGTH_SHORT).show()
            }

        }

        btn_like_id.setEventListener(object : SparkEventListener {
            override fun onEvent(button: ImageView, buttonState: Boolean) {
                if (buttonState) {
                    //Toast.makeText(context, buttonState.toString(), Toast.LENGTH_SHORT).show()

                    val apiService = ApiInterface.create()
                    val call = apiService.likeApi(main_user_id, item.id)
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {

                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                // Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                fav_from_stg = main_user_id
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })

                } else {
                    val apiService = ApiInterface.create()
                    val call = apiService.likeApi(main_user_id, item.id)
                    Log.d("REQUEST", call.toString() + "")
                    call.enqueue(object : Callback<ContactusResponse> {
                        override fun onResponse(
                            call: Call<ContactusResponse>,
                            response: retrofit2.Response<ContactusResponse>?
                        ) {
                            //  if (response != null) {
                            if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals(
                                    "success"
                                )
                            ) {

                                val user_data = response.body()!!.data
                                //  Toast.makeText(context, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                                // Toast.makeText(context, "No data", Toast.LENGTH_SHORT).show()
                                fav_from_stg = "0"
                            }

                            //  }
                        }

                        override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                            Log.w("Result_Order_details", t.toString())
                        }
                    })
                }
            }

            override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
            }

            override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
            }
        })

    }


    fun memberShipAlertDialog() {


        success_dialog = Dialog(context)
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        success_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        success_dialog.setContentView(R.layout.success_dialog_layout)
        val success_window = success_dialog.window
        success_window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        img_profile_pic_id = success_dialog.findViewById(R.id.img_profile_pic_id)
        tv_msg_title_id = success_dialog.findViewById(R.id.tv_msg_title_id)
        tv_msg_title_id.text = "Alert!"
        tv_full_msg_id = success_dialog.findViewById(R.id.tv_full_msg_id)
        tv_full_msg_id.text =
            "You need an active membership plan to view the profile. Proceed to membership plans?"
        iv_close = success_dialog.findViewById(R.id.iv_close)
        iv_close.visibility = View.VISIBLE
        tv_ok_btn_id = success_dialog.findViewById(R.id.tv_ok_btn_id)
        tv_ok_btn_id.setOnClickListener {
            success_dialog.dismiss()
            val intent = Intent(context, MembershipPlanActivity::class.java)
            context.startActivity(intent)
        }

        Picasso.with(context)
            .load(photbaseurl + user_details.get(DllSessionManager.PHOTO1).toString())
            .error(R.drawable.ic_man_user)
            .into(img_profile_pic_id)

        iv_close.setOnClickListener {
            success_dialog.dismiss()
        }

        success_dialog.show()


        /*val builder = AlertDialog.Builder(context)
        builder.setTitle("Alert!")
            .setMessage("You need an active membership plan to view the profile. Proceed to membership plans?")
            .setCancelable(false)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()

                }
            })
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, id: Int) {
                    dialog!!.dismiss()
                }
            })

        val alertDialog = builder.create()
        alertDialog.show()*/
    }
    fun View.setMarginExtensionFunction(left: Int, top: Int, right: Int, bottom: Int) {
        val params = layoutParams as ViewGroup.MarginLayoutParams
        params.setMargins(left, top, right, bottom)
        layoutParams = params
    }
}