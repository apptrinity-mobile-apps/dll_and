package divorcelovelounge.com.dll.ViewPagerAdapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Pair;
import android.view.*;

import android.widget.ImageView;
import android.widget.PopupMenu;
import divorcelovelounge.com.dll.ApiPojo.ApiInterface;
import divorcelovelounge.com.dll.FullProfile;
import divorcelovelounge.com.dll.Helper.CustomTextView;
import divorcelovelounge.com.dll.Helper.CustomTextViewBold;
import divorcelovelounge.com.dll.Helper.RoundedImageView;
import divorcelovelounge.com.dll.R;
import divorcelovelounge.com.dll.SparkButtonHelper.SparkButton;
import divorcelovelounge.com.dll.SparkButtonHelper.SparkEventListener;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CardItem> mData;
    private float mBaseElevation;
    private Context context;


    private String religion_stg = "";
    private String partner_match_distance_stg = "";
    private String partner_match_search_stg = "";
    private String partner_ethnicity_stg = "";
    private String partner_religion_stg = "";
    private String partner_education_stg = "";
    private String partner_occupation_stg = "";
    private String partner_smoke_stg = "";
    private String partner_drink_stg = "";
    private String photo1_stg = "";
    private String photo2_stg = "";
    private String photo3_stg = "";
    private String photo4_stg = "";
    private String photo5_stg = "";
    private String photo6_stg = "";
    private String fav_id_stg = "";
    private String fav_from_stg = "";
    private String fav_to_stg = "";


    public CardPagerAdapter(Context ctx) {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        context = ctx;
    }

    public void addCardItem(CardItem item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.dashboard_list_item, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        CardView cardView = view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(final CardItem item, View view) {
        final ImageView info_id = view.findViewById(R.id.info_id);
        final ImageView iv_favorite_user_image = view.findViewById(R.id.iv_favorite_user_image);
        final RoundedImageView iv_convo_user_image = view.findViewById(R.id.iv_convo_user_image);
        final CustomTextViewBold tv_convo_name = view.findViewById(R.id.tv_convo_name);
        final ImageView iv_gender_id = view.findViewById(R.id.iv_gender_id);
        final CustomTextView tv_age_id = view.findViewById(R.id.tv_age_id);
        final CustomTextView tv_distance_id = view.findViewById(R.id.tv_distance_id);
        final ImageView iv_location_id = view.findViewById(R.id.iv_location_id);
        final SparkButton btn_interest_id = view.findViewById(R.id.btn_interest_id);


        tv_convo_name.setText(item.getFirst_name());
        tv_distance_id.setText(item.getPartner_match_distance());


        if (item.getReligion() == null || item.getReligion().equals("")) {
            religion_stg = "0";
        } else {
            religion_stg = item.getReligion();
        }
        if (item.getPartner_match_distance() == null || item.getPartner_match_distance().equals("")) {
            partner_match_distance_stg = "0";
        } else {
            partner_match_distance_stg = item.getPartner_match_distance();
        }

        if (item.getPartner_match_search() == null || item.getPartner_match_search().equals("")) {
            partner_match_search_stg = "0";
        } else {
            partner_match_search_stg = item.getPartner_match_search();
        }

        if (item.getPartner_ethnicity() == (null) || item.getPartner_ethnicity().equals("")) {
            partner_ethnicity_stg = "0";
        } else {
            partner_ethnicity_stg = item.getPartner_ethnicity();
        }
        if (item.getPartner_religion() == (null) || item.getPartner_religion().equals("")) {
            partner_religion_stg = "0";
        } else {
            partner_religion_stg = item.getPartner_religion();
        }

        if (item.getPartner_education() == (null) || item.getPartner_education().equals("")) {
            partner_education_stg = "0";
        } else {
            partner_education_stg = item.getPartner_education();
        }
        if (item.getPartner_occupation() == (null) || item.getPartner_occupation().equals("")) {
            partner_occupation_stg = "0";
        } else {
            partner_occupation_stg = item.getPartner_occupation();
        }
        if (item.getPartner_smoke() == (null) || item.getPartner_smoke().equals("")) {
            partner_smoke_stg = "0";
        } else {
            partner_smoke_stg = item.getPartner_smoke();
        }
        if (item.getPartner_drink() == (null) || item.getPartner_drink().equals("")) {
            partner_drink_stg = "0";
        } else {
            partner_drink_stg = item.getPartner_drink();
        }

        if (item.getPhoto1() == (null) || item.getPhoto1().equals("")) {
            photo1_stg = "0";
        } else {
            photo1_stg = item.getPhoto1();
        }
        if (item.getPhoto2() == (null) || item.getPhoto2().equals("")) {
            photo2_stg = "0";
        } else {
            photo2_stg = item.getPhoto2();
        }
        if (item.getPhoto3() == (null) || item.getPhoto3().equals("")) {
            photo3_stg = "0";
        } else {
            photo3_stg = item.getPhoto3();
        }
        if (item.getPhoto4() == (null) || item.getPhoto4().equals("")) {
            photo4_stg = "0";
        } else {
            photo4_stg = item.getPhoto4();
        }
        if (item.getPhoto5() == (null) || item.getPhoto5().equals("")) {
            photo5_stg = "0";
        } else {
            photo5_stg = item.getPhoto5();
        }
        if (item.getPhoto6() == (null) || item.getPhoto6().equals("")) {
            photo6_stg = "0";
        } else {
            photo6_stg = item.getPhoto6();
        }

        if (item.getFav_id() == (null) || item.getFav_id().equals("")) {
            fav_id_stg = "0";
        } else {
            fav_id_stg = item.getFav_id();
        }
        if (item.getFav_from() == (null) || item.getFav_from().equals("")) {
            fav_from_stg = "0";
        } else {
            fav_from_stg = item.getFav_from();
        }
        if (item.getFav_to() == (null) || item.getFav_to().equals("")) {
            fav_to_stg = "0";
        } else {
            fav_to_stg = item.getFav_to();
        }


        iv_favorite_user_image.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FullProfile.class);
                Pair[] pairs = new Pair[7];
                pairs[0] = new Pair<View, String>(iv_favorite_user_image, "mainimageTreansition");
                pairs[1] = new Pair<View, String>(iv_convo_user_image, "profiletransition");
                pairs[2] = new Pair<View, String>(tv_convo_name, "nametransition");
                pairs[3] = new Pair<View, String>(iv_gender_id, "gendertransition");
                pairs[4] = new Pair<View, String>(tv_age_id, "agetransition");
                pairs[5] = new Pair<View, String>(iv_location_id, "locationtransition");
                pairs[6] = new Pair<View, String>(tv_distance_id, "distancetransition");
                ActivityOptions aOptions = ActivityOptions.makeSceneTransitionAnimation((Activity) context, pairs);


                intent.putExtra("id", item.getId());
                intent.putExtra("date_id", item.getDate_id());
                intent.putExtra("gender", item.getGender());
                intent.putExtra("looking_for", item.getLooking_for());
                intent.putExtra("first_name", item.getFirst_name());
                intent.putExtra("email", item.getEmail());
                intent.putExtra("zipcode", item.getZipcode());
                intent.putExtra("state", item.getState());
                intent.putExtra("referal_from", item.getReferal_from());
                intent.putExtra("children", item.getChildren());
                intent.putExtra("city", item.getCity());
                intent.putExtra("dob", item.getDob());
                intent.putExtra("ethnicity", item.getEthnicity());
                intent.putExtra("religion", religion_stg);
                intent.putExtra("denomination", item.getDenomination());
                intent.putExtra("education", item.getEducation());
                intent.putExtra("occupation", item.getOccupation());
                intent.putExtra("income", item.getIncome());
                intent.putExtra("smoke", item.getSmoke());
                intent.putExtra("drink", item.getDrink());
                intent.putExtra("height_ft",item.getHeight_ft());
                intent.putExtra("height_inc",item.getHeight_inc());
                intent.putExtra("height_cms",item.getHeight_cms());
                intent.putExtra("passionate", item.getPassionate());
                intent.putExtra("leisure_time", item.getLeisure_time());
                intent.putExtra("thankful_1", item.getThankful_1());
                intent.putExtra("thankful_2", item.getThankful_2());
                intent.putExtra("thankful_3", item.getThankful_3());
                intent.putExtra("partner_age_min", item.getPartner_age_min());
                intent.putExtra("partner_age_max", item.getPartner_age_max());
                intent.putExtra("partner_match_distance", partner_match_distance_stg);
                intent.putExtra("partner_match_search", partner_match_search_stg);
                intent.putExtra("partner_ethnicity", partner_ethnicity_stg);
                intent.putExtra("partner_religion", partner_religion_stg);
                intent.putExtra("partner_education", partner_education_stg);
                intent.putExtra("partner_occupation", partner_occupation_stg);
                intent.putExtra("partner_smoke", partner_smoke_stg);
                intent.putExtra("partner_drink", partner_drink_stg);
                intent.putExtra("photo1", photo1_stg);
                intent.putExtra("photo2", photo2_stg);
                intent.putExtra("photo3", photo3_stg);
                intent.putExtra("photo4", photo4_stg);
                intent.putExtra("photo5", photo5_stg);
                intent.putExtra("photo6", photo6_stg);
                intent.putExtra("status", item.getStatus());
                intent.putExtra("signup_step", item.getSignup_step());
                intent.putExtra("fav_id", fav_id_stg);
                intent.putExtra("fav_from", fav_from_stg);
                intent.putExtra("fav_to", fav_to_stg);
                intent.putExtra("interest_from", item.getInterest_from());
                intent.putExtra("interest_to", item.getInterest_to());
                intent.putExtra("interest_status",item.getInterest_status());
                intent.putExtra("from_screen", "matches");

                context.startActivity(intent, aOptions.toBundle());
            }
        });

        info_id.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenu);
                final PopupMenu popup = new PopupMenu(wrapper, info_id, Gravity.CENTER);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setGravity(20);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.action_profile_id) {
                            //do something
                            Intent intent = new Intent(context, FullProfile.class);
                            Pair[] pairs = new Pair[7];
                            pairs[0] = new Pair<View, String>(iv_favorite_user_image, "mainimageTreansition");
                            pairs[1] = new Pair<View, String>(iv_convo_user_image, "profiletransition");
                            pairs[2] = new Pair<View, String>(tv_convo_name, "nametransition");
                            pairs[3] = new Pair<View, String>(iv_gender_id, "gendertransition");
                            pairs[4] = new Pair<View, String>(tv_age_id, "agetransition");
                            pairs[5] = new Pair<View, String>(iv_location_id, "locationtransition");
                            pairs[6] = new Pair<View, String>(tv_distance_id, "distancetransition");
                            ActivityOptions aOptions = ActivityOptions.makeSceneTransitionAnimation((Activity) context, pairs);

                            return true;
                        } else if (i == R.id.action_msg_id) {
                            //do something
                            return true;
                        } else if (i == R.id.action_block_user_id) {
                            //do something
                            return true;
                        } else if (i == R.id.action_report_user_id) {
                            //do something
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });

                popup.show();

            }
        });


        btn_interest_id.setEventListener(new SparkEventListener(){


            @Override
            public void onEvent(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        });

        btn_interest_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* val apiService = ApiInterface.create()
            val call = apiService.sendInterestApi("2","85")
            Log.d("REQUEST", call.toString() + "")
            call.enqueue(object : Callback<ContactusResponse> {
                override fun onResponse(
                    call: Call<ContactusResponse>,
                    response: retrofit2.Response<ContactusResponse>?
                ) {

                    //  if (response != null) {
                    if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                        val user_data = response.body()!!.data
                        Toast.makeText(this@FullProfileActivity,response.body()!!.result.toString(),Toast.LENGTH_SHORT).show()

                    }

                    //  }
                }

                override fun onFailure(call: Call<ContactusResponse>, t: Throwable) {
                    Log.w("Result_Order_details", t.toString())
                }
            })*/

            }
        });


    }


}
